//
//  GTRTimeFormatterHelper.m
//  gotribal
//
//  Created by Sambya Aryasa on 1/1/14.
//  Copyright (c) 2014 gotribal. All rights reserved.
//

#import "GTRTimeFormatterHelper.h"
#import "TTTTimeIntervalFormatter.h"

@implementation GTRTimeFormatterHelper
+(NSString*)relativeTimeStringFromEpoch:(NSNumber*)epochTime
{
    NSString *nowString = NSLocalizedString(@"now", @"now");
    static TTTTimeIntervalFormatter *_timeIntervalFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _timeIntervalFormatter = [[TTTTimeIntervalFormatter alloc] init];
        [_timeIntervalFormatter setLocale:[NSLocale currentLocale]];
        [_timeIntervalFormatter setUsesIdiomaticDeicticExpressions:YES];
        [_timeIntervalFormatter setPresentDeicticExpression:nowString];
        [_timeIntervalFormatter stringForTimeInterval:0];
        [_timeIntervalFormatter setPresentTimeIntervalMargin:60];
    });
    
    if (epochTime == NULL){
        return @"";
    }
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[epochTime doubleValue]];
    NSTimeInterval timeIntervalSinceNow =  [date timeIntervalSinceNow];
    
    if (timeIntervalSinceNow > 0){
        return nowString;
    } else {
        return [_timeIntervalFormatter stringForTimeInterval:[date timeIntervalSinceNow]];
    }
}
@end
