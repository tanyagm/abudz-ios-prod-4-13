//
//  GTRNotificationsCell.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/3/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSettingsCell.h"
#import "GTRNotificationCell.h"

extern NSInteger const CONFIRM_BUTTON_TAG;
extern NSInteger const IGNORE_BUTTON_TAG;
extern NSInteger const PENDING_REQUEST_CELL_HEIGHT;

@interface GTRPendingRequestCell : GTRNotificationCell

@property(nonatomic,strong) UIButton *confirmButton;
@property(nonatomic,strong) UIButton *ignoreButton;

@end
