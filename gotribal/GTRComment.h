//
//  GTRComment.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/12/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "MTLModel.h"

@interface GTRComment : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSNumber *commentID;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSNumber *createdAt;
@property (strong, nonatomic) GTRUser *owner;

-(id)initWithMessage:(NSString*)message user:(GTRUser*)user;
-(NSString*) createdAtRelativeTime;
@end
