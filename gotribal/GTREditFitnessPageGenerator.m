//
//  GTREditFitnessPageGenerator.m
//  gotribal
//
//  Created by loaner on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTREditFitnessPageGenerator.h"
#import "GTRAppDelegate.h"
#import "GTRUser.h"
#import "GTRUserDetail.h"
#import "GTRConstants.h"
#import "GTRPursuitHelper.h"

@implementation GTREditFitnessPageGenerator

-(id)init
{
    
    return self;
}

- (void)initiateFitnessProfilePages
{
    [self initiateReferenceDictionary];
    [self initiateSurveyQuestionData];

    self.surveyQuestionPages = [ [NSMutableArray alloc] initWithArray:
                                @[[[GTRFitnessPageTemplateViewController alloc]initWithTitle:EDIT_FITNESS_FAVORITE_WORKOUT_TIME_TITLE
                                                                                        data:self.surveyQuestionData[0]
                                                                                  withHeader:NO withQuestionType:YES
                                                                            isSurveyPageMode:NO ],
                                  
                                  [[GTRFitnessRolePageViewController alloc]initWithTitle:EDIT_FITNESS_MY_ROLE_TITLE
                                                                                        withHeader:NO withQuestionType:YES
                                                                                  isSurveyPageMode:NO
                                                                               referenceDictionary:self.referenceDictionary],
                                  
                                  [[GTRFitnessRolePreferencePageViewController alloc]initWithTitle:EDIT_FITNESS_LOOKING_FOR_TITLE
                                                                                        withHeader:NO withQuestionType:YES
                                                                                  isSurveyPageMode:NO
                                                                               referenceDictionary:self.referenceDictionary],
                                  
                                  [[GTRFitnessPageTemplateViewController alloc]initWithTitle:EDIT_FITNESS_DESIRED_ACTIVEBUDZ_GENDER
                                                                                        data:self.surveyQuestionData[2]
                                                                                  withHeader:NO
                                                                            withQuestionType:YES
                                                                            isSurveyPageMode:NO],
                                  
                                  [[GTRFitnessPursuitViewController alloc]initWithTitle:EDIT_FITNESS_FITNESS_PURSUIT_TITLE
                                                                             withHeader:NO
                                                                       withQuestionType:YES
                                                                       isSurveyPageMode:NO
                                                                    referenceDictionary:self.referenceDictionary],
                                  
                                  [[GTRFitnessPageTemplateViewController alloc]initWithTitle:EDIT_FITNESS_FITNESS_ABILITY_TITLE
                                                                                        data:self.surveyQuestionData[3]
                                                                                  withHeader:NO
                                                                            withQuestionType:YES
                                                                            isSurveyPageMode:NO ],
                                  
                                  [[GTRFitnessPageTemplateViewController alloc]initWithTitle:EDIT_FITNESS_DAILY_ROUTINE_TITLE
                                                                                        data:self.surveyQuestionData[4]
                                                                                  withHeader:NO
                                                                            withQuestionType:YES
                                                                            isSurveyPageMode:NO ],
                                  
                                  [[GTRFitnessPageTemplateViewController alloc]initWithTitle:EDIT_FITNESS_LIFESTYLE_TITLE
                                                                                        data:self.surveyQuestionData[5]
                                                                                  withHeader:NO
                                                                            withQuestionType:YES
                                                                            isSurveyPageMode:NO ],
                                  
                                  [[GTRFitnessLocationPageViewController alloc]initWithTitle:EDIT_FITNESS_LOCATION_TITLE
                                                                                  withHeader:NO
                                                                            withQuestionType:YES
                                                                            isSurveyPageMode:NO
                                                                         referenceDictionary:self.referenceDictionary],
                                  
                                  ]];
}

-(void)loadPreviousAnswers
{
    //get user data
    GTRUser *user = [GTRUserHelper getCurrentUser];
    
    #pragma mark User Detail Key
    
    NSArray *userDetailKey =  @[USER_DETAIL_WORKOUT_TIME_KEY,
                                USER_DETAIL_ROLE_KEY,
                                USER_DETAIL_ROLE_PREFERENCE_KEY,
                                USER_DETAIL_GENDER_PREFERENCE_KEY,
                                USER_DETAIL_FITNESS_PURSUIT_KEY,
                                USER_DETAIL_FITNESS_LEVEL_KEY,
                                USER_DETAIL_WEEKLY_ACTIVITIES_KEY,
                                USER_DETAIL_LIFESTYLE_KEY,
                                USER_DETAIL_LIVING_PLACE_KEY
                                ];
    
    //prepopulate selected fields
    int count = (int)userDetailKey.count;
    
    GTRUserDetail *answerDetail;
    
    for (int i =0; i < count; i++) {
        answerDetail = [[user.details filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"detailKey == %@", userDetailKey[i]]] firstObject];
        DDLogInfo(@"%@ : %@", userDetailKey[i],answerDetail);
        
        if(answerDetail){
            
             GTRFitnessPageTemplateViewController *gtrFitnessPageTempleteVC = ((GTRFitnessPageTemplateViewController *)self.surveyQuestionPages[i]);
            [gtrFitnessPageTempleteVC initiateSelectedAnswerStatus];
            [gtrFitnessPageTempleteVC initiateSelectedAnswerTracker];
            [gtrFitnessPageTempleteVC initiateLockButton];
            
            if (i == 1) {
                
                GTRFitnessRolePageViewController *gtrFitnessRolePageVC = ((GTRFitnessRolePageViewController*)self.surveyQuestionPages[i]);
                if([answerDetail.detailValue isKindOfClass:[NSArray class]]) {
                    gtrFitnessRolePageVC.selectedAnswerTracker = [[NSMutableArray alloc] initWithArray: ((NSArray*)answerDetail.detailValue)];
                } else {
                    gtrFitnessRolePageVC.selectedAnswerTracker = [[NSMutableArray alloc] initWithArray:@[answerDetail.detailValue]];
                }
                [gtrFitnessRolePageVC updateAnswerSelectedStatus];
                [gtrFitnessRolePageVC setAnswerLocked:answerDetail.detailPrivate];
            }else if(i == 2){ //Looking for
                
                GTRFitnessRolePreferencePageViewController *gtrFitnessRolePreferencePageVC = ((GTRFitnessRolePreferencePageViewController*)self.surveyQuestionPages[i]);
                
                gtrFitnessRolePreferencePageVC.selectedAnswerTracker = [[NSMutableArray alloc] initWithArray: ((NSArray*)answerDetail.detailValue)];
                [gtrFitnessRolePreferencePageVC updateAnswerSelectedStatus];
                [gtrFitnessRolePreferencePageVC setAnswerLocked:answerDetail.detailPrivate];
                
            }else if(i == 4){//Fitness pursuit
                
                NSArray *result = [GTRPursuitHelper getIntegerPursuitArray:((NSArray*)answerDetail.detailValue)];
                NSString *otherTextString = [GTRPursuitHelper getOtherPursuitStringFromArray:((NSArray*)answerDetail.detailValue)];
                GTRFitnessPursuitViewController *gtrFitnessPursuitPageVC =  ((GTRFitnessPursuitViewController*)self.surveyQuestionPages[i]);
                
                gtrFitnessPursuitPageVC.selectedAnswerTracker = [[NSMutableArray alloc] initWithArray: result];
                [gtrFitnessPursuitPageVC setOtherPursuitText:otherTextString];
                [gtrFitnessPursuitPageVC updateAnswerSelectedStatus];
                [gtrFitnessPursuitPageVC setAnswerLocked:(BOOL)answerDetail.detailPrivate];
                
            }else if(i == 8){//Location
                
                GTRFitnessLocationPageViewController *gtrFitnessPageLocationVC = ((GTRFitnessLocationPageViewController*)self.surveyQuestionPages[i]);
                
                [gtrFitnessPageLocationVC setCityText:((NSArray*)answerDetail.detailValue)[0]];
                [gtrFitnessPageLocationVC setAnswerLocked:(BOOL)answerDetail.detailPrivate];
                
            }else{
                [gtrFitnessPageTempleteVC.selectedAnswerTracker addObject:((NSNumber*)answerDetail.detailValue)];
                [gtrFitnessPageTempleteVC updateAnswerSelectedStatus];
                [gtrFitnessPageTempleteVC setAnswerLocked:(BOOL)answerDetail.detailPrivate];
                
            }
            
            [self.referenceDictionary setValue:[MTLJSONAdapter JSONDictionaryFromModel:answerDetail] forKey:userDetailKey[i]];
        }
    }
}


@end
