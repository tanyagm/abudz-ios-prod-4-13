//
//  GTRNavigationBarHelper.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRNavigationBarHelper.h"
#import "UIBarButtonItem+NegativeSpacer.h"

@implementation GTRNavigationBarHelper

#pragma mark Specific button type (menu/back/etc)
+(void)setLeftSideBarButtonItem:(UINavigationItem*)navigationItem
                     withTarget:(id)target
                       selector:(SEL)selector
{
    UIBarButtonItem *button = [self createBarButtonItemWithTarget:target selector:selector imageNamed:@"ico-menu"];
    [self setLeftBarButtonItems:navigationItem
                      withItems:@[button]
                       animated:NO];
}

+(void)setBackButtonItem:(UINavigationItem*)navigationItem
                     withTarget:(id)target
                       selector:(SEL)selector
{
    UIBarButtonItem *button = [self createBarButtonItemWithTarget:target selector:selector imageNamed:@"ico-back"];
    [self setLeftBarButtonItems:navigationItem
                      withItems:@[button]
                       animated:NO];
}

#pragma mark Single right custom button
+(void)setRightBarButtonItem:(UINavigationItem*)navigationItem
              withTarget:(id)target
                selector:(SEL)selector
              imageNamed:(NSString *)imageName
                animated:(BOOL)animated
{
    UIBarButtonItem *button = [self createBarButtonItemWithTarget:target selector:selector imageNamed:imageName];
    [self setRightBarButtonItems:navigationItem
                      withItems:@[button]
                       animated:animated];
}

+(void)setRightBarButtonItem:(UINavigationItem*)navigationItem
                  withButton:(UIBarButtonItem*)button
                    animated:(BOOL)animated
              withSpacerType:(GTRSpacerType)spacerType
{
    [self setRightBarButtonItems:navigationItem
                       withItems:@[button]
                        animated:animated
                  withSpacerType:spacerType];
}


#pragma mark Custom bar button item creation
+(UIBarButtonItem*)createBarButtonItemWithTarget:(id)target
                            selector:(SEL)selector
                          imageNamed:(NSString *)imageName
{
    UIBarButtonItem *item;
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [rightButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [rightButton sizeToFit];
    item = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    return item;
}

+(UIBarButtonItem*)createBarButtonItemWithTarget:(id)target
                                        selector:(SEL)selector
                                     buttonTitle:(NSString*)buttonTitle;
{
    return [[UIBarButtonItem alloc] initWithTitle:buttonTitle
                                            style:UIBarButtonItemStylePlain
                                           target:target
                                           action:selector];
}


#pragma mark Multiple bar button items
+(void)setLeftBarButtonItems:(UINavigationItem*)navigationItem
                    withItems:(NSArray*)items
                     animated:(BOOL)animated
{
    [self setLeftBarButtonItems:navigationItem
                      withItems:items
                       animated:animated
                 withSpacerType:GTRSpacerTypeButton];
}

+(void)setRightBarButtonItems:(UINavigationItem*)navigationItem
                    withItems:(NSArray*)items
                     animated:(BOOL)animated
{
    [self setRightBarButtonItems:navigationItem
                       withItems:items
                        animated:animated
                  withSpacerType:GTRSpacerTypeButton];
}

+(void)setLeftBarButtonItems:(UINavigationItem*)navigationItem
                   withItems:(NSArray*)items
                    animated:(BOOL)animated
              withSpacerType:(GTRSpacerType)spacerType
{
    UIBarButtonItem *spacer = [self getSpacer:spacerType];
    NSMutableArray *itemsWithSpacer = [[NSMutableArray alloc] init];
    [itemsWithSpacer addObject:spacer];
    [itemsWithSpacer addObjectsFromArray:items];
    
    [navigationItem setLeftBarButtonItems:items animated:NO];
    [navigationItem setLeftItemsSupplementBackButton:NO];
    [navigationItem setLeftBarButtonItems:[[NSArray alloc] initWithArray:itemsWithSpacer] animated:animated];
}

+(void)setRightBarButtonItems:(UINavigationItem*)navigationItem
                    withItems:(NSArray*)items
                     animated:(BOOL)animated
               withSpacerType:(GTRSpacerType)spacerType
{
    UIBarButtonItem *spacer = [self getSpacer:spacerType];
    NSMutableArray *itemsWithSpacer = [[NSMutableArray alloc] init];
    [itemsWithSpacer addObject:spacer];
    [itemsWithSpacer addObjectsFromArray:items];
    
    [navigationItem setRightBarButtonItems:[[NSArray alloc] initWithArray:itemsWithSpacer] animated:animated];
}

#pragma mark UIBarButtonSystemItem
+(UIBarButtonItem*)setRightBarButtonItemWithText:(NSString*)buttonTitle
                                onNavigationItem:(UINavigationItem*)navigationItem
                                      withTarget:(id)target
                                        selector:(SEL)selector
{
    UIBarButtonItem *button = [self createBarButtonItemWithTarget:target
                                                         selector:selector
                                                      buttonTitle:buttonTitle];
    [self setRightBarButtonItems:navigationItem
                       withItems:@[button]
                        animated:NO
                  withSpacerType:GTRSpacerTypePlainText];
    return button;
}

+(UIBarButtonItem*)setLeftBarButtonItemWithText:(NSString*)buttonTitle
                               onNavigationItem:(UINavigationItem*)navigationItem
                                     withTarget:(id)target
                                       selector:(SEL)selector
{
    UIBarButtonItem *button = [self createBarButtonItemWithTarget:target
                                                         selector:selector
                                                      buttonTitle:buttonTitle];
    
    [self setLeftBarButtonItems:navigationItem
                      withItems:@[button]
                       animated:NO
                 withSpacerType:GTRSpacerTypePlainText];
    return button;
}

+(UIBarButtonItem*)setLeftBarButtonItemInPushedViewControllerWithText:(NSString*)buttonTitle
                               onNavigationItem:(UINavigationItem*)navigationItem
                                     withTarget:(id)target
                                       selector:(SEL)selector
{
    UIBarButtonItem *button = [self createBarButtonItemWithTarget:target
                                                         selector:selector
                                                      buttonTitle:buttonTitle];
    
    GTRSpacerType spacerType = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? GTRSpacerTypePlainText:GTRSpacerTypePlaintextPushediOS6;
    
    [self setLeftBarButtonItems:navigationItem
                      withItems:@[button]
                       animated:NO
                 withSpacerType:spacerType];
    return button;
}



#pragma mark spacer calculation
+(UIBarButtonItem*)getSpacer:(GTRSpacerType)spacerType
{
    UIBarButtonItem *spacer = nil;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        switch (spacerType) {
            case GTRSpacerTypeButton:
                spacer = [UIBarButtonItem negativeSpacerWithWidth:5.0f];
                break;
            case GTRSpacerTypePlainText:
                spacer = [UIBarButtonItem negativeSpacerWithWidth:0.0f];
                break;
            case GTRSpacerTypePlaintextPushediOS6:
                break;
        }
    } else {
        spacer = [UIBarButtonItem negativeSpacerWithWidth:0.0f];
        switch (spacerType) {
            case GTRSpacerTypeButton:
                spacer.width = 3;
                break;
            case GTRSpacerTypePlainText:
                spacer.width = -7;
                break;
            case GTRSpacerTypePlaintextPushediOS6:
                spacer.width = 1;
                break;
        }
    }
    
    return spacer;
}

@end
