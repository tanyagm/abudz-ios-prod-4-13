//
//  GTRTribeSearchViewController.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/19/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRMyTribeViewController.h"
#import "GTRTribeDetailViewController.h"
#import "GTRTribeCell.h"

@interface GTRTribeSearchViewController : GTRViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, GTRTribeDetailViewControllerDelegate, GTRTribeCellDelegate>

@end
