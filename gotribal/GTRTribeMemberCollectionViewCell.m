//
//  GTRActiveBudzCollectionViewCell.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/17/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRTribeMemberCollectionViewCell.h"
#import <UIImageView+AFNetworking.h>

@interface GTRTribeMemberCollectionViewCell()

@property (strong,nonatomic) GTRUser *user;
@property (strong,nonatomic) UIImageView *imageView;

@end

@implementation GTRTribeMemberCollectionViewCell
@synthesize user, imageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        imageView.backgroundColor = [UIColor clearColor];
        imageView.layer.cornerRadius = self.bounds.size.width/2;
        imageView.layer.masksToBounds = YES;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:imageView];
    }
    return self;
}

#pragma mark - Content setter
-(void) setContent:(GTRUser *)theUser
{
    user = theUser;
    [imageView setImageWithURL:[NSURL URLWithString:user.avatarURL] placeholderImage:DEFAULT_AVATAR];
}

@end
