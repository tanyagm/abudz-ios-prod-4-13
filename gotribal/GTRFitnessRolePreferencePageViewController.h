//
//  GTREditFitnessRolePreferencePageViewController.h
//  gotribal
//
//  Created by loaner on 11/29/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GTRSurveyRolePreferencePageViewController.h"

@interface GTRFitnessRolePreferencePageViewController : GTRSurveyRolePreferencePageViewController


-(id)initWithTitle:(NSString *)title
        withHeader:(BOOL)header
  withQuestionType:(BOOL)questionType
  isSurveyPageMode:(BOOL)value
referenceDictionary:(NSMutableDictionary *)theReferenceDictionary;

@end
