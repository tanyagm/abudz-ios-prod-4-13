//
//  GTRTribeSearchViewController.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/19/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRTribeSearchViewController.h"
#import "TPKeyboardAvoidingTableView.h"
#import "GTRTribeRestService.h"
#import "NSMutableArray+AddMantleObject.h"
#import <UIKitExtension/UIAlertView.h>

NSString *NO_TRIBES_FOUND       =
@"Hmmm, we couldn't find that exact match.\nTry broadening your search a tiny bit for a better result!";
NSString *SEARCH_BY_ACTIVITY    = @"Search by Activity or by Location";

@interface GTRTribeSearchViewController ()

@property(nonatomic,strong) NSMutableArray *tribeArray;
@property(nonatomic,strong) TPKeyboardAvoidingTableView *table;
@property(nonatomic,strong) UISearchBar *searchBar;
@property(nonatomic,strong) GTRTribeCell *cellHeightCalculator;
@property(nonatomic,strong) UIActivityIndicatorView *activityIndicator;
@property(nonatomic,strong) UILabel *noDataLabel;

@end

@implementation GTRTribeSearchViewController
@synthesize tribeArray = _tribeArray;
@synthesize table = _table;
@synthesize searchBar = _searchBar;
@synthesize cellHeightCalculator = _cellHeightCalculator;
@synthesize activityIndicator = _activityIndicator;
@synthesize noDataLabel = _noDataLabel;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setAppearance];
    [self applySearchbar];
    [self applyTable];
    
    // initiate indicators
    [self applyNoUserLabel];
    [self applyActivitiIndicator];
    
    //initiate cell height calculator
    _cellHeightCalculator = [[GTRTribeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellHeightCalculator"];
    _tribeArray = [[NSMutableArray alloc]init];
}

#pragma mark - Set Appearance

-(void)setAppearance
{
    self.view.backgroundColor = GTR_MAIN_ACTIVITIES_BACKGROUND_COLOR;
    self.title = @"Find Tribes";
    
    //set the cancel button
    [GTRNavigationBarHelper setLeftBarButtonItemInPushedViewControllerWithText:NSLocalizedString(@"Cancel", @"cancel")
                                                              onNavigationItem:self.navigationItem
                                                                    withTarget:self
                                                                      selector:@selector(backButtonAction:)];
}

-(void)applySearchbar
{
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    _searchBar.backgroundColor = [UIColor whiteColor];
    _searchBar.placeholder = SEARCH_BY_ACTIVITY;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        _searchBar.barTintColor = [UIColor whiteColor];
    }
    else{
        _searchBar.tintColor = [UIColor whiteColor];
    }
    _searchBar.delegate = self;
    [_searchBar becomeFirstResponder];
    [self.view addSubview:_searchBar];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard:)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
}

-(void)hideKeyboard:(UIGestureRecognizer*)gesture
{
    [_searchBar resignFirstResponder];
}

-(void)applyTable
{
    CGRect tableFrame = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? CGRectMake(0, 0,320, self.view.bounds.size.height - 64): CGRectMake(0, 0, 320, self.view.bounds.size.height - 20);
    
    _table = [[TPKeyboardAvoidingTableView alloc]initWithFrame:CGRectOffset(tableFrame, 0, 40)];
    _table.backgroundColor = GTR_MAIN_ACTIVITIES_BACKGROUND_COLOR;
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    _table.delegate = self;
    _table.dataSource = self;
    _table.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_table];
}

-(void)applyActivitiIndicator
{
    _activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activityIndicator.frame = CGRectMake(150, 50, 20, 20);
    [self.view addSubview:_activityIndicator];
}

-(void)applyNoUserLabel
{
    _noDataLabel = [[UILabel alloc]init];
    _noDataLabel.backgroundColor = GTR_MAIN_ACTIVITIES_BACKGROUND_COLOR;
    _noDataLabel.frame = CGRectMake(5, 45, 310, 35);
    _noDataLabel.text = NO_TRIBES_FOUND;
    _noDataLabel.textColor = NOTIFICATIONS_CELL_SUBTEXT_COLOR;
    _noDataLabel.font = TRIBE_SEARCH_NO_DATA_FONT;
    _noDataLabel.textAlignment = NSTextAlignmentCenter;
    _noDataLabel.numberOfLines = 0;
    _noDataLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _noDataLabel.hidden = YES;
    [self.view addSubview:_noDataLabel];
}

#pragma mark - Show / hide label or activity indicator

-(void)showNoDataLabel:(BOOL)show
{
    if (show) {
        _noDataLabel.hidden = NO;
        _table.hidden = YES;
    }
    else {
        _noDataLabel.hidden = YES;
        _table.hidden = NO;
    }
}

#pragma mark - Show / hide ActivityIndicator

-(void)showActivityIndicator:(BOOL)show
{
    if (show) {
        [_activityIndicator startAnimating];
    }
    else
    {
        [_activityIndicator stopAnimating];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source and delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tribeArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_cellHeightCalculator setContent:[_tribeArray objectAtIndex:indexPath.row]];
    
    return _cellHeightCalculator.frame.size.height;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TribeCell";
    GTRTribeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    GTRTribe *tribe = [_tribeArray objectAtIndex:indexPath.row];
    
    if (!cell) {
        cell = [[GTRTribeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.delegate = self;
    }
    
    //set the tribe to cell
    [cell setContent:tribe];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GTRTribe *tribe = [_tribeArray objectAtIndex:indexPath.row];
    
    if (tribe) {
        [self showTribeDetailsOfTribe:tribe];
    }
}

-(void)showTribeDetailsOfTribe:(GTRTribe*)tribe
{
    GTRTribeDetailViewController *tribeDetailViewController = [[GTRTribeDetailViewController alloc] init];
    [tribeDetailViewController setContent:tribe];
    tribeDetailViewController.delegate = self;
    [self.navigationController pushViewController:tribeDetailViewController animated:YES];
}

#pragma mark - UISeacrhBar delegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText isEqualToString:@""]) {
        [_tribeArray removeAllObjects];
        [_table reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationTop];
    }
    // todo mamaz: implements dynamic on the fly searching
//    else {
//        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(doSearchForSomeTribes:) object:searchText];
//        
//        [self performSelector:@selector(doSearchForSomeTribes:) withObject:searchText afterDelay:1.5];
//    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_searchBar resignFirstResponder];
    [self doSearchForSomeTribes:_searchBar.text];
}

#pragma mark - Do Search some tribes!

-(void)doSearchForSomeTribes:(NSString*)keyword
{
    [_tribeArray removeAllObjects];
    [_table reloadData];
    
    [self showNoDataLabel:NO];
    [self showActivityIndicator:YES];
    
    // make time to show indicator
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // search tribes
        if (keyword.length>0) {
            [GTRTribeRestService searchTribeWithKeyword:keyword onSuccess:^(id responseObject) {
                [self showActivityIndicator:NO];
                NSArray *tribes = [((NSDictionary*)responseObject) objectForKey:@"data"];
                
                if (tribes.count > 0) {
                    [_tribeArray addObjectsFromArray:tribes modelOfClass:[GTRTribe class]];
                    [_table reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationBottom];
                    [self showNoDataLabel:NO];
                }
                else {
                    [self showNoDataLabel:YES];
                }
            } onFail:^(id responseObject, NSError *error) {
                [self showActivityIndicator:NO];
                [self showNoDataLabel:YES];
                [UIAlertView showNoticeWithTitle:@"Oops!"
                                         message:[NSString stringWithFormat:@"Error: %@", [error localizedDescription]] cancelButtonTitle:@"OK"];
            }];
        }
    });
}

#pragma - mark GTRTribeCellDelegate

-(void)didClickJoin:(GTRTribe *)tribe inCell:(GTRTribeCell *)cell
{
    [GTRTribeRestService joinTribe:tribe OnSuccess:^(id responseObject) {
        GTRTribe *updatedTribe = [MTLJSONAdapter modelOfClass:GTRTribe.class
                                           fromJSONDictionary:responseObject error:nil];
        
        [self showTribeDetailsOfTribe:updatedTribe];
    } onFail:^(id responseObject, NSError *error) {
        [UIAlertView showNoticeWithTitle:@"Oops!" message:[NSString stringWithFormat:@"Error: %@", [error localizedDescription]] cancelButtonTitle:@"OK"];
    }];
}

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
