//
//  GTRConversationTableViewCell.h
//  gotribal
//
//  Created by Muhammad Taufik on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GTRConversation;

@interface GTRConversationTableViewCell : UITableViewCell
@property (nonatomic, strong) GTRConversation *conversation;
@end
