//
//  GTRUIConstant.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/17/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#ifndef gotribal_GTRUIConstant_h
#define gotribal_GTRUIConstant_h

#pragma mark - Colors
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define GTRSIDE_MENU_COLOR [UIColor colorWithRed:0.15 green:0.15 blue:0.16 alpha:1.0];
#define GTR_FORM_TEXT_COLOR [UIColor colorWithHue:0.115 saturation:0.149 brightness:0.341 alpha:1.000]
#define CELL_TEXT_FONT_COLOR [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]
#define CELL_SUBTEXT_FONT_COLOR [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:0.8]
#define CELL_PENDING_REQUEST_BUTTON_BORDER [UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:1.0]
#define AVATAR_BORDER_COLOR [UIColor colorWithRed:195.0/255.0 green:195.0/255.0 blue:195.0/255.0 alpha:1.0]
#define GTR_MAIN_ACTIVITIES_BACKGROUND_COLOR [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0]

#define NOTIFICATIONS_CELL_TEXT_COLOR [UIColor colorWithRed:75.0/255.0 green:75.0/255.0 blue:75.0/255.0 alpha:1.0]
#define NOTIFICATIONS_CELL_SUBTEXT_COLOR [UIColor colorWithRed:75.0/255.0 green:75.0/255.0 blue:75.0/255.0 alpha:0.7]
#define GTR_TURQUOISE_COLOR [UIColor colorWithRed:1.0/255.0 green:149.0/255.0 blue:152.0/255.0 alpha:1.0]

#pragma mark - Fonts
#define CONTEXT_FONT [UIFont fontWithName:@"HelveticaNeue-Medium" size:15]
#define SUBCONTEXT_FONT [UIFont fontWithName:@"HelveticaNeue-Light" size:13.5]
#define TIMESTAMP_FONT [UIFont fontWithName:@"HelveticaNeue-Light" size:10.5]
#define NUMBER_LABEL_FONT [UIFont fontWithName:@"HelveticaNeue" size:16]

#define CELL_FONT [UIFont fontWithName:@"HelveticaNeue-Light" size:14]
#define CELL_LOGOUT_FONT [UIFont fontWithName:@"HelveticaNeue-Medium" size:14]
#define NOTIFICATIONS_CELL_TEXT_FONT CONTEXT_FONT
#define NOTIFICATIONS_CELL_SUBTEXT_FONT SUBCONTEXT_FONT
#define TRIBE_ACTIVITY_FONT [UIFont fontWithName:@"HelveticaNeue-Medium" size:14]
#define TRIBE_ERROR_FONT [UIFont fontWithName:@"HelveticaNeue" size:14]
#define TRIBE_ACTIVITY_CHOSEN_FONT [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:14]
#define CUSTOM_ACTION_SHEET_FONT [UIFont fontWithName:@"HelveticaNeue-Light" size:16]
#define TRIBE_SEARCH_NO_DATA_FONT [UIFont fontWithName:@"HelveticaNeue-Light" size:13]

#pragma mark - Table corner radius, border color, etc

#define TABLE_BORDER_WIDTH  0.5f
#define TABLE_BORDER_COLOR  [UIColor colorWithRed:0.8 green:0.796 blue:0.8 alpha:1.0].CGColor
#define TABLE_CORNER_RADIUS 5.0f;

typedef enum {
    GTRImageOrientationPortrait = 0,
    GTRImageOrientationLandscape
} GTRImageOrientation;

#define DEFAULT_AVATAR [UIImage imageNamed:@"default_avatar"]
#define DEFAULT_ACTIVITY_PLACEHOLDER [UIImage imageNamed:@"placeholder_0"]

#endif
