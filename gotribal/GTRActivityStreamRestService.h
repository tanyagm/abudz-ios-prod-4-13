//
//  GTRActivityStreamRestService.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTRActivityStreamRestService : NSObject

+(void)retrieveActivityStreamWithMaxID:(NSNumber*) maxID
                             onSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler;
@end
