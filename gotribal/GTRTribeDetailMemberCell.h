//
//  GTRTribeDetailMemberCell.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/13/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRTribe.h"
#import "GTRUser.h"

#define TRIBE_DETAIL_MEMBER_CELL_HEIGHT 100

@protocol GTRTribeDetailMemberCellDelegate

-(void)onMemberSelected:(GTRUser*)theMember;

@end

@interface GTRTribeDetailMemberCell : UITableViewCell

@property (weak) id <GTRTribeDetailMemberCellDelegate> delegate;

-(void)setContent:(GTRTribe*)theTribe;

@end
