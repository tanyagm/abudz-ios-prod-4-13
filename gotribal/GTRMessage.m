//
//  GTRMessage.m
//  gotribal
//
//  Created by Muhammad Taufik on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRMessage.h"

@implementation GTRMessage
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"body" : @"body",
             @"messageID" : @"id",
             @"receiverID" : @"receiver_id",
             @"user": @"sender",
             @"senderID": @"sender_id",
             @"createdAt": @"created_at"
             };
}

+(NSValueTransformer *)userJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[GTRUser class]];
}

@end
