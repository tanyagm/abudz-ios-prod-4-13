//
//  GTRTribeDetailMemberCell.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/13/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRTribeDetailMemberCell.h"
#import "GTRActiveBudzCarouselItem.h"
#import "GTRTribeMemberCollectionViewCell.h"
#import "GTRUser.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "GTRTribeMemberCollectionViewCell.h"
#import "GTRTribeRestService.h"

#define MEMBER_REFRESH 0
#define MEMBER_LOAD_MORE 1

NSString* const TRIBE_MEMBER_TITLE_STRING = @"Tribe Members";
NSString* const NO_MEMBER_PLACEHOLDER_STRING = @"No member yet.";

@interface GTRTribeDetailMemberCell () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (strong,nonatomic) GTRTribe *tribe;
@property (strong,nonatomic) NSMutableArray *users;
@property (strong,nonatomic) UIView *customContentView;
@property (strong,nonatomic) UICollectionView *memberCollectionView;
@property (strong,nonatomic) NSNumber *maxID;
@property (strong,nonatomic) UILabel *placeholderLabel;

@end

@implementation GTRTribeDetailMemberCell
@synthesize tribe, users, customContentView, memberCollectionView, maxID, placeholderLabel, delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        users = [[NSMutableArray alloc] init];
        maxID = nil;
        [self setAppearance];
        
    }
    return self;
}

#pragma mark - Appearance methods
-(void)setAppearance
{
    self.backgroundColor = [UIColor clearColor];
    
    CGFloat customContentViewHeight = TRIBE_DETAIL_MEMBER_CELL_HEIGHT;
    CGFloat customContentViewWidth = self.bounds.size.width;
    
    customContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, customContentViewWidth, customContentViewHeight)];
    
    customContentView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:customContentView];
    
    //set cell's title
    NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:TRIBE_MEMBER_TITLE_STRING attributes:@{
                                                                                                                        NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:12]
                                                                                                                        }];
    
    CGFloat titleLabelWidth = 150;
    CGFloat titleLabelHeight = 25;
    CGFloat titleLabelX = (self.bounds.size.width - titleLabelWidth)/2;
    CGFloat titleLabelY = 5;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelX, titleLabelY, titleLabelWidth, titleLabelHeight)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    [titleLabel setAttributedText:titleString];
    [customContentView addSubview:titleLabel];
    
    //set cell's collectionView
    CGFloat memberCollectionViewX = 0;
    CGFloat memberCollectionViewY = titleLabelY+titleLabelHeight;
    CGFloat memberCollectionViewWidth = customContentViewWidth;
    CGFloat memberCollectionViewHeight = customContentViewHeight - memberCollectionViewY;
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(42, 42);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumInteritemSpacing = 0.0f;
    
    memberCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(memberCollectionViewX, memberCollectionViewY, memberCollectionViewWidth, memberCollectionViewHeight) collectionViewLayout:flowLayout];
    memberCollectionView.bounces = YES;
    memberCollectionView.showsHorizontalScrollIndicator = YES;
    memberCollectionView.showsVerticalScrollIndicator = NO;
    memberCollectionView.dataSource = self;
    memberCollectionView.delegate = self;
    memberCollectionView.backgroundColor = [UIColor clearColor];
    memberCollectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    
    [memberCollectionView registerClass:[GTRTribeMemberCollectionViewCell class] forCellWithReuseIdentifier:@"tribeMemberCell"];

    [customContentView addSubview:memberCollectionView];
    
    //set placeholder label
    NSAttributedString *placeholderText = [[NSAttributedString alloc] initWithString:NO_MEMBER_PLACEHOLDER_STRING attributes:@{
                                                                                                                          NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16.0f],
                                                                                                                          NSForegroundColorAttributeName : [UIColor colorWithWhite:0.333 alpha:1.000]
                                                                                                                          }];
    
    placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(memberCollectionViewX, memberCollectionViewY, memberCollectionViewWidth, memberCollectionViewHeight)];
    placeholderLabel.attributedText = placeholderText;
    placeholderLabel.backgroundColor = [UIColor clearColor];
    placeholderLabel.textAlignment = NSTextAlignmentCenter;
    [customContentView addSubview:placeholderLabel];
    
    //set cell's bottom separator
    UIView *bottomSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, customContentViewHeight-1, customContentViewWidth, 1)];
    bottomSeparator.backgroundColor = [UIColor colorWithRed:0.800 green:0.796 blue:0.812 alpha:1.000];
    [customContentView addSubview:bottomSeparator];
    
    [self initiateReloadFunctionality];
    
}

#pragma mark - Infinite scroll initiator
-(void)initiateReloadFunctionality
{
    __weak GTRTribeDetailMemberCell *weakSelf = self;
    
    [memberCollectionView addRightInfiniteScrollingWithActionHandler:^{
        [weakSelf reload:MEMBER_LOAD_MORE];
    }];
    
}

#pragma mark - Content setter
-(void)setContent:(GTRTribe*)theTribe;
{
    tribe = theTribe;
    [self reload:MEMBER_REFRESH];
}

#pragma mark - UICollectionView delegates

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger memberCount = users.count;
    
    placeholderLabel.hidden = (memberCount != 0);
    
    return memberCount;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    GTRTribeMemberCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"tribeMemberCell" forIndexPath:indexPath];

    if (cell == nil) {
        cell = [[GTRTribeMemberCollectionViewCell alloc] initWithFrame:CGRectMake(0, 0, 42, 42)];
    }
    
    GTRUser *member = [users objectAtIndex:indexPath.row];
    [cell setContent:member];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    GTRUser *member = (GTRUser*)[users objectAtIndex:indexPath.row];

    if ([(id) delegate respondsToSelector:@selector(onMemberSelected:)]) {
        [delegate onMemberSelected:member];
    }
}

#pragma mark - Infinite scrolling
-(void)reload:(NSInteger)theReloadMode
{
    DDLogInfo(@"reload called! mode : %d",theReloadMode);

    NSNumber *buffMaxID = maxID;
    
    if (theReloadMode == MEMBER_REFRESH) {
        maxID = nil;
    }
    
    [GTRTribeRestService retrieveTribeMembers:tribe MaxID:maxID OnSuccess:^(id responseObject) {
        
        if (theReloadMode == MEMBER_REFRESH) {
            [users removeAllObjects];
        }
        
        NSArray *userArray = (NSArray*)[responseObject objectForKey:@"data"];
        NSMutableArray *bufferUsers = [[NSMutableArray alloc] init];
        
        for (NSDictionary* dictionary in userArray) {
            [bufferUsers addObject:[MTLJSONAdapter modelOfClass:[GTRUser class] fromJSONDictionary:dictionary error:nil]];
        }
        
        if(bufferUsers.count > 0) {
            [users addObjectsFromArray:bufferUsers];
            maxID = [[responseObject objectForKey:@"pagination"] objectForKey:@"max_id"];
        } else {
            maxID = buffMaxID;
        }

        [memberCollectionView.infiniteScrollingView stopAnimating];
        
        [memberCollectionView reloadData];
                 
     } onFail:^(id responseObject, NSError *error) {
         
         maxID = buffMaxID;
        [memberCollectionView.infiniteScrollingView stopAnimating];
         
     }];
    
}
@end
