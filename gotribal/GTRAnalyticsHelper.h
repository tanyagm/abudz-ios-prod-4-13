//
//  GTRAnalyticsHelper.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/23/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTRAnalyticsHelper : NSObject

#pragma mark Fitness Data

+(void)sendFitnessSurveySkippedEvent;

+(void)sendFitnessProfileChangedEvent;

#pragma mark ActiveBudz

+(void)sendActiveBudzRequestToConnectEvent;

+(void)sendActiveBudzAcceptRequestEvent;

#pragma mark Invitations

+(void)sendInviteFriendsEvent;

@end
