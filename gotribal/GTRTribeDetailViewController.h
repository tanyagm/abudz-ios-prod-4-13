//
//  GTRTribeDetailViewController.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/13/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRStreamPageViewController.h"
#import "GTRTribe.h"

@protocol GTRTribeDetailViewControllerDelegate

@optional

-(void)onLeftFromTribe;

@end

@interface GTRTribeDetailViewController : GTRStreamPageViewController

@property (weak) id <GTRTribeDetailViewControllerDelegate> delegate;

-(void)setContent:(GTRTribe*)theTribe;

@end
