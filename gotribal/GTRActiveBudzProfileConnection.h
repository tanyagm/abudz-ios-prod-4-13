//
//  GTRActiveBudzProfileConnection.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/3/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GTRActiveBudzProfileConnectionDelegate

-(void)onUserSelected:(GTRUser*)theUser;

@end

@interface GTRActiveBudzProfileConnection : UIView

@property (weak) id <GTRActiveBudzProfileConnectionDelegate> delegate;

-(id)initWithFrame:(CGRect)frame connections:(NSArray*)theConnections;
-(void)setConnections:(NSArray*)theConnections;
-(void)resetAppearance;

@end
