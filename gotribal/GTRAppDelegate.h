//
//  GTRAppDelegate.h
//  gotribal
//
//  Created by Muhammad Taufik on 11/15/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRUser.h"
#import "GTRUAPushHandler.h"

#ifdef GTR_FLURRY_ANALYTICS
#import "Flurry.h"
#endif

@interface GTRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) GTRUser *currentUser;
@property (strong, nonatomic) GTRUAPushHandler *pushHandler;
@property (strong, nonatomic) MFSideMenuContainerViewController *menuViewController;
@property (strong, nonatomic) NSMutableDictionary* settingConf;

-(void)reloadData;
-(void)logoutThisApp;
-(void)logoutThisAppWhenBackendFails;

@end
