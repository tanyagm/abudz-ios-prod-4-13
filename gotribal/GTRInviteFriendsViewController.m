//
//  GTRInviteFriendsViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/19/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRInviteFriendsViewController.h"
#import "GTRConstants.h"
#import "GTRNavigationBarHelper.h"
#import "GTRInviteTwitterFriendsViewController.h"
#import "GTRInviteActiveBudzViewController.h"
#import "GTRInviteContactsViewController.h"
#import "GTRSocialButtonCell.h"

//title string
NSString* const INVITE_FRIENDS_TITLE = @"Invite Friends";
NSString* const INVITE_FRIENDS_TO_TRIBE_TITLE = @"Invite Friends To Tribe";

//invite button strings
NSString* const INVITE_FACEBOOK_BUTTON_TEXT = @"Invite Friends From Facebook";
NSString* const INVITE_TWITTER_BUTTON_TEXT = @"Invite Friends From Twitter";
NSString* const INVITE_CONTACTS_BUTTON_TEXT = @"Invite Friends From Contacts";
NSString* const INVITE_ACTIVEBUDZ_BUTTON_TEXT = @"Invite Your Activebudz";

//invite button image names
NSString* const INVITE_FACEBOOK_IMAGE_NAME = @"ico-fb";
NSString* const INVITE_TWITTER_IMAGE_NAME = @"ico-twitter";
NSString* const INVITE_CONTACTS_IMAGE_NAME = @"ico-contacts";
NSString* const INVITE_ACTIVEBUDZ_IMAGE_NAME = @"ico-invite-activebudz";

@interface GTRInviteFriendsViewController ()

@property BOOL isPushed;
@property BOOL isTribeMode;

@property (nonatomic, strong) NSMutableArray *socialButtons;
@end

@implementation GTRInviteFriendsViewController
@synthesize isPushed, isTribeMode, tribe;

-(id)initWithPushedMode:(BOOL)thePushedMode tribeMode:(BOOL)theTribeMode;
{
    self = [super init];
    if (self) {
        isPushed = thePushedMode;
        isTribeMode = theTribeMode;
        self.title = isTribeMode ? INVITE_FRIENDS_TO_TRIBE_TITLE : INVITE_FRIENDS_TITLE;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = GTR_MAIN_ACTIVITIES_BACKGROUND_COLOR;
    
    if (isPushed) {
        [GTRNavigationBarHelper setBackButtonItem:self.navigationItem withTarget:self selector:@selector(popInviteFriendsViewController)];
    } else {
        [GTRNavigationBarHelper setLeftSideBarButtonItem:self.navigationItem withTarget:self selector:@selector(toggleSideMenu:)];
    }
    
    //[self setAppearance];
    
    /*
    NSDictionary *facebook = @{ @"title": INVITE_FACEBOOK_BUTTON_TEXT,
                                @"imageName" : INVITE_FACEBOOK_IMAGE_NAME,
                                @"selector" : NSStringFromSelector(@selector(inviteFacebookButtonAction:))
                                };
     */
    
    NSDictionary *twitter = @{
                                @"title": INVITE_TWITTER_BUTTON_TEXT,
                                @"imageName" : INVITE_TWITTER_IMAGE_NAME,
                                @"selector" : NSStringFromSelector(@selector(inviteTwitterButtonAction:))
                                };
    
    NSDictionary *contact = @{
                                @"title": INVITE_CONTACTS_BUTTON_TEXT,
                                @"imageName" : INVITE_CONTACTS_IMAGE_NAME,
                                @"selector" : NSStringFromSelector(@selector(inviteContactsButtonAction:))
                                };
    
    NSDictionary *activebudz = @{
                                @"title": INVITE_ACTIVEBUDZ_BUTTON_TEXT,
                                @"imageName" : INVITE_ACTIVEBUDZ_IMAGE_NAME,
                                @"selector" : NSStringFromSelector(@selector(inviteActiveBudzButtonAction:))
                                };
    
    self.socialButtons = [NSMutableArray array];
    //[self.socialButtons addObject:facebook];
    [self.socialButtons addObject:twitter];
    [self.socialButtons addObject:contact];
    if (isTribeMode) [self.socialButtons addObject:activebudz];
    
    self.tableView.separatorColor = [UIColor clearColor];
    [self.tableView setContentInset:UIEdgeInsetsMake(5,0,0,0)];
    [self.tableView reloadData];
}

#pragma mark - Appearance methods
-(void)setAppearance
{
    //set the buttons
    CGFloat buttonHorizontalPadding = 10;
    CGFloat buttonVerticalPadding = 10;
    CGFloat buttonWidth = self.view.bounds.size.width - (buttonHorizontalPadding*2);
    CGFloat buttonHeight = 50;
    
    CGFloat buttonX = buttonHorizontalPadding;
    CGFloat buttonYOffset = buttonVerticalPadding*2;
    
    NSArray *buttonTitleArray = @[INVITE_FACEBOOK_BUTTON_TEXT, INVITE_TWITTER_BUTTON_TEXT, INVITE_CONTACTS_BUTTON_TEXT, INVITE_ACTIVEBUDZ_BUTTON_TEXT];
    
    NSArray *buttonImageNameArray = @[INVITE_FACEBOOK_IMAGE_NAME, INVITE_TWITTER_IMAGE_NAME, INVITE_CONTACTS_IMAGE_NAME, INVITE_ACTIVEBUDZ_IMAGE_NAME];
    
    int count = isTribeMode? buttonTitleArray.count : buttonTitleArray.count-1;
    
    UIButton *bufferButton;
    
    for (int i = 0; i < count; i++) {
        
        bufferButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonX, buttonYOffset, buttonWidth, buttonHeight)];
        [bufferButton setTitle:[buttonTitleArray objectAtIndex:i] forState:UIControlStateNormal];
        [bufferButton setImage:[UIImage imageNamed:[buttonImageNameArray objectAtIndex:i]] forState:UIControlStateNormal];
        [bufferButton setTitleColor:[UIColor colorWithWhite:0.333 alpha:1.000] forState:UIControlStateNormal];
        bufferButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [bufferButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        [bufferButton setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        bufferButton.backgroundColor = [UIColor whiteColor];
        bufferButton.layer.cornerRadius = 5.0f;
        bufferButton.layer.borderWidth = 0.5f;
        bufferButton.layer.borderColor = [UIColor colorWithWhite:0.800 alpha:1.000].CGColor;
        switch (i) {
            case 0:
                {
                    [bufferButton addTarget:self action:@selector(inviteFacebookButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                }
                break;
            case 1:
                {
                    [bufferButton addTarget:self action:@selector(inviteTwitterButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                }
                break;
            case 2:
                {
                    [bufferButton addTarget:self action:@selector(inviteContactsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                }
                break;
            case 3:
                {
                    [bufferButton addTarget:self action:@selector(inviteActiveBudzButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                }
                break;
            default:
                break;
        }
        
        [self.view addSubview:bufferButton];
        buttonYOffset += buttonHeight+buttonVerticalPadding;
    }
}

#pragma mark - Button actions

-(void)inviteFacebookButtonAction:(id)sender
{
    DDLogInfo(@"Invite facebook button clicked!");
}

-(void)inviteTwitterButtonAction:(id)sender
{
    DDLogInfo(@"Invite twitter button clicked!");
    GTRInviteTwitterFriendsViewController *inviteTwitterFriendsViewController = [[GTRInviteTwitterFriendsViewController alloc] initWithTribe:self.tribe];
    [self.navigationController pushViewController:inviteTwitterFriendsViewController animated:YES];

}

-(void)inviteContactsButtonAction:(id)sender
{
    DDLogInfo(@"Invite contacts button clicked!");
    GTRInviteContactsViewController *inviteContactsViewController = [[GTRInviteContactsViewController alloc] initWithTribe:self.tribe];
    [self.navigationController pushViewController:inviteContactsViewController animated:YES];
}

-(void)inviteActiveBudzButtonAction:(id)sender
{
    DDLogInfo(@"Invite activebudz button clicked!");
    GTRInviteActiveBudzViewController *inviteActiveBudzViewController = [[GTRInviteActiveBudzViewController alloc] initWithTribe:tribe];
    [self.navigationController pushViewController:inviteActiveBudzViewController animated:YES];
}

#pragma mark - Custom methods

#pragma mark Dismiss
-(void)popInviteFriendsViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.socialButtons.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *const CellIdentifier = @"CellIdentifier";
    GTRSocialButtonCell *cell = (GTRSocialButtonCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[GTRSocialButtonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *socialButton = [self.socialButtons objectAtIndex:indexPath.row];
    cell.textLabel.text = [socialButton valueForKey:@"title"];
    cell.imageView.image = [UIImage imageNamed:[socialButton valueForKey:@"imageName"]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *socialButton = [self.socialButtons objectAtIndex:indexPath.row];
    SEL selector = NSSelectorFromString([socialButton valueForKey:@"selector"]);
    
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self performSelector:selector withObject:nil];
    #pragma clang diagnostic pop
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
