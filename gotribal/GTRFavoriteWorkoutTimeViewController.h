//
//  GTRFavoriteWorkoutTimeViewController.h
//  gotribal
//
//  Created by loaner on 11/26/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRFitnessPageTempleteViewController.h"
@interface GTRFavoriteWorkoutTimeViewController : GTRFitnessPageTempleteViewController

@property (strong,nonatomic) NSArray * favWorkoutTime;

@end
