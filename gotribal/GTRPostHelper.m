//
//  GTRPostHelper.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRPostHelper.h"

@implementation GTRPostHelper
+(NSMutableArray *)getArchivedActivityStreams
{
    NSData *archive = [[NSUserDefaults standardUserDefaults] valueForKey:ACTIVITY_STREAM_KEY];
    
    if (archive) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:archive];
    } else {
        return [[NSMutableArray alloc] init];
    }
}

+(void)archiveActivityStreams:(NSMutableArray *)activityStreams
{
    NSData *archive = [NSKeyedArchiver archivedDataWithRootObject:activityStreams];
    [[NSUserDefaults standardUserDefaults] setObject:archive forKey:ACTIVITY_STREAM_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
