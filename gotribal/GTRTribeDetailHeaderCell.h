//
//  GTRTribeDetailHeaderCell.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/13/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRTribe.h"

@protocol GTRTribeDetailHeaderCellDelegate

-(void)postButtonAction:(id)sender;
-(void)leaveTribeButtonAction:(id)sender;

@end

@interface GTRTribeDetailHeaderCell : UITableViewCell

@property (weak) id <GTRTribeDetailHeaderCellDelegate> delegate;

-(void)setContent:(GTRTribe*)theTribe;

@end
