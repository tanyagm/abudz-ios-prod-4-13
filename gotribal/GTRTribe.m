//
//  GTRTribe.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/11/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRTribe.h"
#import "GTRUser.h"

@implementation GTRTribe

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"tribeID" : @"id",
             @"tribeName" : @"name",
             @"tribeImage": @"image",
             @"tribeActivity": @"activity",
             @"tribeDescription" : @"description",
             @"tribeImageURL" : @"image",
             @"isJoined" : @"joined",
             @"tribeOwner" : @"owner",
             @"tribeMembers" : @"users"
             };
}

+(NSValueTransformer *)tribeOwnerJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:GTRUser.class];
}

+(NSValueTransformer *)tribeMembersJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:GTRUser.class];
}

@end
