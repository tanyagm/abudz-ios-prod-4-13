//
//  GTRPostCell.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRStreamPostCell.h"
#import <UIImageView+AFNetworking.h>
#import "GTRUserImageView.h"
#import "GTRImagePickerHelper.h"

@interface GTRStreamPostCell()
@property (nonatomic,strong) UIView *postContainer;
@property (nonatomic,strong) UILabel *ownerNameLabel;
@property (nonatomic,strong) UILabel *messageLabel;
@property (nonatomic,strong) UILabel *postDateLabel;
@property (nonatomic,strong) GTRUserImageView *ownerAvatar;
@property (nonatomic,strong) UIImageView *postImage;
@property (nonatomic,strong) UIView *postControlPanel;
@property (nonatomic,strong) UILabel *likesCountLabel;
@property (nonatomic, readwrite, strong) UIButton *likeButton;

@property (nonatomic, strong) UILabel *commentsCountLabel;
@property (nonatomic, readwrite, strong) UIButton *commentButton;

@property (nonatomic, readwrite, strong) UIButton *moreButton;

@property (nonatomic, strong) GTRPost *post;
@end

@implementation GTRStreamPostCell

@synthesize postContainer, messageLabel, ownerNameLabel, ownerAvatar, postImage, postDateLabel, postControlPanel, likeButton, post,
commentsCountLabel, likesCountLabel, commentButton, moreButton;

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self setAppearance];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        #ifdef DEBUG_COLOR
        likeButton.backgroundColor = [UIColor purpleColor];
        postControlPanel.backgroundColor = [UIColor cyanColor];
        ownerAvatar.backgroundColor = [UIColor blackColor];
        postContainer.backgroundColor = [UIColor yellowColor];
        messageLabel.backgroundColor = [UIColor greenColor];
        ownerNameLabel.backgroundColor = [UIColor redColor];
        #endif
    }
    return self;
}

- (CGFloat)getHeight
{
    return self.frame.size.height;
}

#pragma mark Update cell content

-(void)setContent:(GTRPost *)xpost
{
    post = xpost;
    
    ownerNameLabel.text = [post.owner fullName];
    
    [self setMessageLabelwithText:post.message];
    postDateLabel.text = [post humanDate];
    
    [self setAvatarImageView];
    [self setPostImageView];
    [self setPostControlPanelPosition];
    
    [self setPostContainerHeight];
    [self setLikeIconConditon];
    likesCountLabel.text = [NSString stringWithFormat:@"%@",post.likesCount];
    commentsCountLabel.text = [NSString stringWithFormat:@"%@",post.commentsCount];
}

#pragma mark Setting cell appearance

-(void)setAppearance
{
    self.backgroundColor = [UIColor colorWithWhite:1 alpha:0.0];
    
    postContainer = [[UIView alloc] initWithFrame:CGRectMake(10, 5, 300, 0)];
    postContainer.backgroundColor = [UIColor whiteColor];
    postContainer.layer.cornerRadius = 4;
    postContainer.layer.borderColor = UIColorFromRGB(0xBCBCBC).CGColor;
    postContainer.layer.borderWidth = 0.3;
    
    
    ownerAvatar = [[GTRUserImageView alloc]initWithFrame:CGRectMake(7, 9, 45, 45)];
    ownerAvatar.userPicture.contentMode = UIViewContentModeScaleAspectFill;
    ownerAvatar.userPicture.backgroundColor = [UIColor colorWithWhite:0.337 alpha:1.000];
    ownerAvatar.userPicture.layer.cornerRadius = ownerAvatar.bounds.size.width/2;
    ownerAvatar.userPicture.layer.masksToBounds = YES;
    
    
    ownerNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(59, 9, 137, 18)];
    ownerNameLabel.font = CONTEXT_FONT;
    ownerNameLabel.text = @"fullname";
    ownerNameLabel.textAlignment = NSTextAlignmentLeft;
    ownerNameLabel.textColor = UIColorFromRGB(0x5E5E5E);
    ownerNameLabel.backgroundColor = [UIColor clearColor];
    ownerNameLabel.clipsToBounds = YES;
    
    
    messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(59, 29, 200, 150)];
    messageLabel.font = SUBCONTEXT_FONT;
    messageLabel.textAlignment = NSTextAlignmentLeft;
    messageLabel.text = @"message";
    messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    messageLabel.textColor = UIColorFromRGB(0x5E5E5E);
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.numberOfLines = 10;
    
    
    postDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, 9, 93, 18)];
    postDateLabel.font = TIMESTAMP_FONT;
    postDateLabel.textAlignment = NSTextAlignmentRight;
    postDateLabel.textColor =UIColorFromRGB(0x9A9A9A);
    postDateLabel.text = @"About an hour ago";
    
    
    postImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 70, 300, 215)];
    postImage.contentMode = UIViewContentModeScaleAspectFill;
//    postImage.backgroundColor = [UIColor colorWithWhite:0.337 alpha:1.000]; // change to appropriate final color
    postImage.backgroundColor = self.backgroundColor;
    

    likeButton = [[UIButton alloc] initWithFrame:CGRectMake(77, -7, 44, 44)];
    [likeButton setImage:[UIImage imageNamed:@"ico-heart-grey"] forState:UIControlStateNormal];
    likeButton.imageEdgeInsets = UIEdgeInsetsMake(0,0,0,17);
    
    likesCountLabel =  [[UILabel alloc] initWithFrame:CGRectMake(50, 7, 30, 15)];
    likesCountLabel.font = NUMBER_LABEL_FONT;
    likesCountLabel.textAlignment = NSTextAlignmentRight;
    likesCountLabel.textColor =UIColorFromRGB(0x9A9A9A);
    likesCountLabel.text = [NSString stringWithFormat:@"%d",42];
    
    commentButton = [[UIButton alloc] initWithFrame:CGRectMake(27, -7, 44, 44)];
    [commentButton setImage:[UIImage imageNamed:@"ico-comment"] forState:UIControlStateNormal];
    commentButton.imageEdgeInsets = UIEdgeInsetsMake(0,0,0,17);
    
    commentsCountLabel =  [[UILabel alloc] initWithFrame:CGRectMake(0, 7, 27, 15)];
    commentsCountLabel.font = NUMBER_LABEL_FONT;
    commentsCountLabel.textAlignment = NSTextAlignmentRight;
    commentsCountLabel.textColor =UIColorFromRGB(0x9A9A9A);
    commentsCountLabel.text = [NSString stringWithFormat:@"%d",42];
    
    moreButton = [[UIButton alloc] initWithFrame:CGRectMake(260, -7, 44, 44)];
    [moreButton setImage:[UIImage imageNamed:@"ico-more"] forState:UIControlStateNormal];
    moreButton.imageEdgeInsets = UIEdgeInsetsMake(0,0,0,17);

    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 0.5)];
    lineView.backgroundColor = UIColorFromRGB(0xDADADA);
    
    postControlPanel = [[UIView alloc] initWithFrame:CGRectMake(0, 70, 300, 44)];

    [postControlPanel addSubview:lineView];
    [postControlPanel addSubview:likesCountLabel];
    [postControlPanel addSubview:likeButton];
    [postControlPanel addSubview:commentsCountLabel];
    [postControlPanel addSubview:commentButton];
    [postControlPanel addSubview:moreButton];
    
    [self addSubview:postContainer];
    [postContainer addSubview:ownerNameLabel];
    [postContainer addSubview:messageLabel];
    [postContainer addSubview:ownerAvatar];
    [postContainer addSubview:postImage];
    [postContainer addSubview:postDateLabel];
    [postContainer addSubview:postControlPanel];
}

-(void)setMessageLabelwithText:(NSString*)text
{
    CGRect frame = messageLabel.frame;
    frame.size = CGSizeMake(236, 300);
    messageLabel.frame = frame;
    messageLabel.text = text;
    [messageLabel sizeToFit];
}

-(void) setPostContainerHeight{
    const double totalTopPadding = 16;
    double containerHeight = 0.0;
    
    containerHeight += totalTopPadding;
    containerHeight += [self getPostImageHeight];
    containerHeight += [self getPostBodyHeight];
    containerHeight += [self getPostControlPanelHeight];
    
    CGRect frame = postContainer.frame;
    frame.size.height = containerHeight;
    postContainer.frame = frame;
    
    frame.size.height += 5;
    self.frame = frame;
}

-(void) setPostControlPanelPosition
{
    const double totalTopPadding = 30;
    CGRect frame = postControlPanel.frame;
    frame.origin.y = [self getPostBodyHeight] +
                     [self getPostImageHeight] +
                     totalTopPadding;
    postControlPanel.frame = frame;
}

-(void) setPostImageView
{
    const double totalTopPadding = 30;
    CGRect frame = postImage.frame;
    frame.origin.y = [self getPostBodyHeight] + totalTopPadding;
    postImage.frame = frame;
    postImage.contentMode = UIViewContentModeScaleToFill;

    NSString *imageURL =  post.imageURL;
    if (imageURL){
        [GTRImagePickerHelper setAndAdjustOrientationImageView:postImage
                                                       withURL:[NSURL URLWithString:imageURL]
                                                andPlaceholder:DEFAULT_ACTIVITY_PLACEHOLDER];
        
        postImage.clipsToBounds = YES;
        postImage.hidden = NO;
    } else {
        postImage.hidden = YES;
    }
}

-(void) setAvatarImageView
{
    NSString *avatarURL = [post.owner avatarURL];
    int userRank = [self.post.owner userRank];
    [ownerAvatar setUserPictureWithURL:[NSURL URLWithString:avatarURL] badgeLevel:userRank badgePosition:Left];
}

-(void) setLikeIconConditon
{
    if (post.liked){
        [likeButton setImage:[UIImage imageNamed:@"ico-heart"] forState:UIControlStateNormal];
    } else {
        [likeButton setImage:[UIImage imageNamed:@"ico-heart-grey"] forState:UIControlStateNormal];
    }
}

#pragma mark Property getter
-(double) getPostControlPanelHeight{
    return postControlPanel.frame.size.height;
}

-(double) getPostBodyHeight{
    return MAX( messageLabel.frame.size.height +  ownerNameLabel.frame.size.height,
               ownerAvatar.frame.size.height);
}

-(double) getPostImageHeight{
    if (post.imageURL){
        return postImage.frame.size.height;
    } else {
        return 0.0;
    }
}


@end
