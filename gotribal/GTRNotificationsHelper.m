//
//  GTRNotificationsHelper.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRNotificationsHelper.h"

@implementation GTRNotificationsHelper

+(UIView*)createHeaderViewWithText:(NSString*)text withFont:(UIFont*)font
{
    UIView *headerView = [[UIView alloc] init];
    headerView.frame = CGRectMake(0, 0, 320, 35);
    headerView.backgroundColor = TABLEVIEW_BACKGROUND_COLOR;
    
    UILabel *textLabel = [[UILabel alloc]initWithFrame:CGRectOffset(headerView.frame, 10, 0)];
    textLabel.backgroundColor = TABLEVIEW_BACKGROUND_COLOR;
    textLabel.text = text;
    textLabel.font = font;
    textLabel.textColor = NOTIFICATIONS_CELL_SUBTEXT_COLOR;
    textLabel.contentMode = UIViewContentModeCenter;
    [headerView addSubview:textLabel];
    
    return headerView;
}

+(UIView*)createGroupedViewInCell:(GTRPendingRequestCell*)cell atIndexPath:(NSIndexPath*)indexPath
{
    UITableView *tableView = (UITableView*)cell.superview.superview;
    CGFloat cornerRadius = 3.f;
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
    CGMutablePathRef pathRef = CGPathCreateMutable();
    CGRect bounds = CGRectInset(cell.bounds, 5, 0);
    BOOL addLine = NO;
    
    if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
        CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
    } else if (indexPath.row == 0) {
        CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
        CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
        addLine = YES;
    } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
        CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
        CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
    } else {
        CGPathAddRect(pathRef, nil, bounds);
        addLine = YES;
    }
    
    UIColor *lineColor = [UIColor colorWithRed:219/255.0
                                         green:219/255.0
                                          blue:219/255.0 alpha:1.0];
    
    layer.path = pathRef;
    CFRelease(pathRef);
    layer.fillColor = [UIColor colorWithWhite:1.f alpha:1.0f].CGColor;
    
    if (addLine == YES) {
        CALayer *lineLayer = [[CALayer alloc] init];
        CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
        lineLayer.frame = CGRectMake(CGRectGetMinX(bounds)+10, bounds.size.height-lineHeight, bounds.size.width-10, lineHeight);
        lineLayer.backgroundColor = lineColor.CGColor;
        [layer addSublayer:lineLayer];
    }
    
    // border color
    layer.lineWidth = 0.8;
    layer.strokeColor = lineColor.CGColor;
    
    UIView *testView = [[UIView alloc] initWithFrame:bounds];
    [testView.layer insertSublayer:layer atIndex:0];
    testView.backgroundColor = TABLEVIEW_BACKGROUND_COLOR;
    
    return testView;
}

@end
