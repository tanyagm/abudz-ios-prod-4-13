//
//  GTRTableViewController.h
//  gotribal
//
//  Created by Muhammad Taufik on 12/8/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRNavigationBarHelper.h"

@interface GTRTableViewController : UITableViewController
-(void)toggleSideMenu:(id)sender;
@end
