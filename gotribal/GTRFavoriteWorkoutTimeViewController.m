//
//  GTRFavoriteWorkoutTimeViewController.m
//  gotribal
//
//  Created by loaner on 11/26/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRFavoriteWorkoutTimeViewController.h"

#import "GTRSurveyPageGenerator.h"

@interface GTRFavoriteWorkoutTimeViewController()

@end

@implementation GTRFavoriteWorkoutTimeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Favorite Workout Time";
    }

    return self;
}






@end
