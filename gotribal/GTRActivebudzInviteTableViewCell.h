//
//  GTRActivebudzInviteTableViewCell.h
//  gotribal
//
//  Created by Muhammad Taufik on 1/3/14.
//  Copyright (c) 2014 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, GTRActivebudzInviteState) {
    GTRActivebudzInviteStateUninvited,
    GTRActivebudzInviteStateJoined,
    GTRActivebudzInviteStateInvited
};

@interface GTRActivebudzInviteTableViewCell : UITableViewCell

@property (nonatomic, assign) GTRActivebudzInviteState inviteState;
@property (nonatomic, strong) UIButton *inviteButton;

@end
