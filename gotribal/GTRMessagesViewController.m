//
//  GTRMessagesViewController.m
//  gotribal
//
//  Created by Muhammad Taufik on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRMessagesViewController.h"
#import "MFSideMenu.h"

#import "AFOAuth2Client.h"
#import "AFNetworking.h"
#import "GTRMessage.h"
#import "Mantle.h"
#import "GTRAppDelegate.h"
#import "UIImageView+AFNetworking.h"
#import "UIBarButtonItem+NegativeSpacer.h"
#import "NSString+JSMessagesView.h"

#import "GTRUser.h"
#import <Pusher/Pusher.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "GTRRestServiceErrorHandler.h"

#define kSubtitleMe @"Me"
#define kSubtitleBuddy @"Buddy"

@interface GTRMessagesViewController () <JSMessagesViewDelegate, JSMessagesViewDataSource, PTPusherDelegate, PTPusherPresenceChannelDelegate>
@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) NSDictionary *metadata;

@property (nonatomic, copy) NSNumber *receiverID;
@property (nonatomic, copy) NSNumber *conversationID;

@property (strong, nonatomic) PTPusher *pusherClient;
@property (strong, nonatomic) PTPusherPresenceChannel *presenceChannel;
@property (strong, nonatomic) PTPusherConnection *connection;

@property (strong, nonatomic) GTRUser *currentUser;
@property (strong, nonatomic) GTRUser *otherUser;

@property (strong, nonatomic) NSArray *presenceMembers;

@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (getter = isReachable, nonatomic) BOOL reachable;
@end

@implementation GTRMessagesViewController

-(id)init
{
    if (self = [super init]) {
        GTRAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        self.currentUser = appDelegate.currentUser;
        
        [self setupPusher];
    }
    
    return self;
}

-(id)initWithConversation:(GTRConversation *)conversation
{
    if ([self init]) {
        self.title = [NSString stringWithFormat:@"%@ %@", conversation.user.firstName, conversation.user.lastName];
        self.receiverID = conversation.user.userID;
        self.conversationID = conversation.conversationID;
    }
    
    return self;
}

-(id)initWithUser:(GTRUser *)user
{
    if ([self init]) {
        self.title = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
        self.receiverID = user.userID;
    }
    
    return self;
}

-(id)initWithConversationID:(NSNumber *)conversationID
{
    if ([self init]) {
        self.conversationID = conversationID;
    }
    
    return self;
}

-(void)setupPusher
{
    // Initialize Pusher
    self.pusherClient = [PTPusher pusherWithKey:PUSHER_KEY delegate:self encrypted:YES];
    self.pusherClient.reconnectDelay = 3.0;
    self.pusherClient.authorizationURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", BASE_URL, DIRECT_MESSAGE_AUTH_ENDPOINT]];
}

#pragma mark - PTPusher delegate

-(void)pusher:(PTPusher *)pusher willAuthorizeChannel:(PTPusherChannel *)channel withRequest:(NSMutableURLRequest *)request
{
    AFOAuthCredential *credential = [AFOAuthCredential retrieveCredentialWithIdentifier:[[NSURL URLWithString:BASE_URL] host]];
    [request setValue:[NSString stringWithFormat:@"%@%@", @"Bearer ", credential.accessToken] forHTTPHeaderField:@"Authorization"];
}

-(void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection failedWithError:(NSError *)error
{
    DDLogInfo(@"%@ %@", connection, [error localizedDescription]);
}

- (void)pusher:(PTPusher *)pusher connectionDidConnect:(PTPusherConnection *)connection;
{
    DDLogInfo(@"%@", connection);
    self.connection = connection;
}

-(void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection didDisconnectWithError:(NSError *)error
{
    DDLogInfo(@"%@", [error localizedDescription]);
}

#pragma mark - PTPusherChannelPresenceDelegate

-(void)presenceChannelDidSubscribe:(PTPusherPresenceChannel *)channel
{
    
}

- (void)presenceChannel:(PTPusherPresenceChannel *)channel memberAdded:(PTPusherChannelMember *)member
{
    
}

- (void)presenceChannel:(PTPusherPresenceChannel *)channel memberRemoved:(PTPusherChannelMember *)member
{
    
}

-(void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection didDisconnectWithError:(NSError *)error willAttemptReconnect:(BOOL)willAttemptReconnect
{
    if (!willAttemptReconnect) {
        [self handleDisconnectionWithError:error];
    }
}

#pragma mark - Error Handling

-(void)handleDisconnectionWithError:(NSError *)error
{
    if (error && [error.domain isEqualToString:PTPusherFatalErrorDomain]) {
        DDLogInfo(@"%@", [error localizedDescription]);
    } else {
        if (self.reachable) {
            [self.pusherClient performSelector:@selector(connect) withObject:nil afterDelay:5.0];
        }
    }
}

#pragma mark - UIViewController lifecycles

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.pusherClient connect];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.pusherClient disconnect];
}

- (void)viewDidLoad
{
    self.delegate = self;
    self.dataSource = self;
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]){
        self.automaticallyAdjustsScrollViewInsets = YES;
    }
    
    [self.tableView setContentInset:UIEdgeInsetsMake(10, 0, 0, 0)];
    
    [GTRNavigationBarHelper setBackButtonItem:self.navigationItem withTarget:self selector:@selector(backButtonAction:)];
    
    [self setBackgroundColor:[UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0]];
    
    self.messageInputView.textView.placeHolder = NSLocalizedString(@"Type Something...", @"type something");
    
    self.messages = [NSMutableArray array];
    
    // Use UIRefreshControl
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    [self.tableView addSubview:self.refreshControl];
    
    // Reachability
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
            case AFNetworkReachabilityStatusUnknown:
                self.reachable = NO;
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
            case AFNetworkReachabilityStatusReachableViaWWAN:
                self.reachable = YES;
                break;
            default:
                break;
        }
    }];
    
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(dismissKeyboard:)];
    
    [self.view addGestureRecognizer:tapGesture];
}

-(void)setReachable:(BOOL)flag
{
    if (_reachable != flag) {
        _reachable = flag;
        [self textViewDidChange:self.messageInputView.textView];
        if (_reachable) [self.pusherClient connect];
    }
}

-(void)dealloc
{
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
}

-(void)textViewDidChange:(UITextView *)textView
{
    [super textViewDidChange:textView];
    self.messageInputView.sendButton.enabled = (self.isReachable && ([textView.text js_stringByTrimingWhitespace].length > 0));
}

#pragma mark - UIRefreshControl target-action

-(void)refresh:(id)sender
{
    [self requestMessagesWithID:self.metadata[@"max_id"]];
}

-(void)backButtonAction:(id)sender
{
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!self.conversationID) {
        [self initiateDirectMessageRequest];
    } else {
        [self conversationRequest];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - JSMessagesViewController methods

- (BOOL)shouldPreventScrollToBottomWhileUserScrolling
{
    return YES;
}

- (void)buttonPressed:(UIButton *)sender
{
    GTRMessagesViewController *vc = [[GTRMessagesViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.messages.count;
}

#pragma mark - Messages view delegate: REQUIRED

- (void)didSendText:(NSString *)text
{
    GTRMessage *lastMessage = self.messages.lastObject;
    
    GTRMessage *message = [[GTRMessage alloc] init];
    message.messageID = (lastMessage && lastMessage.messageID ? [NSNumber numberWithInt:lastMessage.messageID.intValue + 1] : 0 );
    NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
    message.createdAt = [NSNumber numberWithInt:interval];
    message.body = text;
    message.receiverID = self.receiverID;
    message.senderID = self.currentUser.userID;
    message.user = self.currentUser;
    message.socketID = self.connection.socketID;

    [self sendMessage:message];
}


- (JSBubbleMessageType)messageTypeForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GTRMessage *message = [self.messages objectAtIndex:indexPath.row];
    
    if ([message.senderID isEqualToNumber:self.currentUser.userID]) {
        return JSBubbleMessageTypeOutgoing;
    } else {
        return JSBubbleMessageTypeIncoming;
    }
}

- (UIImageView *)bubbleImageViewWithType:(JSBubbleMessageType)type
                       forRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [JSBubbleImageViewFactory classicBubbleImageViewForType:type style:JSBubbleImageViewStyleGotribalPlainTop];
}

- (JSMessagesViewTimestampPolicy)timestampPolicy
{
    return JSMessagesViewTimestampPolicyAll;
}

- (JSMessagesViewAvatarPolicy)avatarPolicy
{
    return JSMessagesViewAvatarPolicyAll;
}

- (JSMessagesViewSubtitlePolicy)subtitlePolicy
{
    return JSMessagesViewSubtitlePolicyNone;
}

- (JSMessageInputViewStyle)inputViewStyle
{
    return JSMessageInputViewStyleFlat;
}

#pragma mark - Messages view delegate: OPTIONAL

//
//  *** Implement to customize cell further
//
- (void)configureCell:(JSBubbleMessageCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    [cell.bubbleView setTextColor:[UIColor colorWithRed:147.0/255.0 green:147.0/255.0 blue:147.0/255.0 alpha:1.0f]];
    
    if(cell.timestampLabel) {
        cell.timestampLabel.textColor = [UIColor lightGrayColor];
        cell.timestampLabel.shadowOffset = CGSizeZero;
    }
    
    if(cell.subtitleLabel) {
        cell.subtitleLabel.textColor = [UIColor lightGrayColor];
    }
}

#pragma mark - Messages view data source: REQUIRED

- (NSString *)textForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GTRMessage *message = [self.messages objectAtIndex:indexPath.row];
    
    return message.body;
}

-(GTRMessageSendingStatus)sendingMessageStatusForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GTRMessage *message = [self.messages objectAtIndex:indexPath.row];
    
    return message.sendingStatus;
}

- (NSDate *)timestampForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GTRMessage *message = [self.messages objectAtIndex:indexPath.row];
    NSTimeInterval interval = [message.createdAt doubleValue];

    return [NSDate dateWithTimeIntervalSince1970:interval];
}

- (UIImageView *)avatarImageViewForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GTRMessage *message = [self.messages objectAtIndex:indexPath.row];
    
    NSString *imageURL = message.user.avatarURL;
    
    UIImageView *avatar = [[UIImageView alloc] init];
    avatar.contentMode = UIViewContentModeScaleAspectFill;
    
    avatar.layer.masksToBounds = YES;
    avatar.layer.cornerRadius = kJSAvatarImageSize/2;
    
    [avatar setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:DEFAULT_AVATAR];
    
    return avatar;
}

- (NSString *)subtitleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO : assign subtitles from GTRMessage' property
    //return [self.subtitles objectAtIndex:indexPath.row];
    return nil;
}

#pragma mark - AFNetworking requests

-(void)conversationRequest
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager GET:MESSAGE_CONVERSATIONS_ENDPOINT(self.conversationID) parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        self.metadata = json[@"meta_data"];
        
        NSString *presenceWord = @"presence-";
        NSString *channelName = self.metadata[@"channel_name"];
        NSString *eventName = self.metadata[@"event_name"];
        
        if ([channelName rangeOfString:presenceWord].location != NSNotFound) {
            channelName = [channelName stringByReplacingOccurrencesOfString:presenceWord withString:@""];
        }
        
        self.presenceChannel = [self.pusherClient subscribeToPresenceChannelNamed:channelName delegate:self];
        [self.presenceChannel bindToEventNamed:eventName target:self action:@selector(pusherPresenceChannelEventHandler:)];
        
        NSArray *results = json[@"data"];
        NSValueTransformer *transformer = [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:GTRMessage.class];
        NSArray *messages = [transformer transformedValue:results];

        [self.messages addObjectsFromArray:messages];
        DDLogInfo(@"%@", self.messages);

        [self finishSend];
        [self scrollToBottomAnimated:YES];
        
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"request failed");
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
    }];
}

-(void)initiateDirectMessageRequest
{
    // Designed to handle direct message request without any conversationID
    
    NSDictionary *parameters = @{ @"receiver_id" : self.receiverID };
    
    NSURL *baseURL = [NSURL URLWithString:BASE_URL];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
    AFOAuthCredential *credential = [AFOAuthCredential retrieveCredentialWithIdentifier:baseURL.host];
    
    // Set request serializer manually to prevent request error
    // Do not set the same thing on AFManagerHelper because it will cause problems on Survey request
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", credential.accessToken] forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:INITIATE_DIRECT_MESSAGE_ENDPOINT parameters:parameters success:^(AFHTTPRequestOperation *operation, id json) {

        NSString *channel = json[@"channel_name"];
        NSString *eventName = json[@"event_name"];
        
        NSString *presenceWord = @"presence-";
        NSString *channelName = [channel copy];
        if ([channelName rangeOfString:presenceWord].location != NSNotFound) {
            channelName = [channelName stringByReplacingOccurrencesOfString:presenceWord withString:@""];
        }
        
        self.presenceChannel = [self.pusherClient subscribeToPresenceChannelNamed:channelName delegate:self];
        [self.presenceChannel bindToEventNamed:eventName target:self action:@selector(pusherPresenceChannelEventHandler:)];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"request failed");
    }];
}

-(void)requestMessagesWithID:(NSNumber *)maxID
{
    DDLogInfo(@"%@", maxID);
    
    if ([maxID isEqual:[NSNull null]]) {
        [self.refreshControl endRefreshing];
        return;
    }
    
    if (!self.conversationID) return;
    
    NSDictionary *parameters = nil;
    if (maxID) parameters = @{@"max_id": maxID};
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager GET:MESSAGE_CONVERSATIONS_ENDPOINT(self.conversationID) parameters:parameters success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSArray *results = json[@"data"];
        self.metadata = json[@"meta_data"];
        
        DDLogInfo(@"%@", results);
        if (results.count) {
            
            NSValueTransformer *transformer = [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:GTRMessage.class];
            NSArray *messages = [transformer transformedValue:results];
            
            [self prependConversationWithMessages:messages];
        }

        [self.refreshControl endRefreshing];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"request failed");
        [self.refreshControl endRefreshing];
    }];
}

-(void)prependConversationWithMessages:(NSArray *)earlierMessages
{
    NSIndexSet *indexes = [earlierMessages indexesOfObjectsPassingTest:^ BOOL (id obj, NSUInteger idx, BOOL *stop) {
        return [earlierMessages containsObject:obj];
    }];
    
    [self.messages insertObjects:earlierMessages atIndexes:indexes];
    
    [self.tableView reloadData];
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:earlierMessages.count inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];

}

#pragma mark - PTPusher event handler

-(void)pusherPresenceChannelEventHandler:(PTPusherEvent *)event
{
    DDLogInfo(@"received event hash: %@", event);
    
    GTRMessage *newMessage = [MTLJSONAdapter modelOfClass:GTRMessage.class fromJSONDictionary:(NSDictionary *)event.data error:nil];
    DDLogInfo(@"%@", newMessage);
        
    if (![newMessage.senderID isEqualToNumber:self.currentUser.userID]) {
        [self addTextMessage:newMessage];
    }
}

#pragma mark - Send message

-(void)sendMessage:(GTRMessage *)message
{
    DDLogInfo(@"Adding a message: %@", message);
    [self.messages addObject:message];
    [self finishSend];
    [self scrollToBottomAnimated:YES];
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    NSDictionary *parameters = [MTLJSONAdapter JSONDictionaryFromModel:message];
    
    [manager POST:SEND_DIRECT_MESSAGE_ENDPOINT parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"request success: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"request failed: %@", [error localizedDescription]);
        //NSIndexPath *failedMessageIndexPath = [self.messages indexOfObject:message];
        GTRMessage *failedMessage = [self.messages objectAtIndex:[self.messages indexOfObject:message]];
        failedMessage.sendingStatus = GTRMessageSendingStatusFailed;
        [self.messages replaceObjectAtIndex:[self.messages indexOfObject:message] withObject:failedMessage];
        [self finishSend];
        [self scrollToBottomAnimated:YES];
        
    }];
}

-(void)addTextMessage:(GTRMessage *)message
{
    DDLogInfo(@"Adding a message: %@", message);
    [self.messages addObject:message];
    [self finishSend];
    [self scrollToBottomAnimated:YES];
}

-(void)removeTextMessage:(GTRMessage *)message
{
    DDLogInfo(@"Remove a message: %@", message);
    [self.messages removeObject:message];
    [self finishSend];
    [self scrollToBottomAnimated:YES];
}

-(void)dismissKeyboard:(id)sender {
    [self.messageInputView.textView resignFirstResponder];
}

@end
