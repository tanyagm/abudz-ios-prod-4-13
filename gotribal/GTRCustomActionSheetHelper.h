//
//  GTRCustomActionSheetHelper.h
//  gotribal
//
//  Created by loaner on 12/24/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRCustomActionSheet.h"

@class GTRCustomActionSheetHelper;

@protocol GTRCustomActionSheetHelperDelegate
@optional
-(void)deleteAction;
-(void)reportForSpamAction;
@end

@interface GTRCustomActionSheetHelper : NSObject <GTRCustomActionSheetDelegate>

@property (weak) id <GTRCustomActionSheetHelperDelegate> delegate;

-(id)initWithSuperViewController:(UIViewController*)theViewController;
-(void)openCustomActionSheetWithMode:(GTRCustomActionSheetMode)theMode;

@end

