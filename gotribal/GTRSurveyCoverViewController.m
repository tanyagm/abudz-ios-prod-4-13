//
//  GTRSurveyCoverViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/20/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSurveyCoverViewController.h"
#import "GTRSurveyPageGenerator.h"
#import "SVProgressHUD.h"
#import "GTRAnalyticsHelper.h"

@interface GTRSurveyCoverViewController ()

@property (strong, nonatomic) NSArray *surveyQuestionPages;
@property (strong, nonatomic) GTRSurveyPageGenerator *surveyPageGenerator;

@end

@implementation GTRSurveyCoverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.surveyPageGenerator = [[GTRSurveyPageGenerator alloc] init];
        self.surveyQuestionPages = [self.surveyPageGenerator getSurveyPagesWithDestination:nil];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [super.view setBackgroundColor:[UIColor blackColor]];
    
    //add imageView
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,
                                                                          self.view.bounds.size.width, self.view.bounds.size.height)];
    imageView.contentMode = UIViewContentModeTopLeft;
    
    imageView.image = [UIImage imageNamed:@"img-survey-0-iphone5"];

    
    [self.view addSubview:imageView];
    
    //add skip button
    UIImage *skipButtonImage = [UIImage imageNamed:@"btn-tour-skip"];
    UIButton *skipButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    CGRect skipButtonFrame = CGRectZero;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        skipButtonFrame = CGRectMake(self.view.bounds.size.width - 42, 40, skipButtonImage.size.width + 15, skipButtonImage.size.height + 25);
    } else {
        skipButtonFrame = CGRectMake(self.view.bounds.size.width - 42, 20, skipButtonImage.size.width + 15, skipButtonImage.size.height + 25);
    }
    
    skipButton.frame = skipButtonFrame;
    [skipButton setImage: [UIImage imageNamed:@"btn-tour-skip"] forState:UIControlStateNormal];
    [skipButton addTarget:self action:@selector(skipButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:skipButton];
    
    
    //add take survey button
    UIButton *takeSurveyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    CGFloat takeSurveyButtonHeight = 50.0f;
    CGFloat takeSurveyButtonWidth = 219.0f;
    CGFloat takeSurveyButtonBottomPadding = 10.0f;
    
    
    CGRect takeSurveyButtonFrame = CGRectMake((self.view.bounds.size.width-takeSurveyButtonWidth)/2, self.view.bounds.size.height-takeSurveyButtonHeight-takeSurveyButtonBottomPadding, takeSurveyButtonWidth, takeSurveyButtonHeight);
    takeSurveyButton.frame = takeSurveyButtonFrame;

    NSAttributedString *takeSurveyButtonText  = [[NSAttributedString alloc]
                                                 initWithString: @"TAKE IT NOW"
                                                 attributes: @{
                                                               NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:19],
                                                               NSForegroundColorAttributeName: [UIColor whiteColor]
                                                               }];
    
    [takeSurveyButton setAttributedTitle:takeSurveyButtonText forState:UIControlStateNormal];
    [[takeSurveyButton layer] setBorderWidth:1.0f];
    [[takeSurveyButton layer] setBorderColor:[UIColor whiteColor].CGColor];
    [takeSurveyButton addTarget:self action:@selector(takeSurveyButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:takeSurveyButton];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button actions

- (void)skipButtonAction:(id)sender
{
    DDLogInfo(@"Skip button clicked!");
    
    //sent the data as event
    #ifdef GTR_FLURRY_ANALYTICS
    [GTRAnalyticsHelper sendFitnessSurveySkippedEvent];
    #endif
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [self performSelector:@selector(dismiss) withObject:nil afterDelay:1.0];
}

- (void)takeSurveyButtonAction:(id)sender
{
    DDLogInfo(@"Take survey button clicked!");
    
    [self.navigationController pushViewController:self.surveyQuestionPages.firstObject animated:YES];
    
    
}

#pragma mark - Custom methods

-(void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:^{
        [SVProgressHUD dismiss];
    }];
}

@end
