//
//  GTRSuggestedActiveBudzCell.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActiveBudzCell.h"
#import "GTRPursuitHelper.h"

@interface GTRActiveBudzCell()

@property CGFloat cellHeight;
@property BOOL isSuggestedMode;
@property (strong,nonatomic) UIView *customAccessoryView;


@end


@implementation GTRActiveBudzCell

- (id)initWithHeight:(CGFloat)height
     isSuggestedMode:(BOOL)theSuggestedMode
               style:(UITableViewCellStyle)style
     reuseIdentifier:(NSString *)reuseIdentifier;
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.cellHeight = height;
        self.isSuggestedMode = theSuggestedMode;
        [self setAppearance];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - Set appearance methods

- (void)setAppearance
{
    //set cell's image view
    CGFloat imageViewHeight = 50;
    CGFloat imageViewWidth = 50;
    CGFloat imageViewX = 5;
    CGFloat imageViewY = (self.cellHeight - imageViewHeight)/2;
    self.customImageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageViewX, imageViewY, imageViewWidth, imageViewHeight)];
    self.customImageView.layer.cornerRadius = imageViewWidth/2;
    self.customImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.customImageView.layer.masksToBounds = YES;
    self.customImageView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.customImageView];
    
    //set cell's custom accessory view
    
    CGFloat customAccessoryViewHeight = self.cellHeight;
    CGFloat customAccessoryViewWidth = 120;
    CGFloat customAccessoryViewX = self.frame.size.width-customAccessoryViewWidth;
    CGFloat customAccessoryViewY = 0;
    
    self.customAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(customAccessoryViewX, customAccessoryViewY, customAccessoryViewWidth, customAccessoryViewHeight)];
    self.customAccessoryView.backgroundColor = [UIColor clearColor];
    self.accessoryView = self.customAccessoryView;
    self.editingAccessoryView = self.customAccessoryView;

    if (self.isSuggestedMode) {
        [self setExtraButtons];
    }
    
    //set cell's label
    CGFloat textLabelLeftPadding = 5;
    CGFloat textLabelX = imageViewX+imageViewWidth+textLabelLeftPadding;
    CGFloat textLabelY = 0;
    CGFloat textLabelWidth = self.customAccessoryView.frame.origin.x-textLabelX;
    CGFloat textLabelHeight = self.cellHeight;
    
    self.customLabel = [[UILabel alloc] initWithFrame:CGRectMake(textLabelX, textLabelY, textLabelWidth, textLabelHeight)];
    self.customLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.customLabel];
}


- (void)setExtraButtons
{
    //add accept button
    CGFloat acceptButtonHeight = self.cellHeight;
    CGFloat acceptButtonWidth = 60;
    
    CGFloat acceptButtonX = 0;
    CGFloat acceptButtonY = 0;
    UIImage *acceptButtonImage = [UIImage imageNamed:@"btn-check"];
    
    self.acceptButton = [[GTRCustomButton alloc] initWithFrame:
                         CGRectMake(acceptButtonX, acceptButtonY, acceptButtonHeight, acceptButtonWidth)];
    [self.acceptButton setImage:acceptButtonImage forState:UIControlStateNormal];
    self.acceptButton.backgroundColor = [UIColor clearColor];
    
    //add reject button
    CGFloat rejectButtonHeight = self.cellHeight;
    CGFloat rejectButtonWidth = 60;
    
    CGFloat rejectButtonX = acceptButtonX+acceptButtonWidth;
    CGFloat rejectButtonY = 0;
    
    UIImage *rejectButtonImage = [UIImage imageNamed:@"btn-x"];
    
    self.rejectButton = [[GTRCustomButton alloc] initWithFrame:
                         CGRectMake(rejectButtonX, rejectButtonY, rejectButtonWidth, rejectButtonHeight)];
    [self.rejectButton setImage:rejectButtonImage forState:UIControlStateNormal];
    self.rejectButton.backgroundColor = [UIColor clearColor];
    
    //add it to the custom accessory view.
    [self.customAccessoryView addSubview:self.rejectButton];
    [self.customAccessoryView addSubview:self.acceptButton];

}

- (void)setPursuitIcons
{
    
    [self refreshCustomAccessoryView];
    
    if (self.pursuitArray && self.pursuitArray.count > 0) {
        
        CGFloat pursuitImageWidth = 40;
        CGFloat pursuitImageHeight = self.cellHeight;
        CGFloat pursuitImageXOffset = 40;
        CGFloat pursuitImageY = 0;
        
        for (NSString* pursuitName in self.pursuitArray ) {
            UIImage *pursuitImage;
            
            if (![GTRPursuitHelper isCustomPursuit:pursuitName]) {
                pursuitImage = [UIImage imageNamed:[GTRPursuitHelper getPursuitIconName:pursuitName]];
            } else {
                pursuitImage = [UIImage imageNamed:PURSUIT_OTHER_ICON_NAME];
            }
            
            UIImageView *pursuitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(pursuitImageXOffset, pursuitImageY, pursuitImageWidth, pursuitImageHeight)];
            pursuitImageView.image = pursuitImage;
            pursuitImageView.contentMode = UIViewContentModeCenter;
            pursuitImageView.backgroundColor = [UIColor clearColor];
            [self.customAccessoryView addSubview:pursuitImageView];
            
            pursuitImageXOffset += pursuitImageWidth;
        }
    }
}

- (void)refreshCustomAccessoryView
{
    for (UIView *subView in [self.customAccessoryView subviews]) {
        [subView removeFromSuperview];
    }
}

@end
