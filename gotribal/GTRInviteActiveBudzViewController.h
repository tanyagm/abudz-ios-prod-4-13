//
//  GTRInviteActiveBudzViewController.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/20/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRViewController.h"
#import "GTRTribe.h"

@interface GTRInviteActiveBudzViewController : GTRTableViewController

-(id)initWithTribe:(GTRTribe*)theTribe;
@end
