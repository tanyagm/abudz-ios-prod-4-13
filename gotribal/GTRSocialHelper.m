//
//  GTRSocialHelper.m
//  gotribal
//
//  Created by Muhammad Taufik on 12/25/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSocialHelper.h"
#import "TWAPIManager.h"

@import Social;
@import Accounts;

@interface GTRSocialHelper ()
@property (nonatomic, strong) ACAccountStore *accountStore;
@property (nonatomic, strong) ACAccount *facebookAccount;
@property (nonatomic, strong) ACAccount *twitterAccount;
@property (nonatomic, strong) TWAPIManager *apiManager;
@end

@implementation GTRSocialHelper

+(instancetype)sharedHelper
{
    static dispatch_once_t once;
    static id sharedHelper;
    dispatch_once(&once, ^{
        sharedHelper = [[self alloc] init];
    });
    return sharedHelper;
}

-(instancetype)init
{
    if (self = [super init]) {
        self.accountStore = [[ACAccountStore alloc] init];
        self.apiManager = [[TWAPIManager alloc] init];
    }
    return self;
}

#pragma mark - Twitter

-(void)retrieveTwitterAccountWithSuccess:(GTRSuccessBlock)success failure:(GTRFailureWithResponseObjectBlock)failure
{
    ACAccountType *twitterAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [self.accountStore requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
         dispatch_async(dispatch_get_main_queue(), ^{
             if (granted) {
                 NSArray *twitterAccounts = [self.accountStore accountsWithAccountType:twitterAccountType];
                 if (twitterAccounts.count) {
                     self.twitterAccount = twitterAccounts.firstObject;
                     [self performReverseAuthWithTwitterAccount:self.twitterAccount
                                                        success:^(NSString *oauthToken, NSString *oauthTokenSecret) {
                                                            [self sendOAuthToken:oauthToken withOAuthTokenSecret:oauthTokenSecret success:^{
                                                                success();
                                                            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                failure(operation.responseObject, error);
                                                            }];
                                                        }
                                                          failure:^(NSError *error) {
                                                              failure(nil, error);
                                                          }];
                 } else {
                     failure(nil, error);
                 }
             } else {
                 failure(nil, error);
             }
         });
     }];
}

- (void)performReverseAuthWithTwitterAccount:(ACAccount *)twitterAccount success:(GTRSuccessWithOAuthTokenBlock)success failure:(GTRFailureWithErrorBlock)failure
{
    [self.apiManager performReverseAuthForAccount:twitterAccount withHandler:^(NSData *responseData, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (responseData) {
                NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
                NSArray *urlComponents = [responseString componentsSeparatedByString:@"&"];
                
                for (NSString *keyValuePair in urlComponents)
                {
                    NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
                    NSString *key = pairComponents.firstObject;
                    NSString *value = pairComponents.lastObject;
                    
                    [parameters setObject:value forKey:key];
                }
                
                NSString *oauthToken = parameters[@"oauth_token"];
                NSString *oauthTokenSecret = parameters[@"oauth_token_secret"];
                
                success(oauthToken, oauthTokenSecret);
            }
            else if (error) {
                DDLogInfo(@"%@", [error localizedDescription]);
                failure(error);
            }
        });
    }];
}

-(void)sendOAuthToken:(NSString *)oauthToken withOAuthTokenSecret:(NSString *)oauthTokenSecret success:(GTRSuccessBlock)success failure:(GTRFailureWithOperationBlock)failure
{
    NSDictionary *parameters = @{
                                 @"social_type": @"twitter",
                                 @"token": oauthToken,
                                 @"secret": oauthTokenSecret
                                 };
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager POST:@"/api/v1/socials" parameters:parameters success:^(AFHTTPRequestOperation *operation, id json) {
        success();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation, error);
    }];
}

-(void)retrieveFollowersListWithCursor:(NSNumber *)cursor success:(GTRSuccessWithUsersBlock)success failure:(GTRFailureWithErrorDictionaryBlock)failure {
    
    if ([cursor isEqual:@(0)]) {
        NSDictionary *error = nil;
        failure(error);
        return;
    }
    
    NSString *URLString = [NSString stringWithFormat:@"https://api.twitter.com/1.1/followers/list.json?count=200&cursor=%@", cursor];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:URL parameters:nil];
    request.account = self.twitterAccount;
    
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
            if (json[@"errors"]) {
                DDLogInfo(@"%@", json[@"errors"]);
                NSArray *errors = json[@"errors"];
                NSDictionary *error = errors.firstObject;
                failure(error);
            } else {
                NSNumber *nextCursor = json[@"next_cursor"];
                NSArray *users = json[@"users"];
                success(nextCursor, users);
            }
        });
    }];
}

-(void)postToTwitterWithMessage:(NSString *)message image:(UIImage *)postImage success:(GTRSuccessBlock)success failure:(GTRFailureWithErrorBlock)failure
{
    NSURL *postURL = [[NSURL alloc] initWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
    NSURL *postImageURL = [[NSURL alloc] initWithString:@"https://api.twitter.com/1.1/statuses/update_with_media.json"];
    
    NSDictionary *tweetDetails = @{ @"status": message };
    
    SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                requestMethod:SLRequestMethodPOST
                                                          URL:(postImage? postImageURL : postURL)
                                                   parameters:tweetDetails];
    
    if (postImage) {
        NSData *data = UIImageJPEGRepresentation(postImage, 1.0f);
        
        [postRequest addMultipartData:data
                             withName:@"media[]"
                                 type:@"image/jpeg"
                             filename:@"image.jpg"];
    }
    
    [postRequest setAccount:self.twitterAccount];
    
    [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([urlResponse statusCode] == 200) {
                success();
            }
            else {
                failure(error);
            }
        });
    }];
}

-(void)deleteTwitterAccountWithSuccess:(GTRSuccessBlock)success failure:(GTRFailureWithOperationBlock)failure {
    NSDictionary *parameters = @{
                                 @"social_type": @"twitter"
                                 };
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager DELETE:@"/api/v1/socials" parameters:parameters success:^(AFHTTPRequestOperation *operation, id json) {
        success();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation, error);
    }];
}

#pragma mark - Facebook

-(void)retrieveFacebookAccountWithPermissions:(NSArray *)permissions success:(GTRSuccessWithAccessTokenBlock)success failure:(GTRFailureWithErrorBlock)failure;
{
    ACAccountType *facebookAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    NSDictionary *options = @{
                              ACFacebookAppIdKey : [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"],
                              ACFacebookPermissionsKey : permissions
                              };
    
    [self.accountStore requestAccessToAccountsWithType:facebookAccountType
                                               options:options
                                            completion:^(BOOL granted, NSError *error) {
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    if (granted) {
                                                        NSArray *accounts = [self.accountStore accountsWithAccountType:facebookAccountType];
                                                        self.facebookAccount = accounts.firstObject;
                                                        
                                                        ACAccountCredential *credential = self.facebookAccount.credential;
                                                        NSString *accessToken = credential.oauthToken;
                                                        success(accessToken);
                                                    } else {
                                                        failure(error);
                                                    }
                                                });
                                            }];
}

-(void)requestReadWriteAccessToFacebookAccountWithSuccess:(GTRSuccessBlock)success failure:(GTRFailureWithErrorBlock)failure
{
    NSArray *permissions = @[@"read_stream", @"publish_stream"];
    NSDictionary *facebookOptions = @{
                                      ACFacebookAppIdKey : [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"],
                                      ACFacebookAudienceKey : ACFacebookAudienceFriends,
                                      ACFacebookPermissionsKey : permissions
                                      };
    
    ACAccountType *facebookAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    [self.accountStore requestAccessToAccountsWithType:facebookAccountType options:facebookOptions completion:^(BOOL granted, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             if (granted) {
                 NSArray *facebookAccounts = [self.accountStore accountsWithAccountType:facebookAccountType];
                 self.facebookAccount = facebookAccounts.firstObject;
                 success();
             } else {
                 failure(error);
             }
         });
     }];
}

-(void)postToFacebookWithMessage:(NSString *)message image:(UIImage *)postImage success:(GTRSuccessBlock)success failure:(GTRFailureWithErrorBlock)failure
{
    NSURL *postURL = [NSURL URLWithString:@"https://graph.facebook.com/me/feed"];
    NSURL *postImageURL = [NSURL URLWithString:@"https://graph.facebook.com/me/photos"];
    
    NSString *link = @"http://www.gotribal.com/";
    
    NSDictionary *parameters = @{
                                 @"link": link,
                                 @"message" : message
                                 };
    
    SLRequest *facebookRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                    requestMethod:SLRequestMethodPOST
                                                              URL:(postImage? postImageURL : postURL)
                                                       parameters:parameters];
    
    if (postImage) {
        NSData *data = UIImageJPEGRepresentation(postImage, 1.0f);
        [facebookRequest addMultipartData:data withName:@"picture" type:@"image/jpeg" filename:nil];
    }
    
    [facebookRequest setAccount:self.facebookAccount];
    
    [facebookRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            DDLogInfo(@"Status Code: %li", (long)[urlResponse statusCode]);
            DDLogInfo(@"Response Data: %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
            
            if ([urlResponse statusCode] == 200) {
                success();
            } else {
                failure(error);
            }
        });
    }];
}

-(void)retrieveFacebookAccountWithSuccess:(GTRSuccessBlock)success failure:(GTRFailureWithErrorBlock)failure
{
    ACAccountType *facebookAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    NSDictionary *options = @{
                              ACFacebookAppIdKey : [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"],
                              ACFacebookPermissionsKey : @[@"basic_info"]
                              };
    
    [self.accountStore requestAccessToAccountsWithType:facebookAccountType
                                               options:options
                                            completion:^(BOOL granted, NSError *error) {
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    if (granted) {
                                                        NSArray *accounts = [self.accountStore accountsWithAccountType:facebookAccountType];
                                                        self.facebookAccount = accounts.firstObject;
                                                        
                                                        ACAccountCredential *credential = self.facebookAccount.credential;
                                                        NSString *accessToken = credential.oauthToken;
                                                        [self sendFacebookToken:accessToken success:^{
                                                            success();
                                                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                            failure(error);
                                                        }];
                                                    } else {
                                                        failure(error);
                                                    }
                                                });
                                            }];
}

-(void)sendFacebookToken:(NSString *)accessToken success:(GTRSuccessBlock)success failure:(GTRFailureWithOperationBlock)failure
{
    NSDictionary *parameters = @{ @"social_type": @"facebook", @"token":accessToken };
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager POST:@"/api/v1/socials" parameters:parameters success:^(AFHTTPRequestOperation *operation, id json) {
        success();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation, error);
    }];
}

-(void)deleteFacebookAccountWithSuccess:(GTRSuccessBlock)success failure:(GTRFailureWithOperationBlock)failure {
    NSDictionary *parameters = @{
                                 @"social_type": @"facebook"
                                 };
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager DELETE:@"/api/v1/socials" parameters:parameters success:^(AFHTTPRequestOperation *operation, id json) {
        success();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation, error);
    }];
}

@end
