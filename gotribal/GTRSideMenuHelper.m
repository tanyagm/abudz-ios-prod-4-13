//
//  GTRSideMenuHelper.m
//  gotribal
//
//  Created by Hisma Mulya S on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSideMenuHelper.h"
#import "GTRSideMenuCell.h"
#import <QuartzCore/QuartzCore.h>
#import <UIImageView+AFNetworking.h>

NSString* const NOTIFICATION_NAME = @"ico-menu-alert";


@implementation GTRSideMenuHelper

#pragma mark - Set Image in Cell

+(void)setAvatarImage:(UIImage*)avatarImage
                 text:(NSString*)string
               inCell:(GTRSideMenuCell*)cell
{
    cell.avatarImageView.image = avatarImage;
    cell.avatarImageView.frame = CGRectMake(cell.avatarImageView.frame.origin.x, cell.avatarImageView.frame.origin.y, avatarImage.size.width > 30 ? 30:avatarImage.size.width, avatarImage.size.height > 30 ? 30: avatarImage.size.height);
    cell.text.text = string;
    
    //assumption: this method is not use to set user's avatar cell
    cell.avatarImageView.layer.masksToBounds = NO;
    cell.avatarImageView.layer.borderColor = [UIColor clearColor].CGColor;
    
}

+(void)setAvatarImageURL:(NSURL*)url
         withPlaceholder:(UIImage*)placeHolderImage
                    text:(NSString*)string
                  inCell:(GTRSideMenuCell*)cell
{
    cell.avatarImageView.frame = CGRectMake(cell.avatarImageView.frame.origin.x,
                                            cell.avatarImageView.frame.origin.y, 30, 30);
    cell.avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    cell.avatarImageView.layer.cornerRadius = 15;
    cell.avatarImageView.layer.masksToBounds = YES;
    cell.avatarImageView.layer.borderWidth = 0.1;
    cell.avatarImageView.layer.borderColor = AVATAR_BORDER_COLOR.CGColor;
    
    [cell.avatarImageView setImageWithURL:url
                         placeholderImage:placeHolderImage];
    
    
    cell.text.text = string;
}

+(UIView*)createHeaderViewWithText:(NSString*)text
                    withImageCache:(NSMutableDictionary*)sideBarImageCache
{
    UIView *headerView = [[UIView alloc] init];
    headerView.frame = CGRectMake(0, 0, 320, 45);
    headerView.backgroundColor = GTRSIDE_MENU_COLOR;
    
    UILabel *textLabel = [[UILabel alloc]initWithFrame:CGRectOffset(headerView.frame, LEFT_PADDING, 0)];
    textLabel.backgroundColor = GTRSIDE_MENU_COLOR;
    textLabel.text = text;
    textLabel.font = HEADER_FONT;
    textLabel.textColor = [UIColor whiteColor];
    textLabel.contentMode = UIViewContentModeCenter;
    [headerView addSubview:textLabel];
    
    UIImageView *lineImageView = [[UIImageView alloc]initWithImage:sideBarImageCache[@"LINE_IMAGE_KEY"]];
    lineImageView.backgroundColor = GTRSIDE_MENU_COLOR;
    lineImageView.frame = CGRectMake(LEFT_PADDING, headerView.frame.size.height - 1, 220, 1);
    [headerView addSubview:lineImageView];
    
    return headerView;
}

+(void)setNotificationBadgeWithNumber:(NSInteger)number inCell:(GTRSideMenuCell*)cell
{
    if (!cell.rightImageView.image) {
        cell.rightImageView.image = [UIImage imageNamed:NOTIFICATION_NAME];
        cell.rightImageView.backgroundColor = GTRSIDE_MENU_COLOR;
    }
    
    cell.rightImageView.hidden = NO;
    
    UILabel *label = (UILabel*)[cell.rightImageView viewWithTag:NOTIFICATION_LABEL_TAG];
    if (label) {
        label.text = [NSString stringWithFormat:@"%d", number];
    }
}

+(void)hideNotificationBadgeInCell:(GTRSideMenuCell*)cell
{
    if (cell.rightImageView) {
        cell.rightImageView.hidden = YES;
    }
}

+(GTRAppDelegate*)appDelegate
{
    return (GTRAppDelegate*)[UIApplication sharedApplication].delegate;
}

@end
