//
//  GTRActiveBudzHelper.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/3/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRUser.h"
#import "GTRActiveBudzProfileViewController.h"

@interface GTRActiveBudzHelper : NSObject

+(NSArray*)getArchivedSuggestedActiveBudz;
+(void)archiveSuggestedActiveBudz:(NSArray*)theActiveBudzList;
+(NSArray*)getArchivedMyActiveBudz;
+(void)archiveMyActiveBudz:(NSArray*)theActiveBudzList;
+(void)pushActiveBudzProfile:(GTRUser*)theActiveBudz
                   indexPath:(NSIndexPath*)theIndexPath
          rootViewController:(UIViewController*)aViewController
                   onSuccess:(void (^)(id responseObject))success
                      onFail:(void (^)(id responseObject, NSError *error))errorHandler;

@end
