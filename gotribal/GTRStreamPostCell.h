//
//  GTRPostCell.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRPost.h"

@interface GTRStreamPostCell : UITableViewCell

@property (nonatomic, readonly, strong) UIButton *likeButton;
@property (nonatomic, readonly, strong) UIButton *commentButton;
@property (nonatomic, readonly, strong) UIButton *moreButton;

-(void)setContent:(GTRPost *)post;
- (CGFloat)getHeight;

@end
