//
//  GTRPostCommentViewController.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/12/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRCommentViewController.h"
#import "GTRAppDelegate.h"
#import "SVPullToRefresh.h"
#import "SVProgressHUD.h"
#import "GTRStreamPostCell.h"
#import "GTRNavigationBarHelper.h"
#import "GTRCommentRestService.h"
#import "NSMutableArray+AddMantleObject.h"
#import "GTRCommentCell.h"
#import "JSDismissiveTextView.h"
#import "JSMessageInputView.h"
#import "NSString+JSMessagesView.h"
#import "GTRLoadMoreButton.h"
#import "GTRLikeHelper.h"
#import "GTRCustomActionSheetHelper.h"
#import "GTRPostRestService.h"
#import "GTRRestServiceErrorHandler.h"

typedef enum {
    GTRPostCommentSectionPost = 0,
    GTRPostCommentSectionLoadMoreButton = 1,
    GTRPostCommentSectionComments = 2
} GTRPostCommentSection;

@interface GTRCommentViewController ()  <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, JSDismissiveTextViewDelegate, GTRCustomActionSheetHelperDelegate>

@property (strong, nonatomic) UITableView *myTableView;

@property (strong, nonatomic) GTRPost *post;
@property (strong, nonatomic) GTRStreamPostCell *postCell;

@property (strong, nonatomic) GTRCommentCell *commentCellHeightCalculator;
@property (strong, nonatomic) NSMutableArray *comments;
@property (strong, nonatomic) NSNumber *lastItemID;
@property (strong, nonatomic) GTRCustomActionSheetHelper *customActionSheet;

@property (strong, nonatomic) GTRLoadMoreButton *loadMoreButton;
@property BOOL loadMoreEnabled;

@property (strong, nonatomic, readonly) JSMessageInputView *messageInputView;
@property (assign, nonatomic) BOOL isUserScrolling;
@property (assign, nonatomic, readonly) UIEdgeInsets originalTableViewContentInset;
@property (assign, nonatomic) CGFloat previousTextViewContentHeight;
@property NSInteger rowID;
@end

@implementation GTRCommentViewController
@synthesize post, comments, postCell, myTableView, lastItemID, loadMoreEnabled, loadMoreButton, commentCellHeightCalculator, messageInputView, customActionSheet;

- (id)initWithPost:(GTRPost *)inPost rowID:(NSInteger)aRowID
{
    self = [super init];
    if(self) {
        self.title = NSLocalizedString(@"Comment", @"Comment");
        post = inPost;
        comments = [[NSMutableArray alloc] init];
        commentCellHeightCalculator = [[GTRCommentCell alloc] init];
        self.rowID = aRowID;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setAppearance];
    [self reload:GTRReloadModeRefresh];
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(dismissKeyboard:)];
    
    [self.view addGestureRecognizer:tapGesture];
}

#pragma mark Appearance
- (void)setAppearance
{
    [self initNavbarButton];
    [self initLoadMoreButton];
    [self initPostView];
    [self initTableView];
    [self initiateActionSheet];
}

- (void)initNavbarButton
{
    [GTRNavigationBarHelper setBackButtonItem:self.navigationItem
                                   withTarget:self
                                     selector:@selector(backButtonAction:)];
}

-(void)initiateActionSheet
{
    customActionSheet = [[GTRCustomActionSheetHelper alloc]initWithSuperViewController:self];
    customActionSheet.delegate =self;
}

- (void)initLoadMoreButton
{
    loadMoreButton = [[GTRLoadMoreButton alloc] initWithFrame:CGRectMake(0,0,320,40)];
    [loadMoreButton addTarget:self action:@selector(loadMoreButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    loadMoreEnabled = TRUE;
}

-(void)initPostView
{
    postCell = [[GTRStreamPostCell alloc] init];
    [postCell.likeButton addTarget:self action:@selector(likeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [postCell.moreButton addTarget:self action:@selector(moreButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [postCell setContent:post];
}


- (void)initTableView
{
    CGSize size = self.view.frame.size;
    CGFloat inputViewHeight = 45;
    CGRect tableFrame = CGRectMake(0.0f, 0.0f, size.width, size.height - inputViewHeight);
    
    myTableView = [[UITableView alloc] initWithFrame:tableFrame style:UITableViewStylePlain];
    myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    myTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    myTableView.backgroundColor = UIColorFromRGB(0xf2f2f2);
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    
    CGRect inputFrame = CGRectMake(0.0f, size.height - inputViewHeight, size.width, inputViewHeight);
    messageInputView = [[JSMessageInputView alloc] initWithFrame:inputFrame
                                                           style:JSMessageInputViewStyleFlat
                                                        delegate:self
                                            panGestureRecognizer:myTableView.panGestureRecognizer];
    messageInputView.sendButton.enabled = NO;
    [messageInputView.sendButton addTarget:self
                                    action:@selector(sendButtonAction:)
                          forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:myTableView];
    [self.view addSubview:messageInputView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self scrollToBottomAnimated:NO];
    
    //  FIXME: this is a hack
    //  ---------------------
    //  Possibly an iOS 7 bug?
    //  tableView.contentInset.top = 0.0 on iOS 6
    //  tableView.contentInset.top = 64.0 on iOS 7
    //  save here in order to reset in [ keyboardWillShowHide: ]
    //  ---------------------
    _originalTableViewContentInset = myTableView.contentInset;
    
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(handleWillShowKeyboardNotification:)
												 name:UIKeyboardWillShowNotification
                                               object:nil];
    
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(handleWillHideKeyboardNotification:)
												 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (BOOL)shouldAutorotate
{
    return NO;
}


#pragma mark - Button Actions

- (void)likeButtonAction:(id)sender
{
    __weak UITableView *tableView = self.myTableView;
    
    [GTRLikeHelper toggleLike:post
                    onSuccess:^(GTRPost *updatedPost) {
                        [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:GTRPostCommentSectionPost]]
                                         withRowAnimation: UITableViewRowAnimationNone];
                        if ([self.delegate respondsToSelector:@selector(onPostUpdated:)]){
                            [self.delegate onPostUpdated:self.rowID];
                        }
                    } onFail:nil];
}

- (void)moreButtonAction:(id)sender
{
    [self dismissKeyboard:nil];
    GTRUser *currentUser = [GTRUserHelper getCurrentUser];

    if([currentUser.userID isEqual:post.owner.userID]){
        [customActionSheet openCustomActionSheetWithMode:GTRCustomActionSheetModeOwnedPost];
    } else {
        [customActionSheet openCustomActionSheetWithMode:GTRCustomActionSheetModeOtherPost];
    }
}

- (void)sendButtonAction:(UIButton *)sender
{
    DDLogInfo(@"sendButtonAction");
    
    NSString *commentMessage = messageInputView.textView.text;
    DDLogInfo(@"Comment message: %@", commentMessage);
    
    __block GTRComment *newComment = [[GTRComment alloc] initWithMessage:commentMessage user:[GTRUserHelper getCurrentUser]];
    [comments insertObject:newComment atIndex:0];
    [self finishSend];
    
    [GTRCommentRestService createCommentForPost:post
                                    WithMessage:commentMessage
                                      onSuccess:^(id responseObject) {
                                          newComment = [MTLJSONAdapter modelOfClass:GTRComment.class fromJSONDictionary:[responseObject objectForKey:@"comment"] error:nil];

                                          [comments replaceObjectAtIndex:0 withObject:newComment];
                                          post.commentsCount = [responseObject objectForKey:@"post_comments_count"];
                                        
                                          if ([self.delegate respondsToSelector:@selector(onPostUpdated:)]){
                                              [self.delegate onPostUpdated:self.rowID];
                                          }
                                          [self finishSend];
                                      } onFail:nil];
}

- (void)finishSend
{
    [self.messageInputView.textView setText:nil];
    [self textViewDidChange:self.messageInputView.textView];
    [myTableView reloadData];
    [self scrollToBottomAnimated:YES];
    
}

-(void)backButtonAction:(id)sender
{
    DDLogInfo(@"backButtonAction");
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)loadMoreButtonAction:(id)sender
{
    DDLogInfo(@"loadMoreButtonAction");
    [self reload:GTRReloadModeLoadMore];
}

#pragma mark Reload
-(void)reload:(GTRReloadMode)reloadMode
{
    [loadMoreButton startAnimating];
    
    if (reloadMode == GTRReloadModeRefresh) {
        lastItemID = nil;
    }
    
    [GTRCommentRestService
     retrieveCommentsFromPost:post
     withMaxID:lastItemID
     onSuccess:^(id responseObject) {
         [loadMoreButton stopAnimating];
         
         if(reloadMode == GTRReloadModeRefresh){
             [comments removeAllObjects];
         }
         
         NSArray *commentArray = [responseObject objectForKey:@"data"];
         [comments addObjectsFromArray:commentArray modelOfClass:[GTRComment class]];
        
         if ([commentArray count] < MAX_PER_PAGE){
             lastItemID = nil;
             loadMoreEnabled = FALSE;
         } else {
             lastItemID = [[responseObject objectForKey:@"pagination"] objectForKey:@"max_id"];
         }
         
         [myTableView reloadData];
         
         
         if (reloadMode == GTRReloadModeRefresh){
             [self scrollToBottomAnimated:YES];
         }
         
         DDLogInfo(@"Received total: %d comments,loadMoreEnabled: %hhd", [comments count], loadMoreEnabled);
     } onFail:^(id operation, NSError *error) {
         [loadMoreButton stopAnimating];
     }];
}

#pragma mark Custom action sheet delegates

- (void)deleteAction
{ 
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [GTRPostRestService deletePostWithPostID:post.postID onSuccess:^(id response) {
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Post deleted!", @"Post deleted!")];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
        if ([self.delegate respondsToSelector:@selector(onPostDeleted:)]){
            [self.delegate onPostDeleted:self.rowID];
        }
    } onFail:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}

-(void)reportForSpamAction
{

}

#pragma mark UITableView delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case GTRPostCommentSectionPost:
            return 1;
            break;
        case GTRPostCommentSectionLoadMoreButton:
            return loadMoreEnabled;
        case GTRPostCommentSectionComments:
            return [comments count];
            break;
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case GTRPostCommentSectionPost:
            [postCell setContent:post];
            return postCell;
            break;
        case GTRPostCommentSectionLoadMoreButton:
            return [self getLoadMoreButtonCell:tableView];
            break;
        case GTRPostCommentSectionComments:
            return [self tableView:tableView commentCellAtIndexPath:indexPath];
            break;
        default:
            return nil;
            break;
    }
}

- (UITableViewCell *)getLoadMoreButtonCell:(UITableView *)tableView
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadMoreButtonCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:loadMoreButton.frame];
        cell.backgroundColor = [UIColor clearColor];
        [cell addSubview:loadMoreButton];
    }
    return cell;
}

- (GTRComment*) getCommentForRow:(NSInteger)row
{
    NSInteger maxCommentRowID = [comments count] - 1;
    GTRComment *comment = [comments objectAtIndex:maxCommentRowID - row];
    return comment;
}

- (UITableViewCell *)tableView:(UITableView *)tableView commentCellAtIndexPath:(NSIndexPath *)indexPath {
    
    GTRCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commentCell"];
    
    if (cell == nil) {
        cell = [[GTRCommentCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:@"commentCell"];
    }
    
    GTRComment *comment = [self getCommentForRow:indexPath.row];
    [cell setContent:comment];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case GTRPostCommentSectionPost:
            return [postCell getHeight];
            break;
        case GTRPostCommentSectionLoadMoreButton:
            return [loadMoreButton getHeight];
            break;
        case GTRPostCommentSectionComments:
            return [self calculateCommentHeightAtRow:indexPath.row];
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)calculateCommentHeightAtRow:(NSInteger)row
{
    GTRComment *comment = [self getCommentForRow:row];
    [commentCellHeightCalculator setContent:comment];
    return [commentCellHeightCalculator getHeight];
}

-(void)dismissKeyboard:(id)sender {
    [messageInputView.textView resignFirstResponder];
}

#pragma mark - Code from JSMessageTableview Pod

#pragma mark - Keyboard notifications

- (void)handleWillShowKeyboardNotification:(NSNotification *)notification
{
    [self keyboardWillShowHide:notification];
}

- (void)handleWillHideKeyboardNotification:(NSNotification *)notification
{
    [self keyboardWillShowHide:notification];
}

- (void)keyboardWillShowHide:(NSNotification *)notification
{
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	UIViewAnimationCurve curve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
	double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0.0f
                        options:[self animationOptionsForCurve:curve]
                     animations:^{
                         CGFloat keyboardY = [self.view convertRect:keyboardRect fromView:nil].origin.y;
                         
                         CGRect inputViewFrame = self.messageInputView.frame;
                         CGFloat inputViewFrameY = keyboardY - inputViewFrame.size.height;
                         
                         // for ipad modal form presentations
                         CGFloat messageViewFrameBottom = self.view.frame.size.height - inputViewFrame.size.height;
                         if(inputViewFrameY > messageViewFrameBottom)
                             inputViewFrameY = messageViewFrameBottom;
						 
                         self.messageInputView.frame = CGRectMake(inputViewFrame.origin.x,
																  inputViewFrameY,
																  inputViewFrame.size.width,
																  inputViewFrame.size.height);
                         
                         UIEdgeInsets insets = self.originalTableViewContentInset;
                         insets.bottom = self.view.frame.size.height
                         - self.messageInputView.frame.origin.y
                         - inputViewFrame.size.height;
                         
                         myTableView.contentInset = insets;
                         myTableView.scrollIndicatorInsets = insets;
                     }
                     completion:^(BOOL finished) {
                     }];
}

#pragma mark - Dismissive text view delegate

- (void)keyboardDidScrollToPoint:(CGPoint)point
{
    CGRect inputViewFrame = self.messageInputView.frame;
    CGPoint keyboardOrigin = [self.view convertPoint:point fromView:nil];
    inputViewFrame.origin.y = keyboardOrigin.y - inputViewFrame.size.height;
    self.messageInputView.frame = inputViewFrame;
}

- (void)keyboardWillBeDismissed
{
    CGRect inputViewFrame = self.messageInputView.frame;
    inputViewFrame.origin.y = self.view.bounds.size.height - inputViewFrame.size.height;
    self.messageInputView.frame = inputViewFrame;
}

- (void)keyboardWillSnapBackToPoint:(CGPoint)point
{
    CGRect inputViewFrame = self.messageInputView.frame;
    CGPoint keyboardOrigin = [self.view convertPoint:point fromView:nil];
    inputViewFrame.origin.y = keyboardOrigin.y - inputViewFrame.size.height;
    self.messageInputView.frame = inputViewFrame;
}

#pragma mark - Utilities

- (UIViewAnimationOptions)animationOptionsForCurve:(UIViewAnimationCurve)curve
{
    switch (curve) {
        case UIViewAnimationCurveEaseInOut:
            return UIViewAnimationOptionCurveEaseInOut;
            
        case UIViewAnimationCurveEaseIn:
            return UIViewAnimationOptionCurveEaseIn;
            
        case UIViewAnimationCurveEaseOut:
            return UIViewAnimationOptionCurveEaseOut;
            
        case UIViewAnimationCurveLinear:
            return UIViewAnimationOptionCurveLinear;
            
        default:
            return kNilOptions;
    }
}

#pragma mark - Scroll view delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	self.isUserScrolling = YES;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    self.isUserScrolling = NO;
}

#pragma mark - Text view delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView becomeFirstResponder];
	
    if(!self.previousTextViewContentHeight)
		self.previousTextViewContentHeight = textView.contentSize.height;
    
    [self scrollToBottomAnimated:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
}

- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat maxHeight = [JSMessageInputView maxHeight];
    
    //  TODO:
    //
    //  CGFloat textViewContentHeight = textView.contentSize.height;
    //
    //  The line above is broken as of iOS 7.0
    //
    //  There seems to be a bug in Apple's code for textView.contentSize
    //  The following code was implemented as a workaround for calculating the appropriate textViewContentHeight
    //
    //  https://devforums.apple.com/thread/192052
    //  https://github.com/jessesquires/MessagesTableViewController/issues/50
    //  https://github.com/jessesquires/MessagesTableViewController/issues/47
    //
    // BEGIN HACK
    //
    CGSize size = [textView sizeThatFits:CGSizeMake(textView.frame.size.width, maxHeight)];
    CGFloat textViewContentHeight = size.height;
    //
    //  END HACK
    //
    
    BOOL isShrinking = textViewContentHeight < self.previousTextViewContentHeight;
    CGFloat changeInHeight = textViewContentHeight - self.previousTextViewContentHeight;
    
    if(!isShrinking && (self.previousTextViewContentHeight == maxHeight || textView.text.length == 0)) {
        changeInHeight = 0;
    }
    else {
        changeInHeight = MIN(changeInHeight, maxHeight - self.previousTextViewContentHeight);
    }
    
    if(changeInHeight != 0.0f) {
        
        [UIView animateWithDuration:0.25f
                         animations:^{
                             UIEdgeInsets insets = UIEdgeInsetsMake(0.0f,
                                                                    0.0f,
                                                                    myTableView.contentInset.bottom + changeInHeight,
                                                                    0.0f);
                             
                             myTableView.contentInset = insets;
                             myTableView.scrollIndicatorInsets = insets;
                             [self scrollToBottomAnimated:NO];
                             
                             if(isShrinking) {
                                 // if shrinking the view, animate text view frame BEFORE input view frame
                                 [self.messageInputView adjustTextViewHeightBy:changeInHeight];
                             }
                             
                             CGRect inputViewFrame = self.messageInputView.frame;
                             self.messageInputView.frame = CGRectMake(0.0f,
                                                                      inputViewFrame.origin.y - changeInHeight,
                                                                      inputViewFrame.size.width,
                                                                      inputViewFrame.size.height + changeInHeight);
                             
                             if(!isShrinking) {
                                 // growing the view, animate the text view frame AFTER input view frame
                                 [self.messageInputView adjustTextViewHeightBy:changeInHeight];
                             }
                         }
                         completion:^(BOOL finished) {
                         }];
        
        self.previousTextViewContentHeight = MIN(textViewContentHeight, maxHeight);
    }
    
    self.messageInputView.sendButton.enabled = ([textView.text js_stringByTrimingWhitespace].length > 0);
}

#pragma mark Scrolling
- (void)scrollToBottomAnimated:(BOOL)animated
{
	if(![self shouldAllowScroll])
        return;
	
    NSInteger rows = [myTableView numberOfRowsInSection:GTRPostCommentSectionComments];
    
    if(rows > 0) {
        [myTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:rows - 1 inSection:GTRPostCommentSectionComments]
                           atScrollPosition:UITableViewScrollPositionBottom
                                   animated:animated];
    }
}


- (void)scrollToRowAtIndexPath:(NSIndexPath *)indexPath
			  atScrollPosition:(UITableViewScrollPosition)position
					  animated:(BOOL)animated
{
	if(![self shouldAllowScroll])
        return;
	
	[myTableView scrollToRowAtIndexPath:indexPath
                       atScrollPosition:position
                               animated:animated];
}

- (BOOL)shouldAllowScroll
{
    if(self.isUserScrolling) {
        return NO;
    }
    
    return YES;
}


@end
