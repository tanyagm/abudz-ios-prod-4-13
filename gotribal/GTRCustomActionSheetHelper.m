//
//  GTRCustomActionSheetHelper.m
//  gotribal
//
//  Created by loaner on 12/24/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRCustomActionSheetHelper.h"
#import "GTRNewPostViewController.h"

@interface GTRCustomActionSheetHelper() <UIActionSheetDelegate, UINavigationControllerDelegate>

@property NSMutableArray *actionSheetButtons;
@property(nonatomic,strong) UIViewController *viewController;
@property(nonatomic,strong) GTRCustomActionSheet *customActionSheet;

@end

@implementation GTRCustomActionSheetHelper
@synthesize actionSheetButtons, viewController;
@synthesize customActionSheet = _customActionSheet;


- (id) initWithSuperViewController:(UIViewController*)theViewController
{
    self = [super init];
    if (self!=nil) {
        viewController = theViewController;
        actionSheetButtons = [[NSMutableArray alloc] init];
    }
    return self;
}


#pragma mark - Custom Action Sheet
-(void)openCustomActionSheetWithMode:(GTRCustomActionSheetMode)theMode
{
    _customActionSheet = [[GTRCustomActionSheet alloc] initWithMode:theMode superView:viewController.view delegate:self];
    [_customActionSheet show];
}

#pragma mark - UINavigationController delegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

#pragma mark - Custom actionsheet delegate methods

-(void)customActionSheet:(GTRCustomActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *clickedButtonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([clickedButtonTitle isEqualToString:CUSTOM_ACTION_SHEET_DELETE_BUTTON_STRING]){
        if ([(id) self.delegate respondsToSelector:@selector(deleteAction)]) {
            [self.delegate deleteAction];
        }
    } else if ([clickedButtonTitle isEqualToString:CUSTOM_ACTION_SHEET_SHARE_BUTTON_STRING]){
        DDLogInfo(@"Share button clicked!");
    } else if ([clickedButtonTitle isEqualToString:CUSTOM_ACTION_SHEET_REPORT_FOR_SPAM_BUTTON]) {
        if ([(id) self.delegate respondsToSelector:@selector(reportForSpamAction)]) {
            [self.delegate reportForSpamAction];
        }        
    }
}

@end

