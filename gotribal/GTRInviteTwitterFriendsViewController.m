//
// Created by Muhammad Taufik on 12/19/13.
// Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRInviteTwitterFriendsViewController.h"
#import "UIImageView+AFNetworking.h"
#import "SVProgressHUD.h"
#import "GTRFriendsTableViewCell.h"
#import "SVPullToRefresh.h"
#import "GTRAnalyticsHelper.h"
#import "GTRSocialHelper.h"
#import "GTRTribe.h"
#import "GTRRestServiceErrorHandler.h"

@interface GTRInviteTwitterFriendsViewController ()

@property (nonatomic, strong) NSMutableArray *users;
@property (nonatomic, strong) NSNumber *nextCursor;

@property (nonatomic, strong) NSMutableArray *invitedUsers;
@property (nonatomic, copy) GTRTribe *tribe;
@end

@implementation GTRInviteTwitterFriendsViewController {

}

- (id)initWithTribe:(GTRTribe *)tribe {
    if (self = [super init]) {
        self.tribe = tribe;
        self.title = @"Twitter";
        self.tableView.separatorColor = [UIColor clearColor];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *invitedTwitterUsersArray = nil;
    if (self.tribe) {
        invitedTwitterUsersArray = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"invitedTwitterUsersWithTribe-%@", self.tribe.tribeID]];
    } else {
        invitedTwitterUsersArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"invitedTwitterUsers"];
    }
    
    self.invitedUsers = [NSMutableArray arrayWithArray:invitedTwitterUsersArray];

    self.view.backgroundColor = GTR_MAIN_ACTIVITIES_BACKGROUND_COLOR;
    
    [GTRNavigationBarHelper setBackButtonItem:self.navigationItem withTarget:self.navigationController selector:@selector(popViewControllerAnimated:)];
    
    [self requestAccessToTwitterAccounts:nil];
    
    // SVInfiniteScrolling
    __weak GTRInviteTwitterFriendsViewController *weakSelf = self;
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf refresh:nil];
        [weakSelf.tableView.infiniteScrollingView stopAnimating];
    }];
}

-(void)refresh:(id)sender
{
    [self retrieveFollowersList];
}

#pragma mark - Twitter Requests

-(void)requestAccessToTwitterAccounts:(id)sender
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    GTRSocialHelper *socialHelper = [GTRSocialHelper sharedHelper];
    [socialHelper retrieveTwitterAccountWithSuccess:^{
        [self.tableView triggerInfiniteScrolling];
    } failure:^(id responseObject, NSError *error) {
        [SVProgressHUD dismiss];
        if (responseObject) {
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [SVProgressHUD showErrorWithStatus:responseObject[@"error"]];
        }
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Enter a Twitter account in Settings to Post with Twitter", @"enter twitter accounts") message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
    }];
}

-(void)retrieveFollowersList
{
    self.tableView.showsInfiniteScrolling = NO;
    GTRSocialHelper *socialHelper = [GTRSocialHelper sharedHelper];
    [socialHelper retrieveFollowersListWithCursor:(self.nextCursor? self.nextCursor : @(-1)) success:^(NSNumber *nextCursor, NSArray *users) {
        self.nextCursor = nextCursor;
        [self filterInvitedUsersWithArray:users];
        [SVProgressHUD dismiss];
        self.tableView.showsInfiniteScrolling = YES;
    } failure:^(NSDictionary *error) {
        DDLogInfo(@"%@", error? error : @"cancel API request with cursor 0");
        self.tableView.showsInfiniteScrolling = YES;
        if (!error) [SVProgressHUD dismiss];
        if (error) {
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [SVProgressHUD showErrorWithStatus:error[@"message"]];
        }
    }];
}

-(void)filterInvitedUsersWithArray:(NSArray *)users
{
    if (!self.users) self.users = [NSMutableArray array];
    [self.users addObjectsFromArray:users];
    
    for (NSDictionary *invitedUserID in self.invitedUsers) {
        for (NSMutableDictionary *user in self.users) {
            NSNumber *userID = [user valueForKey:@"id"];
            if ([userID isEqual:invitedUserID]) {
                user[@"invited"] = @YES;
            }
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - Table View

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.users.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 34 + 10 + 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *const CellIdentifier = @"TwitterUserCell";
    GTRFriendsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[GTRFriendsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *user = [self.users objectAtIndex:indexPath.row];
    
    NSString *profileImageURL = user[@"profile_image_url"];
    NSURL *URL = [NSURL URLWithString:profileImageURL];
    
    cell.textLabel.text = user[@"name"];
    
    [cell.inviteButton addTarget:self action:@selector(inviteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [cell.imageView setImageWithURL:URL placeholderImage:DEFAULT_AVATAR];
    
    cell.invited = [user[@"invited"] boolValue];
    
    return cell;
}

-(void)inviteButtonTapped:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath) {
        [self sendInviteRequestForIndexPath:indexPath];
    }
}

-(void)sendInviteRequestForIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *user = [self.users objectAtIndex:indexPath.row];
    
    if (user[@"invited"]) return;
    
    NSNumber *friendsID = user[@"id"];
    NSDictionary *parameters = @{
                                 @"platform_type": @"twitter",
                                 @"twitter_id" : friendsID,
                                 };
    
    NSString *endPoint = @"/api/v1/invites";
    if (self.tribe) endPoint = [NSString stringWithFormat:@"/api/v1/tribes/%@/invites", self.tribe.tribeID];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager POST:endPoint parameters:parameters success:^(AFHTTPRequestOperation *operation, id json) {
        DDLogInfo(@"%@", json);
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Invitation sent!", @"invite sent")];
        
        NSMutableDictionary *user = [self.users objectAtIndex:indexPath.row];
        user[@"invited"] = @YES;
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        if (!self.invitedUsers) self.invitedUsers = [NSMutableArray array];
        
        [self.invitedUsers addObject:user[@"id"]];
        
        if (self.tribe) {
            [[NSUserDefaults standardUserDefaults] setObject:self.invitedUsers forKey:[NSString stringWithFormat:@"invitedTwitterUsersWithTribe-%@", self.tribe.tribeID]];
        } else {
            [[NSUserDefaults standardUserDefaults] setObject:self.invitedUsers forKey:@"invitedTwitterUsers"];
        }
        
        // send analytics data
        #ifdef GTR_FLURRY_ANALYTICS
        [GTRAnalyticsHelper sendInviteFriendsEvent];
        #endif
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"%@", error);

        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
    }];
}

@end
