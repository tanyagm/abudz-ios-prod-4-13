//
//  UIBarButtonItem+NegativeSpacer.m
//  gotribal
//
//  Created by Muhammad Taufik on 12/3/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "UIBarButtonItem+NegativeSpacer.h"

@implementation UIBarButtonItem (NegativeSpacer)
+(UIBarButtonItem*)negativeSpacerWithWidth:(NSInteger)width {
    UIBarButtonItem *item = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                             target:nil
                             action:nil];
    item.width = (width >= 0 ? -width : width);
    return item;
}
@end
