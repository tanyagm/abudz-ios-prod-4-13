//
//  GTRNotificationsViewController.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/3/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRNotificationsViewController.h"
#import "GTRPendingRequestCell.h"
#import "GTRSettingsHelper.h"
#import "GTRUserRestService.h"
#import "NSMutableArray+AddMantleObject.h"
#import <UIImageView+AFNetworking.h>
#import <objc/runtime.h>
#import "GTRNotificationsHelper.h"
#import "UIBarButtonItem+NegativeSpacer.h"
#import "GTRNavigationBarHelper.h"
#import "SVPullToRefresh.h"
#import "GTRUserRestService.h"
#import "GTRNotificationItem.h"
#import "GTRNotificationItemCell.h"
#import "GTRActivityNotificationRestService.h"
#import "GTRAnalyticsHelper.h"

#define NOTIFICATIONS NSLocalizedString(@"Notifications", @"Notifications")
NSString *const REQUESTS = @"Requests";
NSString *const NO_PENDING_REQUESTS = @"No Pending Requests yet.";
NSString *const NO_PENDING_NOTIFICATIONS = @"No Notifications yet.";
NSInteger const NO_PENDING_DATA_LABEL_TAG = 2020;

typedef enum {
    GTRNotificationsSectionPendingRequests = 0,
    GTRNotificationsSectionNotifications
} GTRNotificationsSection;


@interface GTRNotificationsViewController ()
{
    UIImage *placeholderImage;
}

@property(nonatomic,strong) UITableView *table;
@property(nonatomic,strong) NSMutableArray *pendingRequests;
@property(nonatomic,strong) NSNumber *lastMaxID;

@property(nonatomic,strong) NSMutableArray *notifications;

@property(nonatomic,strong) UILabel *noPendingRequestLabel;
@property(nonatomic,strong) UILabel *noPendingNotificationsLabel;

@property(nonatomic,strong) UITableViewCell *noPendingRequestCell;
@property(nonatomic,strong) UITableViewCell *noPendingNotificationsCell;

@property(nonatomic) BOOL isLastRequest;

@end

@implementation GTRNotificationsViewController
@synthesize table = _table;
@synthesize pendingRequests = _pendingRequests;
@synthesize lastMaxID = _lastMaxID;
@synthesize noPendingRequestLabel, noPendingNotificationsLabel, noPendingRequestCell, noPendingNotificationsCell, isLastRequest;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Notifications";
    
    isLastRequest = NO;
    
    [GTRNavigationBarHelper setLeftSideBarButtonItem:self.navigationItem withTarget:self
                                            selector:@selector(toggleSideMenu:)];
        
    CGRect tableFrame = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? CGRectMake(0, 0,320, self.view.bounds.size.height - 64): CGRectMake(0, 0, 320, self.view.bounds.size.height - 20);
    
    _table = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStyleGrouped];
    _table.backgroundView = nil;
    _table.backgroundColor = TABLEVIEW_BACKGROUND_COLOR;
    _table.separatorStyle = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? UITableViewCellSeparatorStyleNone : UITableViewCellSeparatorStyleSingleLine;
    _table.delegate = self;
    _table.dataSource = self;
    [self.view addSubview:_table];
    
    placeholderImage = DEFAULT_AVATAR;
    
    // last max id for pagination
    _lastMaxID = [NSNumber numberWithInt:-1]; // initial value
    
    //prepare placeholder cell for no pending request and notifications, respectively
    
    CGFloat placeholderLabelWidth = SYSTEM_VERSION_LESS_THAN(@"7.0")? tableFrame.size.width-20 : tableFrame.size.width;
    
    noPendingRequestCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"noPendingRequestCell"];
    noPendingRequestCell.selectionStyle = UITableViewCellSelectionStyleNone;
    noPendingRequestCell.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    noPendingRequestCell.backgroundColor = [UIColor clearColor];
    noPendingRequestCell.contentView.backgroundColor = [UIColor clearColor];

    
    noPendingRequestLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, placeholderLabelWidth, noPendingRequestCell.bounds.size.height)];
    noPendingRequestLabel.tag = NO_PENDING_DATA_LABEL_TAG;
    noPendingRequestLabel.backgroundColor = [UIColor clearColor];
    noPendingRequestLabel.text = NO_PENDING_REQUESTS;
    noPendingRequestLabel.textColor = NOTIFICATIONS_CELL_SUBTEXT_COLOR;
    noPendingRequestLabel.font = CELL_FONT;
    noPendingRequestLabel.textAlignment = NSTextAlignmentCenter;
    noPendingRequestLabel.numberOfLines = 0;
    noPendingRequestLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [noPendingRequestCell.contentView addSubview:noPendingRequestLabel];
    
    noPendingNotificationsCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"noPendingNotificationsCell"];
    noPendingNotificationsCell.selectionStyle = UITableViewCellSelectionStyleNone;
    noPendingNotificationsCell.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    noPendingNotificationsCell.backgroundColor = [UIColor clearColor];
    noPendingNotificationsCell.contentView.backgroundColor = [UIColor clearColor];
    
    noPendingNotificationsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, placeholderLabelWidth, noPendingNotificationsCell.bounds.size.height)];
    noPendingNotificationsLabel.tag = NO_PENDING_DATA_LABEL_TAG;
    noPendingNotificationsLabel.backgroundColor = [UIColor clearColor];
    noPendingNotificationsLabel.text = NO_PENDING_NOTIFICATIONS;
    noPendingNotificationsLabel.textColor = NOTIFICATIONS_CELL_SUBTEXT_COLOR;
    noPendingNotificationsLabel.font = CELL_FONT;
    noPendingNotificationsLabel.textAlignment = NSTextAlignmentCenter;
    noPendingNotificationsLabel.numberOfLines = 0;
    noPendingNotificationsLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [noPendingNotificationsCell.contentView addSubview:noPendingNotificationsLabel];
    
    // alloacting array
    _notifications = [[NSMutableArray alloc] init];
    _pendingRequests = [[NSMutableArray alloc]init];
    
    __weak GTRNotificationsViewController *weakSelf = self;
    
    // Init initial data
    [self reloadNotifications:GTRReloadModeRefresh];
    
    // fetch pending request
    [self fetchPendingRequestDataWithLastMaxID:_lastMaxID withCompletion:^(NSError *error) {

    }];
    
    [_table addPullToRefreshWithActionHandler:^{
        [weakSelf refreshPendingRequest];
        [weakSelf reloadNotifications:GTRReloadModeRefresh];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
     _table = nil;
    _pendingRequests = nil;;
    _lastMaxID = nil;
}

#pragma mark - This will retrieve next batch of data.

-(void)retrieveNextPage
{
    __weak GTRNotificationsViewController *weakSelf = self;
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf fetchPendingRequestDataWithLastMaxID:_lastMaxID withCompletion:^(NSError *error) {
            if (error) {
                DDLogError(@"Error: %@",error);
            }
            
        }];
    });
}

#pragma mark - This will refresh with new top part data

-(void)refreshPendingRequest
{
    __weak GTRNotificationsViewController *weakSelf = self;
    __weak UITableView *weakTable = _table;
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf fetchPendingRequestDataWithCompletion:^(NSError *error) {
            if (error) {
                DDLogError(@"Error: %@",error);
            }
            
            [weakTable.pullToRefreshView stopAnimating];
        }];
    });
}

#pragma mark - Fetchers

-(void)reloadNotifications:(GTRReloadMode)reloadMode
{
    [GTRActivityNotificationRestService retrieveActivityNotificationWithMaxID:nil onSuccess:^(id responseObject) {
        if (reloadMode == GTRReloadModeRefresh){
            [_notifications removeAllObjects];
        }
        
        NSArray *notificationParams = [responseObject objectForKey:@"data"];
        [_notifications addObjectsFromArray:notificationParams
                               modelOfClass:[GTRNotificationItem class]];
        [_table reloadData];
    } onFail:^(id operation, NSError * error) {
        
    }];
}

-(void)fetchPendingRequestDataWithLastMaxID:(NSNumber*)maxID withCompletion:(void(^)(NSError* error))completion
{
    [GTRUserRestService retrieveCurrentUserPendingRequestWithLastMaxID:maxID onSuccess:^(id responseObject) {
        if (responseObject) {
            NSInteger numberOfUsersBefore = _pendingRequests.count;
            if (_pendingRequests.count > 0) {
                [_pendingRequests addObjectsFromArray:(NSArray*)responseObject[@"data"]
                                         modelOfClass:[GTRUser class]];
            } else {
                [_pendingRequests removeAllObjects];
                [_pendingRequests addObjectsFromArray:(NSArray*)responseObject[@"data"]
                                         modelOfClass:[GTRUser class]];
            }
            
            _lastMaxID = (responseObject[@"pagination"][@"max_id"] == [NSNull null]) ? nil:responseObject[@"pagination"][@"max_id"];
            
            if (numberOfUsersBefore < _pendingRequests.count) {
                [_table reloadData];
            }
        }
        
        if (completion) {
            completion(nil);
        }
    } onFail:^(id response, NSError *error) {
        if (completion) {
            completion(error);
        }
    }];
}

-(void)fetchPendingRequestDataWithCompletion:(void(^)(NSError* error))completion
{
    [GTRUserRestService retrieveCurrentUserPendingRequestonSuccess:^(id responseObject) {
        if (responseObject) {
            NSInteger numberOfUsersBefore = _pendingRequests.count;
            
            [_pendingRequests removeAllObjects];
            [_pendingRequests addObjectsFromArray:(NSArray*)responseObject[@"data"]
                                     modelOfClass:[GTRUser class]];
            
            if (numberOfUsersBefore < _pendingRequests.count) {
                [_table reloadData];
            }
            
            if (completion) {
                completion(nil);
            }
        }
    } onFail:^(id response, NSError *error) {
        if (completion) {
            completion(error);
        }
    }];
}

#pragma mark - Table view data source and delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case GTRNotificationsSectionPendingRequests:
            return _pendingRequests.count == 0 && !isLastRequest ? 1 : _pendingRequests.count; //at least, show the placeholder cell
            break;
        case GTRNotificationsSectionNotifications:
            return _notifications.count == 0 ?  1 : _notifications.count; //at least, show the placeholder cell
            break;
    }
    
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case GTRNotificationsSectionPendingRequests:
            {
                if (_pendingRequests.count > 0) {
                    return PENDING_REQUEST_CELL_HEIGHT;
                } else {
                    return noPendingRequestCell.bounds.size.height;
                }
            }
            break;
        case GTRNotificationsSectionNotifications:
            {
                if (_notifications.count > 0) {
                    return NOTIFICATION_CELL_HEIGHT;
                } else {
                    return noPendingNotificationsCell.bounds.size.height;
                }
            }
            break;
        default:
            return 0;
            break;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case GTRNotificationsSectionPendingRequests:
            
            return [GTRNotificationsHelper createHeaderViewWithText:REQUESTS
                                                           withFont:NOTIFICATIONS_CELL_TEXT_FONT];
            break;
        case GTRNotificationsSectionNotifications:
            return [GTRNotificationsHelper createHeaderViewWithText:NOTIFICATIONS
                                                           withFont:NOTIFICATIONS_CELL_TEXT_FONT];
            break;
    }
    
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case GTRNotificationsSectionPendingRequests:
            {
                if (_pendingRequests.count > 0){
                    return [self tableView:tableView pendingRequestCellForRowAtIndexPath:indexPath];
                } else  {
                    return noPendingRequestCell;
                }
            }
            break;
        case GTRNotificationsSectionNotifications:
            {
                if (_notifications.count > 0) {
                    return [self tableView:tableView notificationCellForRowAtIndexPath:indexPath];
                } else {
                    return noPendingNotificationsCell;
                }
            }
            break;
        default:
            return nil;
            break;
    }
}

-(GTRNotificationItemCell *)tableView:(UITableView *)tableView notificationCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DDLogInfo(@"rendering for row %d, count %d", indexPath.row, [_notifications count]);
    static NSString *CellIdentifier = @"NotificationCell";
    GTRNotificationItemCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[GTRNotificationItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    GTRNotificationItem *item = [_notifications objectAtIndex:indexPath.row];
    [cell setContent:item];
    return  cell;
}

-(UITableViewCell *)tableView:(UITableView *)tableView pendingRequestCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NormalCell";
    GTRPendingRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[GTRPendingRequestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.rightView = nil;
    }
    
    return [self pendingRequestCell:cell forRowAtIndex:indexPath.row];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
                                        forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (tableView == self.table) {
            cell.backgroundView = [GTRNotificationsHelper createGroupedViewInCell:(GTRPendingRequestCell*)cell
                                                                      atIndexPath:indexPath];
        }
    }
}

#pragma mark - Pending request cell

-(GTRPendingRequestCell*)pendingRequestCell:(GTRPendingRequestCell*)cell forRowAtIndex:(NSInteger)row
{
    cell.indexpath = [NSIndexPath indexPathForRow:row
                                        inSection:GTRNotificationsSectionPendingRequests];
    
    NSURL *url = [NSURL URLWithString:[_pendingRequests[row] avatarURL]];
    [cell.avatarImageView setImageWithURL:url placeholderImage:placeholderImage];
    
    NSString *firstName = [[_pendingRequests[row] firstName] stringByTrimmingCharactersInSet:
                           [NSCharacterSet whitespaceCharacterSet]];
    
    NSString *lastName = [[_pendingRequests[row] lastName] stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
    
    cell.text.text = [NSString stringWithFormat:@"%@ %@", firstName,lastName];
    cell.subtext.text = [NSString stringWithFormat:@"%@, %@ wants to be your Activebud", [[GTRUserHelper getCurrentUser] firstName],firstName];
    
    [cell.confirmButton addTarget:self action:@selector(buttonHandler:) forControlEvents:UIControlEventTouchUpInside];
    [cell.ignoreButton addTarget:self action:@selector(buttonHandler:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - Button handler

-(void)buttonHandler:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    if (button.tag == CONFIRM_BUTTON_TAG) {
        NSIndexPath *indexPath = objc_getAssociatedObject(button, "confirmButton");
        
        if (indexPath.row < _pendingRequests.count) {
            GTRUser *user = _pendingRequests[indexPath.row];
            
            __block NSIndexPath *blockIndexPath = indexPath;
            [GTRUserRestService sendConfirmationWithType:GTRUserRestServiceFriendRequestConfirm withUserID:user.userID onSuccess:^(id responseObject) {
                
                [self updateRowAfterIgnoreOrConfirmInIndexPath:blockIndexPath];
                
                //send analytics data
                #ifdef GTR_FLURRY_ANALYTICS
                [GTRAnalyticsHelper sendActiveBudzAcceptRequestEvent];
                #endif
                
            } onFail:^(id response, NSError *error){

            }];
        }
    }
    else {
        NSIndexPath *indexPath = objc_getAssociatedObject(button, "ignoreButton");
        
        if (indexPath.row < _pendingRequests.count) {
            GTRUser *user = _pendingRequests[indexPath.row];
            
            __block NSIndexPath *blockIndexPath = indexPath;
            [GTRUserRestService sendConfirmationWithType:GTRUserRestServiceFriendRequestIgnore withUserID:user.userID onSuccess:^(id responseObject) {
                
                [self updateRowAfterIgnoreOrConfirmInIndexPath:blockIndexPath];
            } onFail:^(id response, NSError *error){

            }];
        }
    }
}

-(void)updateRowAfterIgnoreOrConfirmInIndexPath:(NSIndexPath*)indexPath
{
    isLastRequest = (_pendingRequests.count == 1);
    
    [_table beginUpdates];
    [_pendingRequests removeObjectAtIndex:indexPath.row];
    [_table deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
    [_table reloadData];
    [_table endUpdates];
    
    //if last request, show the no pending request placeholder
    if (isLastRequest) {
        isLastRequest = !isLastRequest;
        [_table reloadData];
    }
}

@end
