//
//  AFHTTPRequestOperationManager+GTRForceLogout.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/6/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFHTTPRequestOperationManager(GTRForceLogout)
- (AFHTTPRequestOperation *)GET:(NSString *)URLString
                     parameters:(NSDictionary *)parameters
                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
      forceLogoutOnUnauthorized:(BOOL)forceLogout;

- (AFHTTPRequestOperation *)DELETE:(NSString *)URLString
                        parameters:(NSDictionary *)parameters
                           success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
         forceLogoutOnUnauthorized:(BOOL)forceLogout;
@end
