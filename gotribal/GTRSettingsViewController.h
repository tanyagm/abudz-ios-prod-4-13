//
//  GTRSettingsViewController.h
//  gotribal
//
//  Created by Hisma Mulya S on 11/29/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "GTRSettingsCustomRightView.h"

@interface GTRSettingsViewController : GTRViewController <UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate, GTRSettingsCustomRightViewDelegate>

@end
