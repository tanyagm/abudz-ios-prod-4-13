//
//  GTRLoadMoreButton.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/16/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRLoadMoreButton.h"

@interface GTRLoadMoreButton ()
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@end

@implementation GTRLoadMoreButton
@synthesize button, label, indicator;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        CGRect buttonFrame = frame;
        buttonFrame.origin.y += 5;
        buttonFrame.size.height -= 10;
        
        button = [[UIButton alloc] initWithFrame:buttonFrame];
        button.backgroundColor = [UIColor darkGrayColor];
        
        label = [[UILabel alloc] initWithFrame:buttonFrame];
        label.font = [UIFont systemFontOfSize:13.0];
        label.text = NSLocalizedString(@"Load more", @"Load more");
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        
        indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [indicator setCenter:button.center];
        
        [self addSubview: button];
        [self addSubview: label];
        [self addSubview: indicator];
        
    }
    return self;
}

- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents
{
    [button addTarget:target action:action forControlEvents:controlEvents];
}

- (void)setText:(NSString*)labelText
{
    label.text = labelText;
}

- (void)startAnimating
{
    [label setHidden:YES];
    [indicator startAnimating];
}

- (void)stopAnimating
{
    [indicator stopAnimating];
    [label setHidden:NO];
}

- (CGFloat)getHeight
{
    return self.frame.size.height;
}

@end
