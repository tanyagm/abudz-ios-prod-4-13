//
//  GTRActiveBudzProfileInformation.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTRActiveBudzProfileInformation : UIView

-(id)initWithFrame:(CGRect)frame User:(GTRUser*)theUser;
-(void)setUserData:(GTRUser*)theUser;
-(void)resetAppearance;
@end
