//
//  GTRCustomActionSheet.h
//  gotribal
//
//  Created by loaner on 12/24/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CUSTOM_ACTION_SHEET_CANCEL_BUTTON_STRING @"Cancel"
#define CUSTOM_ACTION_SHEET_DELETE_BUTTON_STRING @"Delete"
#define CUSTOM_ACTION_SHEET_SHARE_BUTTON_STRING @"Share"
#define CUSTOM_ACTION_SHEET_REPORT_FOR_SPAM_BUTTON @"Report for spam"

typedef enum{
    GTRCustomActionSheetModeOwnedPost = 0,
    GTRCustomActionSheetModeOtherPost, //post made by other user
    GTRCustomActionSheetModeActiveBudzProfile
} GTRCustomActionSheetMode;


@class GTRCustomActionSheet;

@protocol GTRCustomActionSheetDelegate <NSObject>

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)customActionSheet:(GTRCustomActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;
@end

@interface GTRCustomActionSheet : NSObject
@property(nonatomic,weak) id <GTRCustomActionSheetDelegate> delegate;

-(id)initWithMode:(GTRCustomActionSheetMode)mode superView:(UIView*)superview delegate:(id)delegate;

-(void)show;
-(void)remove;

-(NSString*)buttonTitleAtIndex:(NSInteger)index;

@end
