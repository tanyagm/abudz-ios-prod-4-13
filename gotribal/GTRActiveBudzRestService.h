//
//  GTRActiveBudzRestService.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/9/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRUser.h"

@interface GTRActiveBudzRestService : NSObject

+(void)retrieveSuggestedActiveBudz:(void(^)(id responseObject))success
                    onFail:(void (^) (id responseObject, NSError *error))errorHandler;

+(void)retrieveSuggestedActiveBudzWithStartPoint:(int)theStartPoint
                                  onSuccess:(void(^)(id responseObject))success
                            onFail:(void (^) (id responseObject, NSError *error))errorHandler;

+(void)retrieveMyActiveBudz:(void(^)(id responseObject))success
                            onFail:(void (^) (id responseObject, NSError *error))errorHandler;

+(void)retrieveMyActiveBudzWithMaxID:(NSNumber*)theMaxID
                                  onSuccess:(void(^)(id responseObject))success
                                     onFail:(void (^) (id responseObject, NSError *error))errorHandler;

+(void)retrievePublicProfile:(GTRUser*)theUser
                   onSuccess:(void(^)(id responseObject))success
                      onFail:(void (^) (id responseObject, NSError *error))errorHandler;

+(void)addActiveBudzRequestWithUser:(GTRUser*)theUser onSuccess:(void(^)(id responseObject))success onFail:(void (^) (id responseObject, NSError *error))errorHandler;

+(void)removeActiveBudzWithUser:(GTRUser*)theUser onSuccess:(void(^)(id responseObject))success onFail:(void (^) (id responseObject, NSError *error))errorHandler;

+(void)endorseWithId:(NSNumber *)theUserId 
           onSuccess:(void (^)(id))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

+(void)unEndorseWithId:(NSNumber *)theUserId
             onSuccess:(void (^)(id))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

@end
