//
//  GTRCustomButton.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTRCustomButton : UIButton

/*
 * This property is mainly used to save the reference to GTRActiveBudzCell of the accept and reject buttons.
 */
@property id customObjectPointer;

@end
