//
//  GTRTribe.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/11/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "MTLModel.h"

@interface GTRTribe : MTLModel<MTLJSONSerializing>

@property (strong,nonatomic) NSNumber *tribeID;
@property (strong,nonatomic) NSString *tribeName;
@property (strong,nonatomic) NSString *tribeActivity;
@property (strong,nonatomic) NSString *tribeDescription;
@property (strong,nonatomic) NSString *tribeImageURL;
@property (nonatomic) BOOL isJoined;
@property (strong,nonatomic) GTRUser *tribeOwner;
@property (strong,nonatomic) NSArray *tribeMembers;

@end
