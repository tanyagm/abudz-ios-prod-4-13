//
//  GTRSurveyRolePageViewController.h
//  gotribal
//
//  Created by loaner on 12/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSurveyPageTemplateViewController.h"

@interface GTRSurveyRolePageViewController : GTRSurveyPageTemplateViewController

-(id)initWithReferenceDictionary:(NSMutableDictionary *)theReferenceDictionary;

@end
