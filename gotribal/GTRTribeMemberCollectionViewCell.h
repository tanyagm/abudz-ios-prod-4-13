//
//  GTRActiveBudzCollectionViewCell.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/17/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRUser.h"

@interface GTRTribeMemberCollectionViewCell : UICollectionViewCell

-(void)setContent:(GTRUser*)theUser;

@end
