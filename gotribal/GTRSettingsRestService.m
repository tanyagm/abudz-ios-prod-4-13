//
//  GTRSettingsRestService.m
//  gotribal
//
//  Created by loaner on 12/30/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSettingsRestService.h"
#import "GTRConstants.h"
#import "GTRRestServiceErrorHandler.h"

@implementation GTRSettingsRestService

+(void)retrieveSettingOnSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager GET:USER_SETTING_ENDPOINT parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}

+(void)updateSetting:(NSDictionary *)params onSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager POST:USER_SETTING_ENDPOINT parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success)
            DDLogInfo(@"Response : %@", responseObject);
            success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler)
            errorHandler(operation, error);
    }];
}

@end
