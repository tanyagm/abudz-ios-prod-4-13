//
//  GTRCustomSurveyCell.m
//  gotribal
//
//  Created by loaner on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRCustomSurveyCell.h"

@implementation GTRCustomSurveyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.customTextLabel = [[UILabel alloc]init];
        self.customImageView = [[UIImageView alloc]init];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setText:(NSString *)aText{
    self.customTextLabel.text = aText;
    self.customTextLabel.frame = CGRectMake(50,0,200,40);
    [self.contentView addSubview:self.customTextLabel];
}

-(void)setImage:(UIImage *)theImageName{
    self.customImageView.image = theImageName;
    
    if([[UIDevice currentDevice].systemVersion floatValue]<7.0){
        self.customImageView.frame = CGRectMake(0, 0, 40, 40);
    }
    else {
        self.customImageView.frame = CGRectMake(5, 0, 40, 40);
    }
    
    self.customImageView.contentMode = UIViewContentModeCenter;
    [self.contentView addSubview:self.customImageView];
}

@end
