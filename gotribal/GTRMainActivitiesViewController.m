//
//  GTRMainActivitiesViewController.m
//  gotribal
//
//  Created by Muhammad Taufik on 11/15/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRMainActivitiesViewController.h"
#import "GTRAppDelegate.h"
#import "GTRUser.h"
#import "MFSideMenu.h"
#import "GTRIntroViewController.h"
#import "GTRLoginViewController.h"
#import "SVProgressHUD.h"
#import "AFOAuth2Client.h"
#import "GTRSideMenuViewController.h"
#import "GTRSideMenuHelper.h"
#import "UAirship.h"
#import "UAConfig.h"
#import "UAPush.h"
#import "GTRUserRestService.h"
#import "GTRActivityStreamViewController.h"
#import "UIBarButtonItem+NegativeSpacer.h"

@interface GTRMainActivitiesViewController ()

@end

@implementation GTRMainActivitiesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Activities", @"Activities");
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(registerAirship)
                                                     name:REGISTER_AIRSHIP
                                                   object:nil];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AFOAuthCredential *credential = [AFOAuthCredential retrieveCredentialWithIdentifier:[[NSURL URLWithString:BASE_URL] host]];
    
    if (!credential) {
        [self showIntroScreenAnimated:NO];
    } else {
        [self registerAirship];
        if ([GTRUserHelper getCurrentUser] == nil){
            [self retrieveUserProfile];
        }
    }
    
    [self setAppearance];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    GTRAppDelegate *appDelegate = (GTRAppDelegate *)[UIApplication sharedApplication].delegate;
    
    if ([appDelegate currentUser]){
        [self showActivityStreamScreen];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}

-(void)setAppearance
{
    self.view.backgroundColor = GTR_MAIN_ACTIVITIES_BACKGROUND_COLOR;
    
    [GTRNavigationBarHelper setLeftSideBarButtonItem:self.navigationItem withTarget:self selector:@selector(toggleSideMenu:)];
    
    UIImageView *backgroundProfileImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed: @"bg-profile"]];
    backgroundProfileImageView.contentMode = UIViewContentModeScaleToFill;
    backgroundProfileImageView.frame = CGRectMake(0, 0, 320, 120);
    [self.view addSubview:backgroundProfileImageView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)showActivityStreamScreen
{
    GTRActivityStreamViewController *viewcontroller = [[GTRActivityStreamViewController alloc] init];
    [self.navigationController pushViewController:viewcontroller animated:NO];
}

-(void)showIntroScreenAnimated:(BOOL)animated
{
    GTRIntroViewController *introViewController = [[GTRIntroViewController alloc] init];
    UINavigationController *introNavigationController = [[UINavigationController alloc] initWithRootViewController:introViewController];
    [self.menuContainerViewController presentViewController:introNavigationController animated:animated completion:nil];
}

-(void)showLoginScreen
{
    GTRLoginViewController *loginViewController = [[GTRLoginViewController alloc] init];
    UINavigationController *loginNavigationController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    [self presentViewController:loginNavigationController animated:NO completion:nil];
}

-(void)retrieveUserProfile
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];    
    [GTRUserRestService retrieveCurrentUserOnSuccess:^(id responseObject) {
        [self reloadSidebarMenuData];
        [self showActivityStreamScreen];
        [SVProgressHUD dismiss];
    } onFail:^(id responseObject, NSError *error) {
    }];
}

-(void)reloadSidebarMenuData
{
    [[GTRSideMenuHelper appDelegate] reloadData];
}

-(void)registerAirship
{
    [[UAPush shared] setPushEnabled:YES];
}

@end
