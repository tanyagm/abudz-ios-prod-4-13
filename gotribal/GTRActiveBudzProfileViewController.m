//
//  GTRActiveBudzProfileViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "GTRActiveBudzProfileViewController.h"
#import "GTRConstants.h"
#import "GTRUserDetail.h"
#import "GTRActiveBudzProfileInformation.h"
#import "GTRActiveBudzProfileConnection.h"
#import "GTRActiveBudzProfileRanking.h"
#import "UIImageView+AFNetworking.h"
#import "SVProgressHUD.h"
#import "GTRPursuitHelper.h"
#import "GTRNavigationBarHelper.h"
#import "GTRMessagesViewController.h"
#import "GTRActiveBudzRestService.h"
#import "GTRUserImageView.h"
#import "GTRActiveBudzProfileEndorsement.h"
#import "GTRAnalyticsHelper.h"
#import "GTRCustomActionSheetHelper.h"
#import "GTRActiveBudzHelper.h"

//HEADER
NSString* const HEADER_TITLE = @"Profile";

//INFORMATION SECTION
NSString* const INFORMATION_TITLE_STRING = @"Information";


//CONNETIONS SECTION (already connected)
NSString* const CONNECTION_TITLE_STRING = @"Connections";

//RANKING AND ENDORSEMENT SECTION (already connected)
NSString* const RANKING_TITLE_STRING = @"Ranking";

//CONNECT SECTION (not connected yet)
NSString* const CONNECT_BUTTON_STRING = @"Connect";

//ICON AND IMAGE NAMES

//location icon
NSString* const LOCATION_ICON_NAME = @"ico-loc-white";

//profile header image
NSString* const PROFILE_HEADER_IMAGE_NAME = @"bg-profile";

//report for spam strings
NSString* const REPORT_FOR_SPAM_SUBJECT = @"GOTRIbal Spam Report";
NSString* const REPORT_FOR_SPAM_BODY = @"Hi, I want to report %@ for spamming, because : ";
NSString* const REPORT_FOR_SPAM_RECIPIENT = @"info@gotribalnow.com";

@interface GTRActiveBudzProfileViewController () <GTRActiveBudzProfileConnectionDelegate,GTRActiveBudzProfileEndorsementDelegate, GTRCustomActionSheetHelperDelegate,MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) UIImageView *profileImageHeader;

@property ActiveBudzProfileType profileType;
@property (strong,nonatomic) UIScrollView *scrollView;
@property (strong,nonatomic) GTRActiveBudzProfileInformation *informationView;
@property (strong,nonatomic) GTRActiveBudzProfileConnection *connectionView;
@property (strong,nonatomic) GTRActiveBudzProfileRanking *rankingView;
@property (strong,nonatomic) GTRActiveBudzProfileEndorsement *endorseView;
@property (strong,nonatomic) NSIndexPath *indexPath;
@property (strong,nonatomic) GTRUserImageView *userImage;
@property (strong,nonatomic) UILabel *friendsCountLabel;
@property BOOL endorseStatus;
@property (strong,nonatomic) GTRCustomActionSheetHelper *actionSheetHelper;

@property int userRank;

@end

@implementation GTRActiveBudzProfileViewController
@synthesize delegate,rankingView, userImage, endorseView, actionSheetHelper;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = HEADER_TITLE;
    }
    return self;
}

-(id) initWithUser:(GTRUser*)theUser
         indexPath:(NSIndexPath*)theIndexPath
       connections:(NSArray*)theConnections
       profileType:(ActiveBudzProfileType)theProfileType
{
    self = [super init];
    if (self) {
        DDLogInfo(@"Attached user : %@",theUser);
        self.user = theUser;
        self.indexPath = theIndexPath;
        self.userConnections = theConnections;
        self.profileType = theProfileType;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [super.view setBackgroundColor:[UIColor colorWithWhite:0.941 alpha:1.000]];
    
    //set navigation bar left button
    if (self == self.navigationController.viewControllers.firstObject) {
        [GTRNavigationBarHelper setLeftSideBarButtonItem:self.navigationItem withTarget:self selector:@selector(toggleSideMenu:)];
    } else {
        [GTRNavigationBarHelper setBackButtonItem:self.navigationItem withTarget:self selector:@selector(backButtonAction:)];
    }
    
    //set navigation bar right buttons
    if (self.profileType == ActiveBudzProfileTypeConnected) {
        UIBarButtonItem *item0 = [GTRNavigationBarHelper createBarButtonItemWithTarget:self selector:@selector(messageButtonAction:) imageNamed:@"ico-menu-msg"];
        UIBarButtonItem *item1 = [GTRNavigationBarHelper createBarButtonItemWithTarget:self selector:@selector(moreButtonAction:) imageNamed:@"ico-nav-more"];
        [GTRNavigationBarHelper setRightBarButtonItems:self.navigationItem withItems:@[item0,item1] animated:NO];
    }
    
    self.userRank = [self.user userRank];
    self.endorseStatus = self.user.endorsed;
    DDLogInfo(@"rank : %i", self.userRank);
    if(self.user.endorsed) {
       DDLogInfo(@"endorsed : YES");
    } else {
        DDLogInfo(@"endorsed : NO");
    }
    //set the page appearance
    [self setAppearance];
    
    //set the actionsheet helper
    actionSheetHelper = [[GTRCustomActionSheetHelper alloc] initWithSuperViewController:self];
    actionSheetHelper.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Button actions

-(void)backButtonAction:(id)sender
{
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)moreButtonAction:(id)sender
{
    DDLogInfo(@"More button clicked!");
    [actionSheetHelper openCustomActionSheetWithMode:GTRCustomActionSheetModeActiveBudzProfile];
}

- (void)messageButtonAction:(id)sender
{
    GTRMessagesViewController *messagesViewController = [[GTRMessagesViewController alloc] initWithUser:self.user];
    [self.navigationController pushViewController:messagesViewController animated:YES];
}

- (void)connectButtonAction:(id)sender
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    [GTRActiveBudzRestService addActiveBudzRequestWithUser:self.user onSuccess:^(id responseObject) {
            [SVProgressHUD showSuccessWithStatus:@"Request sent!"];
            [self.navigationController popViewControllerAnimated:YES];
            
            if ([(id) delegate respondsToSelector:@selector(onRequestSent:)]){
                [delegate onRequestSent:self.indexPath];
            }
        
            //send analytics data
            [GTRAnalyticsHelper sendActiveBudzRequestToConnectEvent];
        
    } onFail:^(id responseObject, NSError *error) {
    }];
}

- (void)endorseButtonAction:(id)sender
{
    NSNumber *userID = self.user.userID;
    
    [GTRActiveBudzRestService endorseWithId:userID onSuccess:^(id responseObject) {
        
        int rank = [responseObject[@"tribal_rank"] intValue];
        [self endorseUserWithRank:rank];
    } onFail:nil];
}

- (void)endorseUserWithRank:(int)rank
{
    self.endorseStatus = NO;
    self.userRank=rank-1;
    [userImage setUserPictureWithURL:[NSURL URLWithString:self.user.avatarURL] badgeLevel:self.userRank badgePosition:Center];
    [rankingView setProfileRank:self.userRank];
    [rankingView resetAppearance];
    [endorseView hideEndorse];
}

#pragma mark Appearance methods

- (void)setAppearance
{
    //prepare the scrollview
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.scrollView];
    
    // set profile header
    CGFloat profileImageHeaderWidth = self.view.frame.size.width;
    CGFloat profileImageHeaderHeight = 140.0f;
    self.profileImageHeader = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, profileImageHeaderWidth, profileImageHeaderHeight)];
    self.profileImageHeader.image = [UIImage imageNamed:PROFILE_HEADER_IMAGE_NAME];
    self.profileImageHeader.contentMode = UIViewContentModeScaleToFill;
    [self.scrollView addSubview:self.profileImageHeader];
    
    //user picture
    CGFloat userPictureX = 20.0f;
    CGFloat userPictureY = 20.0f;
    CGFloat userPictureHeight = 66;
    CGFloat userPictureWidth = 66;
    
    userImage = [[GTRUserImageView alloc]initWithFrame:CGRectMake(userPictureX, userPictureY, userPictureHeight, userPictureWidth)];
    [userImage setUserPictureWithURL:[NSURL URLWithString:self.user.avatarURL] badgeLevel:self.userRank badgePosition:Center];
    
    userImage.userPicture.layer.cornerRadius = userPictureWidth/2;
    userImage.userPicture.layer.cornerRadius = userPictureWidth/2;
    userImage.userPicture.contentMode = UIViewContentModeScaleAspectFill;
    userImage.userPicture.layer.masksToBounds = YES;
    userImage.userPicture.backgroundColor = [UIColor clearColor];
    
    [self.profileImageHeader addSubview:userImage];
    
    //user name
    CGFloat userNameX = userPictureX + userPictureWidth + 15;
    CGFloat userNameY = userPictureY + 8;
    CGFloat userNameWidth = 200;
    CGFloat userNameHeight = 25;
    
    NSAttributedString *userNameString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", self.user.firstName, self.user.lastName]
                                                                   attributes:@{
                                                                                NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:18.0f],
                                                                                NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                                }];
    
    UILabel *userName = [[UILabel alloc] initWithFrame:CGRectMake(userNameX, userNameY, userNameWidth, userNameHeight)];
    userName.attributedText = userNameString;
    userName.backgroundColor = [UIColor clearColor];
    [self.profileImageHeader addSubview:userName];
    
    CGFloat locationIconX = userNameX;
    CGFloat locationIconY = userNameY + userNameHeight + 5;
    CGFloat locationIconHeight = -5; //placeholder
    
    GTRUserDetail *locationDetail = [[self.user.details filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"detailKey == %@", USER_DETAIL_LIVING_PLACE_KEY]] firstObject];
    NSString *locationDetailString;
    
    //sanity check, if the location detail returned from server is not an array
    if(locationDetail && [locationDetail.detailValue isKindOfClass:[NSArray class]])
    {
        locationDetailString = [((NSArray*)locationDetail.detailValue) firstObject];
    }
    
    if (locationDetailString) {
        
        //add the location icon
        UIImage *locationIconImage  = [UIImage imageNamed:LOCATION_ICON_NAME];
        locationIconHeight = locationIconImage.size.height;
        
        UIImageView *locationIcon = [[UIImageView alloc] initWithFrame:CGRectMake(locationIconX, locationIconY, locationIconImage.size.width, locationIconImage.size.height)];
        locationIcon.image = locationIconImage;
        locationIcon.backgroundColor = [UIColor clearColor];
        [self.profileImageHeader addSubview:locationIcon];

        //add the location string
        CGFloat userLocationX = locationIconX + locationIconImage.size.width + 2;
        CGFloat userLocationY = locationIconY;
        CGFloat userLocationWidth = 190;
        CGFloat userLocationHeight = 19;
        
        NSAttributedString *userLocationString = [[NSAttributedString alloc] initWithString:locationDetailString
                                                                                 attributes:@{
                                                                                              NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:16.0f],
                                                                                              NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                                              }];
        
        UILabel *userLocation = [[UILabel alloc] initWithFrame:CGRectMake(userLocationX, userLocationY, userLocationWidth, userLocationHeight)];
        userLocation.attributedText = userLocationString;
        userLocation.backgroundColor = [UIColor clearColor];
        [self.profileImageHeader addSubview:userLocation];
    }

    CGFloat pursuitImageXOffset = locationIconX;
    CGFloat pursuitImageY = locationIconY + locationIconHeight + 10;
    CGFloat pursuitImageWidth = 40;
    CGFloat pursuitImageHeight = 40;
    
    GTRUserDetail *pursuitDetail = [[self.user.details filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"detailKey == %@", USER_DETAIL_FITNESS_PURSUIT_KEY]] firstObject];
    
    if  (pursuitDetail) {
        
        for(NSString *pursuitNameBuff in ((NSArray*)pursuitDetail.detailValue)) {
            UIImage *pursuitImage;
            
            NSString *pursuitName = [pursuitNameBuff stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            if(![GTRPursuitHelper isCustomPursuit:pursuitName]) {
                pursuitImage = [UIImage imageNamed:[GTRPursuitHelper getPursuitIconName:pursuitName]];
            } else  if(![[pursuitName stringByReplacingOccurrencesOfString:@" " withString:@""]
                         isEqualToString:@""]){
                pursuitImage = [UIImage imageNamed:PURSUIT_OTHER_ICON_NAME];
            }
            
            UIImageView *pursuitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(pursuitImageXOffset, pursuitImageY, pursuitImageWidth, pursuitImageHeight)];
            pursuitImageView.image = pursuitImage;
            pursuitImageView.contentMode = UIViewContentModeCenter;
            pursuitImageView.backgroundColor = [UIColor clearColor];
            [self.profileImageHeader addSubview:pursuitImageView];
            
            pursuitImageXOffset += pursuitImageWidth + 5;
        }
    }
    

    
    //set information section
    UIFont *titleFont = [UIFont fontWithName:@"HelveticaNeue" size:14.0f];
    UIColor *titleColor = [UIColor colorWithWhite:0.373 alpha:1.000];
    
    CGFloat informationTitleX = 7;
    CGFloat informationTitleY = profileImageHeaderHeight + 10;
    CGFloat informationTitleHeight = 20;
    CGFloat informationTitleWidth = 100;
    
    NSAttributedString *informationTitleString = [[NSAttributedString alloc] initWithString:INFORMATION_TITLE_STRING
                                                                                 attributes:@{
                                                                                              NSFontAttributeName: titleFont,
                                                                                              NSForegroundColorAttributeName : titleColor
                                                                                              }];
    UILabel *informationTitle = [[UILabel alloc] initWithFrame:CGRectMake(informationTitleX, informationTitleY, informationTitleWidth, informationTitleHeight)];
    informationTitle.attributedText = informationTitleString;
    informationTitle.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:informationTitle];
    
    CGFloat informationViewY = informationTitleY + informationTitleHeight + 5;
    CGFloat informationViewHorizontalPadding = 5;
    CGFloat informationViewHeight = 150.0f;
    CGFloat informationViewWidth = self.view.frame.size.width - (informationViewHorizontalPadding)*2;
    
    self.informationView = [[GTRActiveBudzProfileInformation alloc] initWithFrame:CGRectMake(informationViewHorizontalPadding, informationViewY, informationViewWidth, informationViewHeight) User:self.user];
    [self.scrollView addSubview:self.informationView];

    //set type-specific sections
    if (self.profileType == ActiveBudzProfileTypeCurrentUser || self.profileType == ActiveBudzProfileTypeConnected) {
        
        //set connection section
        
        CGFloat connectionTitleX = informationTitleX;
        CGFloat connectionTitleY = informationViewY + informationViewHeight + 10;
        CGFloat connectionTitleHeight = 20;
        CGFloat connectionTitleWidth = 100;
        CGFloat friendsCountLabelX = 320 - connectionTitleX - connectionTitleWidth;

        NSAttributedString *connectionTitleString = [[NSAttributedString alloc] initWithString:CONNECTION_TITLE_STRING
                                                                                     attributes:@{
                                                                                                  NSFontAttributeName: titleFont,
                                                                                                  NSForegroundColorAttributeName : titleColor
                                                                                                  }];
        
        UILabel *connectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(connectionTitleX, connectionTitleY, connectionTitleWidth, connectionTitleHeight)];
        connectionTitle.attributedText = connectionTitleString;
        connectionTitle.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:connectionTitle];
        
        NSAttributedString *friendsCount = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d", self.user.friendsCount]
                                                                           attributes:@{
                                                                                        NSFontAttributeName: titleFont,
                                                                                        NSForegroundColorAttributeName : GTR_TURQUOISE_COLOR
                                                                                        }];
        
        self.friendsCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(friendsCountLabelX, connectionTitleY, connectionTitleWidth, connectionTitleHeight)];
        self.friendsCountLabel.textAlignment = NSTextAlignmentRight;
        self.friendsCountLabel.attributedText = friendsCount;
        self.friendsCountLabel.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:self.friendsCountLabel];
        
        CGFloat connectionViewY = connectionTitleY + connectionTitleHeight + 5;
        CGFloat connectionViewX = informationViewHorizontalPadding;
        CGFloat connectionViewHeight = 77.0f;
        CGFloat connectionViewWidth = informationViewWidth;
        
        self.connectionView = [[GTRActiveBudzProfileConnection alloc] initWithFrame:CGRectMake(connectionViewX, connectionViewY, connectionViewWidth, connectionViewHeight) connections:self.userConnections];
        self.connectionView.delegate = self;
        [self.scrollView addSubview:self.connectionView];
        
        CGFloat rankingTitleX = connectionTitleX;
        CGFloat rankingTitleY = connectionViewY + connectionViewHeight + 10;
        CGFloat rankingTitleHeight = 20;
        CGFloat rankingTitleWidth = 100;
        
        //set ranking section
        NSAttributedString *rankingTitleString = [[NSAttributedString alloc] initWithString:RANKING_TITLE_STRING
                                                                                 attributes:@{
                                                                                              NSFontAttributeName: titleFont,
                                                                                              NSForegroundColorAttributeName : titleColor
                                                                                              }];
        UILabel *rankingLabel = [[UILabel alloc] initWithFrame:CGRectMake(rankingTitleX, rankingTitleY, rankingTitleWidth, rankingTitleHeight)];
        rankingLabel.attributedText = rankingTitleString;
        rankingLabel.backgroundColor = [UIColor clearColor];
        
        [self.scrollView addSubview:rankingLabel];
        
        CGFloat rankingViewX = informationViewHorizontalPadding;
        CGFloat rankingViewY = rankingTitleY + rankingTitleHeight + 10;
        CGFloat rankingViewHeight = 125.0f;
        CGFloat rankingViewWidth = informationViewWidth;

        rankingView = [[GTRActiveBudzProfileRanking alloc] initWithFrame:CGRectMake(rankingViewX, rankingViewY, rankingViewWidth, rankingViewHeight) profileRank:self.userRank];
        [self.scrollView addSubview:rankingView];
        
        //if shown user is a connection, not endorsed yet, and rank is less than 3 (2, by enum)
        if (self.profileType == ActiveBudzProfileTypeConnected && self.endorseStatus == NO && self.userRank < 2) {
            
            //set endorsement section
            CGFloat endorseX = informationTitleX;
            CGFloat endorseY = rankingViewY + rankingViewHeight + 10;
            CGFloat endorseWidth = informationViewWidth;
            CGFloat endorseHeight = 88;

            endorseView = [[GTRActiveBudzProfileEndorsement alloc]initWithFrame:CGRectMake(endorseX, endorseY, endorseWidth, endorseHeight)
                                                                       userName:self.user.firstName];
            endorseView.delegate = self;
            [self.scrollView addSubview:endorseView];
     
            //set scrollview size
            self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, endorseY+endorseHeight + 20);
            
        } else {
            
            //set scrollview size
            self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, rankingViewY+rankingViewHeight+20);
        }
        
    } else { //if not connected
        
        //set connect section
        CGFloat connectButtonHorizontalPadding = 15;
        CGFloat connectButtonY = informationViewY + informationViewHeight + 48;
        CGFloat connectButtonHeight = 48.0f;
        CGFloat connectButtonWidth = self.view.frame.size.width - (connectButtonHorizontalPadding)*2;
        
        NSAttributedString *connectButtonString = [[NSAttributedString alloc] initWithString:CONNECT_BUTTON_STRING
                                                                                 attributes:@{
                                                                                              NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:18.0f],
                                                                                              NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                                              }];
        UIButton *connectButton = [[UIButton alloc] initWithFrame:CGRectMake(connectButtonHorizontalPadding, connectButtonY, connectButtonWidth, connectButtonHeight)];
        connectButton.backgroundColor = [UIColor colorWithRed:0.114 green:0.525 blue:0.541 alpha:1.000];
        [connectButton setAttributedTitle:connectButtonString forState:UIControlStateNormal];
        connectButton.layer.cornerRadius = 5.0f;
        [connectButton addTarget:self action:@selector(connectButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [connectButton setEnabled:YES];
        [self.scrollView addSubview:connectButton];

        //set scrollview size
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, connectButtonY+connectButtonHeight+10);
    }
}

-(void) refresh
{
    //refresh header
    [self refreshHeader];
    
    //refresh information
    [self refreshInformation];
    
    //refresh connection
    [self refreshConnection];

    //refresh rank
    [self refreshRank];
}

-(void)refreshHeader
{
    //assumption : the only thing that need to be refreshed is the tribe rank, because everything else must be the same -
    //since when the user edited his/her own profile, the current user data that saved on the system must be the same on the server,
    //and the only case this one needs to be refreshed is when other user is endorsing this user.
    
    //refresh the user image (with rank)
    self.userRank = [self.user userRank];
    [userImage setUserPictureWithURL:[NSURL URLWithString:self.user.avatarURL] badgeLevel:self.userRank badgePosition:Center];
}

-(void)refreshInformation
{
    [self.informationView setUserData:self.user];
    [self.informationView resetAppearance];
}

-(void)refreshConnection
{
    [self.connectionView setConnections:self.userConnections];
    [self.connectionView resetAppearance];
    
    UIFont *titleFont = [UIFont fontWithName:@"HelveticaNeue" size:14.0f];
    
    NSAttributedString *friendsCount = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d", self.user.friendsCount]
                                                                       attributes:@{
                                                                                    NSFontAttributeName: titleFont,
                                                                                    NSForegroundColorAttributeName : GTR_TURQUOISE_COLOR
                                                                                    }];
    self.friendsCountLabel.attributedText = friendsCount;
}

-(void)refreshRank
{
    self.userRank = [self.user userRank];
    [self.rankingView setProfileRank:self.userRank];
    [self.rankingView resetAppearance];
}

#pragma mark - GTRActiveBudzProfileConnection delegate
-(void)onUserSelected:(GTRUser *)theUser
{
    __weak GTRActiveBudzProfileViewController *weakSelf = self;
    
    [GTRActiveBudzHelper pushActiveBudzProfile:theUser indexPath:nil rootViewController:weakSelf onSuccess:^(id responseObject) {
        
    } onFail:^(id responseObject, NSError *error) {
        
    }];
}

#pragma mark - GTRCustomActionSheetHelper delegates

-(void)deleteAction
{

}

-(void)reportForSpamAction
{
    if([MFMailComposeViewController canSendMail]){
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setSubject:REPORT_FOR_SPAM_SUBJECT];
        [mailViewController setMessageBody:[NSString stringWithFormat:REPORT_FOR_SPAM_BODY, self.user.fullName] isHTML:NO];
        [mailViewController setToRecipients:@[REPORT_FOR_SPAM_RECIPIENT]];
        [self presentViewController:mailViewController animated:YES completion:nil];
    }else {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"You must have a mail account in order to send an email",nil)];
    }
}

#pragma mark - MFMailCompose delegates
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            DDLogInfo(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            DDLogInfo(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            DDLogInfo(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            DDLogInfo(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
