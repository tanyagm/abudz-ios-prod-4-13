//
//  GTRSettingsCustomRightView.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/9/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRSettingsCell.h"

typedef enum{
    GTRSettingsNotificationsModePhone = 0,
    GTRSettingsNotificationsModeEMail,
    GTRSettingsNotificationsModeBoth,
    GTRSettingsNotificationsModeNone
}GTRSettingsNotificationsMode;

typedef enum {
    GTRSettingsNotificationsFrequencyImmediately = 0,
    GTRSettingsNotificationsFrequencyDaily,
    GTRSettingsNotificationsFrequencyWeekly,
    GTRSettingsNotificationsFrequencyNever
}GTRSettingsNotificationsFrequency;

typedef enum {
    GTRSettingsSocialStatusOff = 0,
    GTRSettingsSocialStatusOn = 1
}GTRSettingsSocialStatus;


@class GTRSettingsCustomRightView;

@protocol GTRSettingsCustomRightViewDelegate <NSObject>

@optional
-(void)updateSettingDetailForMode:(NSArray*)theMode inCell:(NSInteger)cell;
-(void)updateSettingDetailForFreq:(NSString*)theFreq inCell:(NSInteger)cellRow;
-(void)customRightView:(GTRSettingsCustomRightView*)view
     notificationsMode:(GTRSettingsNotificationsMode)mode
                inCell:(GTRSettingsCell*)cell;

-(void)customRightView:(GTRSettingsCustomRightView*)view
notificationsFrequency:(GTRSettingsNotificationsFrequency)mode
                inCell:(GTRSettingsCell*)cell;

-(void)customRightView:(GTRSettingsCustomRightView*)view
         twitterStatus:(GTRSettingsSocialStatus)status;

-(void)customRightView:(GTRSettingsCustomRightView*)view
        facebookStatus:(GTRSettingsSocialStatus)status;

@end

@interface GTRSettingsCustomRightView : UIView

@property(nonatomic,weak) id <GTRSettingsCustomRightViewDelegate> delegate;

- (id)initInCell:(GTRSettingsCell*)cell
           frame:(CGRect)frame
    withDelegate:(id<GTRSettingsCustomRightViewDelegate>)delegate;

-(void)setPhoneButtonStatus:(BOOL)phoneStatus emailStatus:(BOOL)emailStatus;

-(void)setFrequency:(GTRSettingsNotificationsFrequency)frequency;

-(void)setOn:(BOOL)on;

@end
