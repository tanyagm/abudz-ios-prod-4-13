//
//  GTRInviteContactsViewController.m
//  gotribal
//
//  Created by Muhammad Taufik on 12/24/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRInviteContactsViewController.h"
#import "GTRFriendsTableViewCell.h"
#import <RHAddressBook/AddressBook.h>
#import "SVProgressHUD.h"
#import "GTRTribe.h"
#import "GTRRestServiceErrorHandler.h"

/*
 *  RHPerson+PersonInvited category
 */
#import <objc/runtime.h>

@interface RHPerson (PersonInvited)
@property (nonatomic) BOOL invited;
@end

static char const * const kPersonInvitedKey = "PersonInvited";

@implementation RHPerson (PersonInvited)

-(BOOL)invited
{
    NSNumber *number = objc_getAssociatedObject(self, &kPersonInvitedKey);
    return [number boolValue];
}

-(void)setInvited:(BOOL)invited
{
    objc_setAssociatedObject(self, &kPersonInvitedKey, @(invited), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
@end


@interface GTRInviteContactsViewController ()
@property (nonatomic, strong) NSMutableArray *people;
@property (nonatomic, strong) RHAddressBook *addressBook;
@property (nonatomic, strong) NSMutableArray *invitedPeople;
@property (nonatomic, copy) GTRTribe *tribe;
@end

@implementation GTRInviteContactsViewController

-(id)initWithTribe:(GTRTribe *)tribe
{
    self = [super init];
    if (self) {
        self.tribe = tribe;
        self.title = @"Contacts";
        self.tableView.separatorColor = [UIColor clearColor];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *invitedPeopleArray = nil;
    if (self.tribe) {
        invitedPeopleArray = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"invitedPeopleWithTribe-%@", self.tribe.tribeID]];
    } else {
        invitedPeopleArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"invitedPeople"];
    }

    self.invitedPeople = [NSMutableArray arrayWithArray:invitedPeopleArray];
    
    self.view.backgroundColor = GTR_MAIN_ACTIVITIES_BACKGROUND_COLOR;
    
    [GTRNavigationBarHelper setBackButtonItem:self.navigationItem withTarget:self.navigationController selector:@selector(popViewControllerAnimated:)];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        RHAddressBook *addressBook = [[RHAddressBook alloc] init];
        if ([RHAddressBook authorizationStatus] == RHAuthorizationStatusNotDetermined) {
            [addressBook requestAuthorizationWithCompletion:^(bool granted, NSError *error) {
                if (granted) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.addressBook = addressBook;
                        [self loadContacts];
                    });
                }
            }];
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.addressBook = addressBook;
                [self loadContacts];
            });
        }
    });
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)loadContacts
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {

        NSArray *allPeople = [self.addressBook peopleOrderedByUsersPreference];
        
        NSMutableArray *people = [NSMutableArray array];
        
        for (RHPerson *person in allPeople) {
            if (person.emails.count) [people addObject:person];
        }
        
        [self filterInvitedPeopleWithArray:people];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [SVProgressHUD dismiss];
        });
    });
        
}

-(void)filterInvitedPeopleWithArray:(NSArray *)people
{
    if (!self.people) self.people = [NSMutableArray array];
    [self.people addObjectsFromArray:people];
    
    for (NSString *invitedPersonEmail in self.invitedPeople) {
        for (RHPerson *person in self.people) {
            NSString *personEmail = [person.emails valueAtIndex:0];
            if ([personEmail isEqual:invitedPersonEmail]) {
                person.invited = YES;
            }
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 34 + 10 + 10;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.people.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ContactCellIdentifier";
    GTRFriendsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[GTRFriendsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    RHPerson *person = [self.people objectAtIndex:indexPath.row];
    cell.textLabel.text = person.name;
    
    if ([person hasImage]) {
        cell.imageView.image = person.thumbnail;
    } else {
        cell.imageView.image = DEFAULT_AVATAR;
    }
    
    [cell.inviteButton addTarget:self action:@selector(inviteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
    cell.invited = person.invited;
    
    return cell;
}

#pragma mark - Invite button

-(void)inviteButtonTapped:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath) {
        [self sendInviteRequestForIndexPath:indexPath];
    }
}

#pragma mark - Invite request

-(void)sendInviteRequestForIndexPath:(NSIndexPath *)indexPath
{
    RHPerson *person = [self.people objectAtIndex:indexPath.row];
    
    if (person.invited) return;
    
    NSString *emailAddress = [person.emails valueAtIndex:0];
    NSDictionary *parameters = @{
                                 @"platform_type": @"email",
                                 @"email" : emailAddress,
                                 };
    
    NSString *endPoint = @"/api/v1/invites";
    if (self.tribe) endPoint = [NSString stringWithFormat:@"/api/v1/tribes/%@/invites", self.tribe.tribeID];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager POST:endPoint parameters:parameters success:^(AFHTTPRequestOperation *operation, id json) {
        DDLogInfo(@"%@", json);
        
        RHPerson *invitedPerson = [self.people objectAtIndex:indexPath.row];
        invitedPerson.invited = YES;
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        NSString *invitedPeopleEmailAddress = [invitedPerson.emails valueAtIndex:0];
        if (!self.invitedPeople) self.invitedPeople = [NSMutableArray array];
        [self.invitedPeople addObject:invitedPeopleEmailAddress];
        
        if (self.tribe) {
            [[NSUserDefaults standardUserDefaults] setObject:self.invitedPeople forKey:[NSString stringWithFormat:@"invitedPeopleWithTribe-%@", self.tribe.tribeID]];
        } else {
            [[NSUserDefaults standardUserDefaults] setObject:self.invitedPeople forKey:@"invitedPeople"];
        }
        
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Invitation sent!", @"invite sent")];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"%@", error);

        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
    }];
}

@end
