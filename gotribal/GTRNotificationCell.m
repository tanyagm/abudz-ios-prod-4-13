//
//  GTRNotificationCell.m
//  
//
//  Created by Sambya Aryasa on 12/16/13.
//
//

#import "GTRNotificationCell.h"

@implementation GTRNotificationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    //custom size for IOS 6
    int leftPadding = 0;
    int bottomPadding = 0;
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")){
        leftPadding = 5;
        bottomPadding = 1;
    }
    
    if (self) {
        self.rightView = nil;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIColor *whiteColor = [UIColor whiteColor];
        self.backgroundColor = whiteColor;
        self.accessoryType = UITableViewCellAccessoryNone;
        
        // avatar
        self.avatarImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 15, 46, 46)];
        self.avatarImageView.backgroundColor = [UIColor whiteColor];
        self.avatarImageView.layer.cornerRadius = 23;
        self.avatarImageView.layer.borderColor = AVATAR_BORDER_COLOR.CGColor;
        self.avatarImageView.layer.masksToBounds = YES;
        self.avatarImageView.layer.borderWidth = 0.1;
        [self.contentView addSubview:self.avatarImageView];
        
        // text
        self.text = super.text;
        self.text.backgroundColor = [UIColor clearColor];
        self.text.frame = CGRectMake(5 + self.avatarImageView.frame.size.width + 13, 14,CGRectGetWidth(self.text.frame) + 100 - leftPadding - 30 - 93, 20);
        self.text.textColor = NOTIFICATIONS_CELL_TEXT_COLOR;
        self.text.font = NOTIFICATIONS_CELL_TEXT_FONT;
        
        // subtext
        CGRect subtextRect = CGRectOffset(self.text.frame,0,25);
        self.subtext = [[UILabel alloc]initWithFrame:subtextRect];
        self.subtext.frame = CGRectMake(subtextRect.origin.x, subtextRect.origin.y, 240,40);
        self.subtext.backgroundColor = [UIColor clearColor];
        self.subtext.font = NOTIFICATIONS_CELL_SUBTEXT_FONT;
        self.subtext.textColor = NOTIFICATIONS_CELL_SUBTEXT_COLOR;
        self.subtext.textAlignment = NSTextAlignmentLeft;
        self.subtext.lineBreakMode = NSLineBreakByWordWrapping;
        self.subtext.numberOfLines = 2;
        [self.contentView addSubview:self.subtext];
    }
    
    return self;
}

@end
