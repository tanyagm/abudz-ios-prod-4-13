//
//  GTRSideMenuHelper.h
//  gotribal
//
//  Created by Hisma Mulya S on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRSideMenuCell.h"
#import "GTRAppDelegate.h"

#define LEFT_PADDING 10

@interface GTRSideMenuHelper : NSObject

+(void)setAvatarImage:(UIImage*)avatarImage
                 text:(NSString*)string
               inCell:(GTRSideMenuCell*)cell;

+(UIView*)createHeaderViewWithText:(NSString*)text
                    withImageCache:(NSMutableDictionary*)sideBarImageCache;

+(void)setAvatarImageURL:(NSURL*)url
         withPlaceholder:(UIImage*)placeHolderImage
                    text:(NSString*)string
                  inCell:(GTRSideMenuCell*)cell;

+(GTRAppDelegate*)appDelegate;

+(void)setNotificationBadgeWithNumber:(NSInteger)number inCell:(GTRSideMenuCell*)cell;

+(void)hideNotificationBadgeInCell:(GTRSideMenuCell*)cell;
@end
