//
//  GTRMyActiveBudzListViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRMyActiveBudzViewController.h"
#import "UIImageView+AFNetworking.h"
#import "AFOAuth2Client.h"
#import "GTRUser.h"
#import "GTRUserDetail.h"
#import "GTRUserHelper.h"
#import "GTRConstants.h"
#import "GTRPaginationHelper.h"
#import "SVProgressHUD.h"
#import "GTRActiveBudzProfileViewController.h"
#import "NSMutableArray+AddMantleObject.h"
#import "GTRActiveBudzRestService.h"
#import "GTRActiveBudzHelper.h"

NSString* const MAX_ID_KEY = @"max_id";
NSString* const MIN_ID_KEY = @"min_id";

@interface GTRMyActiveBudzViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong,nonatomic) NSDictionary *paginationData;

@end

@implementation GTRMyActiveBudzViewController



#pragma mark - Get pagination data
- (int)getPaginationMaxID
{
    int maxid = -1;
    NSNumber *maxIDNumber = ((NSNumber*)[self.paginationData objectForKey:MAX_ID_KEY]);
    if (maxIDNumber && maxIDNumber != ((NSNumber*)[NSNull null])) {
        maxid  = [maxIDNumber intValue];
    }
    
    return maxid;
}

- (int)getPaginationMinID
{
    int minid = -1;
    NSNumber *minIDNumber = ((NSNumber*)[self.paginationData objectForKey:MIN_ID_KEY]);
    if (minIDNumber && minIDNumber != ((NSNumber*)[NSNull null])) {
        minid  = [minIDNumber intValue];
    }
    
    return minid;
}

#pragma mark - UITableView delegates

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    GTRUser *user = ((GTRUser*)[self.activeBudzArray objectAtIndex:indexPath.row]);

    [GTRActiveBudzRestService removeActiveBudzWithUser:user onSuccess:^(id responseObject) {
        
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Activebudz removed.", @"activebudz removed")];
        
        //if succeed, remove from the list
        [self removePersonFromList:indexPath];
        
    } onFail:^(id responseObject, NSError *error) {
    }];
}

- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    //push the respective user profile
    GTRUser *user = ((GTRUser*)[self.activeBudzArray objectAtIndex:indexPath.row]);
    
    [GTRActiveBudzHelper pushActiveBudzProfile:user indexPath:nil rootViewController:self onSuccess:^(id responseObject) {
        
    } onFail:^(id responseObject, NSError *error) {
        
    }];
    
    //de-select the cell image
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

#pragma mark - Overriden other superclass methods

- (void)initiateActiveBudzArrayWithCompletion:(void (^)(void))completion
{
    if (!completion) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    }
    
    //retrieve the cache
    self.activeBudzArray = [[NSMutableArray alloc] initWithArray:[GTRActiveBudzHelper getArchivedMyActiveBudz]];
    [self.tableView reloadData];
    
    [GTRActiveBudzRestService retrieveMyActiveBudz:^(id responseObject) {
        if (self.activeBudzArray.count > 0) {
            [self.activeBudzArray removeAllObjects];
        } else {
            self.activeBudzArray = [[NSMutableArray alloc] init];
        }

        NSArray *users = ((NSArray*) [((NSDictionary*)responseObject) objectForKey:@"data"]);
        self.paginationData = ((NSDictionary*) [((NSDictionary*)responseObject) objectForKey:@"pagination"]);

        [self.activeBudzArray addObjectsFromArray:users modelOfClass:[GTRUser class]];
        [GTRActiveBudzHelper archiveMyActiveBudz:[self.activeBudzArray copy]];
        [self.tableView reloadData];

        if (completion) {
            completion();
        } else {
            [SVProgressHUD dismiss];
        }
        
    } onFail:^(id responseObject, NSError *error) {
        if (completion) {
            completion();
        }
    }];
    
}

-(void)retrieveNextActiveBudzListCompletion:(void (^) (void))completion
{
    
    //offset starts at 0, get as much as NUMBER_OF_ACTIVEBUDZ_DATA_RETRIEVED
    
    [GTRActiveBudzRestService retrieveMyActiveBudzWithMaxID:[NSNumber numberWithInt:[self getPaginationMaxID]] onSuccess:^(id responseObject) {
        
        NSInteger count = self.activeBudzArray.count;
        
        [self.activeBudzArray addObjectsFromArray:(NSArray*)[((NSDictionary*)responseObject) objectForKey:@"data"]
                                     modelOfClass:[GTRUser class]];
        [GTRActiveBudzHelper archiveMyActiveBudz:[self.activeBudzArray copy]];
        
        self.paginationData = (NSDictionary*)[((NSDictionary*)responseObject) objectForKey:@"pagination"];
        
        if (count < self.activeBudzArray.count) {
            [self.tableView reloadData];
        }
        
        if (completion) {
            completion();
        }

    } onFail:^(id responseObject, NSError *error) {
        
        if (completion) {
            completion();
        }

    }];
    
}


@end
