//
//  GTRUserImageView.m
//  gotribal
//
//  Created by loaner on 12/18/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRUserImageView.h"
#import "UIImageView+AFNetworking.h"
#import "GTRActiveBudzProfileRanking.h"

NSString* const TRIBE_BADGE_LEVEL_ONE_ICON_NAME = @"ico-badge-i";
NSString* const TRIBE_BADGE_LEVEL_TWO_ICON_NAME = @"ico-badge-ii";
NSString* const TRIBE_BADGE_LEVEL_THREE_ICON_NAME = @"ico-badge-iii";

@interface GTRUserImageView()
@property (strong,nonatomic) NSURL * imageURL;

@property int userRank;
@property position position;

@end

@implementation GTRUserImageView
@synthesize userPicture, badgePicture;

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        userPicture = [[UIImageView alloc]init];
        badgePicture = [[UIImageView alloc]init];
    }
    return self;
}

-(void)setUserPictureWithURL:(NSURL *)imageURL badgeLevel:(int)userRank badgePosition:(position)badgePosition
{
    userPicture.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    
    if (imageURL) {
        [userPicture setImageWithURL:imageURL placeholderImage:DEFAULT_AVATAR];
    } else {
        [userPicture setImage:DEFAULT_AVATAR];
    }
    
    [self addSubview:userPicture];
    
    CGFloat badgeX = 0;
    if(badgePosition == Center) {
        badgeX = 13;
    }
    else if(badgePosition == Left) {
        badgeX = -10;
    }
    
    //in case the badge picture already attached (when refreshed)
    [badgePicture removeFromSuperview];
    
    badgePicture.frame = CGRectMake(badgeX, self.frame.size.height - 12, 6 * self.frame.size.width / 10 , self.frame.size.height / 2 );
    badgePicture.contentMode = UIViewContentModeScaleAspectFit;
    badgePicture.backgroundColor = [UIColor clearColor];
    
    switch (userRank) {
            
        case ActiveBudzProfileRankOne:
        {
            [badgePicture setImage:[UIImage imageNamed:TRIBE_BADGE_LEVEL_ONE_ICON_NAME]];
        }
            break;
            
        case ActiveBudzProfileRankTwo:
        {
            [badgePicture setImage:[UIImage imageNamed:TRIBE_BADGE_LEVEL_TWO_ICON_NAME]];
        }
            break;
            
        case ActiveBudzProfileRankThree:
        {
            [badgePicture setImage:[UIImage imageNamed:TRIBE_BADGE_LEVEL_THREE_ICON_NAME]];
        }
            break;
        default:
            break;
    }
    
    [self addSubview:badgePicture];
}

@end
