//
//  GTRActiveBudzListViewController.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/6/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTRActiveBudzListViewController : GTRViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong,nonatomic) NSMutableArray *activeBudzArray;
@property (strong,nonatomic) UITableView *tableView;

- (void)removePersonFromList:(NSIndexPath*)theIndexPath;
- (void)initiateActiveBudzArrayWithCompletion:(void (^)(void))completion;
- (void)retrieveNextActiveBudzListCompletion:(void (^)(void))completion;
- (int)getCellHeight;

@end
