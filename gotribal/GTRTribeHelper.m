//
//  GTRTribeHelper.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/17/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRTribeHelper.h"
#import "GTRSideMenuCell.h"

@implementation GTRTribeHelper

+(void)setAvatarImage:(UIImage*)avatarImage
                 text:(NSString*)string
               inCell:(GTRSideMenuCell*)cell
{
    cell.avatarImageView.image = avatarImage;
    CGFloat avatarHeight = avatarImage.size.height > 30 ? 30: avatarImage.size.height;
    
    cell.avatarImageView.frame = CGRectMake(10, (31 - avatarHeight / 2.0),
                                            avatarImage.size.width,
                                            avatarHeight);
    cell.text.text = string;
    cell.text.frame = CGRectMake(60, cell.avatarImageView.frame.origin.y + (avatarHeight/2.0 - 15 ), 120, 30);
    
    cell.lineImageView.frame = CGRectMake(10, 60.5, 310, 1);
    cell.lineImageView.alpha = 0.1;
}

@end
