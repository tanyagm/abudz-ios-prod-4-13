//
//  GTRSurveyData.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/21/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSurveyQuestionData.h"

@implementation GTRSurveyQuestionData

-(id) init
{
    self = [self initWithQuestionType:SingleSelectionAnswer
                       questionImages:nil
                      questionAnswers:nil
                         answerImages:nil
                  referenceDictionary:nil
                        referenceName:nil];
    return self;
    
}

-(id) initWithQuestionType:(SurveyQuestionType)theQuestionType
            questionImages:(NSArray*)theQuestionImages
           questionAnswers:(NSArray*) theQuestionAnswers
              answerImages:(NSArray*) theAnserImages
       referenceDictionary:(NSMutableDictionary*)theDictionary
             referenceName:(NSString*)theName  
{
    self = [super init];
    self.questionType = theQuestionType;
    self.questionImages = theQuestionImages;
    self.questionAnswers = theQuestionAnswers;
    self.answerImages = theAnserImages;
    self.referenceDictionary = theDictionary;
    self.referenceName = theName;
    return self;
}



@end
