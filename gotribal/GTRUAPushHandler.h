//
//  GTRUAPushHandler.h
//  gotribal
//
//  Created by Muhammad Taufik on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UAPush.h"

@class GTRUAPushHandler;

@protocol GTRUAPushHandlerDelegate <NSObject>
@optional
-(void)pushHandler:(GTRUAPushHandler *)pushHandler openConversationWithID:(NSNumber *)conversationID;
-(void)pushHandler:(GTRUAPushHandler *)pushHandler openNotificationScreenWithFriendID:(NSNumber *)friendID;
-(void)pushHandler:(GTRUAPushHandler *)pushHandler openFriendProfileWithFriendID:(NSNumber *)friendID;
@end

@interface GTRUAPushHandler : NSObject <UAPushNotificationDelegate>
@property (nonatomic, weak) id<GTRUAPushHandlerDelegate> delegate;
-(id)initWithDelegate:(id<GTRUAPushHandlerDelegate>)delegate;
@end
