//
//  GTRSessionRestService.m
//  gotribal
//
//  Created by Le Vady on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSessionRestService.h"
#import "AFHTTPRequestOperationManager+GTRForceLogout.h"
#import "GTRRestServiceErrorHandler.h"

@implementation GTRSessionRestService

+(void)login:(NSDictionary *)params onSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager POST:USER_LOGIN_ENDPOINT parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self saveOAuthCredential:[responseObject valueForKey:@"access_token"]];
        [GTRUserHelper updateCurrentUser:[responseObject valueForKey:@"user"]];
        
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}

+(void)logoutOnSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager DELETE:USER_LOGOUT_ENDPOINT parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [GTRUserHelper removeUserCredentials];
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    } forceLogoutOnUnauthorized:NO];
}

+(void)saveOAuthCredential:(NSDictionary *)accessToken
{
    NSString *refreshToken = [accessToken valueForKey:@"refresh_token"];
    AFOAuthCredential *credential = [AFOAuthCredential credentialWithOAuthToken:[accessToken valueForKey:@"access_token"]
                                                                      tokenType:[accessToken valueForKey:@"token_type"]];
    NSDate *expireDate = nil;
    id expiresIn = [accessToken valueForKey:@"expires_in"];
    if (expiresIn != nil && ![expiresIn isEqual:[NSNull null]]) {
        expireDate = [NSDate dateWithTimeIntervalSinceNow:[expiresIn integerValue]];
    }
    
    [credential setRefreshToken:refreshToken expiration:expireDate];
    
    [AFOAuthCredential storeCredential:credential withIdentifier:[[NSURL URLWithString:BASE_URL] host]];
}

+(void)facebookLogin:(NSDictionary *)params onSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager POST:USER_FACEBOOK_LOGIN_ENDPOINT parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) success(operation, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation.responseObject, error);
    }];
}

@end
