//
//  GTRTribeError.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/18/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRTribeError.h"
#import "GTRTribeErrorView.h"

NSInteger tag = 950;

@interface GTRTribeError ()

@property(nonatomic,strong) UIView *thisSuperview;
@property(nonatomic,strong) GTRTribeErrorView *errorView;

@end

@implementation GTRTribeError
@synthesize errorView = _errorView;
@synthesize shown = _shown;

+(GTRTribeError*)sharedInstance
{   static GTRTribeError *tribeError;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tribeError = [[self alloc]init];
        tribeError.shown = NO;
    });
    
    return tribeError;
}

-(void)showInView:(UIView*)superView withTitle:(NSString*)title message:(NSString*)message
{
    _thisSuperview = superView;
    [[superView viewWithTag:tag] removeFromSuperview];

    _errorView = [[GTRTribeErrorView alloc] initWithFrame:CGRectMake(0, -75, 320, 75)
                                                                 withTarget:self
                                                             dismissSelctor:@selector(dismiss)];
    _errorView.titleLabel.text = title;
    _errorView.messageLabel.text = message;
    _errorView.tag = tag;
    [superView addSubview:_errorView];
    
    [UIView animateWithDuration:0.4 animations:^{
        _errorView.frame = CGRectMake(0, 0, 320, 75);
    } completion:^(BOOL finished) {
        _shown = YES;
        if ([self.delegate respondsToSelector:@selector(didShowErrorView:)]) {
            [self.delegate didShowErrorView:_errorView];
        }
    }];
}

-(void)dismiss
{
    GTRTribeErrorView *errorView = (GTRTribeErrorView*)[self.thisSuperview viewWithTag:tag];
    [UIView animateWithDuration:0.4 animations:^{
        errorView.frame = CGRectMake(0, -75, 320, 75);
        errorView.alpha = 0.0;
    } completion:^(BOOL finished) {
        _shown = NO;
        if ([self.delegate respondsToSelector:@selector(didShowErrorView:)]) {
            [self.delegate didDismissErrorView:_errorView];
        }
        
        [errorView removeFromSuperview];
    }];
}

-(void)dealloc
{
    self.thisSuperview = nil;
}

@end
