//
//  GTRNotificationItemCell.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/16/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRNotificationCell.h"
#import "GTRNotificationItem.h"

extern NSInteger const NOTIFICATION_CELL_HEIGHT;

@interface GTRNotificationItemCell : GTRNotificationCell
- (void) setContent:(GTRNotificationItem*) inpNotificationItem;
@end
