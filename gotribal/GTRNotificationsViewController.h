//
//  GTRNotificationsViewController.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/3/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTRNotificationsViewController : GTRViewController <UITableViewDelegate, UITableViewDataSource>

@end
