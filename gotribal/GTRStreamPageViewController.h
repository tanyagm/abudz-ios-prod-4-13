//
//  GTRStreamPageViewController.h
//  gotribal
//
//  Provides the bare bone of a timeline-like page.
//
//  Created by Ricardo Pramana Suranta on 12/10/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRViewController.h"

typedef enum{
    GTRStreamPageReloadModeRefresh = 0,
    GTRStreamPageReloadModeLoadMore
}GTRStreamPageReloadMode;

@interface GTRStreamPageViewController : GTRViewController

@property (strong, nonatomic) UITableView *streamTableView;
@property (strong, nonatomic) NSMutableArray *streamPostArray;
@property (strong, nonatomic) NSMutableArray *actionSheetButtons;
@property NSIndexPath *currentSelectedPost;

-(void)setPlaceHolderText:(NSString*)aString;
-(void)setPlaceHolderAttributedText:(NSAttributedString*)anAttributedString;
-(void)togglePlaceholderLabelVisibility;

-(void)initiateReloadFunctionality;
-(void)removePostOnIndex:(NSInteger)indexRow;

#pragma mark - Button actions
#pragma mark New Post button actions
-(void)openNewPost:(id)sender;

#pragma mark - Custom methods

- (CGFloat) calculatePostCellHeightAtRow:(NSInteger)row;

#pragma mark UITableView-related methods
- (UITableViewCell *)tableView:(UITableView *)tableView postCellAtIndexPath:(NSIndexPath *)indexPath;

#pragma mark - Methods implemented on Sub-classes
-(void)reloadStreamPage:(GTRStreamPageReloadMode)theReloadMode;
-(void)openNewPostwithImage:(UIImage*)postImage;
-(void)likeButtonAction:(id)sender;

@end
