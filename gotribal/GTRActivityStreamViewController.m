//
//  GTRActivityStreamViewController.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActivityStreamViewController.h"
#import "GTRAppDelegate.h"
#import "GTRUser.h"
#import "GTRPost.h"
#import "MFSideMenu.h"
#import "SVPullToRefresh.h"
#import "SVProgressHUD.h"
#import "GTRSideMenuViewController.h"
#import "GTRSideMenuHelper.h"
#import "GTRActivityStreamRestService.h"
#import "GTRPostRestService.h"
#import "GTRNewPostViewController.h"
#import "GTRStreamPostCell.h"
#import "GTRPostHelper.h"
#import "iCarousel.h"
#import "GTRActiveBudzCarouselItem.h"
#import "NSMutableArray+AddMantleObject.h"
#import "GTRActiveBudzRestService.h"
#import "GTRActiveBudzHelper.h"
#import "GTRActiveBudzProfileViewController.h"
#import "GTRSuggestedActiveBudzCell.h"
#import "UIImage+Resize.h"
#import "GTRImagePickerHelper.h"
#import "GTRLikeHelper.h"
#import "GTRCustomActionSheetHelper.h"

#import <Crashlytics/Crashlytics.h>

NSString* const PLACEHOLDER_TEXT = @"How did you lead an active, healthy lifestyle today? Start sharing your updates here!";

#define MORE_BUTTON_DELETE_TITLE NSLocalizedString(@"Delete", @"delete")
#define MORE_BUTTON_SHARE_TITLE NSLocalizedString(@"Share", @"share")

@interface GTRActivityStreamViewController() <UIActionSheetDelegate, UITableViewDataSource, UITableViewDelegate, GTRNewPostViewControllerDelegate,
GTRActiveBudzProfileViewControllerDelegate, iCarouselDataSource, iCarouselDelegate, GTRImagePickerHelperDelegate, GTRCustomActionSheetHelperDelegate>

#define SUGGESTED_ACTIVEBUDZ_SECTION 0
#define ACTIVITY_STREAM_SECTION 1

@property (strong, nonatomic) NSNumber *lastPostID;

@property (strong, nonatomic) NSMutableArray *suggestedActiveBudzArray;
@property (strong, nonatomic) GTRSuggestedActiveBudzCell *suggestedActiveBudzCell;
@property (strong, nonatomic) GTRImagePickerHelper *imagePickerHelper;
@property (strong, nonatomic) GTRCustomActionSheetHelper *customActionSheet;

@property BOOL puclicProfileHasOpen;
@property (nonatomic, getter = isActivebudzSelected) BOOL activebudzSelected;
@end

@implementation GTRActivityStreamViewController
@synthesize lastPostID, suggestedActiveBudzArray, suggestedActiveBudzCell, imagePickerHelper, customActionSheet;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Activities", @"Activities");
        [self initAttributes];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setAppearance];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}

#pragma mark - Custom initiators

- (void) initAttributes
{ 
    self.streamPostArray = [GTRPostHelper getArchivedActivityStreams];

    suggestedActiveBudzArray = [[NSMutableArray alloc] init];
    
    [self initSuggestedActiveBudzCell];
    
    imagePickerHelper = [[GTRImagePickerHelper alloc] init:self];
    imagePickerHelper.delegate = self;
    [self initiateActionSheet];
    
}

- (void) initSuggestedActiveBudzCell
{
    suggestedActiveBudzCell = [[GTRSuggestedActiveBudzCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"suggestedActiveBudzCell"];
    suggestedActiveBudzCell.carousel.delegate = self;
    suggestedActiveBudzCell.carousel.dataSource = self;
}

-(void)initiateActionSheet
{
    customActionSheet = [[GTRCustomActionSheetHelper alloc]initWithSuperViewController:self];
    customActionSheet.delegate =self;
}

#pragma mark - Appearance methods
-(void)setAppearance
{
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) self.edgesForExtendedLayout = UIRectEdgeNone;
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) self.automaticallyAdjustsScrollViewInsets = YES;
    
    [self setPlaceHolderText:NSLocalizedString(PLACEHOLDER_TEXT, @"NoActivityStreamMessage")];
    [self initNavbarButton];
}

- (void) initNavbarButton
{
    [GTRNavigationBarHelper setLeftSideBarButtonItem:self.navigationItem withTarget:self selector:@selector(toggleSideMenu:)];
    
    UIBarButtonItem *item0 = [GTRNavigationBarHelper createBarButtonItemWithTarget:self selector:@selector(openNewPost:) imageNamed:@"ico-pen-round"];
    UIBarButtonItem *item1 = [GTRNavigationBarHelper createBarButtonItemWithTarget:self selector:@selector(openImagePicker:) imageNamed:@"ico-nav-camera"];
    [GTRNavigationBarHelper setRightBarButtonItems:self.navigationItem withItems:@[item0, item1] animated:NO];
}

#pragma mark Reload Suggested ActiveBudz

- (void)reloadSuggestedActiveBudz {
    
    //add caching feature
    [self setSuggestedActiveBudzData:[GTRActiveBudzHelper getArchivedSuggestedActiveBudz]];
    
    [GTRActiveBudzRestService retrieveSuggestedActiveBudz:^(id responseObject) {
        
        NSMutableArray *userArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary *dictionary in ((NSArray*)responseObject)) {
            [userArray addObject:[MTLJSONAdapter modelOfClass:[GTRUser class] fromJSONDictionary:dictionary error:nil]];
        }
        
        [self setSuggestedActiveBudzData:userArray];
        
    } onFail:^(id responseObject, NSError *error) {
        
    }];
}

-(void)setSuggestedActiveBudzData:(NSArray*)theActivebudzArray
{
    [suggestedActiveBudzArray removeAllObjects];
    [suggestedActiveBudzArray addObjectsFromArray:theActivebudzArray];
    [suggestedActiveBudzCell.carousel reloadData];
    [suggestedActiveBudzCell toggleNoSuggestedABLabel:[suggestedActiveBudzArray count] == 0];
}

#pragma mark - Overriden Superclass methods

-(void)reloadStreamPage:(GTRStreamPageReloadMode)theReloadMode
{
    
    __weak NSMutableArray *weakArray = self.streamPostArray;
    __weak UITableView *weakTable = self.streamTableView;
    
    
    if (theReloadMode == GTRStreamPageReloadModeRefresh){
        [self reloadSuggestedActiveBudz];
        lastPostID = nil;
    }
    
    DDLogInfo(@"begin Loading");
    [GTRActivityStreamRestService retrieveActivityStreamWithMaxID:lastPostID
                                                        onSuccess:^(id responseObject) {
                                                            if (theReloadMode == GTRStreamPageReloadModeRefresh){
                                                                [self.streamPostArray removeAllObjects];
                                                            }
                                                            
                                                            NSArray *postsParams = [responseObject valueForKey:@"data"];
                                                            if ([postsParams count] > 0) {
                                                                for (NSDictionary *postParams in postsParams) {
                                                                    GTRPost *post = [MTLJSONAdapter modelOfClass:[GTRPost class] fromJSONDictionary:postParams error:nil];
                                                                    [weakArray addObject:post];
                                                                    lastPostID = post.postID;
                                                                }
                                                            }
                                                            
                                                            [GTRPostHelper archiveActivityStreams:self.streamPostArray];
                                                            
                                                            [weakTable.infiniteScrollingView stopAnimating];
                                                            [weakTable.pullToRefreshView stopAnimating];
                                                            [self togglePlaceholderLabelVisibility];
                                                            
                                                            [weakTable reloadData];
                                                            
                                                            if (theReloadMode == GTRStreamPageReloadModeRefresh){
                                                                [self.streamTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
                                                            }
                                                            DDLogInfo(@"end Loading");
                                                        } onFail:^(id operation, NSError *error) {
                                                            
                                                            [weakTable.infiniteScrollingView stopAnimating];
                                                            [weakTable.pullToRefreshView stopAnimating];
                                                            
                                                            [self togglePlaceholderLabelVisibility];
                                                        }];
}

#pragma mark New Post button actions

-(void)openNewPostwithImage:(UIImage*) postImage
{
    GTRNewPostViewController *newPostVC = [[GTRNewPostViewController alloc] init];
    newPostVC.delegate = self;
    newPostVC.postImage = postImage;
    
    UINavigationController *NewPostNavVC = [[UINavigationController alloc]
                                            initWithRootViewController:newPostVC];
    
    [self presentViewController:NewPostNavVC animated:YES completion:nil];
}

#pragma mark Like button action

-(void)likeButtonAction:(id)sender
{
    
    NSInteger rowID = ((UIControl *) sender).tag;
    __weak GTRPost *post = [self.streamPostArray objectAtIndex:rowID];
    __weak UITableView *tableView = self.streamTableView;
    
    [GTRLikeHelper toggleLike:post
                    onSuccess:^(GTRPost *updatedPost) {
                        [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:rowID inSection:ACTIVITY_STREAM_SECTION]]
                                         withRowAnimation: UITableViewRowAnimationNone];
                    } onFail:nil];
}

#pragma mark More button action
-(void)moreButtonAction:(id)sender
{
    GTRUser *currentUser = [GTRUserHelper getCurrentUser];
    self.currentSelectedPost = sender;
    NSInteger rowID = ((UIControl *) sender).tag;
    __weak GTRPost *post = [self.streamPostArray objectAtIndex:rowID];
    if([currentUser.userID isEqual:post.owner.userID]){
        [customActionSheet openCustomActionSheetWithMode:GTRCustomActionSheetModeOwnedPost];
    } else {
        [customActionSheet openCustomActionSheetWithMode:GTRCustomActionSheetModeOtherPost];
    }
}

#pragma mark - Delegates

#pragma mark - GTRCustomActionSheet delegates

-(void)deleteAction
{
    NSInteger rowID = ((UIControl *) self.currentSelectedPost).tag;
    __weak GTRPost *post = self.streamPostArray[rowID];
    
    [GTRPostRestService deletePostWithPostID:post.postID onSuccess:^(id response) {
        [self removePostOnIndex:rowID];
    } onFail:nil];
}

#pragma mark UITableView delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
            
        case SUGGESTED_ACTIVEBUDZ_SECTION:
            return 1;
            break;
        case ACTIVITY_STREAM_SECTION:
            return [self.streamPostArray count];
            break;
        default:
            return 0;
            break;
    
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
    
        case SUGGESTED_ACTIVEBUDZ_SECTION:
            return [self tableView:tableView suggestedActiveBudzCellAtIndexPath:indexPath];
            break;
        case ACTIVITY_STREAM_SECTION:
            return [self tableView: tableView postCellAtIndexPath:indexPath];
            break;
        default:
            return nil;
            break;
    
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView suggestedActiveBudzCellAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"suggestedActiveBudzCell"];
    if (cell == nil){
        cell = suggestedActiveBudzCell;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case SUGGESTED_ACTIVEBUDZ_SECTION:
            return 120;
            break;
        case ACTIVITY_STREAM_SECTION:
            return [self calculatePostCellHeightAtRow:indexPath.row];
            break;
        default:
            return 20;
            break;
    }

}

#pragma mark GTRNewPost delegate
- (void)onNewPostCreated:(GTRPost *)post
{
    [self reloadStreamPage:GTRStreamPageReloadModeRefresh];
}

#pragma mark iCarousel delegates
- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    NSInteger nSuggestedActiveBudz = [suggestedActiveBudzArray count];
    
    //if less than 5, disable the scroll
    if(nSuggestedActiveBudz < 5){
        carousel.scrollToItemBoundary = NO;
        carousel.centerItemWhenSelected = NO;
        [carousel setScrollEnabled:NO];
        
        //fix the item position
        [carousel scrollToItemAtIndex:(nSuggestedActiveBudz/2) animated:NO];
        if(nSuggestedActiveBudz % 2 == 0) {
            [carousel scrollByOffset:-0.5 duration:0];
        }
    } else {
        carousel.scrollToItemBoundary = YES;
        carousel.centerItemWhenSelected = YES;
        [carousel setScrollEnabled:YES];
    }
    
    return (nSuggestedActiveBudz<5)? 5:nSuggestedActiveBudz;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    NSInteger nSuggestedActiveBudz = [suggestedActiveBudzArray count];
   
    if (view == nil){
        view = [[GTRActiveBudzCarouselItem alloc] initWithFrame:CGRectMake(0, 0, 54, 54)];
    }
    
    if (index < nSuggestedActiveBudz){
        GTRUser *suggestedActiveBudz = [suggestedActiveBudzArray objectAtIndex:index];
        [(GTRActiveBudzCarouselItem*)view setContent:suggestedActiveBudz];
        view.hidden = NO;
    } else {
        view.hidden = YES;
    }
    
    return view;
}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option) {
        case iCarouselOptionWrap:
            return TRUE;
            break;
        case iCarouselOptionSpacing:
            return value * 1.2;
            break;
        default:
            return value;
    }
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    if (!self.activebudzSelected && [suggestedActiveBudzArray count] > index) {
        self.activebudzSelected = !self.activebudzSelected;
        GTRUser *selectedUser = [suggestedActiveBudzArray objectAtIndex:index];

        __weak GTRActivityStreamViewController *weakSelf = self;
        [GTRActiveBudzHelper pushActiveBudzProfile:selectedUser indexPath:nil rootViewController:weakSelf onSuccess:^(id responseObject) {
                    self.activebudzSelected = !self.activebudzSelected;
        } onFail:^(id responseObject, NSError *error) {
                    self.activebudzSelected = !self.activebudzSelected;
        }];

    }
}

#pragma mark GTRActiveBudzProfile delegate
-(void)onRequestSent:(NSIndexPath*)indexPath
{
    [self reloadSuggestedActiveBudz];
}

#pragma mark GTRImagePickerHelper delegate
- (void)openImagePicker:(id)sender
{
    [imagePickerHelper openCustomActionSheet];
}

- (void)didPickImage:(UIImage*)image
{
    if(image){
        [self openNewPostwithImage: image];
    }
}

@end
