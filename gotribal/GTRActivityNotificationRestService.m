//
//  GTRActivityNotificationRestService.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/16/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActivityNotificationRestService.h"
#import "GTRRestServiceErrorHandler.h"

@implementation GTRActivityNotificationRestService

+(void)retrieveActivityNotificationWithMaxID:(NSNumber*) maxID
                                   onSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager GET:GET_ACTIVITY_NOTIFICATIONS_ENDPOINT parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@", error);
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}
@end
