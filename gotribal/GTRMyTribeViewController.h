//
//  GTRMyTribesViewController.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/11/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRViewController.h"

@interface GTRMyTribeViewController : GTRViewController

- (void)initiateReloadFunctionality;
@property (strong,nonatomic) UITableView *tribeTableView;
@property (strong,nonatomic) UILabel *placeholderLabel;

-(void)reload:(NSInteger)theReloadMode;

@end
