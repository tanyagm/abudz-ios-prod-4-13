//
//  GTRNotificationItem.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/16/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRNotificationItem.h"
#import "GTRTimeFormatterHelper.h"

@implementation GTRNotificationItem
+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
              @"notificationItemID": @"id",
              @"description" : @"message",
              @"timestamp" : @"timestamp",
              @"user": @"user",
              @"targetID": @"action_id",
              @"targetType": @"action_type"
              };
}

+(NSValueTransformer *)userJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:GTRUser.class];
}

-(NSString*) relativeTimestamp
{
    return [GTRTimeFormatterHelper relativeTimeStringFromEpoch:self.timestamp];
}
@end
