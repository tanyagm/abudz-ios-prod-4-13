//
//  GTRSurveyImageView.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/21/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTRSurveyImageView : UIView

@property (strong,nonatomic) NSArray *imageArray;

- (id)initWithFrame:(CGRect)frame imageArray:(NSArray*)theImageArray;

- (void)setAppearance;
@end
