//
//  GTRPostRestService.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/3/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRTRibe.h"

@interface GTRPostRestService : NSObject

+(void)likePostWithPostID:(NSNumber*) postID
                onSuccess:(void (^)(id))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

+(void)unlikePostWithPostID:(NSNumber*) postID
                 onSuccess:(void (^)(id))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

+(void)createPostWithParam:(NSDictionary*) params
                 onSuccess:(void (^)(id))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

+(void)deletePostWithPostID:(NSNumber*) postID
                 onSuccess:(void (^)(id))success onFail:(void (^)(AFHTTPRequestOperation *operation, NSError *error))errorHandler;

+(void)createTribePost:(GTRTribe*)theTribe WithParam:(NSDictionary*) params
             onSuccess:(void (^)(id))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;
@end
