//
//  GTRPhotoActionSheet.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/13/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GTRPhotoActionSheet;

@protocol GTRPhotoActionSheetDelegate <NSObject>

-(void)gtrActionSheet:(GTRPhotoActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;

@end


@interface GTRPhotoActionSheet : NSObject

@property(nonatomic,weak) id <GTRPhotoActionSheetDelegate> delegate;
@property(nonatomic,strong) NSString *title;

-(id)initWithSuperView:(UIView *)superview useActivebudzPhotos:(BOOL)useActivebudzPhotos delegate:(id)delegate;
-(void)show;
-(void)remove;
-(NSString*)buttonTitleAtIndex:(NSInteger)index;

@end
