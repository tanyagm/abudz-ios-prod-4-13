//
//  GTRAppDelegate.m
//  gotribal
//
//  Created by Muhammad Taufik on 11/15/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRAppDelegate.h"
#import "MFSideMenu.h"
#import <Crashlytics/Crashlytics.h>
#import "GTRMainActivitiesViewController.h"
#import "GTRSideMenuViewController.h"
#import "GTREditFitnessProfileViewController.h"
#import "GTRActiveBudzProfileViewController.h"
#import "GTRMessagesViewController.h"
#import "UAirship.h"
#import "UAConfig.h"
#import "UAPush.h"
#import "GTRUserRestService.h"
#import <UI7Kit/UI7Kit.h>
#import "SVProgressHUD.h"
#import "FacebookSDK.h"
#import "GTRSettingsRestService.h"
#import "GTRRestServiceErrorHandler.h"
#import "GTRIntroViewController.h"
#import "GTRSessionRestService.h"

@interface GTRAppDelegate () <GTRUAPushHandlerDelegate>
@property (nonatomic, strong) UIView *statusBar;
@property (nonatomic, strong) GTRSideMenuViewController *sideMenuViewController;
@end

@implementation GTRAppDelegate
@synthesize menuViewController;

-(void)setAppearance
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:1.0/255.0 green:149.0/255.0 blue:152.0/255.0 alpha:1.0]];
        
        [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
        
    } else {
        [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:1.0/255.0 green:149.0/255.0 blue:152.0/255.0 alpha:1.0]];
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navbar-background"] forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
        
        [UI7SegmentedControl patchIfNeeded];
        
        [[UIBarButtonItem appearance] setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal
                                              barMetrics:UIBarMetricsDefault];
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{
                                                               UITextAttributeTextShadowColor : [UIColor clearColor],
                                                               UITextAttributeFont: [UIFont systemFontOfSize:18.0f]
                                                               } forState:UIControlStateNormal];
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{
                                                               UITextAttributeTextShadowColor : [UIColor clearColor],
                                                               UITextAttributeFont: [UIFont systemFontOfSize:18.0f]
                                                               } forState:UIControlStateDisabled];
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{
                                                               UITextAttributeTextShadowColor : [UIColor clearColor],
                                                               UITextAttributeFont: [UIFont systemFontOfSize:18.0f]
                                                               } forState:UIControlStateHighlighted];
    }
    
    NSDictionary *commonTextAttributes = @{ UITextAttributeTextColor : [UIColor whiteColor] };
    [[UINavigationBar appearance] setTitleTextAttributes:commonTextAttributes];
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           UITextAttributeTextColor : [UIColor whiteColor],
                                                           UITextAttributeFont : [UIFont fontWithName:@"Helvetica" size:20.0f],
                                                           UITextAttributeTextShadowOffset : [NSValue valueWithUIOffset:UIOffsetMake(0, 0)]

                                                           }];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self setAppearance];
    
    //Clear keychain on first run in case of reinstallation
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"FirstRun"]) {

        // Delete values from keychain here
        [AFOAuthCredential deleteCredentialWithIdentifier:[[NSURL URLWithString:BASE_URL] host]];
        
        [[NSUserDefaults standardUserDefaults] setValue:@"1strun" forKey:@"FirstRun"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [self setupUrbanAirships];
    [self setupFlurryAnalytics];
    [self setupCrashlytics];
    
    self.currentUser = [GTRUserHelper getCurrentUser];
    
    if (self.currentUser) {
        [self retrieveSettingDetail];
    }
    
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    
    GTRMainActivitiesViewController *mainActivitiesViewController = [[GTRMainActivitiesViewController alloc] init];
    UINavigationController *mainActivitiesNavigationController = [[UINavigationController alloc] initWithRootViewController:mainActivitiesViewController];

    
    self.sideMenuViewController = [[GTRSideMenuViewController alloc] init];
    UINavigationController *sideMenuNavigationController = [[UINavigationController alloc] initWithRootViewController:self.sideMenuViewController];
    
    self.menuViewController = [MFSideMenuContainerViewController containerWithCenterViewController:mainActivitiesNavigationController leftMenuViewController:sideMenuNavigationController rightMenuViewController:nil];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    self.window.rootViewController = self.menuViewController;
   
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0) [self setupStatusBar];
    
    [AFManagerHelper applyReachabilityStatus];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBAppCall handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [FBSession.activeSession handleOpenURL:url];
}

#pragma mark Reload data at sidebar menu

-(void)reloadData
{
    MFSideMenuContainerViewController *mfSideMenuViewController = (MFSideMenuContainerViewController*)self.window.rootViewController;
    GTRSideMenuViewController *sideMenu = (GTRSideMenuViewController*)((UINavigationController*)mfSideMenuViewController.leftMenuViewController).topViewController;
    [sideMenu.tableView reloadData];
}

#pragma mark Push Notifications

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    UA_LINFO(@"APNS device token: %@", deviceToken);
    NSString *token = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<" withString:@""]
                        stringByReplacingOccurrencesOfString:@">" withString:@""]
                       stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [[UAPush shared] registerDeviceToken:deviceToken];
    
    if (![defaults valueForKey:USER_DEVICE_TOKEN] || ![[defaults valueForKey:USER_DEVICE_TOKEN] isEqualToString:token]) {
        [GTRUserRestService registerDevice:token];
    }
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    UA_LINFO(@"Received remote notification: %@", userInfo);
    
    // Fire the handlers for both regular and rich push
    [[UAPush shared] handleNotification:userInfo applicationState:application.applicationState];
}


#pragma mark - iOS 7 status bar

-(void)setupStatusBar
{
    self.statusBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.window.bounds.size.width, 20)];
    self.statusBar.backgroundColor = [UIColor blackColor];
    self.statusBar.hidden = YES;
    self.statusBar.alpha = 0.0f;
    [self.window addSubview:self.statusBar];
    [self.window bringSubviewToFront:self.statusBar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(menuStateEventOccurred:)
                                                 name:MFSideMenuStateNotificationEvent
                                               object:nil];
}

- (void)menuStateEventOccurred:(NSNotification *)notification {
    MFSideMenuStateEvent event = [[[notification userInfo] objectForKey:@"eventType"] intValue];
    
    switch (event) {
        case MFSideMenuStateEventMenuWillOpen:
            [self showStatusBar];
            break;
            
        case MFSideMenuStateEventMenuWillClose:
            [self hideStatusBar];
            break;
            
        default:
            break;
    }
}

-(void)hideStatusBar
{
    [UIView animateWithDuration:0.1 animations:^{
        self.statusBar.alpha = 0.0f;
    } completion:^(BOOL finished) {
        self.statusBar.hidden = YES;
    }];
}

-(void)showStatusBar
{
    [UIView animateWithDuration:0.1 animations:^{
        self.statusBar.alpha = 1.0f;
    } completion:^(BOOL finished) {
        self.statusBar.hidden = NO;
    }];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - GTRUAPushHandlerDelegate

-(void)pushHandler:(GTRUAPushHandler *)pushHandler openConversationWithID:(NSNumber *)conversationID
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager GET:MESSAGE_CONVERSATION_ENDPOINT(conversationID) parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        DDLogInfo(@"%@", json);
        GTRConversation *conversation = [MTLJSONAdapter modelOfClass:[GTRConversation class] fromJSONDictionary:json error:nil];
        DDLogInfo(@"%@", conversation);
        
        GTRMessagesViewController *messagesViewController = [[GTRMessagesViewController alloc] initWithConversation:conversation];
        UINavigationController *messagesNavigationController = [[UINavigationController alloc] initWithRootViewController:messagesViewController];
        [self.window.rootViewController presentViewController:messagesNavigationController animated:YES completion:^{
        [SVProgressHUD dismiss];
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"%@", [error localizedDescription]);
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
    }];
}

-(void)pushHandler:(GTRUAPushHandler *)pushHandler openFriendProfileWithFriendID:(NSNumber *)friendID
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager GET:GET_USER_ENDPOINT(friendID) parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        GTRUser *user = [MTLJSONAdapter modelOfClass:GTRUser.class fromJSONDictionary:json[@"user"] error:nil];
        NSArray *connections = json[@"connection"];
        
        GTRActiveBudzProfileViewController *activeBudzProfileViewController = [[GTRActiveBudzProfileViewController alloc] initWithUser:user indexPath:nil connections:connections profileType:ActiveBudzProfileTypeConnected];
        
        UINavigationController *activeBudzProfileNavigationController = [[UINavigationController alloc] initWithRootViewController:activeBudzProfileViewController];
        
        [self.window.rootViewController presentViewController:activeBudzProfileNavigationController animated:YES completion:nil];
        
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"%@", [error localizedDescription]);
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
    }];
}

-(void)pushHandler:(GTRUAPushHandler *)pushHandler openNotificationScreenWithFriendID:(NSNumber *)friendID
{
    [self.sideMenuViewController notificationsViewAction];
}

#pragma mark - Urban Airships

-(void)setupUrbanAirships
{
    [UAPush setDefaultPushEnabledValue:NO];
    // Set log level for debugging config loading (optional)
    // It will be set to the value in the loaded config upon takeOff
    // [UAirship setLogLevel:UALogLevelTrace];
    UAConfig *config = [UAConfig defaultConfig];
#ifdef DEBUG
    config.clearKeychain = YES;
#endif
    config.automaticSetupEnabled = NO;
    config.developmentLogLevel = UALogLevelInfo;
    config.productionLogLevel = UALogLevelNone;
    [UAirship takeOff:config];
    
    // Print out the application configuration for debugging (optional)
    // UA_LDEBUG(@"Config:\n%@", [config description]);
    
    // Set the icon badge to zero on startup (optional)
    [[UAPush shared] resetBadge];
    [UAPush shared].notificationTypes = (UIRemoteNotificationTypeBadge |
                                         UIRemoteNotificationTypeSound |
                                         UIRemoteNotificationTypeAlert);
    [[UAPush shared] registerForRemoteNotifications];
    
    self.pushHandler = [[GTRUAPushHandler alloc] initWithDelegate:self];
    
    [[UAPush shared] setPushNotificationDelegate:self.pushHandler];
}

#pragma mark - Flurry Analytics

-(void)setupFlurryAnalytics
{
    #ifdef GTR_FLURRY_ANALYTICS
    [Flurry startSession:FLURRY_KEY];
    [Flurry setCrashReportingEnabled:YES];
    [Flurry setDebugLogEnabled:YES];
    #endif
}

#pragma mark - Crashlytics

-(void)setupCrashlytics
{
    [Crashlytics startWithAPIKey:CRASHLYTICS_KEY];
#ifdef DEBUG
    [[Crashlytics sharedInstance]setDebugMode:YES];
#endif
}

#pragma mark - get setting config
-(void)retrieveSettingDetail
{
    [GTRSettingsRestService retrieveSettingOnSuccess:^(id response) {
        NSLog(@"%@",response); 
        self.settingConf = [[NSMutableDictionary alloc] initWithDictionary:response];
    }onFail:nil];
}

#pragma mark - Global Logout

-(void)logoutThisApp
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [GTRSessionRestService logoutOnSuccess:^(id responseObject) {
        [self showInitialScreen];
        [SVProgressHUD dismiss];
        
        [self facebookLogout];
    } onFail:^(id operation, NSError *error) {
    }];
}

-(void)logoutThisAppWhenBackendFails
{
    [GTRUserHelper removeUserCredentials];
    [GTRUserHelper removeCurrentUser];
    [self showInitialScreen];
}

-(void)showInitialScreen
{
    GTRIntroViewController *introViewController = [[GTRIntroViewController alloc] init];
    UINavigationController *introNavigationController = [[UINavigationController alloc] initWithRootViewController:introViewController];
    [self.menuViewController presentViewController:introNavigationController animated:YES completion:nil];
    
    GTRMainActivitiesViewController *mainActivities = [[GTRMainActivitiesViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mainActivities];
    self.menuViewController.centerViewController = navigationController;
}

-(void)facebookLogout
{
    if (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        [FBSession.activeSession closeAndClearTokenInformation];
    }
}

@end
