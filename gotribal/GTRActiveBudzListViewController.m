//
//  GTRActiveBudzListViewController.m
//  gotribal
//
//  Abstract class for GTRMyActiveBudzViewController and
//  GTRSuggestedActiveBudzViewController. Never use this class without subclassing.
//
//  Created by Ricardo Pramana Suranta on 12/6/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActiveBudzListViewController.h"
#import "GTRActiveBudzCell.h"
#import "UIImageView+AFNetworking.h"
#import "AFOAuth2Client.h"
#import "GTRUser.h"
#import "GTRUserDetail.h"
#import "GTRUserHelper.h"
#import "GTRConstants.h"
#import "GTRPaginationHelper.h"
#import "SVProgressHUD.h"
#import "SVPullToRefresh.h"
#import "GTRActiveBudzProfileViewController.h"
#import "NSMutableArray+AddMantleObject.h"
#import "GTRInviteFriendsViewController.h"
#import "GTRNavigationBarHelper.h"

#define CELL_HEIGHT 59

#define PLACEHOLDER_TEXT_STRING @"No Budz yet? Your Activebudz community is new and growing. Invite your friends and connect with them here!"

NSString* const INVITE_FRIEND_BUTTON_IMAGE_NAME = @"ico-menu-invite";
NSString* const INVITE_FRIEND_BUTTON_TEXT = @"Invite Friends";

@interface GTRActiveBudzListViewController ()

@property (strong,nonatomic) UILabel *placeholderLabel;
@property (strong,nonatomic) UIButton *inviteFriendButton;

@end

@implementation GTRActiveBudzListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self initiateActiveBudzArrayWithCompletion:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //initiate the tableview
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    __weak GTRActiveBudzListViewController *weakSelf = self;
    
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf refreshActivebudzList];
    }];
    
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf nextActivebudzList];
    }];
    
    
    //set the placeholder label
    NSAttributedString *placeholderText = [[NSAttributedString alloc] initWithString:PLACEHOLDER_TEXT_STRING attributes:@{
                                                                                                                          NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16.0f],
                                                                                                                          NSForegroundColorAttributeName : [UIColor colorWithWhite:0.333 alpha:1.000]
                                                                                                                          }];
    CGFloat placeholderHorizontalPadding = 8;
    CGFloat placeholderHeight = 200;
    CGFloat placeholderWidth = self.view.bounds.size.width - (placeholderHorizontalPadding*2);
    CGFloat placeholderY = ((self.view.bounds.size.height - placeholderHeight)/2)-50;
    
    self.placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(placeholderHorizontalPadding,
                                                                      placeholderY,
                                                                      placeholderWidth,
                                                                      placeholderHeight)];
    
    self.placeholderLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    self.placeholderLabel.backgroundColor = [UIColor clearColor];
    self.placeholderLabel.attributedText = placeholderText;
    self.placeholderLabel.numberOfLines = 0;
    self.placeholderLabel.adjustsFontSizeToFitWidth = NO;
    self.placeholderLabel.contentMode = UIViewContentModeCenter;
    self.placeholderLabel.backgroundColor = [UIColor clearColor];
    [self.tableView addSubview:self.placeholderLabel];
    
    //set invite friend button
    NSAttributedString *inviteFriendText = [[NSAttributedString alloc] initWithString:INVITE_FRIEND_BUTTON_TEXT attributes:@{
                                                                                                                          NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16.0f],
                                                                                                                          NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                                                                          }];
    
    CGFloat inviteFriendButtonWidth = placeholderWidth;
    CGFloat inviteFriendButtonHeight = 50;
    CGFloat inviteFriendButtonX = placeholderHorizontalPadding;
    CGFloat inviteFriendButtonY = placeholderY+80;
    
    //some layout magic
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        inviteFriendButtonY += 20;
    }
    
    self.inviteFriendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.inviteFriendButton.frame = CGRectMake(inviteFriendButtonX, inviteFriendButtonY, inviteFriendButtonWidth, inviteFriendButtonHeight);
    [self.inviteFriendButton setImage:[UIImage imageNamed:INVITE_FRIEND_BUTTON_IMAGE_NAME] forState:UIControlStateNormal];
    [self.inviteFriendButton setAttributedTitle:inviteFriendText forState:UIControlStateNormal];
    [self.inviteFriendButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    self.inviteFriendButton.backgroundColor = [UIColor colorWithRed:0.114 green:0.525 blue:0.541 alpha:1.000];
    self.inviteFriendButton.layer.cornerRadius = 5.0f;
    [self.inviteFriendButton addTarget:self action:@selector(inviteFriendButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.tableView addSubview:self.inviteFriendButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView delegates

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = self.activeBudzArray.count;
    
    if (count > 0) {
        self.placeholderLabel.hidden = YES;
        self.inviteFriendButton.hidden = YES;
    } else {
        self.placeholderLabel.hidden = NO;
        self.inviteFriendButton.hidden = NO;
    }
    
    return count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"ActiveBudzCell";
    GTRActiveBudzCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    GTRUser *user = ((GTRUser*)[self.activeBudzArray objectAtIndex:indexPath.row]);
    GTRUserDetail *pursuitDetail = [[user.details filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"detailKey == %@", USER_DETAIL_FITNESS_PURSUIT_KEY]] firstObject];
    
    if (!cell)
    {
        cell = [[GTRActiveBudzCell alloc] initWithHeight:CELL_HEIGHT
                                         isSuggestedMode:NO
                                                   style:UITableViewCellStyleDefault
                                         reuseIdentifier:cellIdentifier];
        
        //add bottom separator
        UIView *bottomSeparator = [[UIView alloc] initWithFrame:
                                   CGRectMake(0, CELL_HEIGHT-1, self.view.bounds.size.width, 1)];
        bottomSeparator.backgroundColor = [UIColor colorWithRed:0.800 green:0.796 blue:0.812 alpha:1.000];
        [cell.contentView addSubview:bottomSeparator];
    }
    
    NSString *fullname = [NSString stringWithFormat:@"%@ %@", user.firstName,  user.lastName];
    
    NSAttributedString *name = [[NSAttributedString alloc] initWithString:fullname attributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:15.0f]}];
    
    [cell.customLabel setAttributedText:name];
    cell.backgroundColor = [UIColor clearColor];
    
    //add pursuit image to cell
    cell.pursuitArray = ((NSArray*)pursuitDetail.detailValue);
    [cell setPursuitIcons];
    
    //add image to cell
    [cell.customImageView setImageWithURL:[NSURL URLWithString:user.avatarURL] placeholderImage:DEFAULT_AVATAR];
    
    return cell;
}

#pragma mark - Refresh

-(void)refreshActivebudzList
{
    __weak GTRActiveBudzListViewController *weakSelf = self;
    __weak UITableView *weakTable = self.tableView;
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf initiateActiveBudzArrayWithCompletion:^{
            [weakTable.pullToRefreshView stopAnimating];
        }];
    });
}

#pragma mark - Infinite scrolling

-(void)nextActivebudzList
{
    __weak GTRActiveBudzListViewController *weakSelf = self;
    __weak UITableView *weakTable = self.tableView;
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf retrieveNextActiveBudzListCompletion:^{
            [weakTable.infiniteScrollingView stopAnimating];
        }];
    });
}

#pragma mark - Invite friend button action
-(void)inviteFriendButtonAction:(id)sender
{
    DDLogInfo(@"Invite friend button clicked!");
    GTRInviteFriendsViewController *inviteFriendsViewController = [[GTRInviteFriendsViewController alloc] initWithPushedMode:YES tribeMode:NO];
    
    [self.navigationController pushViewController:inviteFriendsViewController animated:YES];
    
}

#pragma mark - Custom actions
- (void)removePersonFromList:(NSIndexPath*)theIndexPath
{
    [self.tableView beginUpdates];
    [self.activeBudzArray removeObjectAtIndex:theIndexPath.row];
    [self.tableView deleteRowsAtIndexPaths:@[theIndexPath] withRowAnimation:YES];
    [self.tableView endUpdates];
}

- (void)initiateActiveBudzArrayWithCompletion:(void (^)(void))completion
{
    self.activeBudzArray = [[NSMutableArray alloc] init];
    
    if(completion){
        completion();
    }
    
    [self.tableView reloadData];
}

- (void)retrieveNextActiveBudzListCompletion:(void (^)(void))completion
{
    //for the subclasses.
}

- (int)getCellHeight
{
    return CELL_HEIGHT;
}

@end
