//
//  GTRSignupViewController.m
//  gotribal
//
//  Created by Muhammad Taufik on 11/17/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSignupViewController.h"
#import "SVProgressHUD.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "TTTAttributedLabel.h"
#import "GTRSurveyCoverViewController.h"
#import "Base64.h"
#import "UIImage+Resize.h"
#import "AFOAuth2Client.h"
#import "GTRBirthdayTextField.h"
#import "GTRSessionRestService.h"
#import "GTRImagePickerHelper.h"
#import "GTRRestServiceErrorHandler.h"
#import "GTRSettingsHelper.h"
#import "GTRSettingsRestService.h"

#ifdef DEBUG
//#define UICOLOR_DEBUG
#include <stdlib.h>
#endif

@interface GTRSignupViewController () <UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, TTTAttributedLabelDelegate, UITextFieldDelegate, GTRImagePickerHelperDelegate>
@property (nonatomic, strong) TPKeyboardAvoidingScrollView *scrollView;

@property (nonatomic, strong) UIBarButtonItem *rightBarButtonItem;

@property (nonatomic, strong) UITextField *firstNameTextField;
@property (nonatomic, strong) UITextField *lasttNameTextField;
@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UITextField *confirmPasswordTextField;
@property (nonatomic, strong) GTRBirthdayTextField *birthdayTextField;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDate *birthdayDate;
@property (nonatomic, strong) UIImagePickerController *imagePickerController;
@property (nonatomic, strong) UIImage *pickedImage;
@property (nonatomic, strong) UIButton *photoPickerView;

@property (nonatomic, getter = isPrivate) BOOL private;
@property BOOL isAgeValid;

@property (strong, nonatomic) GTRImagePickerHelper *imagePickerHelper;
@end

@implementation GTRSignupViewController
@synthesize imagePickerHelper;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Sign up", @"Sign up");
        self.dateFormatter = [[NSDateFormatter alloc] init];
        self.dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        self.dateFormatter.dateFormat = @"MMMM dd, yyyy";
        imagePickerHelper = [[GTRImagePickerHelper alloc] init:self];
        imagePickerHelper.delegate = self;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [GTRNavigationBarHelper setBackButtonItem:self.navigationItem withTarget:self.navigationController selector:@selector(popViewControllerAnimated:)];
    
    self.rightBarButtonItem = [GTRNavigationBarHelper setRightBarButtonItemWithText:NSLocalizedString(@"Done", @"done")
                                                                   onNavigationItem:self.navigationItem
                                                                         withTarget:self
                                                                           selector:@selector(doneButtonAction:)];
    
    self.rightBarButtonItem.enabled = NO;
    
    self.scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.scrollView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0];
    self.scrollView.contentSize = self.scrollView.bounds.size;
    [self.view addSubview:self.scrollView];
    
    // Create form container
    CGFloat containerHeight = 252.0f;
    CGFloat xInset = 8.0f;
    CGFloat yOffset = 10.0f;
    CGFloat photoPickerWidth = 68.0f;
    
    CGRect containerViewFrame = CGRectMake(0, 0, self.view.bounds.size.width, containerHeight);
    containerViewFrame = CGRectInset(containerViewFrame, xInset, 0);
    
    // main container view
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectOffset(containerViewFrame, 0, yOffset)];
    containerView.backgroundColor = [UIColor whiteColor];
    containerView.layer.borderColor = [UIColor colorWithRed:195.0/255.0 green:195.0/255.0 blue:195.0/255.0 alpha:1.0].CGColor;
    containerView.layer.borderWidth = 0.5f;
    containerView.layer.cornerRadius = 3.0f;
    [self.scrollView addSubview:containerView];
    
    // inner container view
    UIView *innerContainerView = [[UIView alloc] initWithFrame:CGRectInset(containerView.bounds, xInset, yOffset)];
    
    #ifdef UICOLOR_DEBUG
    innerContainerView.backgroundColor = [UIColor blueColor];
    #endif
    
    [containerView addSubview:innerContainerView];
    
    // Create photo picker view container
    UIButton *photoPickerView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, photoPickerWidth, photoPickerWidth)];
    
    #ifdef UICOLOR_DEBUG
    photoPickerView.backgroundColor = [UIColor yellowColor];
    #endif
    
    photoPickerView.backgroundColor = [UIColor colorWithWhite:0.337 alpha:1.000];
    photoPickerView.layer.cornerRadius = photoPickerView.bounds.size.width/2;
    photoPickerView.layer.masksToBounds = YES;
    [photoPickerView addTarget:self action:@selector(openImagePicker:) forControlEvents:UIControlEventTouchUpInside];
    [photoPickerView setImage:[UIImage imageNamed:@"ico-camera-signup"] forState:UIControlStateNormal];
    [innerContainerView addSubview:photoPickerView];
    self.photoPickerView = photoPickerView;
        
    // Create form container
    CGRect formContainerViewFrame = CGRectInset(innerContainerView.bounds, photoPickerWidth/2 + xInset, 0);
    UIView *formContainerView = [[UIView alloc] initWithFrame:CGRectOffset(formContainerViewFrame, photoPickerWidth/2, 0)];
    
    #ifdef UICOLOR_DEBUG
    formContainerView.backgroundColor = [UIColor redColor];
    #endif
    
    [innerContainerView addSubview:formContainerView];
    
    
    CGFloat textFieldHeight = 38.0f;
    CGFloat textFieldYOffset = 0.0f;
    CGFloat textFieldFontSize = 15;
    
    
    NSMutableArray *textFields = [NSMutableArray array];
    
    // Add text fields
    UITextField *firstNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, formContainerView.bounds.size.width, textFieldHeight)];
    [firstNameTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    #ifdef UICOLOR_DEBUG
    firstNameTextField.backgroundColor = [UIColor greenColor];
    #endif
    
    firstNameTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    firstNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"First Name", @"first name") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    [formContainerView addSubview:firstNameTextField];
    [textFields addObject:firstNameTextField];
    self.firstNameTextField = firstNameTextField;
    
    textFieldYOffset += firstNameTextField.bounds.size.height + 4;
    
    UITextField *lastNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, formContainerView.bounds.size.width, textFieldHeight)];
    [lastNameTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    #ifdef UICOLOR_DEBUG
    lastNameTextField.backgroundColor = [UIColor greenColor];
    #endif
    
    lastNameTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    lastNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Last Name", @"last name") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    [formContainerView addSubview:lastNameTextField];
    [textFields addObject:lastNameTextField];
    self.lasttNameTextField = lastNameTextField;
    
    textFieldYOffset += lastNameTextField.bounds.size.height;
    
    UITextField *emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, formContainerView.bounds.size.width, textFieldHeight)];
    [emailTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    #ifdef UICOLOR_DEBUG
    emailTextField.backgroundColor = [UIColor greenColor];
    #endif
    
    emailTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Email", @"email") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    [formContainerView addSubview:emailTextField];
    [textFields addObject:emailTextField];
    self.emailTextField = emailTextField;
    
    textFieldYOffset += emailTextField.bounds.size.height;
    
    UITextField *passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, formContainerView.bounds.size.width, textFieldHeight)];
    [passwordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    passwordTextField.secureTextEntry = YES;
    
    #ifdef UICOLOR_DEBUG
    passwordTextField.backgroundColor = [UIColor greenColor];
    #endif
    
    passwordTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Password", @"password") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    [formContainerView addSubview:passwordTextField];
    [textFields addObject:passwordTextField];
    self.passwordTextField = passwordTextField;
    
    textFieldYOffset += passwordTextField.bounds.size.height;
    
    UITextField *confirmPasswordTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, formContainerView.bounds.size.width, textFieldHeight)];
    [confirmPasswordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    confirmPasswordTextField.secureTextEntry = YES;
    
    #ifdef UICOLOR_DEBUG
    confirmPasswordTextField.backgroundColor = [UIColor greenColor];
    #endif
    
    confirmPasswordTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    confirmPasswordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Confirm Password", @"Confirm Password") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    [formContainerView addSubview:confirmPasswordTextField];
    [textFields addObject:confirmPasswordTextField];
    self.confirmPasswordTextField = confirmPasswordTextField;
    
    textFieldYOffset += confirmPasswordTextField.bounds.size.height;
    
    // Use date picker as input view to birthday text field
    UIDatePicker *birthdayDatePicker = [[UIDatePicker alloc] init];
    birthdayDatePicker.datePickerMode = UIDatePickerModeDate;
    birthdayDatePicker.maximumDate = [NSDate date];
    [birthdayDatePicker addTarget:self action:@selector(birthdayDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];

    UIToolbar *inputAccessoryViewToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(birthdayInputAccessoryDoneButtonAction:)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [inputAccessoryViewToolbar setItems:@[flexibleSpace, doneButton]];
    [inputAccessoryViewToolbar setBarStyle:UIBarStyleBlackTranslucent];

    GTRBirthdayTextField *birthdayTextField = [[GTRBirthdayTextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, formContainerView.bounds.size.width, textFieldHeight)];
    [birthdayTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    birthdayTextField.delegate = self;
    
    #ifdef UICOLOR_DEBUG
    birthdayTextField.backgroundColor = [UIColor greenColor];
    #endif
    
    birthdayTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    birthdayTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Birthday", @"birthday") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    birthdayTextField.inputView = birthdayDatePicker;
    birthdayTextField.inputAccessoryView = inputAccessoryViewToolbar;
    
    // Add birthday lock button
    UIButton *birthdayLockButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [birthdayLockButton setImage:[UIImage imageNamed:@"ico-padlock-open"] forState:UIControlStateNormal];
    [birthdayLockButton sizeToFit];
    [birthdayLockButton addTarget:self action:@selector(birthdayLockButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    birthdayTextField.rightView = birthdayLockButton;
    birthdayTextField.rightViewMode = UITextFieldViewModeUnlessEditing;
    
    [formContainerView addSubview:birthdayTextField];
    [textFields addObject:birthdayTextField];
    self.birthdayTextField = birthdayTextField;
    
    // Add hairlines on the bottom of every text fields
    // Set clear button mode to UITextFieldViewModeWhileEditing
    for (UITextField *textField in textFields) {
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.clipsToBounds = NO;
        textField.textColor = GTR_FORM_TEXT_COLOR;
        @autoreleasepool {
            UIView *bottomSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(-1.0f, textField.bounds.size.height - 8.0f, textField.bounds.size.width, 0.5f)];
            bottomSeparatorView.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1.0f];
            [textField addSubview:bottomSeparatorView];
        }
    }
    
    // add some space to main container view
    containerViewFrame = containerView.frame;
    containerViewFrame.size.height += 8.0f;
    containerView.frame = containerViewFrame;
    
    CGRect termsLabelFrame = CGRectInset(containerView.frame, 6, 0);
    UILabel *termsLabel = [[UILabel alloc] initWithFrame:CGRectOffset(termsLabelFrame, 0, containerView.frame.size.height + containerView.frame.origin.y)];
    termsLabel.backgroundColor = [UIColor clearColor];
    
#ifdef UICOLOR_DEBUG
    termsLabel.backgroundColor = [UIColor grayColor];
#endif
    
    termsLabel.textColor = [UIColor colorWithRed:113.0/255.0 green:113.0/255.0 blue:113.0/255.0 alpha:1.0];
    termsLabel.numberOfLines = 0;
    termsLabel.textAlignment = NSTextAlignmentCenter;
    termsLabel.font = [UIFont systemFontOfSize:13];
    termsLabel.text = NSLocalizedString(@"By Signing up you are agreeing with GOTRIbal's", @"footer terms and privacy");
    [termsLabel sizeToFit];
    [self.scrollView addSubview:termsLabel];
    
    TTTAttributedLabel *termsOfServiceAndPrivacyPolicyLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectOffset(termsLabel.frame, 0, termsLabel.frame.size.height)];
    termsOfServiceAndPrivacyPolicyLabel.delegate = self;
    termsOfServiceAndPrivacyPolicyLabel.backgroundColor = [UIColor clearColor];
    
#ifdef UICOLOR_DEBUG
    termsOfServiceAndPrivacyPolicyLabel.backgroundColor = [UIColor grayColor];
#endif
    
    UIColor *textColor = [UIColor colorWithRed:113.0/255.0 green:113.0/255.0 blue:113.0/255.0 alpha:1.0];
    termsOfServiceAndPrivacyPolicyLabel.textColor = textColor;
    termsOfServiceAndPrivacyPolicyLabel.linkAttributes = @{(id)kCTForegroundColorAttributeName : textColor, (id)kCTUnderlineStyleAttributeName : [NSNumber numberWithInt:kCTUnderlinePatternDash]};
    termsOfServiceAndPrivacyPolicyLabel.numberOfLines = 0;
    termsOfServiceAndPrivacyPolicyLabel.font = [UIFont systemFontOfSize:13];
    NSString *termsOfServicePrivacyPolicy = NSLocalizedString(@"Terms of Service and Privacy Policy", "Terms of Service and Privacy Policy");
    termsOfServiceAndPrivacyPolicyLabel.text = termsOfServicePrivacyPolicy;
    
    NSRange termsOfServiceRange = [termsOfServicePrivacyPolicy rangeOfString:NSLocalizedString(@"Terms of Service", @"terms of service")];
    NSRange privacyPolicyRange = [termsOfServicePrivacyPolicy rangeOfString:NSLocalizedString(@"Privacy Policy", @"privacy policy")];
    
    [termsOfServiceAndPrivacyPolicyLabel addLinkToURL:[NSURL URLWithString:@"http://www.gotribalnow.com/content/terms-use"] withRange:termsOfServiceRange];
    [termsOfServiceAndPrivacyPolicyLabel addLinkToURL:[NSURL URLWithString:@"http://www.gotribalnow.com/privacy-policy"] withRange:privacyPolicyRange];
    [termsOfServiceAndPrivacyPolicyLabel sizeToFit];
    termsOfServiceAndPrivacyPolicyLabel.center = CGPointMake(termsLabel.center.x, termsOfServiceAndPrivacyPolicyLabel.center.y);
    
    [self.scrollView addSubview:termsOfServiceAndPrivacyPolicyLabel];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self textFieldDidChange:nil];
}

#pragma mark - UITextField action

-(void)textFieldDidChange:(id)sender
{
    if (![self.birthdayTextField.text length] ||
        ![self.firstNameTextField.text length] ||
        ![self.lasttNameTextField.text length] ||
        ![self.emailTextField.text length] ||
        ![self.passwordTextField.text length] ||
        ![self.confirmPasswordTextField.text length])
    {
        [self enableRightBarButtonItem:NO];
    } else {
        [self enableRightBarButtonItem:YES];
    }
}

#pragma mark - UITextField delegate
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.birthdayTextField) {
        [UIView animateWithDuration:0.3 animations:^{
            [self.scrollView setContentInset:UIEdgeInsetsZero];
        }];
    }
}

#pragma mark - TTTAttributedLabel delegate

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    [[UIApplication sharedApplication] openURL:url];
}

#pragma mark - Custom methods

-(void)birthdayLockButtonAction:(id)sender
{
    UIButton *lockButton = (id)sender;
    
    self.private = !self.private;
    
    if (self.isPrivate) {
        [lockButton setImage:[UIImage imageNamed:@"ico-padlock"] forState:UIControlStateNormal];
    } else {
        [lockButton setImage:[UIImage imageNamed:@"ico-padlock-open"] forState:UIControlStateNormal];
    }
}

-(void)birthdayDatePickerValueChanged:(id)sender
{
    UIDatePicker *datePicker = (UIDatePicker *)sender;
    self.birthdayDate = datePicker.date;
    self.dateFormatter.dateFormat = @"MMMM dd, yyyy";
    self.birthdayTextField.text = [self.dateFormatter stringFromDate:self.birthdayDate];
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *year = [calender components:NSYearCalendarUnit fromDate:self.birthdayDate];
    NSDateComponents *CurrentYear = [calender components:NSYearCalendarUnit fromDate:[NSDate date]];
    if([CurrentYear year] - [year year] <17){
        self.isAgeValid = NO;
    }else {
        self.isAgeValid = YES;
    }
    [self textFieldDidChange:nil];
}

-(void)birthdayInputAccessoryDoneButtonAction:(id)sender
{
    if ([self.birthdayTextField isFirstResponder]) [self.birthdayTextField resignFirstResponder];
}

#pragma mark - UINavigationController delegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


#pragma mark - Custom actions

-(void)doneButtonAction:(id)sender
{
    if (![self.birthdayTextField.text length] ||
        ![self.firstNameTextField.text length] ||
        ![self.lasttNameTextField.text length] ||
        ![self.emailTextField.text length] ||
        ![self.passwordTextField.text length] ||
        ![self.confirmPasswordTextField.text length] ||
        self.isAgeValid == NO)
    {
        if(self.isAgeValid == NO){
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"You must be 17 or above to sign up!",@"must be 17 or above to sign up")];
        }
        return;
    }
    
    [self signUpRequest];
}

-(void)showValidationAlert
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please complete the form", nil)];
}

-(void)enableRightBarButtonItem:(BOOL)barButtonEnabled
{
    self.rightBarButtonItem.enabled = barButtonEnabled;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (barButtonEnabled) {
            [self.rightBarButtonItem setTintColor:[UIColor whiteColor]];
        } else {
            [self.rightBarButtonItem setTintColor:[UIColor colorWithWhite:1.0f alpha:0.50f]];
        }
    }
}

#pragma mark - AFNetworking Requests

-(void)signUpRequest
{
    NSString *avatarContentImageString = nil;
    if (self.pickedImage) {
        avatarContentImageString = [UIImageJPEGRepresentation(self.pickedImage, 1.0f) base64EncodedString];
    } else {
        avatarContentImageString = @"";
    }
    
    self.dateFormatter.dateFormat = @"yyyy/MM/dd";
    
    if (!self.birthdayDate) self.birthdayDate = [NSDate date];
    
    NSDictionary *birthdate = @{
                                 @"value": [self.dateFormatter stringFromDate:self.birthdayDate],
                                 @"private": [NSNumber numberWithBool:self.private]
                                 };
    
    NSDictionary *parameters = @{
                                 @"first_name": self.firstNameTextField.text,
                                 @"last_name": self.lasttNameTextField.text,
                                 @"email": self.emailTextField.text,
                                 @"password": self.passwordTextField.text,
                                 @"password_confirmation": self.confirmPasswordTextField.text,
                                 @"birthdate": birthdate,
                                 @"avatar_content": avatarContentImageString,
                                 @"avatar_filename": @"profile_picture.jpeg"
                                 };
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [self.view endEditing:YES];
    
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager POST:USER_SIGNUP_ENDPOINT parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {        
        [GTRSessionRestService saveOAuthCredential:[responseObject valueForKey:@"access_token"]];
        [GTRUserHelper updateCurrentUser:[responseObject valueForKey:@"user"]];
        
        [GTRSettingsRestService retrieveSettingOnSuccess:^(id response) {
            [GTRSettingsHelper updateSettingDetail:response];
        }onFail:nil];
        
        [self showSurveyScreen];
        
        [SVProgressHUD dismiss];
        
        [[NSNotificationCenter defaultCenter]
            postNotificationName:REGISTER_AIRSHIP
            object:self];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
    }];
}


#pragma mark - Survey screen

-(void)showSurveyScreen
{
    GTRSurveyCoverViewController *surveyCoverViewController = [[GTRSurveyCoverViewController alloc] init];
    [self.navigationController pushViewController:surveyCoverViewController animated:YES];
}


#pragma mark Image picker
- (void)openImagePicker:(id)sender
{

    [self resignFirstResponders];
    [imagePickerHelper openCustomActionSheet];
}

- (void)didPickImage:(UIImage*)image
{
    if (image) {
        self.pickedImage = [image thumbnailImage:300 transparentBorder:0 cornerRadius:0 interpolationQuality:kCGInterpolationHigh];
        [self.photoPickerView setImage:self.pickedImage forState:UIControlStateNormal];
    }
}

- (void)resignFirstResponders
{
    [_firstNameTextField resignFirstResponder];
    [_lasttNameTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_confirmPasswordTextField resignFirstResponder];
    [_birthdayTextField resignFirstResponder];
}

@end
