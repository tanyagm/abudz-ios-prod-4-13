//
//  GTRFitnessRolePageViewController.h
//  gotribal
//
//  Created by loaner on 12/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSurveyRolePageViewController.h"

@interface GTRFitnessRolePageViewController : GTRSurveyRolePageViewController

-(id)initWithTitle:(NSString *)title
        withHeader:(BOOL)header
  withQuestionType:(BOOL)questionType
  isSurveyPageMode:(BOOL)value
referenceDictionary:(NSMutableDictionary *)theReferenceDictionary;

@end
