//
//  GTRFitnessPageTemplateViewController.m
//  gotribal
//
//  Created by loaner on 11/26/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRFitnessPageTemplateViewController.h"

@interface GTRFitnessPageTemplateViewController ()
@end
@implementation GTRFitnessPageTemplateViewController

- (id) initWithTitle:(NSString *)aTitle
                data:(GTRSurveyQuestionData *)theData
          withHeader:(BOOL)theHeaderValue
    withQuestionType:(BOOL)theTypeValue
    isSurveyPageMode:(BOOL)value;
{
    self = [super init];
    if(self){
        self.title = aTitle;
        self.questionData = theData;
        self.withHeader = theHeaderValue;
        self.withQuestionType = theTypeValue;
        self.isSurveyPageMode = value;
    }
    
    return self;
}

-(void)nextButtonAction:(id)sender
{
    //be emtpy to remove 'next' function
}
@end
