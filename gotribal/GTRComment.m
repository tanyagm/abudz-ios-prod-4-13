//
//  GTRComment.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/12/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRComment.h"
#import "GTRTimeFormatterHelper.h"

@implementation GTRComment

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{ @"commentID": @"id",
              @"message" : @"message",
              @"createdAt": @"created_at",
              @"owner": @"user",
              };
}

+(NSValueTransformer *)ownerJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:GTRUser.class];
}

-(NSString*) createdAtRelativeTime
{
    return [GTRTimeFormatterHelper relativeTimeStringFromEpoch:self.createdAt];
}

-(id)initWithMessage:(NSString*)message user:(GTRUser*)user
{
    self = [super init];
    if (self){
        self.owner = user;
        self.message = message;
        self.createdAt = nil;
        self.commentID = @10001;
    }
    return self;
}


@end
