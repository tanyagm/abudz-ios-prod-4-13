//
//  GTRActivityNotificationRestService.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/16/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTRActivityNotificationRestService : NSObject
+(void)retrieveActivityNotificationWithMaxID:(NSNumber*) maxID
                                   onSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler;
@end
