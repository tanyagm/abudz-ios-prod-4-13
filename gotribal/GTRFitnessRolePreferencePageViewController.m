//
//  GTREditFitnessRolePreferencePageViewController.m
//  gotribal
//
//  Created by loaner on 11/29/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRFitnessRolePreferencePageViewController.h"
#import "SVProgressHUD.h"

@interface GTRFitnessRolePreferencePageViewController ()

@end

@implementation GTRFitnessRolePreferencePageViewController

- (id) initWithTitle:(NSString *)title
          withHeader:(BOOL)header
    withQuestionType:(BOOL)questionType
             isSurveyPageMode:(BOOL)value
 referenceDictionary:(NSMutableDictionary *)theReferenceDictionary
{
    self = [super initWithReferenceDictionary:theReferenceDictionary];
    if(self){
        self.title = title;
        self.withHeader = header;
        self.isSurveyPageMode = value;
    }
    
    return self;
}

- (BOOL) isSelectionValid:(NSUInteger)theSelectedRole
{
    BOOL valid = NO;
    int role = [self getSelectedRole];
    
    //TODO : RPS - Update this later, if the logic is already clear!
    switch (role) {
        case 0: //mentor
        {
            if(role == theSelectedRole){ //if looking for another mentor, send an alert.
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please change your selection in My Role if you would like a mentor.", nil)];
        
            }else{ //if not, proceed.
                valid = YES;
            }
        }
            break;
        case 1: //mentee
        {
            if(role == theSelectedRole){ //if looking for another mentee, send an alert.
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please change your selection in My Role if you would like a mentee.", nil)];
           
            }else{ //if not, proceed.
                valid = YES;
            }
        }
            break;
        case 2: //training buddy
        {
            if(theSelectedRole == 1){ //if looking for another mentee, send an alert.
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please change your selection in My Role if you would like a mentee.", nil)];
     
            }else{ //if not, proceed.
                valid = YES;
            }
        }
            break;
        default: //no role selected
        {
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please select a role in My Role first.", nil)];
        
        }
            break;
    }
    
    
    return valid;
}


@end
