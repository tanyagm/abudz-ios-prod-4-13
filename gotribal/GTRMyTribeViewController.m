//
//  GTRMyTribesViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/11/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRMyTribeViewController.h"
#import "GTRTribeCell.h"
#import "GTRTribe.h"
#import "SVPullToRefresh.h"
#import "GTRTribeRestService.h"
#import "NSMutableArray+AddMantleObject.h"
#import "GTRCreateNewTribeViewController.h"
#import "GTRTribeDetailViewController.h"
#import "GTRTribeSearchViewController.h"

//page title
NSString* const MY_TRIBE_PAGE_TITLE = @"My Tribes";

//image names
NSString* const SEARCH_BUTTON_ICON_NAME = @"ico-nav-search";
NSString* const NEW_TRIBE_BUTTON_ICON_NAME = @"ico-nav-new-tribe";

//placeholder label string
NSString* const PLACEHOLDER_LABEL_STRING = @"Organize your Activebudz by creating a Tribe! Or Search for Tribes that match your interests!";

#define REFRESH_TRIBE 0
#define LOAD_MORE_TRIBE 1

@interface GTRMyTribeViewController () <UITableViewDelegate, UITableViewDataSource, GTRTribeDetailViewControllerDelegate, GTRNewPostViewControllerDelegate>

@property (strong,nonatomic) NSMutableArray *tribeArray;
@property (strong,nonatomic) NSNumber *maxID;
@property (strong,nonatomic) GTRTribeCell *cellHeightCalculator;

@end

@implementation GTRMyTribeViewController
@synthesize tribeArray, tribeTableView, placeholderLabel, maxID, cellHeightCalculator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(MY_TRIBE_PAGE_TITLE, @"my tribe page title");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNavigationBarButtons];
    [self setAppearance];
    [self initiateReloadFunctionality];
    
    //initiate cell height calculator
    cellHeightCalculator = [[GTRTribeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellHeightCalculator"];
    
    //initiate tribe array
    tribeArray = [[NSMutableArray alloc] init];
    [self reload:REFRESH_TRIBE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Appearance methods

-(void)setAppearance
{
    //set background color
    self.view.backgroundColor = [UIColor colorWithWhite:0.941 alpha:1.000];

    //set tableview
    tribeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    tribeTableView.backgroundColor = [UIColor clearColor];
    tribeTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tribeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tribeTableView.dataSource = self;
    tribeTableView.delegate = self;
    [self.view addSubview:tribeTableView];
    
    //set placeholder label
    NSDictionary *attributes = @{
                                 NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:14.0f],
                                 NSForegroundColorAttributeName : UIColorFromRGB(0x5E5E5E)
                                 };
    NSAttributedString *placeholderText = [[NSAttributedString alloc] initWithString:PLACEHOLDER_LABEL_STRING
                                                                          attributes:attributes];
    placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0,
                                                                 self.view.bounds.size.width - 20,
                                                                 self.view.bounds.size.height*0.75)];
    placeholderLabel.backgroundColor = self.view.backgroundColor;
    placeholderLabel.attributedText = placeholderText;
    placeholderLabel.textAlignment = NSTextAlignmentCenter;
    placeholderLabel.numberOfLines = 0;
    placeholderLabel.lineBreakMode = NSLineBreakByWordWrapping;
    placeholderLabel.contentMode = UIViewContentModeCenter;
    [self.view addSubview:placeholderLabel];
}

-(void)setNavigationBarButtons
{
    [GTRNavigationBarHelper setLeftSideBarButtonItem:self.navigationItem withTarget:self selector:@selector(toggleSideMenu:)];
    
    UIBarButtonItem *item0 = [GTRNavigationBarHelper createBarButtonItemWithTarget:self selector:@selector(newTribeButtonAction:) imageNamed:NEW_TRIBE_BUTTON_ICON_NAME];
    UIBarButtonItem *item1 = [GTRNavigationBarHelper createBarButtonItemWithTarget:self selector:@selector(searchButtonAction:) imageNamed:SEARCH_BUTTON_ICON_NAME];
    [GTRNavigationBarHelper setRightBarButtonItems:self.navigationItem withItems:@[item0, item1] animated:NO];
}

#pragma mark - Button actions

-(void)searchButtonAction:(id)sender
{
    GTRTribeSearchViewController *tribeSearchViewController = [[GTRTribeSearchViewController alloc]init];
    [self.navigationController pushViewController:tribeSearchViewController animated:YES];
}

-(void)newTribeButtonAction:(id)sender
{
    GTRCreateNewTribeViewController *createNewTribeViewController = [[GTRCreateNewTribeViewController alloc]init];
    createNewTribeViewController.delegate = self;
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:createNewTribeViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark - UITableView delagates
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cellHeightCalculator setContent:[tribeArray objectAtIndex:indexPath.row]];
    
    return cellHeightCalculator.frame.size.height;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = tribeArray.count;

    placeholderLabel.hidden = (count != 0);
    
    return count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TribeCell";
    GTRTribeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    GTRTribe *tribe = [tribeArray objectAtIndex:indexPath.row];
    
    if (!cell) {
        cell = [[GTRTribeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    //set the tribe to cell
    [cell setContent:tribe];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GTRTribe *tribe = [tribeArray objectAtIndex:indexPath.row];
    
    if (tribe) {
        GTRTribeDetailViewController *tribeDetailViewController = [[GTRTribeDetailViewController alloc] init];
        [tribeDetailViewController setContent:tribe];
        tribeDetailViewController.delegate = self;
        [self.navigationController pushViewController:tribeDetailViewController animated:YES];
    }
}

#pragma mark - GTRTribeDetailViewController delegates

-(void)onLeftFromTribe
{
    [self reload:REFRESH_TRIBE];
}

#pragma mark - Reload
-(void)reload:(NSInteger)theReloadMode
{
    NSNumber *buffMaxID = maxID;
    
    if(theReloadMode == REFRESH_TRIBE) {
        maxID = nil;
    }
    
    [GTRTribeRestService retrieveJoinedTribeWithMaxID:maxID OnSuccess:^(id responseObject) {
        
        if(theReloadMode == REFRESH_TRIBE) {
            DDLogInfo(@"Refresh tribe mode!");
            [tribeArray removeAllObjects];
            [tribeTableView reloadData];
        }
        
        NSArray *tribes = [((NSDictionary*)responseObject) objectForKey:@"data"];

        [tribeArray addObjectsFromArray:tribes modelOfClass:[GTRTribe class]];
        
        maxID = ((GTRTribe*)[tribeArray lastObject]).tribeID;
        
        [tribeTableView.infiniteScrollingView stopAnimating];
        [tribeTableView.pullToRefreshView stopAnimating];
        
        [tribeTableView reloadData];
    } onFail:^(id responseObject, NSError *error) {
        //revert the max ID
        maxID = buffMaxID;
        
        [tribeTableView.infiniteScrollingView stopAnimating];
        [tribeTableView.pullToRefreshView stopAnimating];
    }];
}

#pragma mark - Custom methods
- (void)initiateReloadFunctionality
{
    __weak GTRMyTribeViewController *weakSelf = self;
    
    [tribeTableView addPullToRefreshWithActionHandler:^{
        [weakSelf reload:REFRESH_TRIBE];
    }];
    
    [tribeTableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf reload:LOAD_MORE_TRIBE];
    }];
}

-(void)onNewPostCreated:(id)post
{
    [self reload:REFRESH_TRIBE];
}

@end
