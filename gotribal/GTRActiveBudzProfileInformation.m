//
//  GTRActiveBudzProfileInformation.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActiveBudzProfileInformation.h"
#import "GTRConstants.h"
#import <QuartzCore/QuartzCore.h>
#import "GTRUserDetail.h"

//informations
typedef enum {
    Gender = 0,
    Role,
    RolePreference,
    Lifestyle,
    FitnessPursuit,
    FitnessLevel
}GTRActiveBudzProfileInformations;

//title strings
NSString* const USER_DETAIL_GENDER_STRING = @"Gender:";
NSString* const USER_DETAIL_ROLE_STRING = @"My Role:";
NSString* const USER_DETAIL_ROLE_PREFERENCE_STRING = @"Desired Connection:";
NSString* const USER_DETAIL_LIFESTYLE_STRING = @"Lifestyle:";
NSString* const USER_DETAIL_FITNESS_PURSUIT_STRING = @"Fitness Pursuit:";
NSString* const USER_DETAIL_FITNESS_LEVEL_STRING = @"Fitness Level:";

//gender strings
NSString* const GENDER_MALE_STRING = @"Male";
NSString* const GENDER_FEMALE_STRING = @"Female";

//role strings
NSString* const ROLE_MENTOR_STRING = @"Mentor";
NSString* const ROLE_MENTEE_STRING = @"Mentee";
NSString* const ROLE_TRAINING_BUDDY_STRING = @"Training Buddy";

//lifestyle strings
NSString* const LIFESTYLE_SINGLE_STRING = @"Single";
NSString* const LIFESTYLE_MARRIED_STRING = @"Married";
NSString* const LIFESTYLE_FULL_HOUSE_STRING = @"Full House";
NSString* const LIFESTYLE_SOLO_STRING = @"Solo With Kids";
NSString* const LIFESTYLE_RETIREMENT_STRING = @"Rockin Retirement";

//pursuit custom strings
NSString* const FITNESS_PURSUIT_FITNESS_ORIGINAL_STRING = @"Get lean and strong";
NSString* const FITNESS_PURSUIT_FITNESS_CUSTOM_STRING = @"Fitness";


//fitness level strings
NSString* const FITNESS_LEVEL_PROFESSIONAL = @"Professional";
NSString* const FITNESS_LEVEL_ADVANCED = @"Advanced";
NSString* const FITNESS_LEVEL_INTERMEDIATE = @"Intermediate";
NSString* const FITNESS_LEVEL_BEGINNER = @"Beginner";

@interface GTRActiveBudzProfileInformation()

@property (strong,nonatomic) GTRUser *user;

@end


@implementation GTRActiveBudzProfileInformation

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame User:(GTRUser*)theUser
{
    self = [super initWithFrame:frame];
    if (self) {
        self.user = theUser;
        [self setAppearance];
    }
    
    return self;
}

-(void) setAppearance
{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [UIColor colorWithWhite:0.800 alpha:1.000].CGColor;
    self.layer.cornerRadius = 5.0f;
    
    //arrays
    NSArray *titles = @[USER_DETAIL_GENDER_STRING, USER_DETAIL_ROLE_STRING, USER_DETAIL_ROLE_PREFERENCE_STRING,
                        USER_DETAIL_LIFESTYLE_STRING, USER_DETAIL_FITNESS_PURSUIT_STRING, USER_DETAIL_FITNESS_LEVEL_STRING];
    
    NSArray *keys = @[USER_DETAIL_GENDER_KEY, USER_DETAIL_ROLE_KEY, USER_DETAIL_ROLE_PREFERENCE_KEY,
                      USER_DETAIL_LIFESTYLE_KEY, USER_DETAIL_FITNESS_PURSUIT_KEY, USER_DETAIL_FITNESS_LEVEL_KEY];
    
    NSArray *genders = @[GENDER_MALE_STRING, GENDER_FEMALE_STRING];
    
    NSArray *roles = @[ROLE_MENTOR_STRING, ROLE_MENTEE_STRING, ROLE_TRAINING_BUDDY_STRING];
    
    NSArray *lifestyles = @[LIFESTYLE_SINGLE_STRING, LIFESTYLE_MARRIED_STRING, LIFESTYLE_FULL_HOUSE_STRING,
                            LIFESTYLE_SOLO_STRING, LIFESTYLE_RETIREMENT_STRING];
    
    NSArray *fitnessLevels = @[FITNESS_LEVEL_PROFESSIONAL, FITNESS_LEVEL_ADVANCED, FITNESS_LEVEL_INTERMEDIATE, FITNESS_LEVEL_BEGINNER];
    
    UIFont *textFont = [UIFont fontWithName:@"HelveticaNeue" size:14.0f];
    UIColor *titleColor = [UIColor colorWithWhite:0.086 alpha:1.000];
    UIColor *fieldColor = [UIColor colorWithWhite:0.333 alpha:1.000];
    
    CGFloat titleVerticalPadding = 5;
    CGFloat titleYOffset = titleVerticalPadding;
    CGFloat titleX = 0;
    CGFloat titleHeight = 23;
    CGFloat titleWidth = 133;
    
    CGFloat fieldX = titleX + titleWidth + 5;
    CGFloat fieldHeight = titleHeight;
    CGFloat fieldWidth = self.frame.size.width - fieldX;
    
    NSString *bufferString = @"";
    NSAttributedString *bufferAttributedString;
    UILabel *bufferLabel;
    GTRUserDetail *bufferDetail;
    
    int count = (int)titles.count;
    
    for (int i = 0; i < count; i++) {

        
        bufferDetail =  [[self.user.details filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"detailKey == %@",
                                                                         [keys objectAtIndex:i]]] firstObject];

        if (bufferDetail) { //only add the info if it's avaiable.
            
            //add title
            bufferAttributedString = [[NSAttributedString alloc] initWithString:[titles objectAtIndex:i]
                                                                     attributes:@{
                                                                                  NSFontAttributeName : textFont,
                                                                                  NSForegroundColorAttributeName : titleColor}];
            
            bufferLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleX, titleYOffset, titleWidth, titleHeight)];
            bufferLabel.attributedText = bufferAttributedString;
            bufferLabel.backgroundColor = [UIColor clearColor];
            bufferLabel.textAlignment = NSTextAlignmentRight;
            [bufferLabel sizeToFit];
            
            //to fix the label's width after size to fit
            CGRect bufferLabelFrame = bufferLabel.frame;
            bufferLabelFrame.size.width = titleWidth;
            bufferLabel.frame = bufferLabelFrame;
            [self addSubview:bufferLabel];
            
            //add field
            switch (i) {
                case Gender:
                {
                    int value = [((NSNumber*)bufferDetail.detailValue) intValue];
                    bufferString = ((NSString*)[genders objectAtIndex:value]);
                }
                    break;
                    
                case Role:
                {
                    NSArray *value;
                    NSMutableString *mutableBufferString =[[NSMutableString alloc] init];
                    
                    if ([bufferDetail.detailValue isKindOfClass:[NSArray class]]) {
                        value = ((NSArray*)bufferDetail.detailValue);
                    } else {
                        value = @[(bufferDetail.detailValue)];
                    }
                    
                    int roleCount = 0;
                    
                    for (NSNumber *numberValue in value) {
                        if (roleCount > 0) {
                            [mutableBufferString appendString:@", "];
                        }
                        [mutableBufferString appendString:((NSString*)[roles objectAtIndex:[numberValue integerValue]])];
                        
                        roleCount++;
                    }
                    
                    bufferString = [mutableBufferString copy];
                }
                    break;
                    
                case RolePreference:
                {
                    NSArray *value = ((NSArray*)bufferDetail.detailValue);
                    NSMutableString *mutableBufferString = [[NSMutableString alloc] init];
                    
                    int roleCount = 0;
                    
                    for (NSNumber *numberValue in value) {
                        if (roleCount > 0) {
                            [mutableBufferString appendString:@", "];
                        }
                        [mutableBufferString appendString:((NSString*)[roles objectAtIndex:[numberValue integerValue]])];
                        
                        roleCount++;
                    }
                    
                    bufferString = [mutableBufferString copy];
                }
                    break;
                    
                case Lifestyle:
                {
                    int value = [((NSNumber*)bufferDetail.detailValue) intValue];
                    bufferString = ((NSString*)[lifestyles objectAtIndex:value]);
                }
                    break;
                    
                case FitnessPursuit:
                {
                    NSArray *value = ((NSArray*)bufferDetail.detailValue);
                    NSMutableString *mutableBufferString = [[NSMutableString alloc] init];
                    
                    int pursuitCount = 0;
                    
                    for (NSString *stringValueBuff in value) {
                        
                        NSString *stringValue = [stringValueBuff stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                        
                        if (![[stringValue stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""]) {
                            
                            DDLogInfo(@"PURSUIT STRINGVALUE : %@", stringValue);
                            if (pursuitCount > 0) {
                                [mutableBufferString appendString:@", "];
                            }

                            [mutableBufferString appendString:stringValue];
                            
                            pursuitCount++;
                        }
                    }
                    
                    bufferString = [mutableBufferString copy];
                }
                    break;
                    
                case FitnessLevel:
                {
                    int value = [((NSNumber*)bufferDetail.detailValue) intValue];
                    bufferString = ((NSString*)[fitnessLevels objectAtIndex:value]);
                }
                    break;
            }
            
            bufferAttributedString = [[NSAttributedString alloc] initWithString:bufferString
                                                                     attributes:@{
                                                                                  NSFontAttributeName : textFont,
                                                                                  NSForegroundColorAttributeName : fieldColor}];
            
            bufferLabel = [[UILabel alloc] initWithFrame:CGRectMake(fieldX, titleYOffset, fieldWidth, fieldHeight)];
            bufferLabel.attributedText = bufferAttributedString;
            bufferLabel.backgroundColor = [UIColor clearColor];
            bufferLabel.textAlignment = NSTextAlignmentLeft;
            bufferLabel.numberOfLines = 0;
            [bufferLabel sizeToFit];
            [self addSubview:bufferLabel];
            
            titleYOffset += bufferLabel.frame.size.height; //titleHeight;
        }

    }
    
    
    //if all information is set as private..
    if (titleYOffset == titleVerticalPadding) { //create a label to show no information available.
    
        NSAttributedString *noInformationString = [[NSAttributedString alloc] initWithString:@"No information available."
                                                                                  attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:18.0f] }];
        
        UILabel *noInformationLabel = [[UILabel alloc] initWithFrame:self.bounds]; //fill the frame
        noInformationLabel.attributedText = noInformationString;
        noInformationLabel.textColor = titleColor;
        noInformationLabel.backgroundColor = [UIColor clearColor];
        noInformationLabel.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:noInformationLabel];
        self.contentMode = UIViewContentModeCenter;
        
    } else { //if not
        //resize the height.
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, titleYOffset+titleVerticalPadding);
    }

}
-(void)setUserData:(GTRUser*)theUser
{
    self.user = theUser;
}

-(void)resetAppearance
{
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
    }
    
    [self setAppearance];
}

@end
