//
//  main.m
//  gotribal
//
//  Created by Muhammad Taufik on 11/15/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GTRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GTRAppDelegate class]));
    }
}
