//
//  GTRMessageListViewController.m
//  gotribal
//
//  Created by Muhammad Taufik on 11/29/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRMessageListViewController.h"
#import "GTRMessagesViewController.h"
#import "GTRConversation.h"
#import "GTRConversationTableViewCell.h"
#import "UIBarButtonItem+NegativeSpacer.h"
#import "GTRAppDelegate.h"
#import "GTRUser.h"
#import "AFManagerHelper.h"
#import "SVPullToRefresh.h"
#import "SVProgressHUD.h"
#import "GTRRestServiceErrorHandler.h"
#import <Pusher/Pusher.h>

#define EMPTY_PLACEHOLDER_TEXT_STRING @"Reach out to your Activebudz for advice or make plans to get out and play!"

@interface GTRMessageListViewController () <PTPusherDelegate, PTPusherPresenceChannelDelegate>
@property (nonatomic, strong) NSMutableArray *conversations;

@property (strong, nonatomic) PTPusher *pusherClient;
@property (strong, nonatomic) PTPusherPresenceChannel *presenceChannel;

@property (nonatomic, strong) GTRUser *currentUser;
@property (strong, nonatomic) UILabel *placeholderLabel;

@property (strong, nonatomic) UIBarButtonItem *editMessageListButton;
@property (strong, nonatomic) UIBarButtonItem *editDoneButton;

@property (strong, nonatomic) NSDictionary *metadata;
@end

@implementation GTRMessageListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = NSLocalizedString(@"Messages", @"messages title");
        
        // Initialize Pusher
        self.pusherClient = [PTPusher pusherWithKey:PUSHER_KEY delegate:self encrypted:YES];
        self.pusherClient.reconnectDelay = 3.0;
        self.pusherClient.authorizationURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", BASE_URL, DIRECT_MESSAGE_AUTH_ENDPOINT]];
        
        GTRAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        self.currentUser = appDelegate.currentUser;
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)dealloc
{
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(conversations))];
    [self.pusherClient disconnect];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.pusherClient connect];
    
    self.tableView.backgroundColor = [UIColor colorWithWhite:0.949 alpha:1.000];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) self.tableView.separatorInset = UIEdgeInsetsMake(0, 14, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.editDoneButton = [GTRNavigationBarHelper createBarButtonItemWithTarget:self selector:@selector(editDoneButtonAction:) buttonTitle:NSLocalizedString(@"Done", @"done")];
    self.editMessageListButton = [GTRNavigationBarHelper createBarButtonItemWithTarget:self selector:@selector(editButtonAction:) buttonTitle:NSLocalizedString(@"Edit", @"edit")];

    [self assignEditButton];
    
    /*
    NSArray *conversations = (NSArray *)[self loadObjectWithName:NSStringFromSelector(@selector(conversations))];
    self.conversations = [NSMutableArray arrayWithArray:conversations];
    [self addObserver:self forKeyPath:NSStringFromSelector(@selector(conversations)) options:0 context:NULL];
    
    NSDictionary *metadata = (NSDictionary *)[self loadObjectWithName:NSStringFromSelector(@selector(metadata))];
    self.metadata = [NSDictionary dictionaryWithDictionary:metadata];
    if (self.metadata[@"channel_name"] && self.metadata[@"event_name"]) [self connectToPusherWithChannel:self.metadata[@"channel_name"] andEvent:self.metadata[@"event_name"]];
    */
    
    self.conversations = [NSMutableArray array];
    [self addObserver:self forKeyPath:NSStringFromSelector(@selector(conversations)) options:0 context:NULL];

    
    // SVInfiniteScrolling
    __weak GTRMessageListViewController *weakSelf = self;
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf retrieveConversationsWithID:weakSelf.metadata[@"max_id"]];
        [weakSelf.tableView.infiniteScrollingView performSelector:@selector(stopAnimating) withObject:nil afterDelay:1.0];
    }];

    //initiate first conversation list request
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [self retrieveConversationsWithID:self.metadata[@"max_id"]];
}

-(void)editDoneButtonAction:(id)sender
{
    [self assignEditButton];
    
    [self setEditing:NO animated:YES];
}

-(void)editButtonAction:(id)sender
{
    [self assignDoneButton];
    
    [self setEditing:YES animated:YES];
}

-(void)assignEditButton {
    [GTRNavigationBarHelper setRightBarButtonItem:self.navigationItem withButton:self.editMessageListButton animated:YES withSpacerType:GTRSpacerTypePlainText];
}

-(void)assignDoneButton {
    [GTRNavigationBarHelper setRightBarButtonItem:self.navigationItem withButton:self.editDoneButton animated:YES withSpacerType:GTRSpacerTypePlainText];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [GTRNavigationBarHelper setLeftSideBarButtonItem:self.navigationItem withTarget:self selector:@selector(toggleSideMenu:)];
    
}

#pragma mark - API Requests

-(void)retrieveConversationsWithID:(NSNumber *)messageID
{
    NSDictionary *parameters = nil;
    if (messageID) parameters = @{ @"max_id" : messageID };
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager GET:LIST_OF_CONVERSATIONS_ENDPOINT parameters:parameters success:^(AFHTTPRequestOperation *operation, id json) {

        [SVProgressHUD dismiss];
        
        NSArray *conversations = json[@"data"];
        NSDictionary *metadata = json[@"meta_data"];
        
        if (conversations.count) {
            NSValueTransformer *transformer = [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[GTRConversation class]];
            NSArray *transformedConversations = [transformer transformedValue:conversations];
            [self insertConversations:transformedConversations atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, transformedConversations.count)]];
            
            self.metadata = metadata;
            [self connectToPusherWithChannel:self.metadata[@"channel_name"] andEvent:self.metadata[@"event_name"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        DDLogInfo(@"%@", operation.responseObject[@"error"]);
        
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Failed to load conversations", @"failed to load conversations")];
    }];
}

#pragma mark - Pusher

-(void)connectToPusherWithChannel:(NSString *)channel andEvent:(NSString *)event
{
    NSString *presenceWord = @"presence-";
    NSString *channelName = [channel copy];
    if ([channelName rangeOfString:presenceWord].location != NSNotFound) {
        channelName = [channelName stringByReplacingOccurrencesOfString:presenceWord withString:@""];
    }
    
    self.presenceChannel = [self.pusherClient subscribeToPresenceChannelNamed:channelName delegate:self];
    [self.presenceChannel bindToEventNamed:event target:self action:@selector(pusherPresenceChannelEventHandler:)];
}

-(void)pusherPresenceChannelEventHandler:(PTPusherEvent *)event
{
    DDLogInfo(@"%@", event);
    
    GTRConversation *conversationEvent = [MTLJSONAdapter modelOfClass:[GTRConversation class] fromJSONDictionary:(NSDictionary *)event.data error:nil];
    
    if (self.isViewLoaded && self.view.window) {
        conversationEvent.unreadState = GTRConversationStateUnread;
    } else {
        conversationEvent.unreadState = GTRConversationStateRead;
    }
    
    NSUInteger conversationIndex = [self.conversations indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        GTRConversation *conversation = (GTRConversation *)obj;
        if ([conversation.conversationID isEqual:conversationEvent.conversationID]) {
            *stop = YES;
            return YES;
        }
        return NO;
    }];
    
    if (conversationIndex != NSNotFound) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:conversationIndex inSection:0];
        [self replaceObjectInConversationsAtIndex:conversationIndex withObject:conversationEvent];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        if (indexPath.row > 0) {
            id object = [self.conversations objectAtIndex:indexPath.row];
            [self removeObjectFromConversationsAtIndex:indexPath.row];
            [self insertObject:object inConversationsAtIndex:0];
        }
        
    } else {
        [self insertObject:conversationEvent inConversationsAtIndex:0];
    }
}

#pragma mark - PTPusher delegate

-(void)pusher:(PTPusher *)pusher willAuthorizeChannel:(PTPusherChannel *)channel withRequest:(NSMutableURLRequest *)request
{
    AFOAuthCredential *credential = [AFOAuthCredential retrieveCredentialWithIdentifier:[[NSURL URLWithString:BASE_URL] host]];
    [request setValue:[NSString stringWithFormat:@"%@%@", @"Bearer ", credential.accessToken] forHTTPHeaderField:@"Authorization"];
}

#pragma mark - PTPusherChannelPresenceDelegate

-(void)presenceChannelDidSubscribe:(PTPusherPresenceChannel *)channel
{
    DDLogInfo(@"%@", channel);
}

- (void)presenceChannel:(PTPusherPresenceChannel *)channel memberAdded:(PTPusherChannelMember *)member
{
    DDLogInfo(@"%@ %@", channel, member);
}

- (void)presenceChannel:(PTPusherPresenceChannel *)channel memberRemoved:(PTPusherChannelMember *)member
{
    DDLogInfo(@"%@ %@", channel, member);
}

#pragma mark - Table View edit

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        GTRConversation *conversation = [self.conversations objectAtIndex:indexPath.row];
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
        [manager DELETE:DELETE_LIST_OF_CONVERSATIONS_ENDPOINT(conversation.conversationID) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            // Remove conversations
            [self removeObjectFromConversationsAtIndex:indexPath.row];
            
            // Update metadata
            NSDictionary *metadata = responseObject[@"meta_data"];
            if (metadata) {
                self.metadata = metadata;
            }
            
            [SVProgressHUD dismiss];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DDLogInfo(@"%@", [error localizedDescription]);
            [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        }];
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark - Array Accessors

- (NSUInteger)countOfConversations
{
    return [self.conversations count];
}

-(id)objectInConversationsAtIndex:(NSUInteger)index
{
    return [self.conversations objectAtIndex:index];
}

-(void)insertObject:(id)object inConversationsAtIndex:(NSUInteger)index
{
    [self.conversations insertObject:object atIndex:index];
}

-(void)removeObjectFromConversationsAtIndex:(NSUInteger)index
{
    [self.conversations removeObjectAtIndex:index];
}

-(void)insertConversations:(NSArray *)array atIndexes:(NSIndexSet *)indexes
{
    [self.conversations insertObjects:array atIndexes:indexes];
}

-(void)replaceObjectInConversationsAtIndex:(NSUInteger)index withObject:(id)object
{
    [self.conversations replaceObjectAtIndex:index withObject:object];
}

#pragma mark - Key-Value Observing

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    NSIndexSet *indices = [change objectForKey:NSKeyValueChangeIndexesKey];
    if (indices == nil) return;
    
    NSUInteger indexCount = [indices count];
    NSUInteger buffer[indexCount];
    [indices getIndexes:buffer maxCount:indexCount inIndexRange:nil];
    
    NSMutableArray *indexPathArray = [NSMutableArray array];
    for (int i = 0; i < indexCount; i++) {
        NSUInteger indexPathIndices[2];
        indexPathIndices[0] = 0;
        indexPathIndices[1] = buffer[i];
        NSIndexPath *newPath = [NSIndexPath indexPathWithIndexes:indexPathIndices length:2];
        [indexPathArray addObject:newPath];
    }
    
    NSNumber *kind = [change objectForKey:NSKeyValueChangeKindKey];
    if ([kind integerValue] == NSKeyValueChangeInsertion) {
        [self.tableView insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else if ([kind integerValue] == NSKeyValueChangeRemoval) {
        [self.tableView deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

#pragma mark - Custom actions

-(void)toggleSideMenu:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [self.navigationController popToRootViewControllerAnimated:NO];
    }];
}

#pragma mark - Table view delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  82.0f;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!self.placeholderLabel) {
        [self setupPlaceholderLabel];
    }
    
    (self.conversations.count) ? (self.placeholderLabel.hidden = YES) : (self.placeholderLabel.hidden = NO);
    
    return self.conversations.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    GTRConversationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[GTRConversationTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.conversation = [self.conversations objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    GTRConversation *conversation = [self.conversations objectAtIndex:indexPath.row];
    conversation.unreadState = GTRConversationStateRead;
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    GTRMessagesViewController *messagesViewController = [[GTRMessagesViewController alloc] initWithConversation:conversation];
    [self.navigationController pushViewController:messagesViewController animated:YES];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];

}

-(void)setupPlaceholderLabel
{
    //set the placeholder label
    NSAttributedString *placeholderText = [[NSAttributedString alloc] initWithString:NSLocalizedString(EMPTY_PLACEHOLDER_TEXT_STRING, @"no conversations string") attributes:@{
                                                                                                                                NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16.0f],
                                                                                                                                NSForegroundColorAttributeName : [UIColor colorWithWhite:0.333 alpha:1.000]
                                                                                                                                }];
    
    CGFloat placeholderHorizontalPadding = 8;
    CGFloat placeholderVerticalPadding = 20;
    CGFloat placeholderY = 0;
    CGFloat placeholderHeight = self.view.bounds.size.height - (placeholderVerticalPadding*2);
    CGFloat placeholderWidth = self.view.bounds.size.width - (placeholderHorizontalPadding*2);
    
    self.placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(placeholderHorizontalPadding,
                                                                      placeholderY,
                                                                      placeholderWidth,
                                                                      placeholderHeight)];
   
    self.placeholderLabel.hidden = YES;

    [self.tableView addSubview:self.placeholderLabel];
    
    self.placeholderLabel.backgroundColor = [UIColor clearColor];
    self.placeholderLabel.attributedText = placeholderText;
    self.placeholderLabel.textAlignment = NSTextAlignmentCenter;
    self.placeholderLabel.numberOfLines = 0;
    self.placeholderLabel.adjustsFontSizeToFitWidth = NO;
    self.placeholderLabel.contentMode = UIViewContentModeCenter;
    self.placeholderLabel.backgroundColor = [UIColor clearColor];
    [self.placeholderLabel sizeToFit];
    self.placeholderLabel.center = self.tableView.center;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.placeholderLabel.frame = CGRectOffset(self.placeholderLabel.frame, 0, -64);
    }
}

#pragma mark - Mantle persistence

-(void)persistObject:(id)object
{
    NSString *fileName = nil;
    
    if ([object isKindOfClass:[NSDictionary class]]) fileName = NSStringFromSelector(@selector(metadata));
    if ([object isKindOfClass:[NSArray class]]) fileName = NSStringFromSelector(@selector(conversations));

    NSData *archive = [NSKeyedArchiver archivedDataWithRootObject:object];
    [[NSUserDefaults standardUserDefaults] setObject:archive forKey:fileName];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

-(instancetype)loadObjectWithName:(NSString *)name
{
    NSData *archive = [[NSUserDefaults standardUserDefaults] valueForKey:name];
    id objects = [NSKeyedUnarchiver unarchiveObjectWithData:archive];
    return objects;
}

@end
