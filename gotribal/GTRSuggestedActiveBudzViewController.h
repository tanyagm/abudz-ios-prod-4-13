//
//  GTRSuggestedActiveBudzViewController.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRActiveBudzListViewController.h"

@interface GTRSuggestedActiveBudzViewController : GTRActiveBudzListViewController

@end
