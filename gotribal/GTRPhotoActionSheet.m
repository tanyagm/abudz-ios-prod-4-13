//
//  GTRActionSheet.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/13/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRPhotoActionSheet.h"

#define ACTION_SHEET_GREEN_COLOR [UIColor colorWithRed:0.12 green:0.58 blue:0.6 alpha:1.0]
#define BACKGROUND [UIColor colorWithRed:0.15 green:0.16 blue:0.21 alpha:0.9];
#define CANCEL_BACKGROUND_COLOR [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0]

NSString *const ACTION_SHEET_TITLE = @"Add a photo to your tribe";

NSInteger CUSTOM_PHOTO_BUTTON_HEIGHT = 50;
NSInteger CUSTOM_PHOTO_BUTTON_WIDTH  = 300;
NSInteger SUPER_CONTAINER_TAG = 456;

@interface GTRPhotoActionSheet ()

@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UIView *containerView;
@property(nonatomic,strong) UIButton *takePhotoButton;
@property(nonatomic,strong) UIButton *chooseFromLibraryButton;
@property(nonatomic,strong) UIButton *chooseFromActivebudz;
@property(nonatomic,strong) UIButton *cancelButton;
@property(nonatomic,strong) UIView *lineView;
@property(nonatomic,strong) UIView *photoSuperview;
@property(nonatomic,strong) UIView *superView;
@property(nonatomic,strong) UIView *superContainerView;
@property(nonatomic,assign) NSInteger erasedCounterButton;

@end

@implementation GTRPhotoActionSheet
@synthesize titleLabel = _titleLabel;
@synthesize containerView = _containerView;
@synthesize takePhotoButton = _takePhotoButton;
@synthesize chooseFromActivebudz = _chooseFromActivebudz;
@synthesize chooseFromLibraryButton = _chooseFromLibraryButton;
@synthesize cancelButton = _cancelButton;
@synthesize photoSuperview = _photoSuperview;
@synthesize superView = _superView;
@synthesize superContainerView = _superContainerView;
@synthesize erasedCounterButton = _erasedCounterButton;
@synthesize lineView = _lineView;
@synthesize title = _title;

-(id)initWithSuperView:(UIView *)superview useActivebudzPhotos:(BOOL)useActivebudzPhotos delegate:(id)delegate
{
    self = [super init];
    if (self) {
        UIColor *whiteColor = [UIColor whiteColor];
        _superContainerView = [[UIView alloc]initWithFrame:superview.bounds];
        _superContainerView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        _superContainerView.tag = SUPER_CONTAINER_TAG;
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                    action:@selector(cancelButtonHandler:)];
        tapGesture.cancelsTouchesInView = YES;
        [_superContainerView addGestureRecognizer:tapGesture];
        
        _containerView = [[UIView alloc]init]; // background view
        _containerView.backgroundColor = BACKGROUND;
        _containerView.userInteractionEnabled = YES;
        [_superContainerView addSubview:_containerView];
        
        _superView = superview;
        self.delegate = delegate;
        
        _erasedCounterButton = 0;
        
        BOOL isCameraAvailable = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]
        || [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
        
        if (!isCameraAvailable) {
            _erasedCounterButton += 1;
        }
        
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.frame = CGRectMake(10, 5, 300, 30);
        _titleLabel.text = ACTION_SHEET_TITLE;
        _titleLabel.textColor = whiteColor;
        _titleLabel.font = CUSTOM_ACTION_SHEET_FONT;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [_containerView addSubview:_titleLabel];
        
        NSString *takePhoto = @"Take Photo";
        NSString *chooseFromLibrary = @"Choose From Library";
        NSString *chooseFromActiveBudz = @"Choose From Activebudz Photos";
        NSString *cancel = @"Cancel";
        
        _takePhotoButton = [[UIButton alloc]init];
        _takePhotoButton.backgroundColor = ACTION_SHEET_GREEN_COLOR;
        _takePhotoButton.frame = isCameraAvailable ? CGRectMake(10, CGRectGetHeight(_titleLabel.frame)+ 5,
                                                                CUSTOM_PHOTO_BUTTON_WIDTH, CUSTOM_PHOTO_BUTTON_HEIGHT): CGRectZero;
        [_takePhotoButton setTitle:takePhoto forState:UIControlStateNormal];
        [_takePhotoButton setTitle:takePhoto forState:UIControlStateHighlighted];
        [_takePhotoButton setTitleColor:whiteColor forState:UIControlStateNormal];
        _takePhotoButton.layer.cornerRadius = 3.0;
        _takePhotoButton.layer.masksToBounds = YES;
        _takePhotoButton.titleLabel.font = CUSTOM_ACTION_SHEET_FONT;
        [_takePhotoButton addTarget:self action:@selector(takePhotoActionHandler:)
                   forControlEvents:UIControlEventTouchUpInside];
        [_containerView addSubview:_takePhotoButton];
        
        _chooseFromLibraryButton = [[UIButton alloc]init];
        _chooseFromLibraryButton.backgroundColor = ACTION_SHEET_GREEN_COLOR;
        _chooseFromLibraryButton.frame = CGRectMake(10, CGRectGetHeight(_titleLabel.frame)  + 5 +
                                                    CGRectGetHeight(_takePhotoButton.frame) + 10,
                                                    CUSTOM_PHOTO_BUTTON_WIDTH,
                                                    CUSTOM_PHOTO_BUTTON_HEIGHT);
        [_chooseFromLibraryButton setTitle:chooseFromLibrary forState:UIControlStateNormal];
        [_chooseFromLibraryButton setTitle:chooseFromLibrary forState:UIControlStateHighlighted];
        [_chooseFromLibraryButton setTitleColor:whiteColor forState:UIControlStateNormal];
        _chooseFromLibraryButton.layer.cornerRadius = 3.0;
        _chooseFromLibraryButton.layer.masksToBounds = YES;
        _chooseFromLibraryButton.titleLabel.font = CUSTOM_ACTION_SHEET_FONT;
        [_chooseFromLibraryButton addTarget:self action:@selector(chooseFromLibraryActionHandler:)
                           forControlEvents:UIControlEventTouchUpInside];
        [_containerView addSubview:_chooseFromLibraryButton];
        
        _chooseFromActivebudz = [[UIButton alloc]init];
        _chooseFromActivebudz.backgroundColor = ACTION_SHEET_GREEN_COLOR;
        _chooseFromActivebudz.frame = CGRectMake(10, CGRectGetHeight(_titleLabel.frame)  + 5 +
                                                 CGRectGetHeight(_takePhotoButton.frame) + 10 +
                                                 CGRectGetHeight(_chooseFromLibraryButton.frame) + 10,
                                                 CUSTOM_PHOTO_BUTTON_WIDTH, CUSTOM_PHOTO_BUTTON_HEIGHT);
        [_chooseFromActivebudz setTitle:chooseFromActiveBudz forState:UIControlStateNormal];
        [_chooseFromActivebudz setTitle:chooseFromActiveBudz forState:UIControlStateHighlighted];
        [_chooseFromActivebudz setTitleColor:whiteColor forState:UIControlStateNormal];
        _chooseFromActivebudz.layer.cornerRadius = 3.0;
        _chooseFromActivebudz.layer.masksToBounds = YES;
        _chooseFromActivebudz.titleLabel.font = CUSTOM_ACTION_SHEET_FONT;
        [_chooseFromActivebudz addTarget:self action:@selector(chooseFromActivebudzActionHandler:)
                        forControlEvents:UIControlEventTouchUpInside];
        [_containerView addSubview:_chooseFromActivebudz];
        
        if (!useActivebudzPhotos) {
            _erasedCounterButton +=1;
            _chooseFromActivebudz.frame = CGRectZero;
        }
        
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.5];
        _lineView.layer.shadowColor = [UIColor colorWithWhite:0.5 alpha:1.0].CGColor;
        _lineView.layer.shadowOpacity = 0.2;
        _lineView.layer.shadowRadius = 0.5;
        _lineView.layer.shadowOffset = CGSizeMake(0, 0.5);
        _lineView.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, 320, 1.0)].CGPath;
        [_containerView addSubview:_lineView];
        
        _cancelButton = [[UIButton alloc]init];
        _cancelButton.backgroundColor = CANCEL_BACKGROUND_COLOR;
        [_cancelButton setTitle:cancel forState:UIControlStateNormal];
        [_cancelButton setTitle:cancel forState:UIControlStateHighlighted];
        [_cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _cancelButton.layer.cornerRadius = 3.0;
        _cancelButton.layer.masksToBounds = YES;
        _cancelButton.titleLabel.font = CUSTOM_ACTION_SHEET_FONT;
        [_cancelButton addTarget:self action:@selector(cancelButtonHandler:)
                forControlEvents:UIControlEventTouchUpInside];
        [_containerView addSubview:_cancelButton];
    }
    
    return self;
}

-(void)show
{
    [[_superView viewWithTag:SUPER_CONTAINER_TAG] removeFromSuperview];
    
    NSInteger containerHeight = 284;
    for (int i = 0; i<_erasedCounterButton; i++) {
        containerHeight -= (CUSTOM_PHOTO_BUTTON_HEIGHT + 10);
    }
    
    _containerView.frame = CGRectMake(0, _superContainerView.frame.size.height,
                                      320, containerHeight);
    
    _lineView.frame = CGRectMake(0,_containerView.frame.size.height - (CUSTOM_PHOTO_BUTTON_HEIGHT + 20)  , 320, 1);
    // as _containerView's frame is set here, _cancelButton's frame should be set here
    _cancelButton.frame = CGRectMake(10, _containerView.frame.size.height - (CUSTOM_PHOTO_BUTTON_HEIGHT + 10),
                                     CUSTOM_PHOTO_BUTTON_WIDTH,
                                     CUSTOM_PHOTO_BUTTON_HEIGHT);
    [_superView addSubview:_superContainerView];
    
    [UIView animateWithDuration:0.4 animations:^{
        _containerView.frame = CGRectMake(0, _superContainerView.frame.size.height - containerHeight, 320, containerHeight);
    } completion:^(BOOL finished) {}];
}

-(void)remove
{
    [UIView animateWithDuration:0.4 animations:^{
        _containerView.frame = CGRectOffset(_containerView.frame, 0, 284);
    } completion:^(BOOL finished) {
        [_superContainerView removeFromSuperview];
    }];
}

#pragma mark - Set Title of custom ActionSheet

-(void)setTitle:(NSString *)title
{
    _title = title;
    _titleLabel.text = _title;
}

#pragma mark - Button handlers

-(void)takePhotoActionHandler:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(gtrActionSheet:clickedButtonAtIndex:)]) {
        [self.delegate gtrActionSheet:self clickedButtonAtIndex:0];
    }
    
    [self remove];
}

-(void)chooseFromLibraryActionHandler:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(gtrActionSheet:clickedButtonAtIndex:)]) {
        [self.delegate gtrActionSheet:self clickedButtonAtIndex:1];
    }
    
    [self remove];
}

-(void)chooseFromActivebudzActionHandler:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(gtrActionSheet:clickedButtonAtIndex:)]) {
        [self.delegate gtrActionSheet:self clickedButtonAtIndex:2];
    }
    
    [self remove];
}

-(void)cancelButtonHandler:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(gtrActionSheet:clickedButtonAtIndex:)]) {
        [self.delegate gtrActionSheet:self clickedButtonAtIndex:3];
    }
    
    [self remove];
}

-(NSString*)buttonTitleAtIndex:(NSInteger)index
{
    switch (index) {
            case 0:
                return _takePhotoButton.titleLabel.text;
                break;
            
            case 1:
                return _chooseFromLibraryButton.titleLabel.text;
                break;
            
            case 2:
                return _chooseFromActivebudz.titleLabel.text;
                break;
    }
    
    return nil;
}

@end
