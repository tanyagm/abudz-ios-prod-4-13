//
//  GTRUserRestService.m
//  gotribal
//
//  Created by Le Vady on 11/30/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRUserRestService.h"
#import "GTRUserHelper.h"
#import "GTRPaginationHelper.h"
#import "GTRRestServiceErrorHandler.h"
#import <AFNetworking/UIAlertView+AFNetworking.h>

@implementation GTRUserRestService

+(void)retrieveCurrentUserOnSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager GET:CURRENT_USER_ENDPOINT parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        [GTRUserHelper updateCurrentUser:responseObject];
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}

+(void)registerDevice:(NSString *)deviceToken
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    NSDictionary *params = @{
                             @"uid": [GTRUserHelper getUDID],
                             @"name": [[UIDevice currentDevice] name],
                             @"token": deviceToken,
                             @"device_type": DEVICE_TYPE
                            };
    
    [manager POST:REGISTER_DEVICE_ENDPOINT parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@" Success Response : %@", responseObject);
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:deviceToken forKey:USER_DEVICE_TOKEN];
        [defaults synchronize];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@" Failed Response : %@", [error localizedDescription]);
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
    }];
}


+(void)updateFitnessProfile:(NSDictionary *)params onSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];

    [manager PUT:UPDATE_FITNESS_PROFILE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [GTRUserHelper updateCurrentUser:responseObject];
        if (success)
            success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler)
            errorHandler(operation, error);
    }];
}

+(void)retrieveCurrentUserPendingRequestWithLastMaxID:(NSNumber*)lastMaxID
                                            onSuccess:(void(^)(id responseObject))success
                                               onFail:(void (^) (id response, NSError *error))fail
{
    NSString *finaleEndpoint = nil;
    NSNumber *perPage = @10;
    
    if (!lastMaxID) {
        if (success) {
            success(nil);
        }
        
        return;
    }
    else if ([lastMaxID isEqual:[NSNumber numberWithInt:-1]]) {
        finaleEndpoint = [GTRPaginationHelper paginate:FRIENDS_RECEIVE_PENDING_REQUEST
                                             withLimit:perPage
                                              andMaxID:nil];
    } else if([lastMaxID intValue] > -1){
        finaleEndpoint = [GTRPaginationHelper paginate:FRIENDS_RECEIVE_PENDING_REQUEST
                                             withLimit:perPage
                                              andMaxID:lastMaxID];
    }
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager GET:finaleEndpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        if (success){
            success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error retrieving curent user's penidng request : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (fail){
            fail(operation, error);
        }
    }];
}

+(void)retrieveCurrentUserPendingRequestonSuccess:(void(^)(id responseObject))success
                                           onFail:(void (^) (id response, NSError *error))fail
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager GET:FRIENDS_RECEIVE_PENDING_REQUEST parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        if (success){
            success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error retrieving curent user's penidng request : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (fail){
            fail(operation, error);
        }
    }];
}

+(void)sendConfirmationWithType:(GTRUserRestServiceFriendRequest)type withUserID:(NSNumber*)userID onSuccess:(void(^)(id responseObject))success onFail:(void (^) (id response, NSError *error))fail
{
    if (!userID) {
        if (fail) {
            fail(nil,nil);
        }
        
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    void(^successBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);

        if (success){
            success(responseObject);
        }
    };
    
    void(^failBlock)(AFHTTPRequestOperation *operation, NSError *error) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error sending confirmation : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (fail){
            fail(operation, error);
        }
    };
    
    if (type == GTRUserRestServiceFriendRequestConfirm) {
        NSString *deleteEndpoint = [NSString stringWithFormat:@"%@%@", FRIENDS_PENDING_REQUEST_CONFIRM,userID];
        [manager POST:deleteEndpoint parameters:nil success:successBlock failure: failBlock];
    } else if(type == GTRUserRestServiceFriendRequestIgnore){
        NSString *deleteEndpoint = [NSString stringWithFormat:@"%@%@", FRIENDS_PENDING_REQUEST_CONFIRM,userID];
        [manager DELETE:deleteEndpoint parameters:nil success:successBlock failure:failBlock];
    }
}


+(void)recoverPassword:(NSString *)email onSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler
{
    DDLogInfo(@"recoverPassword %@", email);
    NSDictionary *params = @{@"email": email};
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    [manager POST:RECOVER_PASSWORD_ENDPOINT parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        if (success){
            success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error    : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler){
            errorHandler(operation, error);
        }
    }];
}

@end
