//
//  GTRTribeHelper.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/17/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GTRSideMenuCell;

@interface GTRTribeHelper : NSObject

+(void)setAvatarImage:(UIImage*)avatarImage
                 text:(NSString*)string
               inCell:(GTRSideMenuCell*)cell;

@end
