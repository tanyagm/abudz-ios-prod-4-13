//
//  GTRSuggestedActiveBudzCarouselItem.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/9/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTRActiveBudzCarouselItem : UIImageView
- (void) setContent:(GTRUser*) user;
@end
