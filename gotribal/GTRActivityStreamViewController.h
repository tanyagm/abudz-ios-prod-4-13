//
//  GTRActivityStreamViewController.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRStreamPageViewController.h"

@interface GTRActivityStreamViewController : GTRStreamPageViewController

@end
