//
//  GTRTribeRestService.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/11/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRTribeRestService.h"
#import "GTRPaginationHelper.h"
#import "GTRConstants.h"
#import <AFNetworking/UIAlertView+AFNetworking.h>
#import "GTRRestServiceErrorHandler.h"

@implementation GTRTribeRestService

+(void)retrieveAllTribeWithMaxID:(NSNumber*) maxID OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    NSString *endpoint = [GTRPaginationHelper paginate:TRIBE_ENDPOINT withLimit:[NSNumber numberWithInt:MAX_PER_PAGE] andMaxID:maxID];
    
    [manager GET:endpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@",responseObject);
        
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@",error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation.responseObject, error);
    }];
}

+(void)retrieveJoinedTribeWithMaxID:(NSNumber*) maxID OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    NSString *endpoint = [GTRPaginationHelper paginate:[NSString stringWithFormat:@"%@%@",TRIBE_ENDPOINT,@"/joined"]
                                             withLimit:[NSNumber numberWithInt:MAX_PER_PAGE]
                                              andMaxID:maxID];
    
    [manager GET:endpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@",responseObject);
        
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@",error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation.responseObject, error);
    }];
}

+(void)retrieveOwnedTribeWithMaxID:(NSNumber*) maxID OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    NSString *endpoint = [GTRPaginationHelper paginate:[NSString stringWithFormat:@"%@%@",TRIBE_ENDPOINT,@"/owned"]
                                             withLimit:[NSNumber numberWithInt:MAX_PER_PAGE]
                                              andMaxID:maxID];
    
    [manager GET:endpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@",responseObject);
        
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@",error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation.responseObject, error);
    }];
}

+(void)postNewTribeWithPayload:(NSDictionary*)payload onSuccess:(void (^)(id responseObject))success
                        onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager POST:TRIBE_ENDPOINT parameters:payload success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"POST new tribe Response : %@",responseObject);
        
        if (success) {
            success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error POST new tribe: %@",error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) {
            errorHandler(operation.responseObject,error);
        }
    }];
}

+(void)retrieveTribeMembers:(GTRTribe*)theTribe MaxID:(NSNumber*) maxID OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    NSString *endpoint = [GTRPaginationHelper paginate:[NSString stringWithFormat:@"%@/%@/%@",TRIBE_ENDPOINT,theTribe.tribeID,@"members"]
                                             withLimit:[NSNumber numberWithInt:MAX_PER_PAGE]
                                              andMaxID:maxID];
    
    [manager GET:endpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@",responseObject);
        
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@",error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation.responseObject, error);
    }];
}

+(void)retrieveTribePosts:(GTRTribe*)theTribe MaxID:(NSNumber*) maxID OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    NSString *endpoint = [GTRPaginationHelper paginate:[NSString stringWithFormat:@"%@/%@/%@",TRIBE_ENDPOINT,theTribe.tribeID,@"posts"]
                                             withLimit:[NSNumber numberWithInt:MAX_PER_PAGE]
                                              andMaxID:maxID];
    
    [manager GET:endpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@",responseObject);
        
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@",error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation.responseObject, error);
    }];
}

+(void)joinTribe:(GTRTribe*)theTribe OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    NSString *endpoint = [NSString stringWithFormat:@"%@/%@/%@",TRIBE_ENDPOINT,theTribe.tribeID,@"join"];
    
    [manager POST:endpoint parameters:@{} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@",responseObject);
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@",error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation.responseObject, error);
    }];
}

+(void)leaveTribe:(GTRTribe*)theTribe OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    NSString *endpoint = [NSString stringWithFormat:@"%@/%@/%@",TRIBE_ENDPOINT,theTribe.tribeID,@"leave"];
    
    [manager DELETE:endpoint parameters:@{} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@",responseObject);
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@",error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation.responseObject, error);
    }];
}

+(void)searchTribeWithKeyword:(NSString*)keyword
                    onSuccess:(void (^)(id responseObject))success
                       onFail:(void(^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    keyword = [keyword stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    keyword = [keyword stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSString *endpoint = [NSString stringWithFormat:@"%@/search?keyword=%@",TRIBE_ENDPOINT,keyword];
    [manager GET:endpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response Search Tribe: %@",responseObject);
        if (success) {
            success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error Search Tribe: %@",error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) {
            errorHandler(operation,error);
        }
    }];
}

+(void)inviteToTribe:(GTRTribe*)theTribe
          parameters:(NSDictionary*) theParameters
           onSuccess:(void (^)(id responseObject))success
              onFail:(void(^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    NSString *endpoint = [NSString stringWithFormat:@"%@/%@/%@",TRIBE_ENDPOINT,theTribe.tribeID,@"invites"];
    
    [manager POST:endpoint parameters:theParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@",responseObject);
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@",error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation.responseObject, error);
    }];
}

+(void)retrieveInviteableTribeMembers:(GTRTribe*)theTribe MaxID:(NSNumber*)maxID OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    NSString *endpoint = [GTRPaginationHelper paginate:[NSString stringWithFormat:@"%@/%@/%@",TRIBE_ENDPOINT,theTribe.tribeID,@"inviteable"]
                                             withLimit:[NSNumber numberWithInt:MAX_PER_PAGE]
                                              andMaxID:maxID];
    
    [manager GET:endpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@",responseObject);
        
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@",error);
        
        //[GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation];
        if (errorHandler) errorHandler(operation.responseObject, error);
    }];
}

@end
