//
//  GTRFriendsTableViewCell.m
//  gotribal
//
//  Created by Muhammad Taufik on 12/19/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActivebudzInviteTableViewCell.h"

@interface GTRActivebudzInviteTableViewCell ()
@property (nonatomic, strong) UIView *separator;
@end

@implementation GTRActivebudzInviteTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.clipsToBounds = YES;
        self.layer.rasterizationScale = [[UIScreen mainScreen] scale];
        self.layer.shouldRasterize = YES;
        
        self.backgroundColor = GTR_MAIN_ACTIVITIES_BACKGROUND_COLOR;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0f];
        self.imageView.clipsToBounds = YES;
        self.imageView.layer.cornerRadius = 34/2;
        
        self.separator = [[UIView alloc] initWithFrame:CGRectZero];
        self.separator.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1.0f];
        [self addSubview:self.separator];
        
        self.inviteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.inviteButton.layer.cornerRadius = 4.0;
        self.inviteButton.layer.masksToBounds = YES;
        
        [self.inviteButton setBackgroundImage:[[self class] imageWithColor:[UIColor colorWithRed:1.0/255.0 green:149.0/255.0 blue:152.0/255.0 alpha:1.0]] forState:UIControlStateNormal];
        [self.inviteButton setBackgroundImage:[[self class] imageWithColor:[UIColor colorWithRed:1.0/255.0 green:174.0/255.0 blue:177.0/255.0 alpha:1.0]] forState:UIControlStateHighlighted];
        
        [self.inviteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        NSString *buttonTitle = NSLocalizedString(@"Invite", @"invite");
        [self.inviteButton setTitle:buttonTitle forState:UIControlStateNormal];
        
        [self addSubview:self.inviteButton];
        
    }
    return self;
}

-(void)setInviteState:(GTRActivebudzInviteState)inviteState
{
    switch (inviteState) {
        case GTRActivebudzInviteStateUninvited:
            [self.inviteButton setTitle:NSLocalizedString(@"Invite", @"invite") forState:UIControlStateNormal];
            [self.inviteButton setBackgroundImage:[[self class] imageWithColor:[UIColor colorWithRed:1.0/255.0 green:149.0/255.0 blue:152.0/255.0 alpha:1.0]] forState:UIControlStateNormal];
            [self.inviteButton setBackgroundImage:[[self class] imageWithColor:[UIColor colorWithRed:1.0/255.0 green:174.0/255.0 blue:177.0/255.0 alpha:1.0]] forState:UIControlStateHighlighted];
            break;
        case GTRActivebudzInviteStateInvited:
            [self.inviteButton setTitle:NSLocalizedString(@"Invited", @"invited") forState:UIControlStateNormal];
            [self.inviteButton setBackgroundImage:[[self class] imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateNormal];
            [self.inviteButton setBackgroundImage:[[self class] imageWithColor:[UIColor colorWithRed:1.0/255.0 green:174.0/255.0 blue:177.0/255.0 alpha:1.0]] forState:UIControlStateHighlighted];
            break;
        case GTRActivebudzInviteStateJoined:
            [self.inviteButton setTitle:NSLocalizedString(@"Joined", @"joined") forState:UIControlStateNormal];
            [self.inviteButton setBackgroundImage:[[self class] imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateNormal];
            [self.inviteButton setBackgroundImage:[[self class] imageWithColor:[UIColor colorWithRed:1.0/255.0 green:174.0/255.0 blue:177.0/255.0 alpha:1.0]] forState:UIControlStateHighlighted];
            break;
        default:
            break;
    }
    
    [self setNeedsLayout];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.frame = CGRectMake(5+5, 10, 34, 34);
    self.textLabel.frame = CGRectMake(self.imageView.frame.origin.x + 34 + 15, 0, 320 - (5 + 27*2 + 10)*2, 34 + 10 + 10);
    self.separator.frame = CGRectMake(self.textLabel.frame.origin.x, self.bounds.size.height - 0.5, 320 - 34 - 20, 0.5);
    self.inviteButton.frame = CGRectMake(320 - 30*2 - 10, 10, 30*2, 34);
    
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
