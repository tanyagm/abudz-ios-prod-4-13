//
//  GTRPaginationHelper.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTRPaginationHelper : NSObject

+(NSString*)paginate: (NSString*) url withLimit:(NSNumber*)limit andMaxID:(NSNumber*)maxID;

@end
