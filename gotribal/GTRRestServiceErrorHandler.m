//
//  GTRRestServiceErrorHandler.m
//  gotribal
//
//  Created by Hisma Mulya S on 1/2/14.
//  Copyright (c) 2014 gotribal. All rights reserved.
//

#import "GTRRestServiceErrorHandler.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "GTRAppDelegate.h"

NSString *const TIMED_OUT_STRING            = @"Connection timed out";
NSString *const RESOURCE_UNAVAILABLE_STRING = @"Service unavailable";
NSString *const HOST_UNREACHABLE_STRING     = @"Host unreachable";
NSString *const RELOGIN_STRING              = @"You need to re-login";

@implementation GTRRestServiceErrorHandler

+(void)handleConnectionRelatedError:(NSError *)error
                           response:(id)response
                   requestOperation:(AFHTTPRequestOperation*)operation
{
    [SVProgressHUD dismiss];
    if (error.code == NSURLErrorTimedOut) {
        [SVProgressHUD showErrorWithStatus:TIMED_OUT_STRING];
    }
    else if (error.code == NSURLErrorNotConnectedToInternet){
        [SVProgressHUD showErrorWithStatus:NO_INTERNET_CONNECTION_MESSAGE];
    }
    else if(error.code == NSURLErrorResourceUnavailable){
        [SVProgressHUD showErrorWithStatus:RESOURCE_UNAVAILABLE_STRING];
        DDLogError(@"Response : %@ \n\n response %@ ", error.localizedDescription, response);
    }
    else if(error.code == NSURLErrorCannotConnectToHost){
        [SVProgressHUD showErrorWithStatus:HOST_UNREACHABLE_STRING];
        DDLogError(@"Response : %@ \n\n response %@ ", error.localizedDescription, response);
    }
    else if(error.code == NSURLErrorBadServerResponse){
        NSInteger statusCode = operation.response.statusCode;
        GTRUser *currentUser = [GTRUserHelper getCurrentUser];
        
        if (statusCode == 401 && currentUser) {
            [SVProgressHUD showErrorWithStatus:RELOGIN_STRING];
            
            double delayInSeconds = 2.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                GTRAppDelegate *appDelegate = (GTRAppDelegate*)[UIApplication sharedApplication].delegate;
                [appDelegate logoutThisAppWhenBackendFails];
            });
        }
        else {
            [SVProgressHUD showErrorWithStatus:response[@"error"]];
        }
        
        DDLogError(@"Response : %@ \n\n response %@ ", error.localizedDescription, response[@"error"]);
    }
    else {
        NSString *errorMessage = [NSString stringWithFormat:@"%@ \n \n %@", response[@"error"], error.localizedDescription];
        [SVProgressHUD showErrorWithStatus:errorMessage];
    }
}

@end
