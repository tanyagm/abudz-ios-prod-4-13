//
//  GTRUser.m
//  gotribal
//
//  Created by Sambya Aryasa on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRUser.h"
#import "GTRAvatar.h"
#import "GTRAppDelegate.h"

@implementation GTRUser

@synthesize birthday = _birthday;
@synthesize firstName, lastName;

-(NSDate *)birthday
{
    GTRUserDetail *birthdayDetail = self.birthdayDetail;
    
    if (birthdayDetail) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy/MM/dd";
        return [dateFormatter dateFromString:birthdayDetail.detailValue];
    } else {
        return nil;
    }
}

-(GTRUserDetail *)birthdayDetail
{
    GTRUserDetail *birthdayDetail;
    for(GTRUserDetail *userDetail in self.details)
    {
        if ([userDetail.detailKey isEqualToString:@"birthdate"]) {
            birthdayDetail = userDetail;
            break;
        }
    }
    
    if (birthdayDetail) {
        return birthdayDetail;
    } else {
        return nil;
    }
}

-(NSString*) fullName
{
    return [NSString stringWithFormat:@"%@ %@", firstName, lastName ];
}

-(int) userRank
{
    return [self.tribalRank intValue] - 1 ;
}

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{ @"firstName" : @"first_name",
              @"lastName" : @"last_name",
              @"email" : @"email",
              @"avatarURL": @"avatar_url",
              @"details": @"details",
              @"userID" : @"id",
              @"tribalRank" : @"tribal_rank",
              @"isFriend" : @"is_friend",
              @"endorsed"   : @"endorsed",
              @"friendsCount" : @"friends_count"
              };
}

+(NSValueTransformer *)detailsJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:GTRUserDetail.class];
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"user %@: %@ %@", self.userID, self.firstName, self.lastName];
}

@end
