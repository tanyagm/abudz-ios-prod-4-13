//
//  GTRNotificationsCell.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/3/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRPendingRequestCell.h"
#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>
#import "GTRConstants.h"

NSInteger const PENDING_REQUEST_CELL_HEIGHT = 127;
NSInteger const BUTTON_WIDTH  = 160;
NSInteger const BUTTON_HEIGHT = 40;
NSInteger const CONFIRM_BUTTON_TAG = 6000;
NSInteger const IGNORE_BUTTON_TAG = 7000;

@implementation GTRPendingRequestCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    //custom size for IOS 6
    int leftPadding = 0;
    int bottomPadding = 0;
   
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")){
        leftPadding = 5;
        bottomPadding = 1;
    }
    //=====================
    if (self) {
        // confirm button
        self.confirmButton = [[UIButton alloc]initWithFrame:CGRectMake(5-leftPadding, PENDING_REQUEST_CELL_HEIGHT - 40, BUTTON_WIDTH - 5 - leftPadding, BUTTON_HEIGHT-bottomPadding)];
        self.confirmButton.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0);
        self.confirmButton.backgroundColor = [UIColor whiteColor];
        [self.confirmButton setTitleColor:CELL_SUBTEXT_FONT_COLOR forState:UIControlStateNormal];
        self.confirmButton.titleLabel.font = NOTIFICATIONS_CELL_TEXT_FONT;
        [self.confirmButton setTitle:@"Confirm" forState:UIControlStateNormal];
        [self.confirmButton setTitle:@"Confirm" forState:UIControlStateHighlighted];
        self.confirmButton.layer.borderColor = CELL_PENDING_REQUEST_BUTTON_BORDER.CGColor;
        self.confirmButton.layer.borderWidth = 0.5;
        self.confirmButton.contentMode = UIViewContentModeCenter;
        self.confirmButton.tag = CONFIRM_BUTTON_TAG;
        
        UIImageView *confirmButtonIcon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico-check-confirm"]];
        confirmButtonIcon.frame = CGRectMake(40, 14, 16, 11);
        [self.confirmButton addSubview:confirmButtonIcon];
        [self.contentView addSubview:self.confirmButton];
        
        // ignore button
        self.ignoreButton = [[UIButton alloc]initWithFrame:CGRectMake(BUTTON_WIDTH - 1 - leftPadding - leftPadding, PENDING_REQUEST_CELL_HEIGHT - 40, BUTTON_WIDTH - 4 - leftPadding, BUTTON_HEIGHT-bottomPadding)];
        self.ignoreButton.contentEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0);
        self.ignoreButton.backgroundColor = [UIColor whiteColor];
        [self.ignoreButton setTitleColor:CELL_SUBTEXT_FONT_COLOR forState:UIControlStateNormal];
        self.ignoreButton.titleLabel.font = NOTIFICATIONS_CELL_TEXT_FONT;
        [self.ignoreButton setTitle:@"Ignore" forState:UIControlStateNormal];
        [self.ignoreButton setTitle:@"Ignore" forState:UIControlStateHighlighted];
        self.ignoreButton.layer.borderColor = CELL_PENDING_REQUEST_BUTTON_BORDER.CGColor;
        self.ignoreButton.layer.borderWidth = 0.5;
        self.ignoreButton.contentMode = UIViewContentModeCenter;
        self.ignoreButton.tag = 7000;
        
        UIImageView *ignoreButtonIcon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico-x-ignore"]];
        ignoreButtonIcon.frame = CGRectMake(40, 14, 12, 12);
        [self.ignoreButton addSubview:ignoreButtonIcon];
        [self.contentView addSubview:self.ignoreButton];
    }
    
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGSize size = [GTRPendingRequestCell getStringSizeFromString:self.subtext.text withFont:NOTIFICATIONS_CELL_SUBTEXT_FONT andSize:CGSizeMake(240, CGFLOAT_MAX)];
    
    // use CGRectIntegral to remove dangling border, probably
    // caused by floating point value in size(x,y)
    self.subtext.frame = CGRectIntegral(CGRectMake(self.subtext.frame.origin.x, self.subtext.frame.origin.y,
                                                   size.width, size.height));
    
    objc_setAssociatedObject(self.confirmButton, "confirmButton", self.indexpath, 0);
    objc_setAssociatedObject(self.ignoreButton, "ignoreButton", self.indexpath, 0);
}

+(CGSize)getStringSizeFromString:(NSString*)string withFont:(UIFont*)font andSize:(CGSize)size
{
    
    NSDictionary *dictionary = @{NSFontAttributeName:font};
    NSAttributedString *attributedString = [[NSAttributedString alloc]initWithString:string
                                                                            attributes:dictionary];

    CGRect rect = [attributedString boundingRectWithSize:size
                                                 options:(NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine)
                                                 context:nil];
    return rect.size;
}

@end
