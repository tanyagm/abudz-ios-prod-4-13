//
//  GTRCustomActionSheet.m
//  gotribal
//
//  Created by loaner on 12/24/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRCustomActionSheet.h"

#define ACTION_SHEET_GREEN_COLOR [UIColor colorWithRed:0.12 green:0.58 blue:0.6 alpha:1.0]
#define ACTION_SHEET_RED_COLOR [UIColor colorWithRed:1.000 green:0.267 blue:0.271 alpha:1.0]
#define BACKGROUND [UIColor colorWithRed:0.15 green:0.16 blue:0.21 alpha:0.9]; // 15%,16%,21%
#define CANCEL_BACKGROUND_COLOR [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0]

#define NO_SHARE 1

NSInteger CUSTOM_BUTTON_HEIGHT = 50;
NSInteger CUSTOM_BUTTON_WIDTH  = 300;
NSInteger SUPER_CUSTOM_CONTAINER_TAG = 456;

NSString* const CUSTOM_ACTION_SHEET_POST_MODE_TITLE = @"More Action";

@interface GTRCustomActionSheet ()

@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UIView *containerView;
@property(nonatomic,strong) UIView *photoSuperview;
@property(nonatomic,strong) UIView *superView;
@property(nonatomic,strong) UIView *superContainerView;
@property CGFloat addingHeight;

@property (nonatomic,strong) NSMutableArray *buttonNames;
@property (nonatomic) GTRCustomActionSheetMode mode;

@end

@implementation GTRCustomActionSheet
@synthesize titleLabel = _titleLabel;
@synthesize containerView = _containerView;
@synthesize photoSuperview = _photoSuperview;
@synthesize superView = _superView;
@synthesize superContainerView = _superContainerView;
@synthesize addingHeight;
@synthesize buttonNames;
@synthesize mode = _mode;

-(id)initWithMode:(GTRCustomActionSheetMode)mode superView:(UIView *)superview delegate:(id)delegate
{
    self = [super init];
    
    if (self) {
        _mode = mode;
        
        _superContainerView = [[UIView alloc]initWithFrame:superview.bounds];
        _superContainerView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        _superContainerView.tag = SUPER_CUSTOM_CONTAINER_TAG;
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                    action:@selector(cancelButtonHandler:)];
        tapGesture.cancelsTouchesInView = YES;
        [_superContainerView addGestureRecognizer:tapGesture];
        
        CGRect superViewFrame = _superContainerView.frame;
        
        _containerView = [[UIView alloc]init]; // background view
        _containerView.backgroundColor = BACKGROUND;
        _containerView.userInteractionEnabled = YES;
        _containerView.frame = CGRectMake(0, superViewFrame.size.height, 320, 284);
        [_superContainerView addSubview:_containerView];

        _superView = superview;
        self.delegate = delegate;
        _titleLabel.font = CUSTOM_ACTION_SHEET_FONT;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [_containerView addSubview:_titleLabel];

        if (mode == GTRCustomActionSheetModeOtherPost || mode == GTRCustomActionSheetModeOwnedPost) {
            [self setAppearancePostMode];
        } else {
            [self setAppearanceActiveBudzProfileMode];
        }
    }
    
    return self;
}

#pragma mark - Appearance setters

-(void)setAppearancePostMode
{
    //set the title label
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.frame = CGRectMake(10, 5, 300, 40);
    _titleLabel.text = CUSTOM_ACTION_SHEET_POST_MODE_TITLE;
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.font = CUSTOM_ACTION_SHEET_FONT;
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [_containerView addSubview:_titleLabel];
    
    //set the buttons
    #if NO_SHARE
    buttonNames = [[NSMutableArray alloc] initWithArray:@[CUSTOM_ACTION_SHEET_DELETE_BUTTON_STRING, CUSTOM_ACTION_SHEET_CANCEL_BUTTON_STRING]];
    #else
    buttonNames = [[NSMutableArray alloc] initWithArray:@[CUSTOM_ACTION_SHEET_DELETE_BUTTON_STRING, CUSTOM_ACTION_SHEET_SHARE_BUTTON_STRING, CUSTOM_ACTION_SHEET_CANCEL_BUTTON_STRING]];
    #endif

    
    if(_mode == GTRCustomActionSheetModeOtherPost) {
        //if other's post, remove the delete button.
        [buttonNames removeObjectAtIndex:0];
    }
    
    int count = buttonNames.count;
    
    CGFloat buttonYOffset = CGRectGetHeight(_titleLabel.frame) + 5;
    
    for (int i = 0; i < count; i++) {
        UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(10, buttonYOffset, CUSTOM_BUTTON_WIDTH, CUSTOM_BUTTON_HEIGHT)];
        [customButton setTitle:(NSString*)[buttonNames objectAtIndex:i] forState:UIControlStateNormal];
        customButton.titleLabel.font = CUSTOM_ACTION_SHEET_FONT;
        customButton.tag = i;
        customButton.layer.cornerRadius = 3.0;
        customButton.layer.masksToBounds = YES;
        
        //set different color for the cancel button
        if (i == (count-1)) {
            [customButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            customButton.backgroundColor = CANCEL_BACKGROUND_COLOR;
        } else if ([buttonNames[i] isEqualToString:CUSTOM_ACTION_SHEET_DELETE_BUTTON_STRING]){
            [customButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            customButton.backgroundColor = ACTION_SHEET_RED_COLOR;
        }else {
            [customButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            customButton.backgroundColor = ACTION_SHEET_GREEN_COLOR;
        }
        
        [customButton addTarget:self action:@selector(buttonActionHandler:) forControlEvents:UIControlEventTouchUpInside];
        
        [_containerView addSubview:customButton];
        buttonYOffset += 10 + CUSTOM_BUTTON_HEIGHT;
    }
    
    addingHeight = buttonYOffset;
}

-(void)setAppearanceActiveBudzProfileMode
{
    //set the buttons
    buttonNames = [[NSMutableArray alloc] initWithArray:@[CUSTOM_ACTION_SHEET_REPORT_FOR_SPAM_BUTTON, CUSTOM_ACTION_SHEET_CANCEL_BUTTON_STRING]];
    
    int count = buttonNames.count;
    
    CGFloat buttonYOffset = 10;
    
    for (int i = 0; i < count; i++) {
        UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(10, buttonYOffset, CUSTOM_BUTTON_WIDTH, CUSTOM_BUTTON_HEIGHT)];
        [customButton setTitle:(NSString*)[buttonNames objectAtIndex:i] forState:UIControlStateNormal];
        customButton.titleLabel.font = CUSTOM_ACTION_SHEET_FONT;
        customButton.tag = i;
        customButton.layer.cornerRadius = 3.0;
        customButton.layer.masksToBounds = YES;
        
        //set different color for the cancel button
        if (i == (count-1)) {
            [customButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            customButton.backgroundColor = CANCEL_BACKGROUND_COLOR;
        } else {
            [customButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            customButton.backgroundColor = ACTION_SHEET_RED_COLOR;
        }
        
        [customButton addTarget:self action:@selector(buttonActionHandler:) forControlEvents:UIControlEventTouchUpInside];
        
        [_containerView addSubview:customButton];
        buttonYOffset += 10 + CUSTOM_BUTTON_HEIGHT;
    }
    
    addingHeight = buttonYOffset;
}


#pragma mark - UIActionSheet-like methods

-(void)show
{
    if (NO_SHARE && _mode != GTRCustomActionSheetModeOtherPost) {
        [[_superView viewWithTag:SUPER_CUSTOM_CONTAINER_TAG] removeFromSuperview];
        [_superView addSubview:_superContainerView];
        
        [UIView animateWithDuration:0.4 animations:^{
            _containerView.frame = CGRectMake(0, _superContainerView.frame.size.height - addingHeight, 320, 230);
        } completion:^(BOOL finished) {}];
    }
}

-(void)remove
{
    [UIView animateWithDuration:0.4 animations:^{
        _containerView.frame = CGRectOffset(_containerView.frame, 0, 284);
    } completion:^(BOOL finished) {
        [_superContainerView removeFromSuperview];
    }];
}

#pragma mark - Button handlers

-(void)buttonActionHandler:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(customActionSheet:clickedButtonAtIndex:)]) {
        [self.delegate customActionSheet:self clickedButtonAtIndex:((UIButton*)sender).tag];
    }
    
    [self remove];
}

-(void)cancelButtonHandler:(id)sender
{
    if (buttonNames && [self.delegate respondsToSelector:@selector(customActionSheet:clickedButtonAtIndex:)]) {
        [self.delegate customActionSheet:self clickedButtonAtIndex:buttonNames.count-1];
    }
    
    [self remove];
}

-(NSString*)buttonTitleAtIndex:(NSInteger)index
{
    NSString *buttonName = @"";
    
    if (buttonNames) {
        buttonName = (NSString*)[buttonNames objectAtIndex:index];
    }
    return buttonName;
}

@end

