//
//  GTRSuggestedActiveBudzCell.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/9/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSuggestedActiveBudzCell.h"

@interface GTRSuggestedActiveBudzCell()
@end

@implementation GTRSuggestedActiveBudzCell
@synthesize suggestedABTitleLabel, noSuggestedABLabel, carousel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        suggestedABTitleLabel = [[UILabel alloc]  initWithFrame:CGRectMake(0, 0, 300, 300)];
        suggestedABTitleLabel.text = NSLocalizedString(@"My Suggested Activebudz",
                                                       @"Suggested Activebudz Label");
        suggestedABTitleLabel.font = [UIFont boldSystemFontOfSize:15.0];
        suggestedABTitleLabel.textColor = [UIColor whiteColor];
        suggestedABTitleLabel.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        [suggestedABTitleLabel sizeToFit];
        suggestedABTitleLabel.center = CGPointMake( self.center.x, 23);
        suggestedABTitleLabel.hidden = TRUE;
        
        noSuggestedABLabel = [[UILabel alloc]  initWithFrame:CGRectMake(0, 0, 250, 300)];
        noSuggestedABLabel.text = NSLocalizedString(@"Activebudz coming here soon!",
                                                    @"No Suggested Activebudz Label");
        noSuggestedABLabel.font = [UIFont systemFontOfSize:15.0];
        noSuggestedABLabel.numberOfLines = 0;
        noSuggestedABLabel.textAlignment = NSTextAlignmentCenter;
        noSuggestedABLabel.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        noSuggestedABLabel.textColor = [UIColor whiteColor];
        [noSuggestedABLabel sizeToFit];
        noSuggestedABLabel.center = CGPointMake( self.center.x, 65);
        noSuggestedABLabel.hidden = TRUE;
        
        carousel = [[iCarousel alloc] initWithFrame:CGRectMake(0, 23, self.frame.size.width, 100)];
        
        UIImageView *suggestedFriendBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed: @"bg-profile"]];
        suggestedFriendBackground.contentMode = UIViewContentModeScaleToFill;
        self.backgroundView = suggestedFriendBackground;
        
        [self addSubview:suggestedABTitleLabel];
        [self addSubview:carousel];
        [self addSubview:noSuggestedABLabel];

    }
    return self;
}

- (void)toggleNoSuggestedABLabel:(BOOL) noSuggestedAB
{
    DDLogInfo(@"noSuggestedAB = %hhd", noSuggestedAB);
    suggestedABTitleLabel.hidden = noSuggestedAB;
    noSuggestedABLabel.hidden = !noSuggestedAB;
}

@end
