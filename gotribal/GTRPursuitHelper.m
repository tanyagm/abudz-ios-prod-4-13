//
//  GTRPursuitHelper.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRPursuitHelper.h"
#import "GTRConstants.h"

static NSDictionary* PURSUIT_ICON_NAME_MAP = nil;
static NSDictionary* PURSUIT_INDEX_MAP = nil;

@implementation GTRPursuitHelper

+(BOOL)isCustomPursuit:(NSString*)thePursuitName
{
    BOOL custom = YES;
    
    if ([thePursuitName isEqualToString:PURSUIT_FITNESS_ANSWER_STRING] ||
        [thePursuitName isEqualToString:PURSUIT_RUNNING_ANSWER_STRING] ||
        [thePursuitName isEqualToString:PURSUIT_CYCLING_ANSWER_STRING] ||
        [thePursuitName isEqualToString:PURSUIT_YOGA_ANSWER_STRING] ||
        [thePursuitName isEqualToString:PURSUIT_SURFING_ANSWER_STRING] ||
        [thePursuitName isEqualToString:PURSUIT_SWIMMING_ANSWER_STRING] ||
        [thePursuitName isEqualToString:PURSUIT_TRIATHLON_ANSWER_STRING] ) {
        
        custom = NO;
    }
    
    return custom;
}

+(NSString*)getPursuitIconName:(NSString*)thePursuitName
{
    if (!PURSUIT_ICON_NAME_MAP) {
        PURSUIT_ICON_NAME_MAP = @{
          PURSUIT_FITNESS_ANSWER_STRING : PURSUIT_FITNESS_ICON_NAME,
          PURSUIT_RUNNING_ANSWER_STRING : PURSUIT_RUNNING_ICON_NAME,
          PURSUIT_CYCLING_ANSWER_STRING : PURSUIT_CYCLING_ICON_NAME,
          PURSUIT_YOGA_ANSWER_STRING : PURSUIT_YOGA_ICON_NAME,
          PURSUIT_SURFING_ANSWER_STRING : PURSUIT_SURFING_ICON_NAME,
          PURSUIT_SWIMMING_ANSWER_STRING : PURSUIT_SWIMMING_ICON_NAME,
          PURSUIT_TRIATHLON_ANSWER_STRING : PURSUIT_TRIATHLON_ICON_NAME
          };

    }
    
    return [PURSUIT_ICON_NAME_MAP objectForKey:thePursuitName];
}

+(NSArray*)getIntegerPursuitArray:(NSArray*)aStringPursuitArray
{
    if (!PURSUIT_INDEX_MAP) {
        PURSUIT_INDEX_MAP = @{
                                  PURSUIT_FITNESS_ANSWER_STRING : [NSNumber numberWithInt:0],
                                  PURSUIT_RUNNING_ANSWER_STRING : [NSNumber numberWithInt:1],
                                  PURSUIT_CYCLING_ANSWER_STRING : [NSNumber numberWithInt:2],
                                  PURSUIT_YOGA_ANSWER_STRING : [NSNumber numberWithInt:3],
                                  PURSUIT_SURFING_ANSWER_STRING : [NSNumber numberWithInt:4],
                                  PURSUIT_SWIMMING_ANSWER_STRING : [NSNumber numberWithInt:5],
                                  PURSUIT_TRIATHLON_ANSWER_STRING : [NSNumber numberWithInt:6]
                                  };
        
    }
    
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    for(NSString *pursuitString in aStringPursuitArray){
        NSNumber *pursuitIndex = [PURSUIT_INDEX_MAP valueForKey:pursuitString];
        
        if (pursuitIndex && pursuitIndex != (NSNumber*)[NSNull null]){
            [result addObject:pursuitIndex];
        }
    }
    
    return [result copy];
}

+(NSString*)getOtherPursuitStringFromArray:(NSArray*)aStringPursuitArray
{
    NSString *otherPursuit=@"";
    
    for(NSString *pursuitString in aStringPursuitArray){
        if ([GTRPursuitHelper isCustomPursuit:pursuitString]){
            otherPursuit = pursuitString;
        }
    }
    
    return otherPursuit;
}

@end
