//
//  GTRPursuitHelper.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTRPursuitHelper : NSObject

+(BOOL)isCustomPursuit:(NSString*)thePursuitName;
+(NSString*)getPursuitIconName:(NSString*)thePursuitName;
+(NSArray*)getIntegerPursuitArray:(NSArray*)aStringPursuitArray;
+(NSString*)getOtherPursuitStringFromArray:(NSArray*)aStringPursuitArray;

@end
