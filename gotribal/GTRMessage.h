//
//  GTRMessage.h
//  gotribal
//
//  Created by Muhammad Taufik on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"

typedef NS_ENUM(NSInteger, GTRMessageSendingStatus) {
    GTRMessageSendingStatusSucceed,
    GTRMessageSendingStatusFailed
};

@class GTRUser;

@interface GTRMessage : MTLModel <MTLJSONSerializing>
@property (nonatomic, copy) NSString *body;
@property (nonatomic, copy) NSNumber *messageID;
@property (nonatomic, copy) NSNumber *receiverID;
@property (nonatomic, copy) GTRUser *user;
@property (nonatomic, copy) NSNumber *senderID;
@property (nonatomic, copy) NSString *socketID;
@property (nonatomic, copy) NSNumber *createdAt;

@property (nonatomic, assign) GTRMessageSendingStatus sendingStatus;
@end
