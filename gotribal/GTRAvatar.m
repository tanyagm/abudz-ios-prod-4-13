//
//  GTRAvatar.m
//  gotribal
//
//  Created by Sambya Aryasa on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRAvatar.h"

@implementation GTRAvatar

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{ @"url": @"url",
              @"image": @"image"
              };
}

@end
