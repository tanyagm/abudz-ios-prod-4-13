//
//  GTRSocialHelper.h
//  gotribal
//
//  Created by Muhammad Taufik on 12/25/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^GTRSuccessBlock) ();
typedef void (^GTRSuccessWithAccessTokenBlock) (NSString *accessToken);
typedef void (^GTRSuccessWithOAuthTokenBlock) (NSString *oauthToken, NSString *oauthTokenSecret);
typedef void (^GTRSuccessWithUsersBlock) (NSNumber *nextCursor, NSArray *users);

typedef void (^GTRFailureWithOperationBlock) (AFHTTPRequestOperation *operation, NSError *error);
typedef void (^GTRFailureWithResponseObjectBlock) (id responseObject, NSError *error);
typedef void (^GTRFailureWithErrorBlock) (NSError *error);
typedef void (^GTRFailureWithErrorDictionaryBlock) (NSDictionary *error);

@interface GTRSocialHelper : NSObject
+(instancetype)sharedHelper;

// Facebook
-(void)retrieveFacebookAccountWithPermissions:(NSArray *)permissions success:(GTRSuccessWithAccessTokenBlock)success failure:(GTRFailureWithErrorBlock)failure;
-(void)requestReadWriteAccessToFacebookAccountWithSuccess:(GTRSuccessBlock)success failure:(GTRFailureWithErrorBlock)failure;
-(void)postToFacebookWithMessage:(NSString *)message image:(UIImage *)postImage success:(GTRSuccessBlock)success failure:(GTRFailureWithErrorBlock)failure;
-(void)retrieveFacebookAccountWithSuccess:(GTRSuccessBlock)success failure:(GTRFailureWithErrorBlock)failure;
-(void)deleteFacebookAccountWithSuccess:(GTRSuccessBlock)success failure:(GTRFailureWithOperationBlock)failure;

// Twitter
-(void)retrieveTwitterAccountWithSuccess:(GTRSuccessBlock)success failure:(GTRFailureWithResponseObjectBlock)failure;
-(void)postToTwitterWithMessage:(NSString *)message image:(UIImage *)postImage success:(GTRSuccessBlock)success failure:(GTRFailureWithErrorBlock)failure;
-(void)retrieveFollowersListWithCursor:(NSNumber *)cursor success:(void(^)(NSNumber *nextCursor, NSArray *users))success failure:(void(^)(NSDictionary *error))failure;
-(void)deleteTwitterAccountWithSuccess:(GTRSuccessBlock)success failure:(GTRFailureWithOperationBlock)failure;
@end
