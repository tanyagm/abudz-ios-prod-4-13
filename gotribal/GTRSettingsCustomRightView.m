//
//  GTRSettingsCustomRightView.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/9/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSettingsCustomRightView.h"
#import "GTRSettingsHelper.h"
#import <MBSwitch/MBSwitch.h>

NSInteger const PHONE_BUTTON_TAG = 757;
NSInteger const EMAIL_BUTTON_TAG = 908;

NSInteger const DAILY_BUTTON_TAG = 909;
NSInteger const WEEKLY_BUTTON_TAG = 919;
NSInteger const NEVER_BUTTON_TAG = 929;

#define NOTIFICATIONS_ICON_PHONE_ON     [UIImage imageNamed:@"ico-settings-phone-on"]
#define NOTIFICATIONS_ICON_PHONE_OFF    [UIImage imageNamed:@"ico-settings-phone-off"]
#define NOTIFICATIONS_ICON_MAIL_ON      [UIImage imageNamed:@"ico-settings-mail-on"]
#define NOTIFICATIONS_ICON_MAIL_OFF     [UIImage imageNamed:@"ico-settings-mail-off"]

#define GREEN_COLOR                     [UIColor colorWithRed:0.13 green:0.64 blue:0.66 alpha:1.0]
#define GREEN_SWITCH_COLOR              [UIColor colorWithRed:0.26 green:0.84 blue:0.32 alpha:1.0]

@interface GTRSettingsCustomRightView ()
{
    UIButton *phoneButton;
    UIButton *emailButton;
    
    UIButton *dailyButton;
    UIButton *weeklyButton;
    UIButton *neverButton;
    
    UISwitch *currentSwitch;
    MBSwitch *fakeSwitch;
}

@property(nonatomic,strong) GTRSettingsCell *cell;

@end

@implementation GTRSettingsCustomRightView

- (id)initInCell:(GTRSettingsCell*)cell
           frame:(CGRect)frame
       withDelegate:(id<GTRSettingsCustomRightViewDelegate>)delegate
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.delegate = delegate;
        self.cell = cell;
        
        NSInteger section = cell.indexpath.section;
        
        switch (section) {
            case GTRSettingsSectionNotifications:
            {
                phoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
                phoneButton.tag = PHONE_BUTTON_TAG;
                phoneButton.frame = CGRectMake(30, 4, 30, 38);
                phoneButton.backgroundColor = self.backgroundColor;
                [phoneButton setImage:NOTIFICATIONS_ICON_PHONE_ON forState:UIControlStateNormal];
                [phoneButton setImage:NOTIFICATIONS_ICON_PHONE_ON forState:UIControlStateHighlighted];
                phoneButton.contentMode = UIViewContentModeCenter;
                [phoneButton addTarget:self
                                action:@selector(handleNotifications:)
                      forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:phoneButton];
                
                emailButton = [UIButton buttonWithType:UIButtonTypeCustom];
                emailButton.tag = EMAIL_BUTTON_TAG;
                emailButton.frame = CGRectMake(phoneButton.frame.size.width + 35, 4, 30, 38);
                emailButton.backgroundColor = self.backgroundColor;
                [emailButton setImage:NOTIFICATIONS_ICON_MAIL_OFF forState:UIControlStateNormal];
                [emailButton setImage:NOTIFICATIONS_ICON_MAIL_OFF forState:UIControlStateHighlighted];
                emailButton.contentMode = UIViewContentModeCenter;
                [emailButton addTarget:self
                                action:@selector(handleNotifications:)
                      forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:emailButton];
                
                break;
            }
            case GTRSettingsSectionNotifificationsFrequency:
            {
                UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:13];
                CGFloat originY = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ?
                (self.frame.size.height/2.0 - 12):(self.frame.size.height/2.0 - 11);
                
                NSString *daily     = @"Immediately";
                NSString *weekly    = @"Weekly";
                NSString *never     = @"Never";
                
                if(cell.indexpath.row != 1) {
                    dailyButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    dailyButton.tag = DAILY_BUTTON_TAG;
                    dailyButton.frame = CGRectMake(0,originY,70, 30);
                    dailyButton.backgroundColor = self.backgroundColor;
                    [dailyButton setTitle:daily forState:UIControlStateNormal];
                    [dailyButton setTitle:daily forState:UIControlStateHighlighted];
                    [dailyButton setTitleColor:NOTIFICATIONS_CELL_SUBTEXT_COLOR forState:UIControlStateNormal];
                    [dailyButton setTitleColor:NOTIFICATIONS_CELL_SUBTEXT_COLOR forState:UIControlStateHighlighted];
                    dailyButton.titleLabel.font = font;
                    dailyButton.contentMode = UIViewContentModeCenter;
                    [dailyButton addTarget:self
                                    action:@selector(handleNotificationsFrequency:)
                          forControlEvents:UIControlEventTouchUpInside];
                    [self addSubview:dailyButton];
                }
                
                // to add padding after daily button
                CGFloat weeklyPadding = cell.indexpath.row != 1? 70:40;
                
                weeklyButton = [UIButton buttonWithType:UIButtonTypeCustom];
                weeklyButton.tag = WEEKLY_BUTTON_TAG;
                weeklyButton.frame = CGRectMake(weeklyPadding+2,
                                                originY, 45, 30);
                weeklyButton.backgroundColor = self.backgroundColor;
                weeklyButton.titleLabel.font = font;
                [weeklyButton setTitle:weekly forState:UIControlStateNormal];
                [weeklyButton setTitle:weekly forState:UIControlStateHighlighted];
                [weeklyButton setTitleColor:NOTIFICATIONS_CELL_SUBTEXT_COLOR forState:UIControlStateNormal];
                [weeklyButton setTitleColor:NOTIFICATIONS_CELL_SUBTEXT_COLOR forState:UIControlStateHighlighted];
                weeklyButton.contentMode = UIViewContentModeCenter;
                [weeklyButton addTarget:self
                                action:@selector(handleNotificationsFrequency:)
                      forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:weeklyButton];

                neverButton = [UIButton buttonWithType:UIButtonTypeCustom];
                neverButton.tag = NEVER_BUTTON_TAG;
                neverButton.frame = CGRectMake(weeklyPadding+2+weeklyButton.frame.size.width+5,
                                               originY,
                                               40, 30);
                neverButton.backgroundColor = self.backgroundColor;
                neverButton.titleLabel.font = font;
                [neverButton setTitle:never forState:UIControlStateNormal];
                [neverButton setTitle:never forState:UIControlStateHighlighted];
                [neverButton setTitleColor:GREEN_COLOR forState:UIControlStateNormal];
                [neverButton setTitleColor:GREEN_COLOR forState:UIControlStateHighlighted];
                neverButton.contentMode = UIViewContentModeCenter;
                [neverButton addTarget:self
                                action:@selector(handleNotificationsFrequency:)
                      forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:neverButton];

                break;
            }
            case GTRSettingsSectionSocial:
            {
                UIColor *grayTint = [UIColor colorWithWhite:0.85 alpha:1.0];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                    currentSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(0,7,50, self.frame.size.height - 10)];
                    currentSwitch.tintColor = grayTint;
                    currentSwitch.onTintColor = GREEN_SWITCH_COLOR;
                    [currentSwitch addTarget:self action:@selector(handleSwitch:) forControlEvents:UIControlEventValueChanged];
                    [self addSubview:currentSwitch];
                }
                else {
                    // ios 6 and below
                    fakeSwitch = [[MBSwitch alloc]initWithFrame:CGRectMake(0, 5, 50, self.frame.size.height - 10)];
                    fakeSwitch.tintColor = grayTint;
                    fakeSwitch.onTintColor = GREEN_SWITCH_COLOR;
                    [fakeSwitch addTarget:self action:@selector(handleSwitch:) forControlEvents:UIControlEventValueChanged];
                    [self addSubview:fakeSwitch];
                }
                
                break;
            }
        }
    }
    
    return self;
}

#pragma mark - Handlers

-(void)handleNotifications:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    if (button.tag == PHONE_BUTTON_TAG) {

        BOOL isPhoneButtonOn = NO;
        if (button.imageView.image == NOTIFICATIONS_ICON_PHONE_OFF) {
            [button setImage:NOTIFICATIONS_ICON_PHONE_ON forState:UIControlStateNormal];
            [button setImage:NOTIFICATIONS_ICON_PHONE_ON forState:UIControlStateHighlighted];
            isPhoneButtonOn = YES;

        }
        else if (button.imageView.image == NOTIFICATIONS_ICON_PHONE_ON){
            [button setImage:NOTIFICATIONS_ICON_PHONE_OFF forState:UIControlStateNormal];
            [button setImage:NOTIFICATIONS_ICON_PHONE_OFF forState:UIControlStateHighlighted];
            isPhoneButtonOn = NO;

        }
        
        
        if (isPhoneButtonOn && emailButton.imageView.image == NOTIFICATIONS_ICON_MAIL_ON) {
            if ([self.delegate respondsToSelector:@selector(customRightView:notificationsMode:inCell:)]) {
                [self.delegate customRightView:self
                             notificationsMode:GTRSettingsNotificationsModeBoth
                                        inCell:self.cell];
                [self.delegate updateSettingDetailForMode:@[@"phone", @"email"] inCell:self.cell.indexpath.row];
            }
        }
        else if(isPhoneButtonOn && emailButton.imageView.image == NOTIFICATIONS_ICON_MAIL_OFF){
            if ([self.delegate respondsToSelector:@selector(customRightView:notificationsMode:inCell:)]) {
                [self.delegate customRightView:self
                             notificationsMode:GTRSettingsNotificationsModePhone
                                        inCell:self.cell];
                [self.delegate updateSettingDetailForMode:@[@"phone"] inCell:self.cell.indexpath.row];
            }
        }
        else if (!isPhoneButtonOn && emailButton.imageView.image == NOTIFICATIONS_ICON_MAIL_ON){
            if ([self.delegate respondsToSelector:@selector(customRightView:notificationsMode:inCell:)]) {
                [self.delegate customRightView:self
                             notificationsMode:GTRSettingsNotificationsModeEMail
                                        inCell:self.cell];
                [self.delegate updateSettingDetailForMode:@[@"email"] inCell:self.cell.indexpath.row];
            }
        }
        else if (!isPhoneButtonOn && emailButton.imageView.image == NOTIFICATIONS_ICON_MAIL_OFF){
            if ([self.delegate respondsToSelector:@selector(customRightView:notificationsMode:inCell:)]) {
                [self.delegate customRightView:self
                             notificationsMode:GTRSettingsNotificationsModeNone
                                        inCell:self.cell];
                [self.delegate updateSettingDetailForMode:@[@"none"] inCell:self.cell.indexpath.row];
            }
        }
        
    } else {
        BOOL isEmailButtonOn = NO;
        if (button.imageView.image == NOTIFICATIONS_ICON_MAIL_OFF) {
            [button setImage:NOTIFICATIONS_ICON_MAIL_ON forState:UIControlStateNormal];
            [button setImage:NOTIFICATIONS_ICON_MAIL_ON forState:UIControlStateHighlighted];
            isEmailButtonOn = YES;
            
        } else if (button.imageView.image == NOTIFICATIONS_ICON_MAIL_ON){
            [button setImage:NOTIFICATIONS_ICON_MAIL_OFF forState:UIControlStateNormal];
            [button setImage:NOTIFICATIONS_ICON_MAIL_OFF forState:UIControlStateHighlighted];
            isEmailButtonOn = NO;

        }
        
        if (isEmailButtonOn && phoneButton.imageView.image == NOTIFICATIONS_ICON_PHONE_ON) {
            if ([self.delegate respondsToSelector:@selector(customRightView:notificationsMode:inCell:)]) {
                [self.delegate customRightView:self
                             notificationsMode:GTRSettingsNotificationsModeBoth
                                        inCell:self.cell];
                [self.delegate updateSettingDetailForMode:@[@"phone", @"email"] inCell:self.cell.indexpath.row];
            }
        }
        else if(isEmailButtonOn && phoneButton.imageView.image == NOTIFICATIONS_ICON_PHONE_OFF){
            if ([self.delegate respondsToSelector:@selector(customRightView:notificationsMode:inCell:)]) {
                [self.delegate customRightView:self
                             notificationsMode:GTRSettingsNotificationsModeEMail
                                        inCell:self.cell];
                [self.delegate updateSettingDetailForMode:@[@"email"] inCell:self.cell.indexpath.row];
            }
        }
        else if (!isEmailButtonOn && phoneButton.imageView.image == NOTIFICATIONS_ICON_PHONE_ON){
            if ([self.delegate respondsToSelector:@selector(customRightView:notificationsMode:inCell:)]) {
                [self.delegate customRightView:self
                             notificationsMode:GTRSettingsNotificationsModePhone
                                        inCell:self.cell];
                [self.delegate updateSettingDetailForMode:@[@"phone"] inCell:self.cell.indexpath.row];
            }
        }
        else if (!isEmailButtonOn && emailButton.imageView.image == NOTIFICATIONS_ICON_MAIL_OFF){
            if ([self.delegate respondsToSelector:@selector(customRightView:notificationsMode:inCell:)]) {
                [self.delegate customRightView:self
                             notificationsMode:GTRSettingsNotificationsModeNone
                                        inCell:self.cell];
                [self.delegate updateSettingDetailForMode:@[@"none"] inCell:self.cell.indexpath.row];
            }
        }
    }
}

-(void)handleNotificationsFrequency:(id)sender
{
    UIButton *button = (UIButton*)sender;
    [self resetFrequencyButtons];
    
    [button  setTitleColor:GREEN_COLOR forState:UIControlStateNormal];
    [button  setTitleColor:GREEN_COLOR forState:UIControlStateHighlighted];
    
    GTRSettingsNotificationsFrequency frequency;
    
    if (button.tag == DAILY_BUTTON_TAG) {
        if ([button.titleLabel.text isEqualToString:@"Immediately"]) {
            frequency = GTRSettingsNotificationsFrequencyImmediately;
            [self.delegate updateSettingDetailForFreq:@"immediate" inCell:self.cell.indexpath.row];
        }
        else {
            frequency = GTRSettingsNotificationsFrequencyDaily;
            [self.delegate updateSettingDetailForFreq:@"daily" inCell:self.cell.indexpath.row];
        }
    }
    else if(button.tag == WEEKLY_BUTTON_TAG){
        frequency = GTRSettingsNotificationsFrequencyWeekly;
        [self.delegate updateSettingDetailForFreq:@"weekly" inCell:self.cell.indexpath.row];
    }
    else {
        frequency = GTRSettingsNotificationsFrequencyNever;
        [self.delegate updateSettingDetailForFreq:@"never" inCell:self.cell.indexpath.row];
    }
    
    if ([self.delegate respondsToSelector:@selector(customRightView:notificationsFrequency:inCell:)]) {
        [self.delegate customRightView:self notificationsFrequency:frequency inCell:self.cell];
    }
}

-(void)handleSwitch:(id)sender
{
    UISwitch *thisSwitch = (UISwitch*)sender;
    if (self.cell.indexpath.row == 0) {
        if ([self.delegate respondsToSelector:@selector(customRightView:twitterStatus:)]) {
            if (thisSwitch.on) {
                [self.delegate customRightView:self twitterStatus:GTRSettingsSocialStatusOn];
            }
            else {
                [self.delegate customRightView:self twitterStatus:GTRSettingsSocialStatusOff];
            }
        }
    }
    else{
        if ([self.delegate respondsToSelector:@selector(customRightView:facebookStatus:)]) {
            if (thisSwitch.on) {
                [self.delegate customRightView:self facebookStatus:GTRSettingsSocialStatusOn];
            }
            else {
                [self.delegate customRightView:self facebookStatus:GTRSettingsSocialStatusOff];
            }
        }
    }
}

#pragma mark - Reset frequency

-(void)resetFrequencyButtons
{
    [dailyButton  setTitleColor:NOTIFICATIONS_CELL_SUBTEXT_COLOR forState:UIControlStateNormal];
    [dailyButton  setTitleColor:NOTIFICATIONS_CELL_SUBTEXT_COLOR forState:UIControlStateHighlighted];
    [weeklyButton setTitleColor:NOTIFICATIONS_CELL_SUBTEXT_COLOR forState:UIControlStateNormal];
    [weeklyButton setTitleColor:NOTIFICATIONS_CELL_SUBTEXT_COLOR forState:UIControlStateHighlighted];
    [neverButton  setTitleColor:NOTIFICATIONS_CELL_SUBTEXT_COLOR forState:UIControlStateNormal];
    [neverButton  setTitleColor:NOTIFICATIONS_CELL_SUBTEXT_COLOR forState:UIControlStateHighlighted];
}

#pragma mark - Setters

-(void)setPhoneButtonStatus:(BOOL)phoneStatus emailStatus:(BOOL)emailStatus
{
    if (phoneStatus) {
        [phoneButton setImage:NOTIFICATIONS_ICON_PHONE_ON forState:UIControlStateNormal];
        [phoneButton setImage:NOTIFICATIONS_ICON_PHONE_ON forState:UIControlStateHighlighted];
    }
    else if(!phoneStatus){
        [phoneButton setImage:NOTIFICATIONS_ICON_PHONE_OFF forState:UIControlStateNormal];
        [phoneButton setImage:NOTIFICATIONS_ICON_PHONE_OFF forState:UIControlStateHighlighted];

    }
    
    if (emailStatus) {
        [emailButton setImage:NOTIFICATIONS_ICON_MAIL_ON forState:UIControlStateNormal];
        [emailButton setImage:NOTIFICATIONS_ICON_MAIL_ON forState:UIControlStateHighlighted];
    }
    else if(!emailStatus){
        [emailButton setImage:NOTIFICATIONS_ICON_MAIL_OFF forState:UIControlStateNormal];
        [emailButton setImage:NOTIFICATIONS_ICON_MAIL_OFF forState:UIControlStateHighlighted];
    }
}

-(void)setFrequency:(GTRSettingsNotificationsFrequency)frequency
{
    [self resetFrequencyButtons];
    switch (frequency) {
        case GTRSettingsNotificationsFrequencyDaily:
            [dailyButton  setTitleColor:GREEN_COLOR forState:UIControlStateNormal];
            [dailyButton  setTitleColor:GREEN_COLOR forState:UIControlStateHighlighted];
            break;
        case GTRSettingsNotificationsFrequencyWeekly:
            [weeklyButton  setTitleColor:GREEN_COLOR forState:UIControlStateNormal];
            [weeklyButton  setTitleColor:GREEN_COLOR forState:UIControlStateHighlighted];
            break;
        case GTRSettingsNotificationsFrequencyImmediately:
            [dailyButton  setTitleColor:GREEN_COLOR forState:UIControlStateNormal];
            [dailyButton  setTitleColor:GREEN_COLOR forState:UIControlStateHighlighted];
            break;
        case GTRSettingsNotificationsFrequencyNever:
            [neverButton  setTitleColor:GREEN_COLOR forState:UIControlStateNormal];
            [neverButton  setTitleColor:GREEN_COLOR forState:UIControlStateHighlighted];
            break;
    }
}

-(void)setOn:(BOOL)on
{
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        fakeSwitch.on = on;
    }
    else {
        currentSwitch.on = on;
    }
}

@end
