//
//  GTRStreamPageViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/10/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRStreamPageViewController.h"
#import "GTRStreamPostCell.h"
#import "SVPullToRefresh.h"
#import "GTRNewPostViewController.h"
#import "GTRCommentViewController.h"
#import "GTRPostRestService.h"

NSString* const DEFAULT_PLACEHOLDER_TEXT = @"No post yet.";
NSString* const STREAM_PAGE_POST_CELL_IDENTIFIER = @"tribeDetailPostCell";
NSString* const CELL_HEIGHT_CALCULATOR_IDENTIFIER = @"streamPostCellCalculator";

@interface GTRStreamPageViewController () <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, GTRCommentViewControllerDelegate>

@property (strong, nonatomic) UILabel *placeholderLabel;
@property (strong, nonatomic) NSNumber *lastPostID;
@property (strong, nonatomic) GTRStreamPostCell *cellHeightCalculator;
@property BOOL placeholderEnabled;

@end

@implementation GTRStreamPageViewController
@synthesize streamPostArray, streamTableView, cellHeightCalculator, actionSheetButtons, placeholderEnabled;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        streamPostArray = [[NSMutableArray alloc] init];
        cellHeightCalculator = [[GTRStreamPostCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                        reuseIdentifier:CELL_HEIGHT_CALCULATOR_IDENTIFIER];
        placeholderEnabled = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initiateTableView];
    
    [self initiateReloadFunctionality];
    
    [self reloadStreamPage:GTRStreamPageReloadModeRefresh];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Custom initiators

- (void)initiateTableView
{
    //initiate table view
    self.streamTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.streamTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.streamTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.streamTableView setBackgroundColor:UIColorFromRGB(0xf2f2f2)];
    self.streamTableView.delegate = self;
    self.streamTableView.dataSource = self;
    [self.view addSubview:self.streamTableView];
    
    //initiate placeholder label
    self.placeholderLabel = [[UILabel alloc] initWithFrame:self.view.bounds];
    self.placeholderLabel.backgroundColor = [UIColor clearColor];
    self.placeholderLabel.text = NSLocalizedString(DEFAULT_PLACEHOLDER_TEXT,
                                               @"no post stream message");
    self.placeholderLabel.textAlignment = NSTextAlignmentCenter;
    self.placeholderLabel.font = [UIFont systemFontOfSize:14.0];
    self.placeholderLabel.textColor = UIColorFromRGB(0x5E5E5E);
    self.placeholderLabel.numberOfLines = 0;
    self.placeholderLabel.hidden = YES;
    
    [self.streamTableView addSubview:self.placeholderLabel];
}

- (void)initiateReloadFunctionality
{
    __weak GTRStreamPageViewController *weakSelf = self;
    
    [streamTableView addPullToRefreshWithActionHandler:^{
        [weakSelf reloadStreamPage:GTRStreamPageReloadModeRefresh];
    }];
    
    [streamTableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf reloadStreamPage:GTRStreamPageReloadModeLoadMore];
    }];

}

#pragma mark - Button actions

#pragma mark New Post button actions

-(void)openNewPost:(id)sender
{
    [self openNewPostwithImage:nil];
}

#pragma mark Comment button action

-(void)commentButtonAction:(id)sender
{
    NSInteger rowID = ((UIControl *) sender).tag;
    self.currentSelectedPost = sender;
    __weak GTRPost *post = [self.streamPostArray objectAtIndex:rowID];
    [self openCommentViewControllerForPost:post];
}

- (void) openCommentViewControllerForPost:(GTRPost*)post
{
    NSInteger rowID = ((UIControl *) self.currentSelectedPost).tag;
    GTRCommentViewController *commentVC = [[GTRCommentViewController alloc] initWithPost:post rowID:rowID];
    commentVC.delegate = self;
    UINavigationController *newNavVC = [[UINavigationController alloc]
                                        initWithRootViewController:commentVC];
    
    [self presentViewController:newNavVC animated:YES completion:nil];
}

#pragma mark - Custom methods

- (CGFloat) calculatePostCellHeightAtRow:(NSInteger)row
{
    if ([self.streamPostArray count] > row){
        [cellHeightCalculator setContent:[self.streamPostArray objectAtIndex:row]];
        return cellHeightCalculator.frame.size.height + 13;
    } else {
        return 20;
    }
}

#pragma mark Placeholder label-related methods

-(void)setPlaceHolderText:(NSString*)aString
{
    placeholderEnabled = YES;
    self.placeholderLabel.text = aString;
}

-(void)setPlaceHolderAttributedText:(NSAttributedString*)anAttributedString
{
    self.placeholderLabel.attributedText = anAttributedString;
}

-(void)togglePlaceholderLabelVisibility
{
    self.placeholderLabel.hidden = ([self.streamPostArray count] != 0) || !placeholderEnabled;
    
}

#pragma mark UITableView-related methods

- (UITableViewCell *)tableView:(UITableView *)tableView postCellAtIndexPath:(NSIndexPath *)indexPath
{
    
    GTRStreamPostCell *cell = [tableView dequeueReusableCellWithIdentifier:STREAM_PAGE_POST_CELL_IDENTIFIER];
    
    if (cell == nil) {
        cell = [[GTRStreamPostCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:STREAM_PAGE_POST_CELL_IDENTIFIER];
    }
    
    GTRPost *post = [self.streamPostArray objectAtIndex:indexPath.row];
    
    [cell setContent:post];
    
    cell.likeButton.tag = indexPath.row;
    [cell.likeButton addTarget:self action:@selector(likeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.moreButton.tag = indexPath.row;
    [cell.moreButton addTarget:self action:@selector(moreButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.commentButton.tag = indexPath.row;
    [cell.commentButton addTarget:self action:@selector(commentButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - Methods implemented on Sub-classes
-(void)reloadStreamPage:(GTRStreamPageReloadMode)theReloadMode
{
    //implemented on subclass
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //implemented on subclass
}


#pragma mark New Post button action
-(void)openNewPostwithImage:(UIImage*)postImage
{
    //implemented on subclass
}

-(void)likeButtonAction:(id)sender
{
    //implemented on subclass
}

-(void)moreButtonAction:(id)sender
{
    //implemented on subclass
}

#pragma mark - Delegates

#pragma mark UITableView delegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.streamPostArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[GTRStreamPostCell alloc] init];
}

#pragma mark GTRCommentViewControllerDelegate

- (void) onPostUpdated:(NSInteger)postIndex
{
    [self.streamTableView reloadData];
}

-(void) onPostDeleted:(NSInteger)indexRow
{
    [self removePostOnIndex:indexRow];
}

#pragma mark Helper
-(void) removePostOnIndex:(NSInteger)indexRow
{
    [streamPostArray removeObjectAtIndex:indexRow];
    [streamTableView reloadData];
    [self togglePlaceholderLabelVisibility];
}

@end
