//
//  GTRActiveBudzProfileRanking.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/9/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActiveBudzProfileRanking.h"

NSString* const TRIBE_RANK_I_BIG = @"ico-badge-i";
NSString* const TRIBE_RANK_I_SMALL = @"ico-badge-i-small";
NSString* const TRIBE_RANK_I_GREY_ROUND = @"ico-badge-i-grey-round";
NSString* const TRIBE_RANK_I_YELLOW_ROUND = @"ico-badge-i-yellow-round";
NSString* const TRIBE_RANK_I_TITLE = @"Bronze";

NSString* const TRIBE_RANK_II_BIG = @"ico-badge-ii";
NSString* const TRIBE_RANK_II_SMALL = @"ico-badge-ii-small";
NSString* const TRIBE_RANK_II_GREY_ROUND = @"ico-badge-ii-grey-round";
NSString* const TRIBE_RANK_II_YELLOW_ROUND = @"ico-badge-ii-yellow-round";
NSString* const TRIBE_RANK_II_TITLE = @"Silver";

NSString* const TRIBE_RANK_III_BIG = @"ico-badge-iii";
NSString* const TRIBE_RANK_III_SMALL = @"ico-badge-iii-small";
NSString* const TRIBE_RANK_III_GREY_ROUND = @"ico-badge-iii-grey-round";
NSString* const TRIBE_RANK_III_YELLOW_ROUND = @"ico-badge-iii-yellow-round";
NSString* const TRIBE_RANK_III_TITLE = @"Gold";

@interface GTRActiveBudzProfileRanking()

@property ActiveBudzProfileRank rank;
@property (strong,nonatomic) NSString* rankName;

@end

@implementation GTRActiveBudzProfileRanking

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame
       profileRank:(ActiveBudzProfileRank)theRank
{
    self = [super initWithFrame:frame];
    if (self) {
        self.rank = theRank;
        [self setAppearance];
    }
    return self;
}

#pragma mark - Appearance methods

-(void)setAppearance
{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [UIColor colorWithWhite:0.800 alpha:1.000].CGColor;
    self.layer.cornerRadius = 5.0f;

    //prepare big rank badge
    CGFloat bigRankBadgeX = 0;
    CGFloat bigRankBadgeY = 0;
    CGFloat bigRankBadgeHeight = 39;
    CGFloat bigRankBadgeWidth = 35;
    
    UIImageView *bigRankBadge = [[UIImageView alloc] initWithFrame:CGRectMake(bigRankBadgeX, bigRankBadgeY, bigRankBadgeWidth, bigRankBadgeHeight)];
    bigRankBadge.contentMode = UIViewContentModeScaleAspectFit;
    bigRankBadge.backgroundColor = [UIColor clearColor];
    
    //for the rank name, too
    NSString *rankNameBuffString = @"";
    
    switch (self.rank) {
        case ActiveBudzProfileRankOne:
        {
            [bigRankBadge setImage:[UIImage imageNamed:TRIBE_RANK_I_BIG]];
            rankNameBuffString = TRIBE_RANK_I_TITLE;
        }
            break;
            
        case ActiveBudzProfileRankTwo:
        {
            [bigRankBadge setImage:[UIImage imageNamed:TRIBE_RANK_II_BIG]];
            rankNameBuffString = TRIBE_RANK_II_TITLE;
        }
            break;
            
        case ActiveBudzProfileRankThree:
        {
            [bigRankBadge setImage:[UIImage imageNamed:TRIBE_RANK_III_BIG]];
            rankNameBuffString = TRIBE_RANK_III_TITLE;
        }
            break;
            
        default:
            break;
    }
    
    //prepare rank name
    CGFloat rankNameWidth = 175;
    CGFloat rankNameHeight = 25;
    CGFloat rankNameX = bigRankBadgeX + bigRankBadgeWidth + 10;
    CGFloat rankNameY = bigRankBadgeY + (bigRankBadgeHeight-rankNameHeight)/2;
    
    NSAttributedString *rankNameString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:rankNameBuffString, self.rankName]
                                    attributes:@{
                                                 NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:18.0f],
                                                 NSForegroundColorAttributeName : [UIColor colorWithWhite:0.086 alpha:1.000]
                                                 }];
    
    UILabel *rankName = [[UILabel alloc] initWithFrame:CGRectMake(rankNameX, rankNameY, rankNameWidth, rankNameHeight)];
    rankName.attributedText = rankNameString;
    rankName.backgroundColor = [UIColor clearColor];
    [rankName sizeToFit];
    

    
    //prepare the view that will contain the badge and name
    CGFloat bufferViewWidth = rankNameX + rankName.bounds.size.width;
    CGFloat bufferViewHeight = bigRankBadgeWidth;
    CGFloat bufferViewY = 20;
    CGFloat bufferViewX = (self.bounds.size.width - bufferViewWidth)/2;
    
    UIView *bufferView = [[UIView alloc] initWithFrame:CGRectMake(bufferViewX, bufferViewY, bufferViewWidth, bufferViewHeight)];
    bufferView.backgroundColor = [UIColor clearColor];
    
    [bufferView addSubview:bigRankBadge];
    [bufferView addSubview:rankName];
    
    [self addSubview:bufferView];
    
    //add grey line of for the ranks
    CGFloat lineViewHorizontalPadding = 5;
    CGFloat lineViewY = bufferViewY + bufferViewHeight + 35;
    CGFloat lineViewHeight = 10;
    CGFloat lineViewWidth = self.bounds.size.width - (lineViewHorizontalPadding)*2;
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(lineViewHorizontalPadding, lineViewY, lineViewWidth, lineViewHeight)];
    lineView.backgroundColor = [UIColor colorWithWhite:0.941 alpha:1.000];
    lineView.layer.cornerRadius = 5.0f;
    
    [self addSubview:lineView];
    
    
    //add small ranks
    CGFloat smallRankWidth = 25;
    CGFloat smallRankHeight = 25;
    CGFloat smallRankHorizontalPadding = (lineViewWidth - (smallRankWidth/2))/4;
    CGFloat smallRankXOffset = smallRankHorizontalPadding;
    CGFloat smallRankY = lineViewY - (smallRankHeight-lineViewHeight)/2;
    
    int count = 3;
    
    for (int i = 0; i < count; i++) {
        UIImage *smallRankImage;
        
        if(i == self.rank) {
            
            switch (i) {
                case ActiveBudzProfileRankOne:
                    {
                        smallRankImage = [UIImage imageNamed:TRIBE_RANK_I_YELLOW_ROUND];
                    }
                    break;
                
                case ActiveBudzProfileRankTwo:
                    {
                        smallRankImage = [UIImage imageNamed:TRIBE_RANK_II_YELLOW_ROUND];
                    }
                    break;
                    
                case ActiveBudzProfileRankThree:
                    {
                        smallRankImage = [UIImage imageNamed:TRIBE_RANK_III_YELLOW_ROUND];
                    }
                    break;
                    
                default:
                    break;
            }
            
        } else {
            
            switch (i) {
                case ActiveBudzProfileRankOne:
                {
                    smallRankImage = [UIImage imageNamed:TRIBE_RANK_I_GREY_ROUND];
                }
                    break;
                    
                case ActiveBudzProfileRankTwo:
                {
                    smallRankImage = [UIImage imageNamed:TRIBE_RANK_II_GREY_ROUND];
                }
                    break;
                    
                case ActiveBudzProfileRankThree:
                {
                    smallRankImage = [UIImage imageNamed:TRIBE_RANK_III_GREY_ROUND];
                }
                    break;
                    
                default:
                    break;
            }
            
        }
        
        UIImageView *smallRank = [[UIImageView alloc] initWithFrame:CGRectMake(smallRankXOffset, smallRankY, smallRankWidth, smallRankHeight)];
        smallRank.backgroundColor = [UIColor clearColor];
        smallRank.image = smallRankImage;
        smallRank.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:smallRank];
        
        smallRankXOffset += smallRankHorizontalPadding;
    }
}

-(void)setProfileRank:(ActiveBudzProfileRank)theRank
{
    self.rank = theRank;
}

-(void)resetAppearance
{
    for(UIView *subView in self.subviews) {
        [subView removeFromSuperview];
    }
    
    [self setAppearance];
}

@end
