//
//  GTRMyTribeCell.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/11/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRTribe.h"

@class GTRTribeCell;
@protocol GTRTribeCellDelegate <NSObject>

-(void)didClickJoin:(GTRTribe*)tribe inCell:(GTRTribeCell*)cell;

@end

@interface GTRTribeCell : UITableViewCell

@property(nonatomic,weak) id <GTRTribeCellDelegate> delegate;

- (void)setContent:(GTRTribe*)theTribe;

@end
