//
//  GTRSuggestedActiveBudzCell.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/9/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface GTRSuggestedActiveBudzCell : UITableViewCell

@property (strong, nonatomic) UILabel *suggestedABTitleLabel;
@property (strong, nonatomic) UILabel *noSuggestedABLabel;
@property (strong, nonatomic) NSMutableArray *suggestedActiveBudzArray;
@property (nonatomic, strong) iCarousel *carousel;

- (void)toggleNoSuggestedABLabel:(BOOL) noSuggestedAB;

@end
