//
//  GTRImagePickerHelper.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/9/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "GTRImagePickerHelper.h"
#import "GTRNewPostViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

NSString *CUSTOM_ACTION_SHEET_TITLE = @"Add a photo";

@interface GTRImagePickerHelper() <UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
#define ImageSourceCameraTitle NSLocalizedString(@"Take Photo", @"take photo")
#define ImageSourceGalleryTitle  NSLocalizedString(@"Choose From Library", @"choose from library")
#define ImageSourceFromActivebudzPhotos  NSLocalizedString(@"Choose From Activebudz Photos", @"choose from activebudz photos")

@property NSMutableArray *actionSheetButtons;
@property(nonatomic,strong) UIViewController *viewController;
@property(nonatomic,strong) GTRPhotoActionSheet *photoActionSheet;

@end

@implementation GTRImagePickerHelper
@synthesize actionSheetButtons, viewController, delegate;
@synthesize photoActionSheet = _photoActionSheet;


- (id) init:(UIViewController*)myViewController
{
    return [self init:myViewController imageSources:ImageSourceDefault];
}

- (id) init:(UIViewController*)myViewController imageSources:(GTRImageSourceSet) imageSourceSet
{
    self = [super init];
    if (self!=nil) {
        viewController = myViewController;
        actionSheetButtons = [[NSMutableArray alloc] init];
        switch (imageSourceSet) {
            case ImageSourceDefault:
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                    [actionSheetButtons addObject:ImageSourceCameraTitle];
                }
                [actionSheetButtons addObject:ImageSourceGalleryTitle];
                break;
            default:
                break;
        }
    }
    return self;
}

# pragma mark ActionSheet

- (void) openImagePickerActionSheet
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Add a photo", @"add a photo")
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];

    for (NSString *actionSheetButton in actionSheetButtons) {
        [actionSheet addButtonWithTitle:actionSheetButton];
    }
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", @"cancel")];
    [actionSheet setCancelButtonIndex:actionSheetButtons.count];
    
    [actionSheet showInView:viewController.view];
}

#pragma mark - Custom Action Sheet

-(void)openCustomActionSheetWithActivebudzPhotos
{
    _photoActionSheet = [[GTRPhotoActionSheet alloc]initWithSuperView:viewController.view useActivebudzPhotos:YES delegate:self];
    [_photoActionSheet show];
}

-(void)openCustomActionSheet
{
    _photoActionSheet = [[GTRPhotoActionSheet alloc]initWithSuperView:viewController.view useActivebudzPhotos:NO delegate:self];
    _photoActionSheet.title = CUSTOM_ACTION_SHEET_TITLE;
    
    [_photoActionSheet show];
}

#pragma mark - show Image Picker

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    
    [viewController presentViewController:imagePickerController animated:YES completion:nil];
}

#pragma mark - UINavigationController delegate

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

#pragma mark - UIActionSheet delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *clickedButtonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];

    if ([clickedButtonTitle isEqualToString:ImageSourceCameraTitle]){
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
    } else if ([clickedButtonTitle isEqualToString:ImageSourceGalleryTitle]){
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

#pragma mark - Custom Photo actionsheet delegate methods

-(void)gtrActionSheet:(GTRPhotoActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *clickedButtonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([clickedButtonTitle isEqualToString:ImageSourceCameraTitle]){
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
    } else if ([clickedButtonTitle isEqualToString:ImageSourceGalleryTitle]){
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    } else if([clickedButtonTitle isEqualToString:ImageSourceFromActivebudzPhotos]){
        [delegate didPickImage:nil];
    } else {
        [delegate didPickImage:nil];
    }
}

#pragma mark - UIImagePickerController delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    [viewController dismissViewControllerAnimated:YES completion:^{
        if ([(id) delegate respondsToSelector:@selector(didPickImage:)]){
            [delegate didPickImage:image];
        }
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [viewController dismissViewControllerAnimated:YES completion:^{
        if ([(id) delegate respondsToSelector:@selector(didPickImage:)]){
            [delegate didPickImage:nil];
        }
    }];
}

+(GTRImageOrientation)checkImageOrientation:(UIImage*)image
{
    if (image.size.height > image.size.width) {
        return GTRImageOrientationPortrait;
    }
    else if(image.size.height < image.size.width) {
        return GTRImageOrientationLandscape;
    }
    else {
        return GTRImageOrientationPortrait;
    }
}

+(void)setAndAdjustOrientationImageView:(UIImageView*)imageView withURL:(NSURL*)url andPlaceholder:(UIImage*)placeholder
{
    __weak UIImageView *weakPostImage = imageView;
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [imageView setImageWithURLRequest:request placeholderImage:placeholder success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        GTRImageOrientation imageOrientation = [GTRImagePickerHelper checkImageOrientation:image];
        if (imageOrientation == GTRImageOrientationPortrait) {
            weakPostImage.contentMode = UIViewContentModeScaleAspectFit;
            weakPostImage.image = image;
        }
        else {
            weakPostImage.contentMode = UIViewContentModeScaleToFill;
            weakPostImage.image = image;
        }
        
        [weakPostImage setNeedsLayout];
    } failure:nil];
}

@end
