//
//  GTRTribeErrorView.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/18/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTRTribeErrorView : UIView

@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UILabel *messageLabel;

-(id)initWithFrame:(CGRect)frame withTarget:(id)target dismissSelctor:(SEL)dismissSelector;

@end
