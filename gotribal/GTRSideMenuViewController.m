//
//  GTRSideMenuViewController.m
//  gotribal
//
//  Created by Muhammad Taufik, mamaz on 11/15/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSideMenuViewController.h"
#import "MFSideMenu.h"
#import "GTRMainActivitiesViewController.h"
#import "GTRMessageListViewController.h"
#import "GTRMessagesViewController.h"
#import "GTRProfileViewController.h"
#import "GTREditFitnessProfileViewController.h"
#import "GTRActiveBudzPageViewController.h"
#import "GTRSideMenuCell.h"
#import "GTRSideMenuHelper.h"
#import "GTRUser.h"
#import "GTRAvatar.h"
#import "GTREditFitnessProfileViewController.h"
#import "GTRSettingsViewController.h"
#import "GTRNotificationsViewController.h"
#import "GTRActiveBudzProfileViewController.h"
#import "GTRMyTribeViewController.h"
#import "GTRUserHelper.h"
#import "AFManagerHelper.h"
#import "SVProgressHUD.h"
#import "GTRInviteFriendsViewController.h"

typedef enum {
    GTRSideMenuSectionMyAccount = 0,
    GTRSideMenuSectionFeed,
    GTRSideMenuSectionAlerts,
    GTRSideMenuSectionTools
}GTRSideMenuSection;

// KEY
NSString* const USER_AVATAR_KEY             = @"USER_AVATAR_KEY";
NSString* const EDIT_AVATAR_KEY             = @"EDIT_AVATAR_KEY";
NSString* const ACTIVITIES_AVATAR_KEY       = @"ACTIVITIES_AVATAR_KEY";
NSString* const TRIBES_AVATAR_KEY           = @"TRIBES_AVATAR_KEY";
NSString* const ACTIVEBUDZ_AVATAR_KEY       = @"ACTIVEBUDZ_AVATAR_KEY";
NSString* const MESSAGES_AVATAR_KEY         = @"MESSAGES_AVATAR_KEY";
NSString* const NOTIFICATIONS_AVATAR_KEY    = @"NOTIFICATIONS_AVATAR_KEY";
NSString* const INVITE_FRIENDS_AVATAR_KEY   = @"INVITE_FRIENDS_AVATAR_KEY";
NSString* const SETTINGS_AVATAR_KEY         = @"SETTINGS_AVATAR_KEY";
NSString* const LINE_IMAGE_KEY              = @"LINE_IMAGE_KEY";

// header
NSString* const MY_ACCOUNT_STRING = @"My Account";
NSString* const FEED_STRING       = @"Feed";
NSString* const ALERTS_STRING     = @"Alerts";
NSString* const TOOLS_STRING      = @"Tools";

// row text
NSString* const EDIT_STRING             = @"Edit My Profile";
NSString* const FITNESS_PROFILE_STRING  = @"My Fitness Profile";
NSString* const ACTIVITIES_STRING       = @"Activities";
NSString* const TRIBES_STRING           = @"Tribes";
NSString* const ACTIVEBUDZ_STRING       = @"Activebudz";
NSString* const MESSAGES_STRING         = @"Messages";
NSString* const NOTIFICATIONS_STRING    = @"Notifications";
NSString* const INVITE_FRIENDS_STRING   = @"Invite Friends";
NSString* const SETTINGS_STRING         = @"Settings";

NSString* const PLACEHOLDER_NAME = @"default_avatar";


@interface GTRSideMenuViewController () <GTRProfileViewControllerDelegate>

@property(nonatomic,strong) NSMutableArray *arrayOfData;
@property(nonatomic,strong) NSMutableDictionary *sideBarImageCache;

@end

@implementation GTRSideMenuViewController
@synthesize sideBarImageCache = _sideBarImageCache;
@synthesize arrayOfData = _arrayOfData;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (UIDevice.currentDevice.systemVersion.floatValue >= 7.0){
        self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
    } else {
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 20, 0);
    }
    
    // cache images
    [self cacheImages];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = GTRSIDE_MENU_COLOR;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Custom action methods

-(void)openMessageButtonAction
{
    GTRMessageListViewController *messageListViewController = [[GTRMessageListViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:messageListViewController];
    self.menuContainerViewController.centerViewController = navigationController;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{}];
}

-(void)viewMyProfileButtonAction
{
    GTRUser *currentUser = [GTRUserHelper getCurrentUser];
    NSArray *userConnectionBuff = [GTRUserHelper getArchivedUserConnections];
    
    //for handling old data.
    NSMutableArray *userConnections = [[NSMutableArray alloc] init];
    for(NSObject *object in userConnectionBuff){
        if ([object isKindOfClass:[NSDictionary class]]) {
            [userConnections addObject:[MTLJSONAdapter modelOfClass:[GTRUser class] fromJSONDictionary:(NSDictionary*)object error:nil]];
        } else {
            [userConnections addObject:object];
        }
    }
    
    GTRActiveBudzProfileViewController *profile = [[GTRActiveBudzProfileViewController alloc] initWithUser:currentUser
                                                                                                 indexPath:nil
                                                                                               connections:userConnections
                                                                                               profileType:ActiveBudzProfileTypeCurrentUser];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:profile];
    
    self.menuContainerViewController.centerViewController = navigationController;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{}];
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager GET:GET_USER_ENDPOINT(currentUser.userID) parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        DDLogInfo(@" Response : %@", responseObject);
        
        [GTRUserHelper updateCurrentUser:[responseObject objectForKey:@"user"]];
        profile.user = [GTRUserHelper getCurrentUser];
        
        NSArray *connectionBuff = [responseObject objectForKey:@"connection"];
        NSMutableArray *connections = [[NSMutableArray alloc] init];
        
        for(NSDictionary *dictionary in connectionBuff){
            [connections addObject:[MTLJSONAdapter modelOfClass:[GTRUser class] fromJSONDictionary:dictionary error:nil]];
        }
        
        [GTRUserHelper archiveUserConnections:[connections copy]];
        profile.userConnections = connections;
        
        [profile refresh];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"    Error : %@", error);
    }];
}

-(void)editProfileButtonAction
{
    GTRProfileViewController *profileViewController = [[GTRProfileViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:profileViewController];
    self.menuContainerViewController.centerViewController = navigationController;
    profileViewController.delegate = self;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{}];
}

-(void)activityButtonAction
{
    GTRMainActivitiesViewController *mainActivities = [[GTRMainActivitiesViewController alloc]init];
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:mainActivities];
    self.menuContainerViewController.centerViewController = navigationController;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{}];
}

-(void)tribeButtonAction
{
    GTRMyTribeViewController *myTribes = [[GTRMyTribeViewController alloc]init];
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:myTribes];
    self.menuContainerViewController.centerViewController = navigationController;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{}];
}

-(void)activeBudzButtonAction
{
    GTRActiveBudzPageViewController *activeBudzPage =[[GTRActiveBudzPageViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:activeBudzPage];
    self.menuContainerViewController.centerViewController = navigationController;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{}];
}

-(void)settingsViewAction
{
    GTRSettingsViewController *settingsViewController =[[GTRSettingsViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:settingsViewController];
    self.menuContainerViewController.centerViewController = navigationController;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{}];
}

-(void)notificationsViewAction
{
    GTRNotificationsViewController *settingsViewController =[[GTRNotificationsViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:settingsViewController];
    self.menuContainerViewController.centerViewController = navigationController;
    
    if (self.menuContainerViewController.menuState == MFSideMenuStateLeftMenuOpen) [self.menuContainerViewController toggleLeftSideMenuCompletion:^{}];
}

-(void)editFitnessButtonAction
{
    GTREditFitnessProfileViewController *editFitnessViewController = [[GTREditFitnessProfileViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:editFitnessViewController];
    self.menuContainerViewController.centerViewController = navigationController;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{}];
}

-(void)inviteFriendsButtonAction
{
    GTRInviteFriendsViewController *inviteFriendsViewController = [[GTRInviteFriendsViewController alloc] initWithPushedMode:NO tribeMode:NO];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:inviteFriendsViewController];
    self.menuContainerViewController.centerViewController = navigationController;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{}];
}

#pragma mark - Table view data source and delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case GTRSideMenuSectionMyAccount:
            return 3;
        case GTRSideMenuSectionFeed:
            return 3;
        case GTRSideMenuSectionAlerts:
            return 2;
        case GTRSideMenuSectionTools:
            return 2;
    }
    
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case GTRSideMenuSectionMyAccount:
            return [GTRSideMenuHelper createHeaderViewWithText:MY_ACCOUNT_STRING
                                                withImageCache:_sideBarImageCache];
        case GTRSideMenuSectionFeed:
            return [GTRSideMenuHelper createHeaderViewWithText:FEED_STRING
                                                withImageCache:_sideBarImageCache];
        case GTRSideMenuSectionAlerts:
            return [GTRSideMenuHelper createHeaderViewWithText:ALERTS_STRING
                                                withImageCache:_sideBarImageCache];
        case GTRSideMenuSectionTools:
            
            return [GTRSideMenuHelper createHeaderViewWithText:TOOLS_STRING
                                                withImageCache:_sideBarImageCache];
    }
    
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    GTRSideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[GTRSideMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor colorWithRed:53.0/255.0 green:53.0/255.0 blue:55.0/255.0 alpha:1.0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    switch (indexPath.section) {
        case GTRSideMenuSectionMyAccount:
            return [self myAccountCell:cell forRowAtIndex:indexPath.row];
        case GTRSideMenuSectionFeed:
            return [self feedCell:cell forRowAtIndex:indexPath.row];
        case GTRSideMenuSectionAlerts:
            return [self alertCell:cell forRowAtIndex:indexPath.row];
        case GTRSideMenuSectionTools:
            return [self toolsCell:cell forRowAtIndex:indexPath.row];
    }

    return cell;
}

#pragma mark - Cells

-(GTRSideMenuCell*)myAccountCell:(GTRSideMenuCell*)cell forRowAtIndex:(NSInteger)rowIndex
{
    switch (rowIndex) {
        case 0:
            [GTRSideMenuHelper setAvatarImageURL:[self getCurrentUserAvatarURL]
                                 withPlaceholder:[UIImage imageNamed:PLACEHOLDER_NAME] //change to correct placeholder image
                                            text:[self getName]
                                          inCell:cell];
            [GTRSideMenuHelper hideNotificationBadgeInCell:cell];
            break;
        case 1:
            [GTRSideMenuHelper setAvatarImage:_sideBarImageCache[EDIT_AVATAR_KEY]
                                         text:EDIT_STRING
                                       inCell:cell];
            [GTRSideMenuHelper hideNotificationBadgeInCell:cell];
            break;
        case 2:
            [GTRSideMenuHelper setAvatarImage:_sideBarImageCache[EDIT_AVATAR_KEY]
                                         text:FITNESS_PROFILE_STRING
                                       inCell:cell];
            [GTRSideMenuHelper hideNotificationBadgeInCell:cell];
            break;
    }
    
    return cell;
}

-(GTRSideMenuCell*)feedCell:(GTRSideMenuCell*)cell forRowAtIndex:(NSInteger)rowIndex
{
    switch (rowIndex) {
        case 0:
            [GTRSideMenuHelper setAvatarImage:_sideBarImageCache[ACTIVITIES_AVATAR_KEY]
                                         text:ACTIVITIES_STRING
                                       inCell:cell];
            [GTRSideMenuHelper hideNotificationBadgeInCell:cell];
            break;
        case 1:
            [GTRSideMenuHelper setAvatarImage:_sideBarImageCache[TRIBES_AVATAR_KEY]
                                         text:TRIBES_STRING
                                       inCell:cell];
            [GTRSideMenuHelper hideNotificationBadgeInCell:cell];
            break;
        case 2:
            [GTRSideMenuHelper setAvatarImage:_sideBarImageCache[ACTIVEBUDZ_AVATAR_KEY]
                                         text:ACTIVEBUDZ_STRING
                                       inCell:cell];
            [GTRSideMenuHelper hideNotificationBadgeInCell:cell];
            break;
            
    }
    
    return cell;
}

-(GTRSideMenuCell*)alertCell:(GTRSideMenuCell*)cell forRowAtIndex:(NSInteger)rowIndex
{
    switch (rowIndex) {
        case 0:
            [GTRSideMenuHelper setAvatarImage:_sideBarImageCache[MESSAGES_AVATAR_KEY]
                                         text:MESSAGES_STRING
                                    inCell:cell];

            break;
        case 1:
            [GTRSideMenuHelper setAvatarImage:_sideBarImageCache[NOTIFICATIONS_AVATAR_KEY]
                                         text:NOTIFICATIONS_STRING
                                       inCell:cell];
            
            break;
    }
    
    return cell;
}

-(GTRSideMenuCell*)toolsCell:(GTRSideMenuCell*)cell forRowAtIndex:(NSInteger)rowIndex
{
    switch (rowIndex) {
        case 0:
            [GTRSideMenuHelper setAvatarImage:_sideBarImageCache[INVITE_FRIENDS_AVATAR_KEY]
                                         text:INVITE_FRIENDS_STRING
                                    inCell:cell];
            [GTRSideMenuHelper hideNotificationBadgeInCell:cell];
            break;
        case 1:
            [GTRSideMenuHelper setAvatarImage:_sideBarImageCache[SETTINGS_AVATAR_KEY]
                                         text:SETTINGS_STRING
                                       inCell:cell];
            [GTRSideMenuHelper hideNotificationBadgeInCell:cell];
            break;
    }
    
    return cell;
}

#pragma mark - image cache

-(void)cacheImages
{
    if (!_sideBarImageCache) {
        _sideBarImageCache = [[NSMutableDictionary alloc]init];
    }
    
    if (!_sideBarImageCache[EDIT_AVATAR_KEY]) {
        [_sideBarImageCache setObject:[UIImage imageNamed:@"ico-menu-pen"] forKey:EDIT_AVATAR_KEY];
    }
    
    if (!_sideBarImageCache[ACTIVITIES_AVATAR_KEY]) {
        [_sideBarImageCache setObject:[UIImage imageNamed:@"ico-menu-activity"] forKey:ACTIVITIES_AVATAR_KEY];
    }
    
    if (!_sideBarImageCache[TRIBES_AVATAR_KEY]) {
        [_sideBarImageCache setObject:[UIImage imageNamed:@"ico-menu-tribes"] forKey:TRIBES_AVATAR_KEY];
    }
    
    if (!_sideBarImageCache[ACTIVEBUDZ_AVATAR_KEY]) {
        [_sideBarImageCache setObject:[UIImage imageNamed:@"ico-menu-friends"] forKey:ACTIVEBUDZ_AVATAR_KEY];
    }
    
    if (!_sideBarImageCache[MESSAGES_AVATAR_KEY]) {
        [_sideBarImageCache setObject:[UIImage imageNamed:@"ico-menu-msg"] forKey:MESSAGES_AVATAR_KEY];
    }
    
    if (!_sideBarImageCache[NOTIFICATIONS_AVATAR_KEY]) {
        [_sideBarImageCache setObject:[UIImage imageNamed:@"ico-menu-notif"] forKey:NOTIFICATIONS_AVATAR_KEY];
    }
    
    if (!_sideBarImageCache[INVITE_FRIENDS_AVATAR_KEY]) {
        [_sideBarImageCache setObject:[UIImage imageNamed:@"ico-menu-invite"] forKey:INVITE_FRIENDS_AVATAR_KEY];
    }
    
    if (!_sideBarImageCache[SETTINGS_AVATAR_KEY]) {
        [_sideBarImageCache setObject:[UIImage imageNamed:@"ico-menu-settings"] forKey:SETTINGS_AVATAR_KEY];
    }
    
    if (!_sideBarImageCache[LINE_IMAGE_KEY]) {
        [_sideBarImageCache setObject:[UIImage imageNamed:@"line-menu"] forKey:LINE_IMAGE_KEY];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case GTRSideMenuSectionMyAccount:
            if (indexPath.row == 0){
                [self viewMyProfileButtonAction];
            } else if (indexPath.row == 1){
                [self editProfileButtonAction];
            } else if (indexPath.row == 2){
                [self editFitnessButtonAction];
            }
            
            break;
        case GTRSideMenuSectionFeed:
            if (indexPath.row == 0){
                [self activityButtonAction];
            } else if(indexPath.row == 1){
            #if ALPHA_RELEASE == 0
                [self tribeButtonAction];
            #endif
            } else if(indexPath.row == 2){
                [self activeBudzButtonAction];
            }
            
            break;
        case GTRSideMenuSectionAlerts:
            if (indexPath.row == 0){
                [self openMessageButtonAction];
            }
            else if (indexPath.row == 1){
                [self notificationsViewAction];
            }
            break;
        case GTRSideMenuSectionTools:
            if (indexPath.row == 0) {
                [self inviteFriendsButtonAction];
            }
            if (indexPath.row == 1){
                [self settingsViewAction];
            }
            
            break;
    }
}

#pragma mark - get User Avatar

-(NSString *)getName
{    
    return [[GTRUserHelper getCurrentUser] fullName]; //currentUserName;
}

#pragma mark - get Current User object

-(GTRUser*)getCurrentUser
{
    return [GTRSideMenuHelper appDelegate].currentUser;
}

#pragma mark - get Current User Avatar URL

-(NSURL*)getCurrentUserAvatarURL
{
    GTRUser *currentUser = [self getCurrentUser];
    
    if (currentUser.avatarURL)
        return [NSURL URLWithString:currentUser.avatarURL];
    else
        return nil;
}

-(void)reloadMyProfile
{
    NSArray *indexPaths = [NSArray arrayWithObjects:
                           [NSIndexPath indexPathForRow:0 inSection:GTRSideMenuSectionMyAccount],
                           nil];
    
    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
}

@end

