//
//  GTRSettingsCell.h
//  gotribal
//
//  Created by Hisma Mulya S on 11/29/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

#define VIEW_BACKGROUND_COLOR       [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0]
#define TABLEVIEW_BACKGROUND_COLOR  VIEW_BACKGROUND_COLOR

@interface GTRSettingsCell : UITableViewCell

@property(nonatomic,strong) NSIndexPath *indexpath;
@property(nonatomic,strong) UILabel *text;
@property(nonatomic,strong) UIView *rightView;

- (void)setRightViewWithText:(NSString *)text;

@end
