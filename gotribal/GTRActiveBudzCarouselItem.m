//
//  GTRSuggestedActiveBudzCarouselItem.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/9/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActiveBudzCarouselItem.h"
#import <UIImageView+AFNetworking.h>

@implementation GTRActiveBudzCarouselItem

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:1.000];
        self.layer.cornerRadius = self.bounds.size.width/2;
        self.layer.masksToBounds = YES;
        self.contentMode = UIViewContentModeScaleAspectFill;
    }
    return self;
}

- (void) setContent:(GTRUser*) user
{
    [self setImageWithURL:[NSURL URLWithString:user.avatarURL] placeholderImage:DEFAULT_AVATAR];
}

@end
