//
//  GTRAvatar.h
//  gotribal
//
//  Created by Sambya Aryasa on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mantle.h"

@interface GTRAvatar : MTLModel <MTLJSONSerializing>
@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) UIImage *image;

@end
