//
//  GTRCreateNewTribeViewController.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/11/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRNewPostViewController.h"
#import "GTRImagePickerHelper.h"
#import "GTRTribeActivityViewController.h"
#import "GTRTribeError.h"

@interface GTRCreateNewTribeViewController : GTRNewPostViewController<UITextViewDelegate,
                                            GTRImagePickerHelperDelegate, GTRTribeActivityViewControllerDelegate, GTRTribeErrorDelegate>

@end
