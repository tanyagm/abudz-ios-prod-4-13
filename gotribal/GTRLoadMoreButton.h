//
//  GTRLoadMoreButton.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/16/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTRLoadMoreButton : UIView

- (id)initWithFrame:(CGRect)frame;
- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents;
- (void)setText:(NSString*)labelText;
- (void)startAnimating;
- (void)stopAnimating;
- (CGFloat)getHeight;

@end
