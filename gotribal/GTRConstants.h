//
//  GTRConstants.h
//  gotribal
//
//  Created by Muhammad Taufik on 11/29/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#ifndef gotribal_GTRConstants_h
#define gotribal_GTRConstants_h

#import "GTRUIConstant.h"

#define GTR_FLURRY_ANALYTICS //comment this line to turn off the analytics

#ifdef DEBUG // DEBUG

//#define BASE_URL @"http://localhost:5000"
#define BASE_URL @"http://api.dev.gotribal.com"
//#define BASE_URL @"http://10.3.1.15:5000"
//#define BASE_URL @"http://10.3.1.76:3000"

#pragma mark Pusher credentials
#define PUSHER_KEY @"8cb00b1ce5eed2a3b336"

#pragma mark Flurry credentials
#define FLURRY_KEY @"54VRCHFJCCCJ9HY2HH4B"

#pragma mark Crashlytics
#define CRASHLYTICS_KEY @"1c8c9703dfb8c37951b8da95b5f3c981ba1684c0"

#elif PRODUCTION

#define BASE_URL @"http://api.gotribal.com"

#pragma mark Pusher credentials
#define PUSHER_KEY @"8cb00b1ce5eed2a3b336"

#pragma mark Flurry credentials
#define FLURRY_KEY @"GJ5CZGQ9BKKKM9ZNR97R"

#pragma mark Crashlytics
#define CRASHLYTICS_KEY @"1c8c9703dfb8c37951b8da95b5f3c981ba1684c0"

#else // RELEASE

#define BASE_URL @"http://api.gotribal.com"

#pragma mark Pusher credentials
#define PUSHER_KEY @"49624dc29927bbbe140c"

#pragma mark Flurry credentials
#define FLURRY_KEY @"GJ5CZGQ9BKKKM9ZNR97R"

#pragma mark Crashlytics
#define CRASHLYTICS_KEY @"1c8c9703dfb8c37951b8da95b5f3c981ba1684c0"

#endif

static NSInteger const NOTIFICATION_LABEL_TAG = 1313;

#pragma mark User Related Constants
#define CURRENT_USER_KEY @"current_user_key"

#pragma mark OAuth Related Constants
#define CLIENT_ID @"b0a0dbf6d680d496d56052039c0c743ba3a2031dc68aadc702c0521c3401f97b"
#define CLIENT_SECRET @"a7fbbcf767ebdd5dc3b61194d5ac6f5bd1d46895f17f7167af32f8fdd5113f6f"
#define CLIENT_ACCESS_TOKEN @"370086868c8e3a5adb1b63679eaf802fec5cf839bbd99eaa1e3f14f138d66a96"

#pragma mark Device Related Constants
#define USER_DEVICE_TOKEN @"user_device_token"
#define REGISTER_AIRSHIP @"register_airship"
#define UDID_KEY @"udid_key"
#define DEVICE_TYPE @0

#pragma mark Activity Stream Related Constant
#define ACTIVITY_STREAM_KEY @"activity_stream_key"
// The number of post loaded per request (default: 6)
#define MAX_PER_PAGE 6

#pragma mark User Connection Constant
#define USER_CONNECTION_KEY @"user_connection_key"

#pragma mark ActiveBudz List Related Constant
#define NUMBER_OF_ACTIVEBUDZ_DATA_RETRIEVED 10
#define SUGGESTED_ACTIVEBUDZ_KEY @"suggested_activebudz_key"
#define MY_ACTIVEBUDZ_KEY @"my_activebudz_key"

#pragma mark API Endpoints

#pragma mark Session Related Endpoints
#define USER_LOGIN_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/sign_in"]
#define USER_LOGOUT_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/sign_out"]
#define USER_FACEBOOK_LOGIN_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/sign_in_fb"]

#pragma mark User Related Endpoints
#define USER_SIGNUP_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/sign_up"]
#define CURRENT_USER_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/me"]
#define UPDATE_FITNESS_PROFILE [NSString stringWithFormat:@"%@", @"/api/v1/fitness_profile"]
#define REGISTER_DEVICE_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/devices"]
#define GET_USER_ENDPOINT(userID) [NSString stringWithFormat:@"%@%@", @"/api/v1/users/", userID]
#define FRIENDS_RECEIVE_PENDING_REQUEST [NSString stringWithFormat:@"%@", @"/api/v1/friends/received_request"]
#define FRIENDS_PENDING_REQUEST_CONFIRM [NSString stringWithFormat:@"%@", @"/api/v1/friends/"]
#define ENDORSE_ENDPOINT(id) [NSString stringWithFormat:@"/api/v1/users/%@/endorse",id]
#define RECOVER_PASSWORD_ENDPOINT @"/api/v1/recover_password/"
//#define USER_SETTING_ENDPOINT @"http://gotribal.apiary.io/notif_settings"
#define USER_SETTING_ENDPOINT @"/api/v1/notif_settings/"

#pragma mark Direct Messages Endpoints
#define INITIATE_DIRECT_MESSAGE_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/direct_messages/new"]
#define LIST_OF_CONVERSATIONS_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/direct_messages"]
#define DELETE_LIST_OF_CONVERSATIONS_ENDPOINT(conversationID) [NSString stringWithFormat:@"%@%@", @"/api/v1/direct_messages/", conversationID]
#define MESSAGE_CONVERSATIONS_ENDPOINT(conversationID) [NSString stringWithFormat:@"/api/v1/direct_messages/%@/conversation", conversationID]
#define MESSAGE_CONVERSATION_ENDPOINT(conversationID) [NSString stringWithFormat:@"/api/v1/direct_messages/%@", conversationID]
#define SEND_DIRECT_MESSAGE_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/direct_messages"]
#define DIRECT_MESSAGE_AUTH_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/direct_messages/auth"]

#pragma mark Friends Related Endpoints
#define FRIENDS_SUGGESTION_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/suggestion"]
#define ADD_OR_REMOVE_FRIENDS_ENDPOINT(friendID) [NSString stringWithFormat:@"%@%@", @"/api/v1/friends/", friendID]
#define FRIENDS_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/friends"]

#pragma mark Activity Stream Related Endpoints
#define GET_ACTIVITY_STREAM_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/posts"]
#define GET_ACTIVITY_NOTIFICATIONS_ENDPOINT @"/api/v1/activities"

#pragma mark Comment Related Endpoints
#define GET_COMMENTS_ENDPOINT(post_id) [NSString stringWithFormat:@"/api/v1/posts/%@/comments", post_id]
#define CREATE_COMMENTS_ENDPOINT(post_id) [NSString stringWithFormat:@"/api/v1/posts/%@/comments", post_id]

#pragma mark Post related Endpoints
#define POST_VOTE_ENDPOINT(id)                   [NSString stringWithFormat:@"/api/v1/posts/%@/vote", id]
#define CREATE_POST_ENDPOINT                     [NSString stringWithFormat:@"/api/v1/posts"]
#define DELETE_POST_ENDPOINT(id)                 [NSString stringWithFormat:@"/api/v1/posts/%@", id]

#pragma mark Tribe Related Endpoints
#define TRIBE_ENDPOINT [NSString stringWithFormat:@"%@", @"/api/v1/tribes"]

#pragma mark - Definition to detect iOS version

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#pragma mark User Detail Keys
#define USER_DETAIL_WORKOUT_TIME_KEY @"workout_time"
#define USER_DETAIL_GENDER_KEY @"gender"
#define USER_DETAIL_ROLE_KEY @"role"
#define USER_DETAIL_ROLE_PREFERENCE_KEY @"role_preference"
#define USER_DETAIL_GENDER_PREFERENCE_KEY @"gender_preference"
#define USER_DETAIL_FITNESS_PURSUIT_KEY @"fitness_pursuit"
#define USER_DETAIL_FITNESS_LEVEL_KEY @"fitness_level"
#define USER_DETAIL_WEEKLY_ACTIVITIES_KEY @"weekly_activities"
#define USER_DETAIL_LIFESTYLE_KEY @"lifestyle"
#define USER_DETAIL_AGE_GROUP_KEY @"age_group"
#define USER_DETAIL_LIVING_PLACE_KEY @"living_place"

#pragma mark Edit Fitness Profile Title String
#define EDIT_FITNESS_FAVORITE_WORKOUT_TIME_TITLE @"Favorite Workout Time"
#define EDIT_FITNESS_DESIRED_ACTIVEBUDZ_GENDER @"Desired Activebudz Gender"
#define EDIT_FITNESS_MY_ROLE_TITLE @"My Role"
#define EDIT_FITNESS_LOOKING_FOR_TITLE @"Looking For"
#define EDIT_FITNESS_FITNESS_PURSUIT_TITLE @"Fitness Pursuit"
#define EDIT_FITNESS_FITNESS_ABILITY_TITLE @"Fitness Ability"
#define EDIT_FITNESS_DAILY_ROUTINE_TITLE @"Daily Routine"
#define EDIT_FITNESS_LIFESTYLE_TITLE @"Lifestyle"
#define EDIT_FITNESS_LOCATION_TITLE @"Location"

#pragma mark Pursuit Icon Names
#define PURSUIT_FITNESS_ICON_NAME @"ico-activity-fitness"
#define PURSUIT_RUNNING_ICON_NAME @"ico-activity-running"
#define PURSUIT_CYCLING_ICON_NAME @"ico-activity-cycling"
#define PURSUIT_YOGA_ICON_NAME @"ico-activity-yoga"
#define PURSUIT_SURFING_ICON_NAME @"ico-activity-surfing"
#define PURSUIT_SWIMMING_ICON_NAME @"ico-activity-swimming"
#define PURSUIT_TRIATHLON_ICON_NAME @"ico-activity-triathlon"
#define PURSUIT_OTHER_ICON_NAME @"ico-activity-other"

#pragma mark Pursuit Answer Strings
#define PURSUIT_FITNESS_ANSWER_STRING @"Get lean and strong"
#define PURSUIT_RUNNING_ANSWER_STRING @"Running"
#define PURSUIT_CYCLING_ANSWER_STRING @"Cycling"
#define PURSUIT_YOGA_ANSWER_STRING @"Yoga or Pilates"
#define PURSUIT_SURFING_ANSWER_STRING @"Surfing"
#define PURSUIT_SWIMMING_ANSWER_STRING @"Swimming"
#define PURSUIT_TRIATHLON_ANSWER_STRING @"Triathlon"

#pragma mark setting detail key
//keys
#define REQUEST_FREQ_KEY  @"activebudz_request_freq"
#define REQUEST_TYPE_KEY  @"activebudz_request_type"
#define SUGGESTED_FREQ_KEY  @"activebudz_suggested_freq"
#define SUGGESTED_TYPE_KEY  @"activebudz_suggested_type"
#define COMMENT_FREQ_KEY  @"comment_freq"
#define COMMENT_TYPE_KEY  @"comment_type"
#define MESSAGE_FREQ_KEY  @"message_freq"
#define MESSAGE_TYPE_KEY  @"message_type"
#define TRIBE_INVITATION_FREQ_KEY  @"tribe_invitation_freq"
#define TRIBE_INVITATION_TYPE_KEY  @"tribe_invitation_type"
#define TRIBE_POST_FREQ_KEY  @"tribe_post_freq"
#define TRIBE_POST_TYPE_KEY  @"tribe_post_type"

#pragma - mark Settings enum

typedef enum {
    GTRSettingsSectionAccount = 0,
    GTRSettingsSectionNotifications,
    GTRSettingsSectionNotifificationsFrequency,
    GTRSettingsSectionSocial,
    GTRSettingsSectionSupport,
    GTRSettingsSectionLogout,
}GTRSettingsSection;

typedef enum{
    GTRReloadModeRefresh = 0,
    GTRReloadModeLoadMore
}GTRReloadMode;


#pragma mark - Definitions

#define ALPHA_RELEASE 0

#endif
