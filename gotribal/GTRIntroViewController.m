//
//  GTRIntroViewController.m
//  gotribal
//
//  Created by Muhammad Taufik on 11/16/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRIntroViewController.h"
#import "GTRSignupViewController.h"
#import "GTRLoginViewController.h"
#import "GTRAppDelegate.h"
#import "GTRSessionRestService.h"
#import "SVProgressHUD.h"
#import "GTRSideMenuHelper.h"
#import "GTRSocialHelper.h"
#import "GTRRestServiceErrorHandler.h"
#import "GTRSurveyCoverViewController.h"

#define NUMBER_OF_PAGES 5

@interface GTRIntroViewController () <UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) UIButton *skipButton;
@property (nonatomic, strong) UIView *overlayView;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) NSArray *tourDescriptions;
@end

@implementation GTRIntroViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Intro Tour"; //set for the sake of analytics
        self.tourDescriptions = @[
                                  @"Take a short survey to discover your perfect Training Buddy, Mentor, or Mentee.",
                                  @"Search, Create and Join Tribes based on interests or locations.",
                                  @"Get a unique curated list of Activebudz just for you.",
                                  @"Find Activebudz in your area and stay updated with your Activebudz activities.",
                                  @""];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.scrollView.delegate = self;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.bounds.size.width * NUMBER_OF_PAGES, self.scrollView.bounds.size.height);
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = [UIColor blackColor];
    
    //Add overlay background
    UIColor *overlayBackgroundColor = [UIColor colorWithWhite:0.067 alpha:1.000];
    CGFloat overlayHeight = 135.0f;
    self.overlayView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-overlayHeight, self.view.bounds.size.width, overlayHeight)];
    self.overlayView.backgroundColor = overlayBackgroundColor;
    
    [self.view addSubview:self.overlayView];
    
    //Add page control
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - (44*3), self.view.bounds.size.width, 44)];
    self.pageControl.userInteractionEnabled = NO;
    self.pageControl.numberOfPages = NUMBER_OF_PAGES;
    [self.view addSubview:self.pageControl];
    
    //Add description label
    CGFloat descriptionHorizontalPadding = 22.0f;
    CGFloat descriptionHeight = 44.0f;
    CGFloat descriptionTopPadding = 44.0f;
    
    self.descriptionLabel = [[UILabel alloc] initWithFrame:
                             CGRectMake(descriptionHorizontalPadding,
                                        descriptionTopPadding,
                                        self.overlayView.bounds.size.width-(descriptionHorizontalPadding * 2),
                                        descriptionHeight)];

    self.descriptionLabel.text = self.tourDescriptions.firstObject;
    self.descriptionLabel.textColor = [UIColor whiteColor];
    self.descriptionLabel.font = [UIFont systemFontOfSize:14];
    self.descriptionLabel.numberOfLines = 0;
    self.descriptionLabel.backgroundColor = [UIColor clearColor];
    [self.descriptionLabel sizeToFit];
    [self.overlayView addSubview:self.descriptionLabel];
    
    
    //Add skip button
    
    UIImage *skipButtonImage = [UIImage imageNamed:@"btn-tour-skip"];
    self.skipButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    CGRect skipButtonFrame = CGRectZero;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        skipButtonFrame = CGRectMake(self.view.bounds.size.width - 42, 40, skipButtonImage.size.width + 15, skipButtonImage.size.height + 25);
    } else {
        skipButtonFrame = CGRectMake(self.view.bounds.size.width - 42, 20, skipButtonImage.size.width + 15, skipButtonImage.size.height + 25);
    }
    
    self.skipButton.frame = skipButtonFrame;
    [self.skipButton setImage: [UIImage imageNamed:@"btn-tour-skip"] forState:UIControlStateNormal];
    [self.skipButton addTarget:self action:@selector(skipButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.skipButton];
    
    // Create tour pages
    NSMutableArray *tourPages = [NSMutableArray arrayWithCapacity:NUMBER_OF_PAGES];
    
    for (int i = 0; i < NUMBER_OF_PAGES; i++) {
        @autoreleasepool {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
            imageView.contentMode = UIViewContentModeTopLeft;
            imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"img-tour%d",(i+1)]];
            [self.scrollView addSubview:imageView];
            [tourPages addObject:imageView];
        }
    }
    
    // Layout tour pages
    CGFloat xOffset = 0;
    
    for (UIView *aView in tourPages) {
        CGRect frame = aView.frame;
        frame.origin = CGPointMake(xOffset, 0);
        aView.frame = frame;
        
        xOffset += frame.size.width;
    }
    
    UIView *lastPageView = [tourPages lastObject];
    [lastPageView setUserInteractionEnabled:YES];
    UIEdgeInsets customButtonInset = UIEdgeInsetsMake(0, 88, 0, 0);
    UIImage *facebookButtonImage = [UIImage imageNamed:@"btn-signin-fb"];
    UIImage *emailButtonImage = [UIImage imageNamed:@"btn-signup-mail"];
    
    
    //Add sign up with email button to last page
    UIButton *emailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    emailButton.frame = CGRectMake(0, self.view.bounds.size.height-emailButtonImage.size.height, self.view.bounds.size.width, emailButtonImage.size.height);
    [emailButton setBackgroundImage:emailButtonImage forState:UIControlStateNormal];
    
    [emailButton setTitle:NSLocalizedString(@"Sign up with E-mail", @"e-mail sign in button")
                      forState:UIControlStateNormal];
    [emailButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [emailButton setContentVerticalAlignment: UIControlContentVerticalAlignmentCenter];
    emailButton.titleEdgeInsets = customButtonInset;
    
    [emailButton setTitleColor:[UIColor whiteColor]
                           forState:UIControlStateNormal];
    [emailButton addTarget:self action:@selector(emailButtonAction:)
               forControlEvents:UIControlEventTouchUpInside];
    [lastPageView addSubview:emailButton];
    
    //Add sign in with facebook button to last page
    UIButton *facebookButton = [UIButton buttonWithType:UIButtonTypeCustom];
    facebookButton.frame = CGRectMake(0, self.view.bounds.size.height-(facebookButtonImage.size.height*2), self.view.bounds.size.width, facebookButtonImage.size.height);
    [facebookButton setBackgroundImage:facebookButtonImage forState:UIControlStateNormal];
    
    [facebookButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [facebookButton setContentVerticalAlignment: UIControlContentVerticalAlignmentCenter];
    [facebookButton setTitle:NSLocalizedString(@"Sign in with Facebook", @"facebook sign in button")
                         forState:UIControlStateNormal];
    facebookButton.titleEdgeInsets = customButtonInset;
    
    [facebookButton setTitleColor:[UIColor whiteColor]
                              forState:UIControlStateNormal];
    [facebookButton addTarget:self action:@selector(facebookButtonAction:)
                  forControlEvents:UIControlEventTouchUpInside];
    [lastPageView addSubview:facebookButton];
    
    // Add sign in button to last page
    NSMutableAttributedString *signInButtonText = [[NSMutableAttributedString alloc] initWithString:@"Already have an account? Sign in."];
    NSInteger textLength = [signInButtonText length];
    
    UIFont *boldFont = [UIFont fontWithName:@"Helvetica-Bold" size:16.0f];

    [signInButtonText addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(textLength-8, 8)];
    [signInButtonText addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, textLength)];

    UIButton *signInButton = [UIButton buttonWithType:UIButtonTypeCustom];
    signInButton.frame = CGRectMake(22, self.view.bounds.size.height-(facebookButtonImage.size.height*2)-44, self.view.bounds.size.width, 22);
    [signInButton setAttributedTitle:signInButtonText forState:UIControlStateNormal];
    [signInButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [signInButton sizeToFit];
    [signInButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [signInButton addTarget:self action:@selector(signInButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [lastPageView addSubview:signInButton];
}

-(void)skipButtonAction:(id)sender
{
    CGPoint lastPageOffset = CGPointMake(self.scrollView.bounds.size.width * (NUMBER_OF_PAGES - 1), 0);
    [self.scrollView setContentOffset:lastPageOffset animated:NO];
}

#pragma mark - Facebook

-(void)facebookButtonAction:(id)sender
{
    GTRSocialHelper *socialHelper = [GTRSocialHelper sharedHelper];
    NSArray *permissions = @[@"basic_info"];
    [socialHelper retrieveFacebookAccountWithPermissions:permissions success:^(NSString *accessToken) {
        [self loginWithFacebookAccessToken:accessToken];
    } failure:^(NSError *error) {
        DDLogInfo(@"Access not granted. %@", [error localizedDescription]);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Turn On Facebook Account in Settings to Sign in with Facebook", @"Turn on facebook account") message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
    }];
}

-(void)loginWithFacebookAccessToken:(NSString *)accessToken
{
    NSDictionary *parameters = @{@"token": accessToken};
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [GTRSessionRestService facebookLogin:parameters onSuccess:^(AFHTTPRequestOperation *operation, id json) {
        DDLogInfo(@"%@", json);
        [GTRSessionRestService saveOAuthCredential:json[@"access_token"]];
        [GTRUserHelper updateCurrentUser:json[@"user"]];
        
        if ([operation.response statusCode] == 201) { // CREATED
            
            [self showSurveyScreen];

            [[NSNotificationCenter defaultCenter]
             postNotificationName:REGISTER_AIRSHIP
             object:self];
            [SVProgressHUD dismiss];
            
        } else if ([operation.response statusCode] == 200) { // OK
            [self dismissViewControllerAnimated:YES completion:^{
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:REGISTER_AIRSHIP
                 object:self];
                [[GTRSideMenuHelper appDelegate] reloadData];
                [SVProgressHUD dismiss];
            }];
        }
    } onFail:^(id responseObject, NSError *error) {
        DDLogInfo(@"%@ %@", [error localizedDescription], responseObject[@"error"]);
    }];
}

-(void)showSurveyScreen
{
    GTRSurveyCoverViewController *surveyCoverViewController = [[GTRSurveyCoverViewController alloc] init];
    [self.navigationController pushViewController:surveyCoverViewController animated:YES];
}

-(void)emailButtonAction:(id)sender
{
    DDLogInfo(@"Email button clicked!");
    GTRSignupViewController *signUpViewController = [[GTRSignupViewController alloc] init];
    [self.navigationController pushViewController:signUpViewController animated:YES];
}

-(void)signInButtonAction:(id)sender
{
    GTRLoginViewController *loginViewController = [[GTRLoginViewController alloc] init];
    [self.navigationController pushViewController:loginViewController animated:YES];
}

-(void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UIScrollView delegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
    
    self.descriptionLabel.text = self.tourDescriptions[page];
    
    if (page == NUMBER_OF_PAGES - 1) {
        self.skipButton.hidden = YES;
        self.overlayView.hidden = YES;
        self.pageControl.hidden = YES;
    } else {
        self.skipButton.hidden = NO;
        self.overlayView.hidden = NO;
        self.pageControl.hidden = NO;
    }
}

@end
