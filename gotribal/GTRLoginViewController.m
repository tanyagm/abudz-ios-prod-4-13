//
//  GTRLoginViewController.m
//  gotribal
//
//  Created by Muhammad Taufik on 11/16/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRLoginViewController.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "TTTAttributedLabel.h"
#import "AFOAuth2Client.h"
#import "GTRSideMenuHelper.h"
#import "GTRUserRestService.h"
#import "GTRSessionRestService.h"
#import "Toast+UIView.h"
#import "GTRSettingsHelper.h"
#import "GTRSettingsRestService.h"

#ifdef DEBUG
//#define LOGIN_COLOR_DEBUG
#endif

@interface GTRLoginViewController () <TTTAttributedLabelDelegate, UIAlertViewDelegate>
@property (nonatomic, strong) TPKeyboardAvoidingScrollView *scrollView;

@property (nonatomic, strong) UIBarButtonItem *rightBarButtonItem;

@property (nonatomic, strong) UITextField *usernameTextField;
@property (nonatomic, strong) UITextField *passwordTextField;

@end

@implementation GTRLoginViewController

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

    if (self) {
        self.title = NSLocalizedString(@"Sign in", @"sign in");
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [GTRNavigationBarHelper setBackButtonItem:self.navigationItem withTarget:self.navigationController selector:@selector(popViewControllerAnimated:)];

    self.rightBarButtonItem = [GTRNavigationBarHelper setRightBarButtonItemWithText:NSLocalizedString(@"Done", @"done")
                                                                   onNavigationItem:self.navigationItem
                                                                         withTarget:self
                                                                           selector:@selector(doneButtonAction:)];

    
    self.scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.scrollView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0];
    self.scrollView.contentSize = self.scrollView.bounds.size;
    [self.view addSubview:self.scrollView];
    
    // Create form container
    CGFloat containerHeight = 112.0f;
    CGFloat xInset = 4.0f;
    CGFloat yOffset = 14.0f;
    CGFloat textFieldFontSize = 15;
    CGFloat textFieldYOffset = 0.0f;
    CGFloat textFieldHeight = 38.0f;
    
    CGRect containerViewFrame = CGRectMake(0, 0, self.view.bounds.size.width, containerHeight);
    containerViewFrame = CGRectInset(containerViewFrame, xInset, 0);
    
    // main container view
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectOffset(containerViewFrame, 0, yOffset)];
    containerView.backgroundColor = [UIColor whiteColor];
    containerView.layer.borderColor = [UIColor colorWithRed:195.0/255.0 green:195.0/255.0 blue:195.0/255.0 alpha:1.0].CGColor;
    containerView.layer.borderWidth = 0.5f;
    containerView.layer.cornerRadius = 3.0f;
    [self.scrollView addSubview:containerView];
    
    // inner container view
    CGFloat innerContainerViewXOffset = 21.0f;
    UIView *innerContainerView = [[UIView alloc] initWithFrame:CGRectInset(containerView.bounds, innerContainerViewXOffset, yOffset)];
    #ifdef LOGIN_COLOR_DEBUG
    innerContainerView.backgroundColor = [UIColor blueColor];
    #endif
    [containerView addSubview:innerContainerView];
    
    NSMutableArray *textFields = [NSMutableArray array];
    
    // Add username text field
    UITextField *usernameTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, innerContainerView.bounds.size.width, textFieldHeight)];
    [usernameTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    #ifdef LOGIN_COLOR_DEBUG
    usernameTextField.backgroundColor = [UIColor yellowColor];
    #endif
    usernameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    usernameTextField.clipsToBounds = NO;
    usernameTextField.keyboardType = UIKeyboardTypeEmailAddress;
    usernameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    usernameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    usernameTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    usernameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Email", @"email") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    usernameTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [innerContainerView addSubview:usernameTextField];
    
    // Assign text field's left view image
    usernameTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico-mail-signin"]];
    usernameTextField.leftViewMode = UITextFieldViewModeAlways;
    
    self.usernameTextField = usernameTextField;
    [textFields addObject:self.usernameTextField];
    
    textFieldYOffset += usernameTextField.bounds.size.height + 8;
    
    // Add password text field
    UITextField *passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, innerContainerView.bounds.size.width, textFieldHeight)];
    [passwordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    #ifdef LOGIN_COLOR_DEBUG
    passwordTextField.backgroundColor = [UIColor greenColor];
    #endif
    passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    passwordTextField.clipsToBounds = NO;
    passwordTextField.secureTextEntry = YES;
    passwordTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Password", @"password") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    passwordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [innerContainerView addSubview:passwordTextField];
    
    // Assign text field's left view image
    passwordTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico-lock-signin"]];
    passwordTextField.leftViewMode = UITextFieldViewModeAlways;
    
    self.passwordTextField = passwordTextField;
    [textFields addObject:self.passwordTextField];
    
    
    // Add hairlines separator at the bottom of text fields
    for (UITextField *textField in textFields) {
        textField.textColor = GTR_FORM_TEXT_COLOR;
        @autoreleasepool {
            UIView *bottomSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(6.0f, textField.bounds.size.height - 8.0f, textField.bounds.size.width, 0.5f)];
            bottomSeparatorView.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1.0f];
            [textField addSubview:bottomSeparatorView];
        }
    }
    
    // Add forgot password attributed label
    TTTAttributedLabel *forgotPasswordLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectOffset(containerView.frame, 0, containerView.frame.size.height + 23.0f)];
    forgotPasswordLabel.delegate = self;
    forgotPasswordLabel.backgroundColor = [UIColor clearColor];
    #ifdef LOGIN_COLOR_DEBUG
    forgotPasswordLabel.backgroundColor = [UIColor grayColor];
    #endif
    UIColor *textColor = [UIColor colorWithRed:113.0/255.0 green:113.0/255.0 blue:113.0/255.0 alpha:1.0];
    forgotPasswordLabel.textColor = textColor;
    forgotPasswordLabel.linkAttributes = @{(id)kCTForegroundColorAttributeName : textColor, (id)kCTUnderlineStyleAttributeName : [NSNumber numberWithInt:kCTUnderlinePatternDash]};
    forgotPasswordLabel.numberOfLines = 0;
    forgotPasswordLabel.font = [UIFont systemFontOfSize:15];
    NSString *forgotPasswordString = NSLocalizedString(@"Forgot Password?", @"forgot password?");
    forgotPasswordLabel.text = forgotPasswordString;
    
    NSRange forgotPasswordRange = [forgotPasswordString rangeOfString:forgotPasswordString];
    
    [forgotPasswordLabel addLinkToURL:[NSURL URLWithString:@"http://www.gotribalnow.com/content/forgot"] withRange:forgotPasswordRange];
    [forgotPasswordLabel sizeToFit];
    forgotPasswordLabel.center = CGPointMake(containerView.center.x, forgotPasswordLabel.center.y);
    
    [self.scrollView addSubview:forgotPasswordLabel];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self enableRightBarButtonItem:NO];
    
    #if DEBUG
    [self enableRightBarButtonItem:YES];
    #endif
}

#pragma mark - UITextField action

-(void)textFieldDidChange:(id)sender
{
    if (![self.usernameTextField.text length] ||
        ![self.passwordTextField.text length])
    {
        [self enableRightBarButtonItem:NO];
    } else {
        [self enableRightBarButtonItem:YES];
    }
}

#pragma mark - Custom actions

-(void)enableRightBarButtonItem:(BOOL)barButtonEnabled
{
    self.rightBarButtonItem.enabled = barButtonEnabled;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (barButtonEnabled) {
            [self.rightBarButtonItem setTintColor:[UIColor whiteColor]];
        } else {
            [self.rightBarButtonItem setTintColor:[UIColor colorWithWhite:1.0f alpha:0.50f]];
        }
    }
}

#pragma mark - TTTAttributedLabel delegate

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Forgot Password?", @"ForgotPasswordDialogTitle")
                                                        message:NSLocalizedString(@"To reset your password, please enter your email address.", @"ForgotPasswordDialogMsg")
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Reset", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView textFieldAtIndex:0].placeholder =  NSLocalizedString(@"Email Address", @"Email Address");
    [alertView show];
}

#pragma mark - alertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1){
        __weak GTRLoginViewController *weakSelf = self;
        NSString *email = [alertView textFieldAtIndex:0].text;
        [GTRUserRestService recoverPassword:email onSuccess:^(id responseObject) {
            [weakSelf.view makeToast:NSLocalizedString(@"An email with reset password instruction was sent to your email address", @"ForgotPasswordSucceed")];
        } onFail:^(id operation, NSError *error) {
            [weakSelf.view makeToast:NSLocalizedString(@"Email not found", @"ForgotPasswordFailed")];
        }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)doneButtonAction:(id)sender
{
    if (!self.usernameTextField.text) return;
    if (!self.passwordTextField.text) return;
    
    [self loginRequestWithUsername:self.usernameTextField.text andPassword:self.passwordTextField.text];
}

-(void)loginRequestWithUsername:(NSString *)username andPassword:(NSString *)password
{
    [self.view endEditing:YES];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    NSDictionary *parameters = @{
                                 @"username" : username,
                                 @"password" : password
                                };
    
    [GTRSessionRestService login:parameters onSuccess:^(id responseObject) {
        [GTRUserHelper updateCurrentUser:[responseObject valueForKey:@"user"]];
        [self dismissViewControllerAnimated:YES completion:^{
            [[NSNotificationCenter defaultCenter]
             postNotificationName:REGISTER_AIRSHIP
             object:self];
            [self reloadSidebarMenuData];
            // get setting config from server
            
            [GTRSettingsRestService retrieveSettingOnSuccess:^(id response) {
                [GTRSettingsHelper updateSettingDetail:response];
            }onFail:nil];
        }];
        [SVProgressHUD dismiss];
    } onFail:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error: %@", error);
    }];
}

-(void)reloadSidebarMenuData
{
    [[GTRSideMenuHelper appDelegate] reloadData];
}

@end
