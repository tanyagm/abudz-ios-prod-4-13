//
//  GTRMessagesViewController.h
//  gotribal
//
//  Created by Muhammad Taufik on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "JSMessagesViewController.h"
#import "GTRConversation.h"

@interface GTRMessagesViewController : JSMessagesViewController

-(id)initWithConversation:(GTRConversation *)conversation;
-(id)initWithUser:(GTRUser *)user;
-(id)initWithConversationID:(NSNumber *)conversationID;
@end
