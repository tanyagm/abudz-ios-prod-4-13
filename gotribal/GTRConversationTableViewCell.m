//
//  GTRConversationTableViewCell.m
//  gotribal
//
//  Created by Muhammad Taufik on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRConversationTableViewCell.h"
#import "GTRConversation.h"
#import "GTRUser.h"

#import "UIImageView+AFNetworking.h"
#import "GTRTimeFormatterHelper.h"

static const CGFloat kImageViewSize = 44.0f;

@interface GTRConversationTableViewCell ()
@property (nonatomic, strong) UIView *bottomSeparator;
@property (nonatomic, strong) UILabel *timestampLabel;
@property (nonatomic, strong) UIView *unreadMarker;
@end

@implementation GTRConversationTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0.949 alpha:1.000];
        
        //self.textLabel.adjustsFontSizeToFitWidth = YES;
        self.textLabel.textColor = [UIColor colorWithWhite:0.369 alpha:1.000];
        self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15];
        self.textLabel.numberOfLines = 1;
        
        self.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:13];
        self.detailTextLabel.textColor = [UIColor colorWithHue:0.000 saturation:0.000 brightness:0.478 alpha:1.000];
        self.detailTextLabel.numberOfLines = 0;
        
        self.timestampLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.timestampLabel.backgroundColor = [UIColor clearColor];
        self.timestampLabel.textAlignment = NSTextAlignmentRight;
        self.timestampLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11];
        self.timestampLabel.textColor = [UIColor colorWithHue:0.000 saturation:0.000 brightness:0.478 alpha:1.000];
        self.timestampLabel.numberOfLines = 1;
        [self addSubview:self.timestampLabel];
        
        self.unreadMarker = [[UIView alloc] initWithFrame:CGRectZero];
        self.unreadMarker.backgroundColor = [UIColor colorWithHue:0.504 saturation:1.000 brightness:0.655 alpha:1.000];
        self.unreadMarker.layer.cornerRadius = 5.0f;
        [self addSubview:self.unreadMarker];
        
        self.bottomSeparator = [[UIView alloc] initWithFrame:CGRectZero];
        self.bottomSeparator.backgroundColor = [UIColor colorWithHue:0.000 saturation:0.000 brightness:0.867 alpha:1.000];
        [self addSubview:self.bottomSeparator];
        
        self.imageView.layer.masksToBounds = YES;
        self.imageView.layer.cornerRadius = kImageViewSize/2;
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setConversation:(GTRConversation *)conversation
{
    _conversation = conversation;

    self.textLabel.text = [NSString stringWithFormat:@"%@ %@", _conversation.user.firstName, _conversation.user.lastName];
    self.detailTextLabel.text = _conversation.message;
    
    self.timestampLabel.text = [GTRTimeFormatterHelper relativeTimeStringFromEpoch:_conversation.updatedAt];
    
    [self.imageView setImageWithURL:[NSURL URLWithString:_conversation.user.avatarURL] placeholderImage:DEFAULT_AVATAR];
    
    [self setNeedsLayout];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
        
    self.imageView.frame = CGRectMake(14, 10, 44, 44);
    
    [self.textLabel sizeToFit];
    [self.detailTextLabel sizeToFit];
    [self.timestampLabel sizeToFit];
    
    CGRect textLabelFrame = self.textLabel.frame;
    self.textLabel.frame = CGRectMake(44+14+7, 12, 140, textLabelFrame.size.height);
  
    CGRect detailTextLabelFrame = CGRectOffset(self.textLabel.frame, 0.0f, 14+7);
    detailTextLabelFrame.size.width = 212.0f;
    detailTextLabelFrame.size.height = 44.0f;
    self.detailTextLabel.frame = detailTextLabelFrame;
    
    self.timestampLabel.frame = CGRectMake(self.bounds.size.width - self.timestampLabel.bounds.size.width - 10, 16, self.timestampLabel.bounds.size.width, self.timestampLabel.bounds.size.height);
    
    self.unreadMarker.frame = CGRectMake(self.bounds.size.width - 20, (self.bounds.size.height/2) - 5, 10, 10);
    if (!self.editing && _conversation.unreadState == GTRConversationStateUnread) {
        self.unreadMarker.hidden = NO;
    } else {
        self.unreadMarker.hidden = YES;
    }
    
    self.bottomSeparator.frame = CGRectMake(14, self.bounds.size.height - 0.5f, self.bounds.size.width - 14, 0.5f);
    
}

-(void)willTransitionToState:(UITableViewCellStateMask)state
{
    [super willTransitionToState:state];
    
    if ((state & UITableViewCellStateShowingEditControlMask) && (state & UITableViewCellStateShowingDeleteConfirmationMask)) {
        if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
            self.timestampLabel.hidden = YES;
        }
    } else {
        if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
            self.timestampLabel.hidden = NO;
        }
    }
}

-(void)didTransitionToState:(UITableViewCellStateMask)state
{
    [super didTransitionToState:state];
    
    if ((state & UITableViewCellStateShowingEditControlMask) && (state & UITableViewCellStateShowingDeleteConfirmationMask)) {
        
        if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
            self.timestampLabel.hidden = YES;
        }
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            CGRect frameRect = self.detailTextLabel.frame;
            frameRect.size.width -= 60;
            self.detailTextLabel.frame = frameRect;
        }
        
    } else {
        if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
            self.timestampLabel.hidden = NO;
        }
    }
}

@end
