//
//  AFManagerHelper.h
//  gotribal
//
//  Created by Le Vady on 11/30/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString *const NO_INTERNET_CONNECTION_MESSAGE;

@interface AFManagerHelper : NSObject

+(AFHTTPRequestOperationManager *)authenticatedManager;
+(void)applyReachabilityStatus;

@end
