//
//  GTRNewPostViewController.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRNewPostViewController.h"
#import "GTRUserHelper.h"
#import "GTRPostRestService.h"
#import "SVProgressHUD.h"
#import "UIImage+Resize.h"
#import <UIImageView+AFNetworking.h>
#import "Base64.h"
#import "GTRSocialHelper.h"

@import Social;
@import Accounts;

@interface GTRNewPostViewController() <UITextViewDelegate, UIActionSheetDelegate,
                        UINavigationControllerDelegate, UIImagePickerControllerDelegate, GTRImagePickerHelperDelegate>

@property (nonatomic, strong) UIButton *twitterButton;
@property (nonatomic, strong) UIButton *facebookButton;

@property (nonatomic, getter = isTwitterEnabled) BOOL twitterEnabled;
@property (nonatomic, getter = isFacebookEnabled) BOOL facebookEnabled;

@property (nonatomic, assign) BOOL createPostFinished;
@property (nonatomic, assign) BOOL twitterPostFinished;
@property (nonatomic, assign) BOOL facebookPostFinished;

@end

@implementation GTRNewPostViewController
@synthesize currentUser, postMessage, doneButton, postImageView, postImage, postMessagePlaceholder, removePostImageButton, delegate, imagePickerHelper, avatar, isOnTribe, tribe;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"New Post", @"New Post");
        [self initAttributes];
    }
    return self;
}

- (void) initAttributes
{
    imagePickerHelper = [[GTRImagePickerHelper alloc] init:self];
    imagePickerHelper.delegate = self;
    isOnTribe = NO;
}

- (void)viewDidLoad
{
    currentUser = [GTRUserHelper getCurrentUser];
    [super viewDidLoad];
    [self setAppearance];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [postMessage becomeFirstResponder];
}

#pragma mark Appearance
-(void)setAppearance
{
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) self.edgesForExtendedLayout = UIRectEdgeNone;
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) self.automaticallyAdjustsScrollViewInsets = YES;

    [self initNavBarButtons];
    [self initNewPostView];
}

-(void)initNavBarButtons
{
    doneButton = [GTRNavigationBarHelper setRightBarButtonItemWithText:NSLocalizedString(@"Done", @"done")
                                                      onNavigationItem:self.navigationItem
                                                            withTarget:self
                                                              selector:@selector(doneButtonAction:)];
    
    [GTRNavigationBarHelper setLeftBarButtonItemWithText:NSLocalizedString(@"Cancel", @"cancel")
                                        onNavigationItem:self.navigationItem
                                              withTarget:self
                                                selector:@selector(cancelButtonAction:)];
    
    [self enableDoneButtonItem:FALSE];
}

-(void)enableDoneButtonItem:(BOOL)buttonEnabled
{
    doneButton.enabled = buttonEnabled;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (buttonEnabled) {
            [doneButton setTintColor:[UIColor whiteColor]];
        } else {
            [doneButton setTintColor:[UIColor colorWithWhite:1.0f alpha:0.50f]];
        }
    }
}

-(void)initNewPostView
{
    UIView *newPostContainer = [[UIView alloc] initWithFrame:self.view.bounds];
    newPostContainer.backgroundColor = UIColorFromRGB(0xF2F2F2);
    
    [self.view addSubview: newPostContainer];
    
    avatar = [[UIImageView alloc]initWithFrame:CGRectMake(12, 20, 45, 45)];
    avatar.contentMode = UIViewContentModeScaleAspectFill;
    avatar.layer.cornerRadius = avatar.bounds.size.width/2;
    avatar.layer.masksToBounds = YES;
    avatar.backgroundColor = [UIColor colorWithWhite:1 alpha:0.000];
    [avatar setImageWithURL:[NSURL URLWithString:currentUser.avatarURL] placeholderImage:DEFAULT_AVATAR];
    
    postMessage = [[UITextView alloc] initWithFrame:CGRectMake(62, 15, 249,200)];
    postMessage.textColor = UIColorFromRGB(0x4A4A4A);
    postMessage.selectedRange = NSMakeRange(0, 0);
    postMessage.editable = TRUE;
    postMessage.delegate = self;
    postMessage.font = [UIFont systemFontOfSize:14.5];
    postMessage.backgroundColor = [UIColor colorWithWhite:1 alpha:0.000];
    
    postMessagePlaceholder = [[UITextView alloc] initWithFrame:postMessage.frame];
    postMessagePlaceholder.text = NSLocalizedString(@"How are you living a healthy, active lifestyle today?", @"postPlaceholder");
    postMessagePlaceholder.textColor = [UIColor lightGrayColor];
    postMessagePlaceholder.font = [UIFont systemFontOfSize:14.5];
    postMessagePlaceholder.editable = false;
    postMessagePlaceholder.backgroundColor = [UIColor colorWithWhite:1 alpha:0.000];
    [postMessagePlaceholder setUserInteractionEnabled:NO];
    
    postImageView = [[UIButton alloc] initWithFrame:CGRectMake(13, 73, 45, 45)];
    postImageView.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    removePostImageButton = [[UIButton alloc] initWithFrame:CGRectMake(35, 95, 44, 44)];
    removePostImageButton.imageEdgeInsets = UIEdgeInsetsMake(13,13,13,13);
    [removePostImageButton setImage:[UIImage imageNamed:@"btn-x"] forState:UIControlStateNormal];
    [removePostImageButton addTarget:self action:@selector(removePostImage:) forControlEvents:UIControlEventTouchUpInside];

    if (postImage) {
        [self showPostImage: postImage];
    } else {
        [self hidePostImage];
    }
    
    UIButton *addImageButton = [[UIButton alloc] initWithFrame:CGRectMake(3, 3, 35, 28)];
    [addImageButton setImage:[UIImage imageNamed:@"ico-camera"] forState:UIControlStateNormal];
    [addImageButton addTarget:self action:@selector(openImagePicker:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *twitterButton = [[UIButton alloc] initWithFrame:CGRectMake(236, -6, 32, 32)];
    [twitterButton setImage:[UIImage imageNamed:@"ico-twitter-off"] forState:UIControlStateNormal];
    [twitterButton addTarget:self action:@selector(toggleTwitterButton:) forControlEvents:UIControlEventTouchUpInside];
    self.twitterButton = twitterButton;
    
    UIButton *facebookButton = [[UIButton alloc] initWithFrame:CGRectMake(236 + 32 + 12, -6, 32, 32)];
    [facebookButton setImage:[UIImage imageNamed:@"ico-fb-off"] forState:UIControlStateNormal];
    [facebookButton addTarget:self action:@selector(toggleFacebookButton:) forControlEvents:UIControlEventTouchUpInside];
    self.facebookButton = facebookButton;

    UIView *keyboardAccessory = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 33)];

    [postMessage setInputAccessoryView:keyboardAccessory];
    [keyboardAccessory addSubview: addImageButton];
    [keyboardAccessory addSubview:self.twitterButton];
    [keyboardAccessory addSubview:self.facebookButton];
    
    [newPostContainer addSubview: avatar];
    [newPostContainer addSubview: postMessage];
    [newPostContainer addSubview: postImageView];
    [newPostContainer addSubview: postMessagePlaceholder];
    [newPostContainer addSubview: removePostImageButton];

    #ifdef DEBUG_COLOR
    postImageView.backgroundColor = [UIColor redColor];
    keyboardAccessory.backgroundColor = [UIColor yellowColor];
    avatar.backgroundColor = [UIColor blackColor];
    postMessage.backgroundColor = [UIColor lightGrayColor];
    #endif
}

#pragma mark TextView delegate

- (void) textViewDidChange:(UITextView *)textView
{
    postMessagePlaceholder.hidden = (textView.text.length != 0);
    [self enableDoneButtonItem:[self validateInput]];
}

#pragma mark Validation

-(BOOL) validateInput
{
    return (!![postMessage.text length]);
}

#pragma mark Prepare parameter

-(NSDictionary*) prepareParams
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:postMessage.text forKey:@"message"];
    
    if (postImage) {
        NSString *encodedImage = [UIImageJPEGRepresentation(postImage, 1.0f) base64EncodedString];
        [params setObject:encodedImage forKey:@"image_data"];
        [params setObject:@"postimage.jpeg" forKey:@"filename"];
        [params setObject:@"image/jpeg" forKey:@"content_type"];
    }
    
    return [[NSDictionary alloc] initWithDictionary:params];
}

#pragma mark Navigation bar button action

-(void)doneButtonAction:(id)sender
{
    [self.view endEditing:YES];
    
    [self createPost];
    if (self.twitterEnabled) [self postWithTwitter];
    if (self.facebookEnabled) [self postWithFacebook];
}

-(void)createPost
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    NSDictionary *params = [self prepareParams];
    
    if (!isOnTribe) {
        [GTRPostRestService createPostWithParam:params onSuccess:^(id responseObject) {
            
            __weak GTRPost *post = [MTLJSONAdapter modelOfClass:[GTRPost class] fromJSONDictionary:responseObject error:nil];
            [delegate onNewPostCreated:post];
            self.createPostFinished = YES;
            [self dismiss];
        } onFail:^(id block, NSError *error) {
            self.createPostFinished = YES;
            [self dismiss];
        }];
    } else {
        [GTRPostRestService createTribePost:tribe WithParam:params onSuccess:^(id responseObject) {
            
            __weak GTRPost *post = [MTLJSONAdapter modelOfClass:[GTRPost class] fromJSONDictionary:responseObject error:nil];
            [delegate onNewPostCreated:post];
            self.createPostFinished = YES;
            [self dismiss];
            
        } onFail:^(id responseObject, NSError *error) {
            self.createPostFinished = YES;
            [self dismiss];
        }];
    }    
}

-(void)cancelButtonAction:(id)sender
{
    [self dismissViewControllerOnCompletion:nil];
}

- (void)dismissViewControllerOnCompletion: (void (^)(void))completion
{
    [self.navigationController dismissViewControllerAnimated:YES completion:completion];
}

-(void)dismiss {
    
    if (!self.createPostFinished) return;
    
    if (self.twitterEnabled) {
        if (!self.twitterPostFinished) return;
    }
    
    if (self.facebookEnabled) {
        if (!self.facebookPostFinished) return;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        [SVProgressHUD dismiss];
    }];
}

#pragma mark Post image actions

-(void) hidePostImage
{
    postImageView.hidden = TRUE;
    removePostImageButton.hidden = TRUE;
}

-(void)showPostImage:(UIImage*) image
{
    self.postImage = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                 bounds:CGSizeMake(500,500)
                                   interpolationQuality:kCGInterpolationHigh];
    [postImageView setImage:postImage forState:UIControlStateNormal];
    postImageView.hidden = FALSE;
    removePostImageButton.hidden = FALSE;
}

-(void)removePostImage:(id)sender
{
    postImage = nil;
    [self hidePostImage];
}

#pragma mark Image picker

- (void)openImagePicker:(id)sender
{
    [postMessage resignFirstResponder];
    [imagePickerHelper openCustomActionSheet];
}

- (void)didPickImage:(UIImage*)image
{
    [postMessage becomeFirstResponder];
    if (image) {
        [self showPostImage:image];
    }
}

#pragma mark - Social button actions

-(void)toggleTwitterButton:(id)sender
{
    self.twitterEnabled = !self.twitterEnabled;
}

-(void)toggleFacebookButton:(id)sender
{
    self.facebookEnabled = !self.facebookEnabled;
}

#pragma mark - Twitter share

-(void)setTwitterEnabled:(BOOL)twitterEnabled
{
    if (_twitterEnabled != twitterEnabled) {
        
        _twitterEnabled = twitterEnabled;
        
        NSString *imageName = _twitterEnabled ? @"ico-twitter-on" : @"ico-twitter-off";
        [_twitterButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        
        if (_twitterEnabled) [self requestAccessToTwitterAccount];
    }
}

-(void)requestAccessToTwitterAccount
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    GTRSocialHelper *socialHelper = [GTRSocialHelper sharedHelper];
    [socialHelper retrieveTwitterAccountWithSuccess:^{
        self.twitterEnabled = YES;
        [SVProgressHUD dismiss];
    } failure:^(id responseObject, NSError *error) {
        self.twitterEnabled = NO;
        [SVProgressHUD dismiss];
        
        NSString *alertTitle = NSLocalizedString(@"Turn On Twitter Account in Settings to Post with Twitter", @"Turn on twitter account");
        
        if (responseObject) {
            alertTitle = [NSString stringWithFormat:@"%@ %@", @"Twitter",responseObject[@"error"]];
        }
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
    }];
}

- (void)postWithTwitter
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    GTRSocialHelper *socialHelper = [GTRSocialHelper sharedHelper];
    [socialHelper postToTwitterWithMessage:self.postMessage.text image:self.postImage success:^{
        DDLogInfo(@"Twitter post succeed.");
        self.twitterPostFinished = YES;
        [self dismiss];
    } failure:^(NSError *error) {
        DDLogInfo(@"%@", error);
        self.twitterPostFinished = YES;
        [self dismiss];
    }];
}

#pragma mark - Facebook share

-(void)setFacebookEnabled:(BOOL)facebookEnabled
{
    if (_facebookEnabled != facebookEnabled) {
        _facebookEnabled = facebookEnabled;
        NSString *imageName = _facebookEnabled ? @"ico-fb-on" : @"ico-fb-off";
        [_facebookButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        
        if (_facebookEnabled) [self requestReadWriteAccessToFacebook];
    }
}

-(void)requestReadWriteAccessToFacebook
{
    GTRSocialHelper *socialHelper = [GTRSocialHelper sharedHelper];
    [socialHelper requestReadWriteAccessToFacebookAccountWithSuccess:^{
        self.facebookEnabled = YES;
    } failure:^(NSError *error) {
        self.facebookEnabled = NO;
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Turn On Facebook Account in Settings to Post with Facebook", @"Turn on facebook account") message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
    }];
}

-(void)postWithFacebook
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    GTRSocialHelper *socialHelper = [GTRSocialHelper sharedHelper];
    [socialHelper postToFacebookWithMessage:self.postMessage.text image:self.postImage success:^{
        DDLogInfo(@"Facebook post succeed.");
        self.facebookPostFinished = YES;
        [self dismiss];
    } failure:^(NSError *error) {
        DDLogInfo(@"%@", error);
        self.facebookPostFinished = YES;
        [self dismiss];
    }];
}

@end
