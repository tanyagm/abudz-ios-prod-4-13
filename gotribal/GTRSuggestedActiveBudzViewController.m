//
//  GTRSuggestedActiveBudzViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSuggestedActiveBudzViewController.h"
#import "GTRActiveBudzCell.h"
#import "UIImageView+AFNetworking.h"
#import "AFOAuth2Client.h"
#import "GTRUser.h"
#import "GTRUserHelper.h"
#import "GTRConstants.h"
#import "SVProgressHUD.h"
#import "SVPullToRefresh.h"
#import "GTRActiveBudzProfileViewController.h"
#import "NSMutableArray+AddMantleObject.h"
#import "GTRCustomButton.h"
#import "GTRActiveBudzRestService.h"
#import "GTRActiveBudzHelper.h"
#import "GTRAnalyticsHelper.h"
#import "GTRRestServiceErrorHandler.h"

@interface GTRSuggestedActiveBudzViewController () <UITableViewDataSource, UITableViewDelegate, GTRActiveBudzProfileViewControllerDelegate>

@end

@implementation GTRSuggestedActiveBudzViewController



#pragma mark - UITableView delegates

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"ActiveBudzCell";
    GTRActiveBudzCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        cell = [[GTRActiveBudzCell alloc] initWithHeight:[super getCellHeight]
                                         isSuggestedMode:YES
                                                   style:UITableViewCellStyleDefault
                                         reuseIdentifier:cellIdentifier];
        
        //add bottom separator
        UIView *bottomSeparator = [[UIView alloc] initWithFrame:
        CGRectMake(0, [super getCellHeight]-1, self.view.bounds.size.width, 1)];
        bottomSeparator.backgroundColor = [UIColor colorWithRed:0.800 green:0.796 blue:0.812 alpha:1.000];
        [cell.contentView addSubview:bottomSeparator];
    }
    
    GTRUser *user = ((GTRUser*)[self.activeBudzArray objectAtIndex:indexPath.row]);
    
    NSString *fullname = [NSString stringWithFormat:@"%@ %@", user.firstName,  user.lastName];
    
    NSAttributedString *name = [[NSAttributedString alloc] initWithString:fullname attributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:15.0f]}];
    
    [cell.customLabel setAttributedText:name];
    cell.backgroundColor = [UIColor clearColor];
    
    //add accept and reject button to the accessory views
    cell.rejectButton.customObjectPointer = cell;
    cell.acceptButton.customObjectPointer = cell;
    
    [cell.rejectButton addTarget:self action:@selector(rejectButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.acceptButton addTarget:self action:@selector(acceptButtonAction:) forControlEvents:UIControlEventTouchUpInside];
 
    //add image to cell
    [cell.customImageView setImageWithURL:[NSURL URLWithString:user.avatarURL] placeholderImage:DEFAULT_AVATAR];

    return cell;
}

- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    //push the respective user profile
    
    GTRUser *user = ((GTRUser*)[self.activeBudzArray objectAtIndex:indexPath.row]);
    
    [GTRActiveBudzHelper pushActiveBudzProfile:user
                                     indexPath:indexPath
                            rootViewController:self onSuccess:^(id responseObject) {
    } onFail:^(id responseObject, NSError *error) {}];
    
    //de-select the cell image
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - Cell button actions

-(void)acceptButtonAction:(id)sender
{
    GTRActiveBudzCell *cell = (GTRActiveBudzCell*)((GTRCustomButton*)sender).customObjectPointer;
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    GTRUser *user = ((GTRUser*)[self.activeBudzArray objectAtIndex:indexPath.row]);
    
    DDLogInfo(@"Index Path : %@, User : %@",indexPath,user);
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    [GTRActiveBudzRestService addActiveBudzRequestWithUser:user onSuccess:^(id responseObject) {
        
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Request sent!", @"request sent")];

        [self removePersonFromList:indexPath];
        
        //send analytics data
        #ifdef GTR_FLURRY_ANALYTICS
        [GTRAnalyticsHelper sendActiveBudzRequestToConnectEvent];
        #endif
        
    } onFail:^(id responseObject, NSError *error) {
    }];
}

-(void)rejectButtonAction:(id)sender
{
    GTRActiveBudzCell *cell = (GTRActiveBudzCell*)((GTRCustomButton*)sender).customObjectPointer;
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    GTRUser *user = ((GTRUser*)[self.activeBudzArray objectAtIndex:indexPath.row]);
    
    DDLogInfo(@"Index Path : %@, User : %@",indexPath,user);
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    [GTRActiveBudzRestService removeActiveBudzWithUser:user onSuccess:^(id responseObject) {
        
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Suggestion removed.", @"suggestion removed")];

        [self removePersonFromList:indexPath];
        
    } onFail:^(id responseObject, NSError *error) {
    }];
}


#pragma mark - Overriden other superclass methods

- (void)initiateActiveBudzArrayWithCompletion:(void (^)(void))completion
{
    if (!completion) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    }

    self.activeBudzArray = [[NSMutableArray alloc] initWithArray:[GTRActiveBudzHelper getArchivedSuggestedActiveBudz]];
    [self.tableView reloadData];
    
    [GTRActiveBudzRestService retrieveSuggestedActiveBudz:^(id responseObject) {
        if (self.activeBudzArray.count > 0) {
            [self.activeBudzArray removeAllObjects];
        } else {
            self.activeBudzArray = [[NSMutableArray alloc] init];
        }

        NSArray *users = ((NSArray*)responseObject);

        [self.activeBudzArray addObjectsFromArray:users modelOfClass:[GTRUser class]];
        [GTRActiveBudzHelper archiveSuggestedActiveBudz:[self.activeBudzArray copy]];
        [self.tableView reloadData];

        if (completion) {
            completion();
        } else {
            [SVProgressHUD dismiss];
        }
    } onFail:^(id responseObject, NSError *error) {
        if (completion) {
            completion();
        }
    }];
}

-(void)retrieveNextActiveBudzListCompletion:(void (^) (void))completion
{
    //TODO : Remove this code (until the return) in Beta release.
    if (completion) {
        completion();
    }
    return;
    
    //offset starts at 0, get as much as NUMBER_OF_ACTIVEBUDZ_DATA_RETRIEVED
    [GTRActiveBudzRestService retrieveSuggestedActiveBudzWithStartPoint:self.activeBudzArray.count onSuccess:^(id responseObject) {
        
        NSInteger count = self.activeBudzArray.count;
        
        [self.activeBudzArray addObjectsFromArray:(NSArray*)responseObject modelOfClass:[GTRUser class]];
        [GTRActiveBudzHelper archiveSuggestedActiveBudz:[self.activeBudzArray copy]];
        if (count < self.activeBudzArray.count) {
            [self.tableView reloadData];
        }
        
        if (completion) {
            completion();
        }
        
    } onFail:^(id responseObject, NSError *error) {
        if (completion) {
            completion();
        }
    }];
}

#pragma mark - GTRActiveBudzProfileViewController delegates
-(void)onRequestSent:(NSIndexPath*)indexPath
{
    [self removePersonFromList:indexPath];
}

@end
