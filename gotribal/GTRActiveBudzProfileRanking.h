//
//  GTRActiveBudzProfileRanking.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/9/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    ActiveBudzProfileRankOne = 0,
    ActiveBudzProfileRankTwo,
    ActiveBudzProfileRankThree
}ActiveBudzProfileRank;

@interface GTRActiveBudzProfileRanking : UIView

-(id)initWithFrame:(CGRect)frame
       profileRank:(ActiveBudzProfileRank)theRank;

-(void)setProfileRank:(ActiveBudzProfileRank)theRank;
-(void)resetAppearance;
@end
