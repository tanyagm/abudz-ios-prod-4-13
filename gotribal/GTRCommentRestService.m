//
//  GTRCommentRestService.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/12/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRCommentRestService.h"
#import "GTRPaginationHelper.h"
#import "GTRRestServiceErrorHandler.h"

@implementation GTRCommentRestService

+(void)retrieveCommentsFromPost:(GTRPost*) post
                      withMaxID:(NSNumber*) maxID
                      onSuccess:(void (^)(id))success
                         onFail:(void (^)(id, NSError *))errorHandler
{
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    NSString *endpoint = [GTRPaginationHelper paginate:GET_COMMENTS_ENDPOINT(post.postID) withLimit:[NSNumber numberWithInt:MAX_PER_PAGE] andMaxID:maxID];
    
    [manager GET:endpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"response: %@", responseObject);
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"error: %@", error);
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
    
}

+(void)createCommentForPost:(GTRPost*) post
                WithMessage:(NSString*) message
                  onSuccess:(void (^)(id))success
                     onFail:(void (^)(id, NSError *))errorHandler
{
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    NSString *endpoint = CREATE_COMMENTS_ENDPOINT(post.postID);
    NSDictionary *params = @{@"message":message};
    
    [manager POST:endpoint parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"response: %@", responseObject);
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"error: %@", error);
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
    
}


@end
