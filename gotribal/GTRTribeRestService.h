//
//  GTRTribeRestService.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/11/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRTribe.h"

@interface GTRTribeRestService : NSObject

+(void)retrieveAllTribeWithMaxID:(NSNumber*) maxID OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

+(void)retrieveJoinedTribeWithMaxID:(NSNumber*) maxID OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

+(void)retrieveOwnedTribeWithMaxID:(NSNumber*) maxID OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

+(void)postNewTribeWithPayload:(NSDictionary*)payload onSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

+(void)retrieveTribeMembers:(GTRTribe*)theTribe MaxID:(NSNumber*) maxID OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

+(void)retrieveInviteableTribeMembers:(GTRTribe*)theTribe MaxID:(NSNumber*)maxID OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

+(void)retrieveTribePosts:(GTRTribe*)theTribe MaxID:(NSNumber*) maxID OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

+(void)joinTribe:(GTRTribe*)theTribe OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

+(void)leaveTribe:(GTRTribe*)theTribe OnSuccess:(void (^)(id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;

+(void)searchTribeWithKeyword:(NSString*)keyword
                    onSuccess:(void (^)(id responseObject))success
                       onFail:(void(^)(id responseObject, NSError *error))errorHandler;

+(void)inviteToTribe:(GTRTribe*)theTribe
          parameters:(NSDictionary*) theParameters
           onSuccess:(void (^)(id responseObject))success
              onFail:(void(^)(id responseObject, NSError *error))errorHandler;
@end
