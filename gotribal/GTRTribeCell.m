//
//  GTRMyTribeCell.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/11/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRTribeCell.h"
#import <UIImageView+AFNetworking.h>
#import "GTRUser.h"
#import "GTRConstants.h"
#import "GTRTribeMemberCollectionViewCell.h"
#import "GTRImagePickerHelper.h"

NSString* const TRIBE_OWNER_AVATAR_CELL_IMAGE_NAME = @"ico-camera-signup";
NSString* const DESCRIPTION_STRING_PLACEHOLDER = @"No description available.";
NSString* const JOIN_BUTTON_TEXT = @"JOIN";

@interface GTRTribeCell() <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong,nonatomic) GTRTribe *tribe;

@property (strong,nonatomic) UIImageView *ownerAvatar;
@property (strong,nonatomic) UILabel *tribeNameLabel;
@property (strong,nonatomic) UILabel *tribeActivityLabel;
@property (strong,nonatomic) UILabel *tribeDescriptionLabel;
@property (strong,nonatomic) UIImageView *tribeImageView;
@property (strong,nonatomic) UICollectionView *tribeMemberView;
@property (strong,nonatomic) UIView *customContentView;
@property (strong,nonatomic) UIButton *joinButton;

@end


@implementation GTRTribeCell
@synthesize tribe, ownerAvatar, tribeNameLabel, tribeActivityLabel, tribeDescriptionLabel, tribeImageView, tribeMemberView, customContentView, joinButton;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setAppearance];
    }
    return self;
}

#pragma mark - Initial appearance setter

-(void)setAppearance
{
    self.backgroundColor = [UIColor clearColor];
    
    //prepare the custom content view
    CGFloat customContentViewHorizontalPadding = 5;
    CGFloat customContentViewY = 10;
    CGFloat customContentViewWidth = self.bounds.size.width - (customContentViewHorizontalPadding*2);
    CGFloat customContentViewHeight = self.bounds.size.height - 10;
    
    customContentView = [[UIView alloc] initWithFrame:CGRectMake(customContentViewHorizontalPadding, customContentViewY, customContentViewWidth, customContentViewHeight)];
    
    customContentView.backgroundColor = [UIColor whiteColor];
    customContentView.layer.cornerRadius = 4;
    customContentView.layer.borderColor = UIColorFromRGB(0xBCBCBC).CGColor;
    customContentView.layer.borderWidth = 0.3;
    [self.contentView addSubview:customContentView];
    
}

#pragma mark - Content setter

-(void)setContent:(GTRTribe *)theTribe
{
    [self refreshCustomContentView];
    
    tribe = theTribe;
    
    //set owner's avatar
    CGFloat ownerAvatarX = 7;
    CGFloat ownerAvatarY = 9;
    CGFloat ownerAvatarHeight = 45;
    CGFloat ownerAvatarWidth = 45;
    
    ownerAvatar = [[UIImageView alloc] initWithFrame:CGRectMake(ownerAvatarX, ownerAvatarY, ownerAvatarWidth, ownerAvatarHeight)];
    
    [ownerAvatar setImageWithURL:[NSURL URLWithString:tribe.tribeOwner.avatarURL]
                placeholderImage:DEFAULT_AVATAR];
    ownerAvatar.layer.cornerRadius = ownerAvatarWidth/2;
    ownerAvatar.layer.masksToBounds = YES;
    
    [customContentView addSubview:ownerAvatar];
    
    //set tribe name label
    CGFloat tribeNameLabelX = ownerAvatarX + ownerAvatarWidth + 10;
    CGFloat tribeNameLabelY = ownerAvatarY + 5;
    CGFloat tribeNameLabelWidth = customContentView.bounds.size.width - tribeNameLabelX;
    CGFloat tribeNameLabelHeight = 20;
    
    NSAttributedString *tribeNameLabelString = [[NSAttributedString alloc] initWithString:tribe.tribeName
                                                                               attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold"
                                                                                                                                  size:16]}];
    
    tribeNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(tribeNameLabelX, tribeNameLabelY, tribeNameLabelWidth, tribeNameLabelHeight)];
    tribeNameLabel.attributedText = tribeNameLabelString;
    tribeNameLabel.textColor = CELL_TEXT_FONT_COLOR;
    tribeNameLabel.backgroundColor = [UIColor clearColor];
    
    [customContentView addSubview:tribeNameLabel];
    
    //set tribe activity label
    CGFloat tribeActivityLabelX = tribeNameLabelX;
    CGFloat tribeActivityLabelY = tribeNameLabelY + tribeNameLabelHeight;
    CGFloat tribeActivityLabelWidth = customContentView.bounds.size.width - tribeActivityLabelX;
    CGFloat tribeActivityLabelHeight = 17;
    
    tribeActivityLabel = [[UILabel alloc] initWithFrame:CGRectMake(tribeActivityLabelX, tribeActivityLabelY, tribeActivityLabelWidth, tribeActivityLabelHeight)];
    tribeActivityLabel.text = tribe.tribeActivity;
    tribeActivityLabel.font = TRIBE_ACTIVITY_CHOSEN_FONT;
    tribeActivityLabel.textColor = CELL_SUBTEXT_FONT_COLOR;
    tribeActivityLabel.backgroundColor = [UIColor clearColor];
    tribeActivityLabel.numberOfLines = 0;
    [customContentView addSubview:tribeActivityLabel];
    
    //set tribe description label
    CGFloat tribeDescriptionLabelX = tribeActivityLabelX;
    CGFloat tribeDescriptionLabelY = tribeActivityLabelY + tribeActivityLabelHeight + 7;
    CGFloat tribeDescriptionLabelWidth = customContentView.bounds.size.width - tribeDescriptionLabelX;
    CGFloat tribeDescriptionLabelHeight = 20;
    
    NSString *descriptionString = DESCRIPTION_STRING_PLACEHOLDER;
    
    if (tribe.tribeDescription) {
        descriptionString = tribe.tribeDescription;
    }
    
    NSAttributedString *tribeDescriptionLabelString = [[NSAttributedString alloc] initWithString:descriptionString
                                                                                   attributes:@{NSFontAttributeName : CELL_FONT}];
    
    tribeDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(tribeDescriptionLabelX, tribeDescriptionLabelY, tribeDescriptionLabelWidth, tribeDescriptionLabelHeight)];
    tribeDescriptionLabel.attributedText = tribeDescriptionLabelString;
    tribeDescriptionLabel.textColor = CELL_SUBTEXT_FONT_COLOR;
    tribeDescriptionLabel.backgroundColor = [UIColor clearColor];
    tribeDescriptionLabel.numberOfLines = 0;
    [tribeDescriptionLabel sizeToFit];
    [customContentView addSubview:tribeDescriptionLabel];
    
    //set tribe image
    CGFloat tribeImageX = 0;
    CGFloat tribeImageY = tribeDescriptionLabelY + tribeDescriptionLabel.frame.size.height + 5;
    CGFloat tribeImageWidth = customContentView.bounds.size.width;
    CGFloat tribeImageHeight = 215;
    
    tribeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(tribeImageX, tribeImageY, tribeImageWidth, tribeImageHeight)];
    tribeImageView.contentMode = UIViewContentModeScaleToFill;
    
    if  (tribe.tribeImageURL) {
        [GTRImagePickerHelper setAndAdjustOrientationImageView:tribeImageView
                                                       withURL:[NSURL URLWithString:tribe.tribeImageURL]
                                                andPlaceholder:DEFAULT_ACTIVITY_PLACEHOLDER];
        
        tribeImageView.clipsToBounds = YES;
    } else {
        [tribeImageView setImage:DEFAULT_ACTIVITY_PLACEHOLDER];
    }
    
    tribeImageView.backgroundColor = self.backgroundColor;
    tribeImageView.clipsToBounds = YES;
    
    [customContentView addSubview:tribeImageView];
    
    CGFloat joinButtonHeight = 28;
    CGFloat joinButtonWidth = 61;
    CGFloat joinButtonX = customContentView.bounds.size.width - joinButtonWidth - 5;
    CGFloat joinButtonY = tribeImageY + tribeImageHeight + 8;
    
    //if tribe is not joined
    if (!tribe.isJoined) {
        
        NSAttributedString *joinButtonText = [[NSAttributedString alloc] initWithString:JOIN_BUTTON_TEXT attributes:@{
                                                                                                                                 NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16.0f],
                                                                                                                                 NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                                                                                 }];
        
        joinButton = [[UIButton alloc] initWithFrame:CGRectMake(joinButtonX, joinButtonY, joinButtonWidth, joinButtonHeight)];
        [joinButton setAttributedTitle:joinButtonText forState:UIControlStateNormal];
        joinButton.backgroundColor = [UIColor colorWithRed:0.114 green:0.525 blue:0.541 alpha:1.000];
        joinButton.layer.cornerRadius = 5.0f;
        [joinButton addTarget:self action:@selector(joinButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [customContentView addSubview:joinButton];
    }
    
    //set tribe member view
    CGFloat tribeMemberViewWidth = tribe.isJoined ? customContentView.bounds.size.width : joinButtonX - 5;
    CGFloat tribeMemberViewHeight = 44;
    CGFloat tribeMemberViewX = 0;
    CGFloat tribeMemberViewY = tribeImageY + tribeImageHeight;
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(38, 38);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumInteritemSpacing = 0.0f;
    
    tribeMemberView = [[UICollectionView alloc] initWithFrame:CGRectMake(tribeMemberViewX, tribeMemberViewY, tribeMemberViewWidth, tribeMemberViewHeight) collectionViewLayout:flowLayout];
    tribeMemberView.backgroundColor = [UIColor clearColor];
    tribeMemberView.bounces = YES;
    tribeMemberView.showsHorizontalScrollIndicator = YES;
    tribeMemberView.showsVerticalScrollIndicator = NO;
    tribeMemberView.contentInset = UIEdgeInsetsMake(0, 10, 0, 0);
    tribeMemberView.delegate = self;
    tribeMemberView.dataSource = self;
    tribeMemberView.clipsToBounds = YES;
    
    [tribeMemberView registerClass:[GTRTribeMemberCollectionViewCell class] forCellWithReuseIdentifier:@"tribeMemberCell"];
    
    [customContentView addSubview:tribeMemberView];
    
    //resize cell's frame
    CGRect bufferFrame = self.frame;
    bufferFrame.size.height = tribeMemberViewY + tribeMemberViewHeight + 10;
    self.frame = bufferFrame;
    
    //resize customContentView's bounds
    bufferFrame = customContentView.frame;
    bufferFrame.size.height = tribeMemberViewY + tribeMemberViewHeight;
    customContentView.frame = bufferFrame;
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(-10, 0, 310, 0.5)];
    lineView.backgroundColor = UIColorFromRGB(0xDADADA);
    [tribeMemberView addSubview:lineView];
}

#pragma mark customContentView refresher

-(void)refreshCustomContentView
{
    for (UIView *subView in customContentView.subviews) {
        [subView removeFromSuperview];
    }
}

#pragma mark - Join Button Action

-(void)joinButtonAction:(id)sender
{
    [UIView animateWithDuration:0.4 animations:^{
        [joinButton setTransform:CGAffineTransformMakeScale(0.0, 0.0)];
        joinButton.alpha = 0.0;
        if ([self.delegate respondsToSelector:@selector(didClickJoin:inCell:)]) {
            [self.delegate didClickJoin:tribe inCell:self];
        }
    } completion:nil];
}

#pragma mark - UICollectionView delegates

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return tribe.tribeMembers.count;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    GTRTribeMemberCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"tribeMemberCell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[GTRTribeMemberCollectionViewCell alloc] initWithFrame:CGRectMake(0, 0, 38, 38)];
    }
    
    GTRUser *member = [tribe.tribeMembers objectAtIndex:indexPath.row];
    [cell setContent:member];
    
    return cell;
}


@end
