//
//  NSMutableArray+AddMantleObject.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/3/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (AddMantleObject)

-(void)addObjectsFromArray:(NSArray *)otherArray modelOfClass:(Class)modelClass;

@end
