//
//  GTRPostHelper.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTRPostHelper : NSObject
+(NSMutableArray *)getArchivedActivityStreams;
+(void)archiveActivityStreams:(NSMutableArray *)activityStreams;
@end
