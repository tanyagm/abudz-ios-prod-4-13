//
//  GTRSettingsViewController.m
//  gotribal
//
//  Created by Hisma Mulya S on 11/29/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSettingsViewController.h"
#import "GTRSettingsCell.h"
#import "GTRSettingsHelper.h"
#import "GTRMainActivitiesViewController.h"
#import "GTRIntroViewController.h"
#import "SVProgressHUD.h"
#import "GTREditFitnessProfileViewController.h"
#import "GTRSessionRestService.h"
#import "GTRNavigationBarHelper.h"
#import "FacebookSDK.h"
#import "GTRSocialHelper.h"
#import "GTRProfileViewController.h"
#import "GTRSettingsRestService.h"
#import "GTRRestServiceErrorHandler.h"
#import "GTRAppDelegate.h"

NSInteger const CELL_HEIGHT = 45;
NSInteger const LOGOUT_BUTTON_TAG = 1313;
NSInteger const CELL_VIEW_TAG = 2121;

// static cell sections
#define VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
NSString* const MY_PROFILE          = @"My Profile";
NSString* const TERMS_OF_SERVICE    = @"Terms of Service";
NSString* const PRIVACY_POLICY      = @"Privacy Policy";
NSString* const REPORT_A_PROBLEM    = @"Have feedback for us?";
NSString* const ABOUT_US            = @"About us";
NSString* const LOGOUT              = @"Logout";
NSString* const ABOUT_THIS_VERSION  = @"About this version";

// header cell
NSString* const ACCOUNT_HEADER          = @"Account";
NSString* const SUPPORT                 = @"Support";
NSString* const NOTIFICATIONS           = @"Notifications";
NSString* const NOTIFICATIONS_FREQUENCY = @"Notifications Frequency";
NSString* const SOCIAL                  = @"Social";

// rows
NSString* const NEW_ACTIVEBUDZ_REQUEST      = @"New Activebudz Request";
NSString* const NEW_SUGGESTED_ACTIVEBUDZ    = @"New Suggested Activebudz";
NSString* const NEW_COMMENTS                = @"New Comments";
NSString* const NEW_MESSAGE                 = @"New Message";
NSString* const NEW_POST_IN_A_TRIBE         = @"New Post In a Tribe";
NSString* const INVITED_TO_A_NEW_TRIBE      = @"Invited To a New Tribe";

NSString* const TWITTER                     = @"Twitter";
NSString* const FACEBOOK                    = @"Facebook";


//feedback
NSString* const FEEDBACK_SUBJECT = @"Feedback";
NSString* const FEEDBACK_MESSAGE_BODY = @"";
NSString* const FEEDBACK_TO_RECIPIENT = @"info@gotribalnow.com";

//Link to gotribal website

NSString* const TERMS_OF_SERVICE_LINK = @"http://www.gotribalnow.com/content/terms-use";
NSString* const PRIVACY_POLICY_LINK = @"http://www.gotribalnow.com/privacy-policy";
NSString* const ABOUT_US_LINK = @"http://www.gotribalnow.com/content/about-us";

#define SETTINGS_FONT [UIFont fontWithName:@"HelveticaNeue-Light" size:13];

@interface UIBarButtonItem (NegativeSpacer)

+(UIBarButtonItem*)negativeSpacerWithWidth:(NSInteger)width;

@end

@implementation UIBarButtonItem (NegativeSpacer)

+(UIBarButtonItem*)negativeSpacerWithWidth:(NSInteger)width
{
    UIBarButtonItem *item = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                             target:nil
                             action:nil];
    item.width = (width >= 0 ? -width : width);
    
    return item;
}

@end

@interface GTRSettingsViewController ()

@property(nonatomic,strong) UITableView *table;
@property (strong,nonatomic) NSMutableDictionary* settingDetail;
@property (strong, nonatomic) NSArray* typeSetting;
@property (strong, nonatomic) NSArray* freqSetting;

@end

@implementation GTRSettingsViewController
@synthesize table = _table;
@synthesize settingDetail, typeSetting, freqSetting;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Settings";
    self.view.backgroundColor = VIEW_BACKGROUND_COLOR;
    
    CGRect tableFrame = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? CGRectMake(0, 0,320, self.view.bounds.size.height - 64): CGRectMake(0, 0, 320, self.view.bounds.size.height - 20);
    
    _table = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStyleGrouped];
    _table.backgroundView = nil;
    _table.backgroundColor = TABLEVIEW_BACKGROUND_COLOR;
    _table.separatorStyle = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? UITableViewCellSeparatorStyleNone : UITableViewCellSeparatorStyleSingleLine;
    _table.delegate = self;
    _table.dataSource = self;
    [self.view addSubview:_table];
    
    [GTRNavigationBarHelper setLeftSideBarButtonItem:self.navigationItem
                                          withTarget:self
                                            selector:@selector(toggleSideMenu:)];
    
    typeSetting = @[REQUEST_TYPE_KEY,
                    SUGGESTED_TYPE_KEY,
                    COMMENT_TYPE_KEY,
                    MESSAGE_TYPE_KEY,
                    TRIBE_POST_TYPE_KEY,
                    TRIBE_INVITATION_TYPE_KEY
                    ];
    
    freqSetting = @[REQUEST_FREQ_KEY,
                    SUGGESTED_FREQ_KEY,
                    COMMENT_FREQ_KEY,
                    MESSAGE_FREQ_KEY,
                    TRIBE_POST_FREQ_KEY,
                    TRIBE_INVITATION_FREQ_KEY
                    ];
    
    settingDetail = [GTRSettingsHelper getSettingDetail];
    
}

#pragma mark - SideMenu notification handler

-(void)sideMenuStateEventChangedTo:(MFSideMenuStateEvent)event
{
    if (event == MFSideMenuStateEventMenuWillOpen) {
        [self saveSettingAndLogout:NO];
    }
}
-(void)setSettingDetailCellMode:(GTRSettingsCell*)theCell view:(GTRSettingsCustomRightView*)rightView index:(NSInteger)i
{

    //setup notification setting
    NSArray *modeArray = settingDetail[typeSetting[i]];
    
    if(modeArray){
        NSLog(@"%@ %@",typeSetting[i], modeArray);
        if([modeArray[0] isEqualToString:@"none"]) {
            [self customRightView:rightView notificationsMode:3 inCell:theCell];
        } else if(modeArray.count==2) {
            [self customRightView:rightView notificationsMode:2 inCell:theCell];
        } else if ([modeArray[0] isEqualToString:@"phone"]) {
            [self customRightView:rightView notificationsMode:0 inCell:theCell];
        } else if ([modeArray[0] isEqualToString:@"email"]){
            [self customRightView:rightView notificationsMode:1 inCell:theCell];
        } else {
            [self customRightView:rightView notificationsMode:3 inCell:theCell];
        }
    }
}

-(void)setSettingDetailCellFreq:(GTRSettingsCell*)theCell view:(GTRSettingsCustomRightView*)rightView index:(NSInteger)i
{
    //setup frequency setting
    NSString *freqString = settingDetail[freqSetting[i]];
    NSLog(@"%@ %@",freqSetting[i], freqString);
    if([freqString isKindOfClass:[NSNull class]]){
        [self customRightView:rightView notificationsFrequency:3 inCell:theCell];
    } else {
        if([freqString isEqualToString:@"immediate"]) {
            [self customRightView:rightView notificationsFrequency:0 inCell:theCell];
        } else if ([freqString isEqualToString:@"daily"]) {
            [self customRightView:rightView notificationsFrequency:1 inCell:theCell];
        } else if ([freqString isEqualToString:@"weekly"]){
            [self customRightView:rightView notificationsFrequency:2 inCell:theCell];
        } else {
            [self customRightView:rightView notificationsFrequency:3 inCell:theCell];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source and delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case GTRSettingsSectionAccount:
            return 1;
        case GTRSettingsSectionNotifications:
            return 6;
        case GTRSettingsSectionNotifificationsFrequency:
            return 6;
        case GTRSettingsSectionSocial:
            return 2;
        case GTRSettingsSectionSupport:
            return 5;
        case GTRSettingsSectionLogout:
            return 1;
    }
    
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case GTRSettingsSectionAccount:
            return [GTRSettingsHelper createHeaderViewWithText:ACCOUNT_HEADER];
        case GTRSettingsSectionNotifications:
            return [GTRSettingsHelper createHeaderViewWithText:NOTIFICATIONS];
        case GTRSettingsSectionNotifificationsFrequency:
            return [GTRSettingsHelper createHeaderViewWithText:NOTIFICATIONS_FREQUENCY];
        case GTRSettingsSectionSocial:
            return [GTRSettingsHelper createHeaderViewWithText:SOCIAL];
        case GTRSettingsSectionSupport:
            return [GTRSettingsHelper createHeaderViewWithText:SUPPORT];
        case GTRSettingsSectionLogout:
            return nil;
    }
    
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == GTRSettingsSectionLogout) {
        return 0.0;
    } else {
        return CELL_HEIGHT;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NormalCell";
    GTRSettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    CGFloat leftOffset = -10;
    if (!cell) {
        cell = [[GTRSettingsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
            cell.text.frame = CGRectOffset(cell.text.frame, leftOffset, 0);
        }
    }
    
    static NSString *logoutCellIdentifier = @"LogoutCell";
    
    GTRSettingsCell *settingsCell = [tableView dequeueReusableCellWithIdentifier:logoutCellIdentifier];
    if (!settingsCell) {
        settingsCell = [[GTRSettingsCell alloc]initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier: logoutCellIdentifier];
        
        settingsCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
            settingsCell.text.frame = CGRectOffset(settingsCell.text.frame, leftOffset, 0);
        }
    }
    
    // cell for Notifications section
    static NSString *notificationsIdentifier = @"NotificationsCell";
    GTRSettingsCell *notificationsCell = [tableView dequeueReusableCellWithIdentifier:notificationsIdentifier];
    if (!notificationsCell) {
        notificationsCell = [[GTRSettingsCell alloc]initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier: notificationsIdentifier];
        
        notificationsCell.selectionStyle = UITableViewCellSelectionStyleNone;
        notificationsCell.accessoryType = UITableViewCellAccessoryNone;
        
        if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
            notificationsCell.text.frame = CGRectOffset(notificationsCell.text.frame, leftOffset, 0);
        }
    }
    
    // cell for Notifications Frequency section
    static NSString *notificationsFrequencyIdentifier = @"NotificationsFrequencyCell";
    GTRSettingsCell *notificationsFrequencyCell = [tableView dequeueReusableCellWithIdentifier:notificationsFrequencyIdentifier];
    if (!notificationsFrequencyCell) {
        notificationsFrequencyCell = [[GTRSettingsCell alloc]initWithStyle:UITableViewCellStyleDefault
                                                  reuseIdentifier:notificationsFrequencyIdentifier ];
        
        notificationsFrequencyCell.selectionStyle = UITableViewCellSelectionStyleNone;
        notificationsFrequencyCell.accessoryType = UITableViewCellAccessoryNone;
        
        if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
            notificationsFrequencyCell.text.frame = CGRectOffset(notificationsFrequencyCell.text.frame, leftOffset, 0);
        }
    }
    
    // cell for Social
    static NSString *socialCellIdentifier = @"NotificationsFrequencyCell";
    GTRSettingsCell *socialCell = [tableView dequeueReusableCellWithIdentifier:socialCellIdentifier];
    if (!socialCell) {
        socialCell = [[GTRSettingsCell alloc]initWithStyle:UITableViewCellStyleDefault
                                                           reuseIdentifier:socialCellIdentifier ];
        
        socialCell.selectionStyle = UITableViewCellSelectionStyleNone;
        socialCell.accessoryType = UITableViewCellAccessoryNone;
        
        if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
            socialCell.text.frame = CGRectOffset(socialCell.text.frame, leftOffset, 0);
        }
    }
    
    cell.text.text = @"";
    
    switch (indexPath.section) {
        case GTRSettingsSectionAccount:
            return [self accountCell:cell forRowAtIndex:indexPath.row];
        case GTRSettingsSectionNotifications:
            return [self notificationsCell:notificationsCell forRowAtIndex:indexPath.row];
        case GTRSettingsSectionNotifificationsFrequency:
            return [self notificationsFrequencyCell:notificationsFrequencyCell forRowAtIndex:indexPath.row];
        case GTRSettingsSectionSocial:
            return [self socialCell:socialCell forRowAtIndex:indexPath.row];
        case GTRSettingsSectionSupport:
            return [self supportCell:cell forRowAtIndex:indexPath.row];
        case GTRSettingsSectionLogout:
            return [self logoutCell:settingsCell forRowAtIndex:indexPath.row];
            
            return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case GTRSettingsSectionAccount:
            if (indexPath.row == 0) {
                [self showEditProfile:YES];
            }
            break;
        case GTRSettingsSectionSupport:
            if (indexPath.row == 0){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:TERMS_OF_SERVICE_LINK]];
            }
            else if (indexPath.row == 1){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:PRIVACY_POLICY_LINK]];
            }
            else if (indexPath.row == 2){
                [self showFeedback:nil];
            }
            else if (indexPath.row == 4){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ABOUT_US_LINK]];
            }
            break;
        case GTRSettingsSectionLogout:
            if (indexPath.row == 0){
                [self logoutButtonAction:nil];
            }
            
            break;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (tableView == self.table) {
            if (indexPath.section < GTRSettingsSectionLogout) {
                cell.backgroundView = [GTRSettingsHelper createGroupedViewInCell:(GTRSettingsCell*)cell
                                                                      atIndexPath:indexPath];
            }
        }
    }
}

#pragma mark - Cells

-(GTRSettingsCell*)accountCell:(GTRSettingsCell*)cell forRowAtIndex:(NSInteger)rowIndex
{
    cell.indexpath = [NSIndexPath indexPathForRow:rowIndex inSection:GTRSettingsSectionAccount];
    
    switch (rowIndex) {
        case 0:
            cell.text.text = MY_PROFILE;
            break;
    }
    
    return cell;
}

-(GTRSettingsCell*)notificationsCell:(GTRSettingsCell*)cell forRowAtIndex:(NSInteger)rowIndex
{
    NSInteger tag = CELL_VIEW_TAG;
    cell.indexpath = [NSIndexPath indexPathForRow:rowIndex inSection:GTRSettingsSectionNotifications];
    
    UIView *viewFetched = [cell.contentView viewWithTag:tag];
    [viewFetched removeFromSuperview];
    viewFetched = nil;
    
    CGFloat paddingRight = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? 102 : 118;
    CGRect notificationFrame = CGRectMake(cell.frame.size.width - paddingRight, 1, 95, cell.frame.size.height - 2);
    
    GTRSettingsCustomRightView *view = [[GTRSettingsCustomRightView alloc]initInCell:cell
                                                           frame:notificationFrame withDelegate:self];
    [self setSettingDetailCellMode:cell view:view index:rowIndex];
    [self applyRightViewStatus:view inCell:cell];
    view.tag = tag;
    [cell.contentView addSubview:view];

    switch (rowIndex) {
        case 0:
            cell.text.text = NEW_ACTIVEBUDZ_REQUEST;
            break;
        case 1:
            cell.text.text = NEW_SUGGESTED_ACTIVEBUDZ;
            break;
        case 2:
            cell.text.text = NEW_COMMENTS;
            break;
        case 3:
            cell.text.text = NEW_MESSAGE;
            break;
        case 4:
            cell.text.text = NEW_POST_IN_A_TRIBE;
            break;
        case 5:
            cell.text.text = INVITED_TO_A_NEW_TRIBE;
            break;
    }
    
    return cell;
}

-(GTRSettingsCell*)notificationsFrequencyCell:(GTRSettingsCell*)cell forRowAtIndex:(NSInteger)rowIndex
{
    NSInteger tag = CELL_VIEW_TAG;
    cell.indexpath = [NSIndexPath indexPathForRow:rowIndex inSection:GTRSettingsSectionNotifificationsFrequency];
    cell.text.font = SETTINGS_FONT;
    
    UIView *viewFetched = [cell.contentView viewWithTag:tag];
    [viewFetched removeFromSuperview];
    viewFetched = nil;
    
    CGFloat paddingRight;
    CGRect notificationFrame;
    
    if (cell.indexpath.row == 1) {
        paddingRight = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? 142 : 154;
        notificationFrame = CGRectMake(cell.frame.size.width - paddingRight, 2, 132, cell.frame.size.height - 4);
    }
    else {
        paddingRight = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? 172 : 184;
        notificationFrame = CGRectMake(cell.frame.size.width - paddingRight, 2, 162, cell.frame.size.height - 4);
    }
    
    GTRSettingsCustomRightView *view = [[GTRSettingsCustomRightView alloc]initInCell:cell
                                                                               frame:notificationFrame
                                                                    withDelegate:self];
    [self setSettingDetailCellFreq:cell view:view index:rowIndex];
    [self applyRightViewStatus:view inCell:cell];
    view.tag = tag;
    [cell.contentView addSubview:view];
    
    switch (rowIndex) {
        case 0:
            cell.text.text = NEW_ACTIVEBUDZ_REQUEST;
            cell.text.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
            break;
        case 1:
            cell.text.text = NEW_SUGGESTED_ACTIVEBUDZ;
            break;
        case 2:
            cell.text.text = NEW_COMMENTS;
            break;
        case 3:
            cell.text.text = NEW_MESSAGE;
            break;
        case 4:
            cell.text.text = NEW_POST_IN_A_TRIBE;
            break;
        case 5:
            cell.text.text = INVITED_TO_A_NEW_TRIBE;
            break;
    }

    return cell;
}

-(GTRSettingsCell*)socialCell:(GTRSettingsCell*)cell forRowAtIndex:(NSInteger)rowIndex
{
    NSInteger tag = CELL_VIEW_TAG;
    cell.indexpath = [NSIndexPath indexPathForRow:rowIndex inSection:GTRSettingsSectionSocial];
    
    UIView *viewFetched = [cell.contentView viewWithTag:tag];
    [viewFetched removeFromSuperview];
    viewFetched = nil;
    
    CGFloat paddingRight = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? 65 : 80;
    CGRect socialFrame = CGRectMake(cell.frame.size.width - paddingRight, 1, 50, cell.frame.size.height - 2);
    
    GTRSettingsCustomRightView *view = [[GTRSettingsCustomRightView alloc]initInCell:cell
                                                                               frame:socialFrame
                                                                        withDelegate:self];
    [self applyRightViewStatus:view inCell:cell];
    view.tag = tag;
    [cell.contentView addSubview:view];
    
    switch (rowIndex) {
        case 0:
            cell.text.text = TWITTER;
            break;
        case 1:
            cell.text.text = FACEBOOK;
            break;
    }
    
    return cell;
}

-(GTRSettingsCell*)supportCell:(GTRSettingsCell*)cell forRowAtIndex:(NSInteger)rowIndex
{
    cell.indexpath = [NSIndexPath indexPathForRow:rowIndex inSection:GTRSettingsSectionSupport];
    switch (rowIndex) {
        case 0:
            cell.text.text = TERMS_OF_SERVICE;
            break;
        case 1:
            cell.text.text = PRIVACY_POLICY;
            break;
        case 2:
            cell.text.text = REPORT_A_PROBLEM;
            break;
        case 3:
            cell.text.text = ABOUT_THIS_VERSION;
            cell.accessoryType = NO;
            [cell setRightViewWithText:VERSION];
            break;
        case 4:
            cell.text.text = ABOUT_US;
            break;
    }

    return cell;
}

-(GTRSettingsCell*)logoutCell:(GTRSettingsCell*)cell forRowAtIndex:(NSInteger)rowIndex
{
    cell.indexpath = [NSIndexPath indexPathForRow:rowIndex inSection:GTRSettingsSectionAccount];
    
    switch (rowIndex) {
        case 0:
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = TABLEVIEW_BACKGROUND_COLOR;
            
            UIColor *buttonBackgroundColor = [UIColor colorWithRed:1.0 green:0.14 blue:0.18 alpha:1.0];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                if (![cell viewWithTag:LOGOUT_BUTTON_TAG]) {
                    UIView *buttonView = [[UIView alloc]initWithFrame:CGRectMake(10, 5, 300, 35)];
                    buttonView.backgroundColor = buttonBackgroundColor;
                    buttonView.userInteractionEnabled = YES;
                    buttonView.layer.cornerRadius = 3.0;
                    buttonView.layer.borderColor = [UIColor colorWithRed:1.0 green:0.4 blue:0.1 alpha:1.0].CGColor;
                    buttonView.layer.borderWidth = 0.5;
                    buttonView.layer.masksToBounds = YES;
                    buttonView.tag = LOGOUT_BUTTON_TAG;
                    
                    UILabel *label = [[UILabel alloc]initWithFrame:buttonView.bounds];
                    label.backgroundColor = buttonBackgroundColor;
                    label.textAlignment = NSTextAlignmentCenter;
                    label.text = @"Log Out";
                    label.font = CELL_LOGOUT_FONT;
                    label.textColor = [UIColor whiteColor];
                    [buttonView addSubview:label];
                    
                    [cell.contentView addSubview:buttonView];
                }
            } else {
                cell.backgroundColor = buttonBackgroundColor;
                cell.text.backgroundColor = [UIColor clearColor];
                cell.text.textAlignment = NSTextAlignmentCenter;
                cell.text.text = @"Log Out";
                cell.text.font = CELL_LOGOUT_FONT;
                cell.text.textColor = [UIColor whiteColor];
                cell.text.frame = CGRectMake(0, 0, cell.frame.size.width - 20, cell.frame.size.height);
            }

            break;
    }
    
    return cell;
}

#pragma mark - Logout

-(void)logoutButtonAction:(id)sender
{
    //save setting when logout
    [self saveSettingAndLogout:YES];
}

-(void)facebookLogout
{
    if (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        [FBSession.activeSession closeAndClearTokenInformation];
    }
}

-(void)showIntroScreenAnimated:(BOOL)animated
{
    GTRIntroViewController *introViewController = [[GTRIntroViewController alloc] init];
    UINavigationController *introNavigationController = [[UINavigationController alloc] initWithRootViewController:introViewController];
    [self.menuContainerViewController presentViewController:introNavigationController animated:animated completion:nil];
}

-(void)showEditProfile:(BOOL)animated
{
    GTRProfileViewController *editProfileVC = [[GTRProfileViewController alloc] initWithFromSetting:YES];
    UINavigationController *mainActivityNavigationController = self.menuContainerViewController.centerViewController;
    [mainActivityNavigationController pushViewController:editProfileVC animated:animated];
}

-(IBAction)showFeedback:(id)sender
{
    if([MFMailComposeViewController canSendMail]){
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate =self;
        [mailViewController setSubject:FEEDBACK_SUBJECT];
        [mailViewController setMessageBody:FEEDBACK_MESSAGE_BODY isHTML:NO];
        [mailViewController setToRecipients:@[FEEDBACK_TO_RECIPIENT]];
        [self presentViewController:mailViewController animated:YES completion:nil];
    }else {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"You must have a mail account in order to send an email",nil)];
    }
}

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            DDLogInfo(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            DDLogInfo(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            DDLogInfo(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            DDLogInfo(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark toggleSideMenu action

-(void)toggleSideMenu:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{}];
}


#pragma mark saveSetting action

-(void)saveSettingAndLogout:(BOOL)isLogout
{
    GTRUser* validUser = [GTRUserHelper getCurrentUser];
    
    if(validUser) {
        NSDictionary *parameters = settingDetail;
        DDLogInfo(@"Setting configuration values : %@",parameters);
        
        dispatch_async(dispatch_queue_create("settings_queue", NULL), ^{
            [GTRSettingsRestService updateSetting:parameters onSuccess:^(id responseObject){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [GTRSettingsHelper updateSettingDetail:parameters];
                    [SVProgressHUD dismiss];
                    
                    if (isLogout) {
                        GTRAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
                        [appDelegate logoutThisApp];
                    }
                });
            }onFail:^(id operation, NSError * error) {}];
        });
    }
}

#pragma mark - Custom right view delegate methods
-(void)updateSettingDetailForMode:(NSArray *)theMode inCell:(NSInteger)cellRow
{
    [settingDetail setObject:theMode forKey:typeSetting[cellRow]];
}

-(void)updateSettingDetailForFreq:(NSString*)theFreq inCell:(NSInteger)cellRow
{
    [settingDetail setObject:theFreq forKey:freqSetting[cellRow]];
}

-(void)customRightView:(GTRSettingsCustomRightView *)view notificationsMode:(GTRSettingsNotificationsMode)mode inCell:(GTRSettingsCell *)cell
{
    [GTRSettingsHelper setCellStatusValue:mode forIndexPath:cell.indexpath];
}

-(void)customRightView:(GTRSettingsCustomRightView *)view notificationsFrequency:(GTRSettingsNotificationsFrequency)mode inCell:(GTRSettingsCell *)cell
{
    [GTRSettingsHelper setCellStatusValue:mode forIndexPath:cell.indexpath];
}

-(void)customRightView:(GTRSettingsCustomRightView *)view twitterStatus:(GTRSettingsSocialStatus)status
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:GTRSettingsSectionSocial];
    [GTRSettingsHelper setCellStatusValue:status forIndexPath:indexPath];
    
    switch (status) {
        case GTRSettingsSocialStatusOn: {
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [[GTRSocialHelper sharedHelper] retrieveTwitterAccountWithSuccess:^{
                [SVProgressHUD dismiss];
                [GTRSettingsHelper setCellStatusValue:GTRSettingsSocialStatusOn forIndexPath:indexPath];
            } failure:^(id responseObject, NSError *error) {
                [SVProgressHUD dismiss];
                [GTRSettingsHelper setCellStatusValue:GTRSettingsSocialStatusOff forIndexPath:indexPath];
            }];
        }
            break;
        case GTRSettingsSocialStatusOff: {
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [[GTRSocialHelper sharedHelper] deleteTwitterAccountWithSuccess:^{
                [SVProgressHUD dismiss];
                [GTRSettingsHelper setCellStatusValue:GTRSettingsSocialStatusOff forIndexPath:indexPath];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error){
                [SVProgressHUD dismiss];
                [GTRSettingsHelper setCellStatusValue:GTRSettingsSocialStatusOn forIndexPath:indexPath];
            }];
        }
            break;
        default:
            break;
    }
}

-(void)customRightView:(GTRSettingsCustomRightView *)view facebookStatus:(GTRSettingsSocialStatus)status
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:GTRSettingsSectionSocial];
    [GTRSettingsHelper setCellStatusValue:status forIndexPath:indexPath];
    
    switch (status) {
        case GTRSettingsSocialStatusOn: {
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [[GTRSocialHelper sharedHelper] retrieveFacebookAccountWithSuccess:^{
                [SVProgressHUD dismiss];
                [GTRSettingsHelper setCellStatusValue:GTRSettingsSocialStatusOn forIndexPath:indexPath];
            } failure:^(NSError *error) {
                [SVProgressHUD dismiss];
                [GTRSettingsHelper setCellStatusValue:GTRSettingsSocialStatusOff forIndexPath:indexPath];
            }];
        }
            break;
        case GTRSettingsSocialStatusOff: {
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [[GTRSocialHelper sharedHelper] deleteFacebookAccountWithSuccess:^{
                [SVProgressHUD dismiss];
                [GTRSettingsHelper setCellStatusValue:GTRSettingsSocialStatusOff forIndexPath:indexPath];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error){
                [SVProgressHUD dismiss];
                [GTRSettingsHelper setCellStatusValue:GTRSettingsSocialStatusOn forIndexPath:indexPath];
            }];
        }
            break;
        default:
            break;
    }
}

#pragma mark - Right View setters

-(void)applyRightViewStatus:(GTRSettingsCustomRightView*)view inCell:(GTRSettingsCell*)cell
{
    NSIndexPath *indexPath = cell.indexpath;
    NSInteger value = [GTRSettingsHelper cellStatusValueForIndexPath:indexPath]? [[GTRSettingsHelper cellStatusValueForIndexPath:indexPath] integerValue]:-1;
    
    switch (indexPath.section) {
        case GTRSettingsSectionNotifications:
        {
            if (value >=0) {
                if (value == GTRSettingsNotificationsModePhone){
                    [view setPhoneButtonStatus:YES emailStatus:NO];
                }
                else if (value == GTRSettingsNotificationsModeEMail){
                    [view setPhoneButtonStatus:NO emailStatus:YES];
                }
                else if (value == GTRSettingsNotificationsModeBoth){
                    [view setPhoneButtonStatus:YES emailStatus:YES];
                }
                else if(value == GTRSettingsNotificationsModeNone){
                    [view setPhoneButtonStatus:NO emailStatus:NO];
                }
            } else {
                [view setPhoneButtonStatus:YES emailStatus:NO];
            }
            
            break;
        }
        case GTRSettingsSectionNotifificationsFrequency:
        {
            if (value >=0) {
                [view setFrequency:value];
            }
            else {
                [view setFrequency:3];
            }
            
            break;
        }
        case GTRSettingsSectionSocial:
        {
            [view setOn:(value == 1)];
            break;
        }
    }
}

@end
