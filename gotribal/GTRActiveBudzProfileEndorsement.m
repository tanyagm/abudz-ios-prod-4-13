//
//  GTRActiveBudzProfileEndorsement.m
//  gotribal
//
//  Created by loaner on 12/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActiveBudzProfileEndorsement.h"
#import "GTRActiveBudzProfileViewController.h"
#import "GTRActiveBudzRestService.h"

NSString* const ENDORSE_TITLE_STRING = @"To help maintain the safety of our community, would you consider endorsing %@?";
NSString* const ENDORSE_BUTTON_STRING = @"Endorse %@";
//endorse button icon
NSString* const ENDORSE_BUTTON_ICON = @"ico-badge-i-small";


@interface GTRActiveBudzProfileEndorsement()
@property (strong,nonatomic) GTRUser* user;
@property (strong,nonatomic) NSString* userName;
@property (strong,nonatomic) UIButton *endorseButton;
@property BOOL endorseStatus;
@end

@implementation GTRActiveBudzProfileEndorsement
@synthesize endorseButton;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame userName:(NSString*)theUserName
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userName = theUserName;
        [self showEndorse];
    }
    
    return self;
}

-(void)showEndorse
{
    UIFont *titleFont = [UIFont fontWithName:@"HelveticaNeue" size:14.0f];
    UIColor *titleColor = [UIColor colorWithWhite:0.373 alpha:1.000];
    
    NSAttributedString *endorseTitleString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:ENDORSE_TITLE_STRING, self.userName]
                                                                             attributes:@{
                                                                                          NSFontAttributeName: titleFont,
                                                                                          NSForegroundColorAttributeName : titleColor
                                                                                          }];
    UILabel *endorseTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 40)];
    endorseTitle.backgroundColor = [UIColor clearColor];
    endorseTitle.attributedText = endorseTitleString;
    endorseTitle.numberOfLines = 0;
    
    [self addSubview:endorseTitle];
    
    CGFloat endorseButtonWidth = self.bounds.size.width;
    CGFloat endorseButtonHeight = 48.0f;
    CGFloat endorseButtonX = 0;
    CGFloat endorseButtonY = self.bounds.size.height - endorseButtonHeight;

    
    NSAttributedString* endorseButtonString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:ENDORSE_BUTTON_STRING, self.userName]
                                                                              attributes:@{
                                                                                           NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:16.0f],
                                                                                           NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                                           }];
    
    endorseButton = [[UIButton alloc] initWithFrame:CGRectMake(endorseButtonX, endorseButtonY, endorseButtonWidth, endorseButtonHeight)];
    [endorseButton setAttributedTitle:endorseButtonString forState:UIControlStateNormal];
    [endorseButton setImage:[UIImage imageNamed:ENDORSE_BUTTON_ICON] forState:UIControlStateNormal];
    [endorseButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    endorseButton.backgroundColor = [UIColor colorWithRed:0.114 green:0.525 blue:0.541 alpha:1.000];
    endorseButton.layer.cornerRadius = 5.0f;
    endorseButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [endorseButton addTarget:self action:@selector(endorse:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview: endorseButton];

}

-(void)hideEndorse
{
    [self removeAllSubView];
}

-(void)removeAllSubView
{
    for(UIView *subView in self.subviews) {
        [subView removeFromSuperview];
    }
}

-(void)endorse:(id)sender
{
    if ([(id) self.delegate respondsToSelector:@selector(endorseButtonAction:)]){
        [self.delegate endorseButtonAction:sender];
    }
}




@end
