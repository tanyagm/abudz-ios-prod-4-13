//
//  GTRPaginationHelper.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRPaginationHelper.h"

@implementation GTRPaginationHelper

+(NSString*)paginate: (NSString*) url withLimit:(NSNumber*)limit
{
    return [NSString stringWithFormat:@"%@/?per_page=%@", url, limit];
}

+(NSString*)paginate: (NSString*) url withLimit:(NSNumber*)limit andMaxID:(NSNumber*)maxID
{
    NSString *limited = [GTRPaginationHelper paginate:url withLimit:limit];
    if (maxID == nil){
        return limited;
    } else {
        return [NSString stringWithFormat:@"%@&max_id=%@", limited, maxID];
    }
}
@end
