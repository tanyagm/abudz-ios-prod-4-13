//
//  GTRUserDetail.m
//  gotribal
//
//  Created by Sambya Aryasa on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRUserDetail.h"

@implementation GTRUserDetail

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"detailKey" : @"key",
             @"detailPrivate" : @"private",
             @"detailValue": @"value"
             };
}

@end
