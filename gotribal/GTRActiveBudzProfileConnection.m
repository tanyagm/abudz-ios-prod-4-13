//
//  GTRActiveBudzProfileConnection.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/3/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActiveBudzProfileConnection.h"
#import "UIImageView+AFNetworking.h"
#import "GTRUser.h"
#import <QuartzCore/QuartzCore.h>
#import "GTRTribeMemberCollectionViewCell.h"
#import "UIScrollView+SVInfiniteScrolling.h"

//activebudz picture placeholder
NSString* const CONNECTION_PLACEHOLDER_IMAGE_NAME = @"default_avatar";

//strings
NSString* const NO_CONNECTION_YET_STRING = @"This Activebudz doesn't have any connections yet.";

@interface GTRActiveBudzProfileConnection() <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (strong,nonatomic) NSArray *userArray;
@property (strong,nonatomic) UICollectionView *collectionView;

@end


@implementation GTRActiveBudzProfileConnection

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id) initWithFrame:(CGRect)frame connections:(NSArray*)theConnections
{
    self = [super initWithFrame:frame];
    if (self) {
        DDLogInfo(@"Added connections : %@",theConnections);
        self.userArray = theConnections;
        [self setAppearance];
    }
    return self;
}

-(void)setConnections:(NSArray *)theConnections
{
    self.userArray = theConnections;
}

-(void) resetAppearance
{
    for (UIView *subView in [self subviews]) {
        [subView removeFromSuperview];
    }
    
    [self setAppearance];
}

-(void) setAppearance
{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [UIColor colorWithWhite:0.800 alpha:1.000].CGColor;
    self.layer.cornerRadius = 5.0f;
    [self setUserInteractionEnabled:YES];
    
    if (!self.userArray || self.userArray.count == 0) {
        UIFont *textFont = [UIFont fontWithName:@"HelveticaNeue" size:14.0f];
        UIColor *titleColor = [UIColor colorWithWhite:0.333 alpha:1.000];
        
        CGFloat noConnectionHorizontalPadding = 0;
        CGFloat noConnectionHeight = self.frame.size.height;
        CGFloat noConnectionWidth = self.frame.size.width;
        CGFloat noConnectionY = 0;
        
        NSAttributedString *noConnectionString = [[NSAttributedString alloc] initWithString:NO_CONNECTION_YET_STRING
                                                                                 attributes:@{
                                                                                              NSFontAttributeName: textFont,
                                                                                              NSForegroundColorAttributeName : titleColor
                                                                                              }];
        
        UILabel *noConnection = [[UILabel alloc] initWithFrame:CGRectMake(noConnectionHorizontalPadding, noConnectionY, noConnectionWidth, noConnectionHeight)];
        noConnection.attributedText = noConnectionString;
        noConnection.lineBreakMode = NSLineBreakByWordWrapping;
        noConnection.numberOfLines = 0;
        noConnection.textAlignment = NSTextAlignmentCenter;
        noConnection.backgroundColor = [UIColor clearColor];
        [self addSubview:noConnection];
        
    } else {
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(55, 55);
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.minimumInteritemSpacing = 0.0f;
        
        self.collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
        self.collectionView.bounces = YES;
        self.collectionView.showsHorizontalScrollIndicator = YES;
        self.collectionView.showsVerticalScrollIndicator = NO;
        self.collectionView.dataSource = self;
        self.collectionView.delegate = self;
        self.collectionView.backgroundColor = [UIColor clearColor];
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
        
        [self.collectionView registerClass:[GTRTribeMemberCollectionViewCell class] forCellWithReuseIdentifier:@"userConnectionCell"];

        [self addSubview:self.collectionView];
    }
}

#pragma mark - UICollectionView delegates

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.userArray.count;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    GTRTribeMemberCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"userConnectionCell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[GTRTribeMemberCollectionViewCell alloc] initWithFrame:CGRectMake(0, 0, 55, 55)];
    }
    
    GTRUser *user = (GTRUser*)[self.userArray objectAtIndex:indexPath.row];
    [cell setContent:user];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    GTRUser *user = (GTRUser*)[self.userArray objectAtIndex:indexPath.row];
    
    if ([(id) self.delegate respondsToSelector:@selector(onUserSelected:)]) {
        [self.delegate onUserSelected:user];
    }
}

@end
