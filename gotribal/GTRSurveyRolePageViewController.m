//
//  GTRSurveyRolePageViewController.m
//  gotribal
//
//  Created by loaner on 12/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSurveyRolePageViewController.h"

#import "SVProgressHUD.h"


@interface GTRSurveyRolePageViewController () <UITableViewDelegate>

@end

@implementation GTRSurveyRolePageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithReferenceDictionary:(NSMutableDictionary *)theReferenceDictionary;
{
    
    GTRSurveyQuestionData *questionData = [[GTRSurveyQuestionData alloc] initWithQuestionType:UpToTwoSelectionAnswer
                                                                               questionImages:@[
                                                                                                [UIImage imageNamed:@"img-survey-3-1"],
                                                                                                [UIImage imageNamed:@"img-survey-3-2"],
                                                                                                [UIImage imageNamed:@"img-survey-3-3"]
                                                                                                ]
                                                                              questionAnswers:@[
                                                                                                @"a) Mentor",
                                                                                                @"b) Mentee",
                                                                                                @"c) Training Buddy"
                                                                                                ]
                                                                                 answerImages:nil referenceDictionary:theReferenceDictionary
                                                                                referenceName:USER_DETAIL_ROLE_KEY];
    //initiate with superclass
    self = [super initWithQuestionNumber:3
                          totalQuestions:11
                            questionText:@"Are you a ____________?"
                            questionData:questionData
                            nextQuestion:nil];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    DDLogInfo(@"Custom didSelectRowAtIndexPath triggered!");
    
    if([self isSelectionValid:indexPath.row])
    {
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    }else{
        //de-select the cell image
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}

//check whether user's current selection valid, according to selected role and the rules.
- (BOOL) isSelectionValid:(NSUInteger)theSelectedRole
{
    BOOL valid = NO;
    
    NSArray* answerStatus = self.answerSelectedStatus;
    //TODO : RPS - Update this later, if the logic is already clear!
    if (theSelectedRole == 0 && [answerStatus[1] intValue] == 1) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"As mentee you cannot also be a mentor.",nil)];
    } else if(theSelectedRole == 1 && [answerStatus[0] intValue] == 1) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"As mentor you cannot also be a mentee.",nil)];
    } else {
        valid = YES;
    }
    return valid;
}

@end