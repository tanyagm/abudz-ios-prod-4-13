//
//  GTRActiveBudzHelper.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/3/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActiveBudzHelper.h"
#import "GTRConstants.h"
#import "SVProgressHUD.h"
#import "GTRActiveBudzRestService.h"


@implementation GTRActiveBudzHelper

+(NSArray*)getArchivedSuggestedActiveBudz
{
    NSData *archive = [[NSUserDefaults standardUserDefaults] valueForKey:SUGGESTED_ACTIVEBUDZ_KEY];
    
    if (archive) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:archive];
    } else {
        return [[NSArray alloc] init];
    }
}

+(void)archiveSuggestedActiveBudz:(NSArray*)theActiveBudzList
{
    NSData *archive = [NSKeyedArchiver archivedDataWithRootObject:theActiveBudzList];
    [[NSUserDefaults standardUserDefaults] setObject:archive forKey:SUGGESTED_ACTIVEBUDZ_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSArray*)getArchivedMyActiveBudz
{
    NSData *archive = [[NSUserDefaults standardUserDefaults] valueForKey:MY_ACTIVEBUDZ_KEY];
    
    if (archive) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:archive];
    } else {
        return [[NSArray alloc] init];
    }
}

+(void)archiveMyActiveBudz:(NSArray*)theActiveBudzList
{
    NSData *archive = [NSKeyedArchiver archivedDataWithRootObject:theActiveBudzList];
    [[NSUserDefaults standardUserDefaults] setObject:archive forKey:MY_ACTIVEBUDZ_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+(void)pushActiveBudzProfile:(GTRUser*)theActiveBudz
                   indexPath:(NSIndexPath*)theIndexPath
          rootViewController:(UIViewController*)aViewController
                   onSuccess:(void (^)(id responseObject))success
                      onFail:(void (^)(id responseObject, NSError *error))errorHandler;
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    [GTRActiveBudzRestService retrievePublicProfile:theActiveBudz onSuccess:^(id responseObject) {
        NSArray *connectionsBuff = [responseObject objectForKey:@"connection"];
        NSDictionary *userParams = [responseObject objectForKey:@"user"];
        
        GTRUser *detailedUser = [MTLJSONAdapter modelOfClass:GTRUser.class
                                          fromJSONDictionary:userParams
                                                       error:nil];
        
        NSMutableArray *connections = [[NSMutableArray alloc] init];
        for (NSDictionary *dictionary in connectionsBuff) {
            [connections addObject:[MTLJSONAdapter modelOfClass:GTRUser.class
                                             fromJSONDictionary:dictionary
                                                          error:nil]];
        }
        
        ActiveBudzProfileType profileMode;
        
        if ([detailedUser.userID isEqual:[GTRUserHelper getCurrentUser].userID]) {
            profileMode = ActiveBudzProfileTypeCurrentUser;
        } else if (detailedUser.isFriend) {
            profileMode = ActiveBudzProfileTypeConnected;
        } else {
            profileMode = ActiveBudzProfileTypeNotConnected;
        }
        
        GTRActiveBudzProfileViewController *publicProfileVC = [[GTRActiveBudzProfileViewController alloc] initWithUser:detailedUser
                                                                                                             indexPath:theIndexPath
                                                                                                           connections:[connections copy]
                                                                                                           profileType:profileMode];
        
        publicProfileVC.delegate = (id)aViewController; //TODO : (if needed) for clarity - add a parameter for delegate on the method definition.
        
        [aViewController.navigationController pushViewController:publicProfileVC animated:YES];
        [SVProgressHUD dismiss];
        
        if (success) success(responseObject);
    } onFail:^(id responseObject, NSError *error) {
        if (errorHandler) errorHandler(responseObject, error);

    }];

}
@end
