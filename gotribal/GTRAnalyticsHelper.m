//
//  GTRAnalyticsHelper.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/23/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRAnalyticsHelper.h"

#ifdef GTR_FLURRY_ANALYTICS
#import "Flurry.h"
#endif

NSString* const FITNESS_SURVEY_SKIPPED_EVENT_STRING = @"Fitness survey skipped";
NSString* const FITNESS_PROFILE_CHANGED_EVENT_STRING = @"Fitness profile changed";

NSString* const ACTIVEBUDZ_REQUEST_TO_CONNECT_EVENT_STRING = @"Activebudz connect request sent";
NSString* const ACTIVEBUDZ_ACCEPT_REQUEST_EVENT_STRING = @"Activebudz request accepted";

NSString* const INVITE_FRIENDS_EVENT_STRING = @"Invitation to friends sent";

@implementation GTRAnalyticsHelper

#pragma mark Fitness Data

+(void)sendFitnessSurveySkippedEvent
{
    #ifdef GTR_FLURRY_ANALYTICS
    [Flurry logEvent:FITNESS_SURVEY_SKIPPED_EVENT_STRING];
    #endif
}

+(void)sendFitnessProfileChangedEvent
{
    #ifdef GTR_FLURRY_ANALYTICS
    [Flurry logEvent:FITNESS_PROFILE_CHANGED_EVENT_STRING];
    #endif
}

#pragma mark ActiveBudz

+(void)sendActiveBudzRequestToConnectEvent
{
    #ifdef GTR_FLURRY_ANALYTICS
    [Flurry logEvent:ACTIVEBUDZ_REQUEST_TO_CONNECT_EVENT_STRING];
    #endif
}

+(void)sendActiveBudzAcceptRequestEvent
{
    #ifdef GTR_FLURRY_ANALYTICS
    [Flurry logEvent:ACTIVEBUDZ_ACCEPT_REQUEST_EVENT_STRING];
    #endif
}

#pragma mark Invitations

+(void)sendInviteFriendsEvent
{
    #ifdef GTR_FLURRY_ANALYTICS
    [Flurry logEvent:INVITE_FRIENDS_EVENT_STRING];
    #endif
}

@end
