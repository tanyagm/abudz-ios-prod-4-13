//
//  GTRProfileViewController.m
//  gotribal
//
//  Created by Sambya Aryasa on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRProfileViewController.h"
#import "GTRAppDelegate.h"
#import "GTRUser.h"
#import "GTRSignupViewController.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "TTTAttributedLabel.h"
#import "GTRSurveyCoverViewController.h"
#import "Base64.h"
#import "UIImage+Resize.h"
#import "AFOAuth2Client.h"
#import "UIImageView+AFNetworking.h"
#import "UIButton+AFNetworking.h"
#import "GTRUserDetail.h"
#import "GTRBirthdayTextField.h"
#import "GTRImagePickerHelper.h"
#import "GTRSideMenuViewController.h"
#import "GTRRestServiceErrorHandler.h"

#ifdef DEBUG
//#define UICOLOR_DEBUG
#include <stdlib.h>
#endif

@interface GTRProfileViewController () <UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, TTTAttributedLabelDelegate, UITextFieldDelegate, GTRImagePickerHelperDelegate>
@property (nonatomic, strong) TPKeyboardAvoidingScrollView *scrollView;

@property (nonatomic, strong) UIBarButtonItem *rightBarButtonItem;

@property (nonatomic, strong) UITextField *firstNameTextField;
@property (nonatomic, strong) UITextField *lasttNameTextField;
@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UITextField *confirmPasswordTextField;
@property (nonatomic, strong) GTRBirthdayTextField *birthdayTextField;
@property (nonatomic, strong) UIDatePicker *birthdayDatePicker;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDate *birthdayDate;
@property (nonatomic, strong) UIImagePickerController *imagePickerController;
@property (nonatomic, strong) UIImage *pickedImage;
@property (nonatomic, strong) UIButton *photoPickerView;

@property (nonatomic, strong) UIButton *birthdayLockButton;

@property (nonatomic, strong) GTRUser *currentUser;

@property (nonatomic, getter = isPrivate) BOOL private;
@property BOOL isAgeValid;
@property (strong,nonatomic) NSString *currentName;

@property GTRImagePickerHelper *imagePickerHelper;
@property BOOL isFromSetting;
@end

@implementation GTRProfileViewController
@synthesize imagePickerHelper, rightBarButtonItem, delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Edit Profile", @"edit_profile");
        self.dateFormatter = [[NSDateFormatter alloc] init];
        self.dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        self.dateFormatter.dateFormat = @"MMMM dd, yyyy";
        self.isAgeValid = YES;
        self.isFromSetting = NO;
        
        imagePickerHelper = [[GTRImagePickerHelper alloc] init:self];
        imagePickerHelper.delegate = self;
    }
    return self;
}

- (id)initWithFromSetting:(BOOL)isFromSetting
{
    self= [super init];
    if (self) {
        self.isFromSetting = isFromSetting;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setExistingValue
{
    self.currentUser = [GTRUserHelper getCurrentUser];
    self.firstNameTextField.text = self.currentUser.firstName;
    self.lasttNameTextField.text = self.currentUser.lastName;
    self.emailTextField.text = self.currentUser.email;
    self.passwordTextField.text = @"";
    self.confirmPasswordTextField.text = @"";
    [self setBirthdateField];
    [self setAvatar];
    
    [self textFieldDidChange:nil];
}

-(void)setBirthdateField
{
    if (self.currentUser.birthday) {
        self.birthdayDate = self.currentUser.birthday;
        self.birthdayTextField.text =  [self.dateFormatter stringFromDate:self.currentUser.birthday];
        [self.birthdayDatePicker setDate:self.currentUser.birthday];
        [self setDetailLockButton:self.currentUser.birthdayDetail.detailPrivate];
    }
}


-(void)setAvatar
{
    if (self.currentUser.avatarURL) {
        self.photoPickerView.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.photoPickerView setImageForState:UIControlStateNormal withURL:[NSURL URLWithString:self.currentUser.avatarURL] placeholderImage:DEFAULT_AVATAR];
    } else {
        self.photoPickerView.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.photoPickerView setImage:DEFAULT_AVATAR forState:UIControlStateNormal];
    }
}

-(BOOL)inputValid
{
    BOOL passwordValid = !![self.passwordTextField.text length] == !![self.confirmPasswordTextField.text length];
    
    return (!![self.birthdayTextField.text length] &&
         !![self.firstNameTextField.text length] &&
         !![self.lasttNameTextField.text length] &&
         !![self.emailTextField.text length] &&
         passwordValid);
}

-(void)setProfilePictureWithImage:(UIImage*) image
{
    [self.photoPickerView setImage: image forState:UIControlStateNormal];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    rightBarButtonItem = [GTRNavigationBarHelper setRightBarButtonItemWithText:NSLocalizedString(@"Done", @"done")
                                                             onNavigationItem:self.navigationItem
                                                                   withTarget:self
                                                                     selector:@selector(doneButtonAction:)];
    
    
    self.scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.scrollView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0];
    self.scrollView.contentSize = self.scrollView.bounds.size;
    [self.view addSubview:self.scrollView];
    
    // Create form container
    CGFloat containerHeight = 252.0f;
    CGFloat xInset = 8.0f;
    CGFloat yOffset = 10.0f;
    CGFloat photoPickerWidth = 68.0f;
    
    CGRect containerViewFrame = CGRectMake(0, 0, self.view.bounds.size.width, containerHeight);
    containerViewFrame = CGRectInset(containerViewFrame, xInset, 0);
    
    // main container view
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectOffset(containerViewFrame, 0, yOffset)];
    containerView.backgroundColor = [UIColor whiteColor];
    containerView.layer.borderColor = [UIColor colorWithRed:195.0/255.0 green:195.0/255.0 blue:195.0/255.0 alpha:1.0].CGColor;
    containerView.layer.borderWidth = 0.5f;
    containerView.layer.cornerRadius = 3.0f;
    [self.scrollView addSubview:containerView];
    
    // inner container view
    UIView *innerContainerView = [[UIView alloc] initWithFrame:CGRectInset(containerView.bounds, xInset, yOffset)];
    #ifdef UICOLOR_DEBUG
    innerContainerView.backgroundColor = [UIColor blueColor];
    #endif
    [containerView addSubview:innerContainerView];
    
    // Create photo picker view container
    UIButton *photoPickerView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, photoPickerWidth, photoPickerWidth)];
    #ifdef UICOLOR_DEBUG
    photoPickerView.backgroundColor = [UIColor yellowColor];
    #endif
    photoPickerView.backgroundColor = [UIColor colorWithWhite:0.337 alpha:1.000];
    photoPickerView.layer.cornerRadius = photoPickerView.bounds.size.width/2;
    photoPickerView.layer.masksToBounds = YES;
    [photoPickerView addTarget:self action:@selector(openImagePicker:) forControlEvents:UIControlEventTouchUpInside];

    [innerContainerView addSubview:photoPickerView];
    self.photoPickerView = photoPickerView;
    
    // Create form container
    CGRect formContainerViewFrame = CGRectInset(innerContainerView.bounds, photoPickerWidth/2 + xInset, 0);
    UIView *formContainerView = [[UIView alloc] initWithFrame:CGRectOffset(formContainerViewFrame, photoPickerWidth/2, 0)];
    #ifdef UICOLOR_DEBUG
    formContainerView.backgroundColor = [UIColor redColor];
    #endif
    [innerContainerView addSubview:formContainerView];
    
    
    CGFloat textFieldHeight = 38.0f;
    CGFloat textFieldYOffset = 0.0f;
    CGFloat textFieldFontSize = 15;
    
    
    NSMutableArray *textFields = [NSMutableArray array];
    
    // Add text fields
    UITextField *firstNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, formContainerView.bounds.size.width, textFieldHeight)];
    [firstNameTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    #ifdef UICOLOR_DEBUG
    firstNameTextField.backgroundColor = [UIColor greenColor];
    #endif
    firstNameTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    firstNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"First Name", @"first name") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    [formContainerView addSubview:firstNameTextField];
    [textFields addObject:firstNameTextField];
    self.firstNameTextField = firstNameTextField;
    
    textFieldYOffset += firstNameTextField.bounds.size.height + 4;
    
    UITextField *lastNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, formContainerView.bounds.size.width, textFieldHeight)];
    [lastNameTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    #ifdef UICOLOR_DEBUG
    lastNameTextField.backgroundColor = [UIColor greenColor];
    #endif
    lastNameTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    lastNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Last Name", @"last name") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    [formContainerView addSubview:lastNameTextField];
    [textFields addObject:lastNameTextField];
    self.lasttNameTextField = lastNameTextField;
    
    textFieldYOffset += lastNameTextField.bounds.size.height;
    
    UITextField *emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, formContainerView.bounds.size.width, textFieldHeight)];
    [emailTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    #ifdef UICOLOR_DEBUG
    emailTextField.backgroundColor = [UIColor greenColor];
    #endif
    emailTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Email", @"email") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    [formContainerView addSubview:emailTextField];
    [textFields addObject:emailTextField];
    self.emailTextField = emailTextField;
    
    textFieldYOffset += emailTextField.bounds.size.height;
    
    UITextField *passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, formContainerView.bounds.size.width, textFieldHeight)];
    [passwordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    passwordTextField.secureTextEntry = YES;
    #ifdef UICOLOR_DEBUG
    passwordTextField.backgroundColor = [UIColor greenColor];
    #endif
    passwordTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Password", @"password") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    [formContainerView addSubview:passwordTextField];
    [textFields addObject:passwordTextField];
    self.passwordTextField = passwordTextField;
    
    textFieldYOffset += passwordTextField.bounds.size.height;
    
    UITextField *confirmPasswordTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, formContainerView.bounds.size.width, textFieldHeight)];
    [confirmPasswordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    confirmPasswordTextField.secureTextEntry = YES;
    #ifdef UICOLOR_DEBUG
    confirmPasswordTextField.backgroundColor = [UIColor greenColor];
    #endif
    confirmPasswordTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    confirmPasswordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Confirm Password", @"Confirm Password") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    [formContainerView addSubview:confirmPasswordTextField];
    [textFields addObject:confirmPasswordTextField];
    self.confirmPasswordTextField = confirmPasswordTextField;
    
    textFieldYOffset += confirmPasswordTextField.bounds.size.height;
    
    // Use date picker as input view to birthday text field
    self.birthdayDatePicker = [[UIDatePicker alloc] init];
    self.birthdayDatePicker.datePickerMode = UIDatePickerModeDate;
    self.birthdayDatePicker.maximumDate = [NSDate date];
    [self.birthdayDatePicker addTarget:self action:@selector(birthdayDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    UIToolbar *inputAccessoryViewToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(birthdayInputAccessoryDoneButtonAction:)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [inputAccessoryViewToolbar setItems:@[flexibleSpace, doneButton]];
    [inputAccessoryViewToolbar setBarStyle:UIBarStyleBlackTranslucent];
    
    GTRBirthdayTextField *birthdayTextField = [[GTRBirthdayTextField alloc] initWithFrame:CGRectMake(0, textFieldYOffset, formContainerView.bounds.size.width, textFieldHeight)];
    [birthdayTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    birthdayTextField.delegate = self;
    #ifdef UICOLOR_DEBUG
    birthdayTextField.backgroundColor = [UIColor greenColor];
    #endif
    birthdayTextField.font = [UIFont systemFontOfSize:textFieldFontSize];
    birthdayTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Birthday", @"birthday") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0f]}];
    birthdayTextField.inputView = self.birthdayDatePicker;
    birthdayTextField.inputAccessoryView = inputAccessoryViewToolbar;
    
    // Add birthday lock button
    self.birthdayLockButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.birthdayLockButton setImage:[UIImage imageNamed:@"ico-padlock-open"] forState:UIControlStateNormal];
    [self.birthdayLockButton sizeToFit];
    [self.birthdayLockButton addTarget:self action:@selector(birthdayLockButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    birthdayTextField.rightView = self.birthdayLockButton;
    birthdayTextField.rightViewMode = UITextFieldViewModeUnlessEditing;
    
    [formContainerView addSubview:birthdayTextField];
    [textFields addObject:birthdayTextField];
    self.birthdayTextField = birthdayTextField;
    
    // Add hairlines on the bottom of every text fields
    // Set clear button mode to UITextFieldViewModeWhileEditing
    for (UITextField *textField in textFields) {
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.clipsToBounds = NO;
        textField.textColor = GTR_FORM_TEXT_COLOR;
        @autoreleasepool {
            UIView *bottomSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(-1.0f, textField.bounds.size.height - 8.0f, textField.bounds.size.width, 0.5f)];
            bottomSeparatorView.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1.0f];
            [textField addSubview:bottomSeparatorView];
        }
    }
    
    // add some space to main container view
    containerViewFrame = containerView.frame;
    containerViewFrame.size.height += 8.0f;
    containerView.frame = containerViewFrame;
    
    [self setExistingValue];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self textFieldDidChange:nil];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self setAppearance];
    
}

-(void)setAppearance
{
    self.view.backgroundColor = [UIColor whiteColor];
    
    if(self.isFromSetting) {
        [GTRNavigationBarHelper setBackButtonItem:self.navigationItem
                                       withTarget:self
                                         selector:@selector(backButtonAction:)];

    } else {
        [GTRNavigationBarHelper setLeftSideBarButtonItem:self.navigationItem withTarget:self selector:@selector(toggleSideMenu:)];
    }
}

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextField action

-(void)textFieldDidChange:(id)sender
{
    [self enableRightBarButtonItem:[self inputValid]];
}

#pragma mark - UITextField delegate
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.birthdayTextField) {
        [UIView animateWithDuration:0.3 animations:^{
            [self.scrollView setContentInset:UIEdgeInsetsZero];
        }];
    }
}


#pragma mark - TTTAttributedLabel delegate

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    [[UIApplication sharedApplication] openURL:url];
}


#pragma mark - Custom methods

-(void)birthdayLockButtonAction:(id)sender
{
    [self toggleDetailLockButton];
}

-(void)toggleDetailLockButton
{
    [self setDetailLockButton:!self.isPrivate];
}

-(void)setDetailLockButton:(BOOL)is_private
{
    UIButton *lockButton = self.birthdayLockButton;
    
    self.private = is_private;
    
    if (self.isPrivate) {
        [lockButton setImage:[UIImage imageNamed:@"ico-padlock"] forState:UIControlStateNormal];
    } else {
        [lockButton setImage:[UIImage imageNamed:@"ico-padlock-open"] forState:UIControlStateNormal];
    }
}

-(void)birthdayDatePickerValueChanged:(id)sender
{
    UIDatePicker *datePicker = (UIDatePicker *)sender;
    self.birthdayDate = datePicker.date;
    self.dateFormatter.dateFormat = @"MMMM dd, yyyy";
    self.birthdayTextField.text = [self.dateFormatter stringFromDate:self.birthdayDate];
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *year = [calender components:NSYearCalendarUnit fromDate:self.birthdayDate];
    NSDateComponents *CurrentYear = [calender components:NSYearCalendarUnit fromDate:[NSDate date]];
    if([CurrentYear year] - [year year] <17){
        self.isAgeValid = NO;
    }else {
        self.isAgeValid = YES;
    }
    [self textFieldDidChange:nil];
}

-(void)birthdayInputAccessoryDoneButtonAction:(id)sender
{
    if ([self.birthdayTextField isFirstResponder]) [self.birthdayTextField resignFirstResponder];
}

#pragma mark - Custom actions

-(void)doneButtonAction:(id)sender
{
    if ([self inputValid] && self.isAgeValid == YES)
    {
        [self updateRequest];
        
    }else if(self.isAgeValid == NO){
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Age must be 17 or above!", @"Age must be 17 or above!")];
    }
}

-(void)enableRightBarButtonItem:(BOOL)barButtonEnabled
{
    rightBarButtonItem.enabled = barButtonEnabled;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (barButtonEnabled) {
            [rightBarButtonItem setTintColor:[UIColor whiteColor]];
        } else {
            [rightBarButtonItem setTintColor:[UIColor colorWithWhite:1.0f alpha:0.50f]];
        }
    }
}

#pragma mark - AFNetworking Requests

-(NSDictionary*)prepareParams
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    self.dateFormatter.dateFormat = @"yyyy/MM/dd";
    
    if (self.pickedImage) {
        NSString *avatarContentImageString = [UIImageJPEGRepresentation(self.pickedImage, 1.0f) base64EncodedString];
        [params setObject:avatarContentImageString forKey:@"avatar_content"];
        [params setObject:@"profile_picture.jpeg" forKey:@"avatar_filename"];
    }
    
    if ([self.passwordTextField.text length]){
        [params setObject:self.passwordTextField.text forKey:@"password"];
        [params setObject:self.confirmPasswordTextField.text forKey:@"password_confirmation"];
    }
    
    [params setObject:self.firstNameTextField.text forKey:@"first_name"];
    [params setObject:self.lasttNameTextField.text forKey:@"last_name"];
    [params setObject:self.emailTextField.text forKey:@"email"];

    
    DDLogInfo(@"self.birthdayDate %@", [self.dateFormatter stringFromDate:self.birthdayDate]);
    DDLogInfo(@"self.private %@", [NSNumber numberWithBool:self.private]);
    
    NSDictionary *birthdayParam = @{
                                @"value": [self.dateFormatter stringFromDate:self.birthdayDate],
                                @"private": [NSNumber numberWithBool:self.private]
                                };
    
    DDLogInfo(@"birthdate param %@", birthdayParam);
    
    [params setObject:birthdayParam forKey:@"birthdate"];
    
    return [[NSDictionary alloc] initWithDictionary:params];
}

-(void)updateRequest
{
    NSDictionary *parameters = [self prepareParams];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [self.view endEditing:YES];

    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager PUT:CURRENT_USER_ENDPOINT parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [GTRUserHelper updateCurrentUser:responseObject];
        
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Edit profile succeed!", @"Edit profile succeed")];
        
        if ([(id) delegate respondsToSelector:@selector(reloadMyProfile)]) {
            [delegate reloadMyProfile];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        DDLogInfo(@"Error: %@", error);
        
        NSDictionary *parsedJSON = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingMutableContainers error:nil];
        
        DDLogInfo(@"%@", parsedJSON);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
    }];
}

#pragma mark Image picker

- (void)openImagePicker:(id)sender
{
    [self resignFirstResponders];
    [imagePickerHelper openCustomActionSheet];
}

- (void)didPickImage:(UIImage*)image
{
    if(image) {
        self.pickedImage = [image thumbnailImage:300 transparentBorder:0 cornerRadius:0 interpolationQuality:kCGInterpolationHigh];
        [self.photoPickerView setImage:self.pickedImage forState:UIControlStateNormal];
    }
}

- (void)resignFirstResponders
{
    [_firstNameTextField resignFirstResponder];
    [_lasttNameTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_confirmPasswordTextField resignFirstResponder];
    [_birthdayTextField resignFirstResponder];
}

@end
