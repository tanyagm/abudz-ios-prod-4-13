//
//  GTRConversation.m
//  gotribal
//
//  Created by Muhammad Taufik on 11/29/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRConversation.h"

@implementation GTRConversation
+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"conversationID": @"id",
             @"message": @"message",
             @"unreadState": @"status",
             @"updatedAt": @"updated_at",
             @"user": @"user"
             };
}

+ (NSValueTransformer *)userJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:GTRUser.class];
}

@end
