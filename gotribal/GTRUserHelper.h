//
//  GTRUserHelper.h
//  gotribal
//
//  Created by Le Vady on 11/30/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRUser.h"

@interface GTRUserHelper : NSObject

+(GTRUser *)getCurrentUser;
+(void)updateCurrentUser:(NSDictionary *)params;
+(void)removeUserCredentials;
+(NSString *)getUDID;

+(NSArray*)getArchivedUserConnections;
+(void)archiveUserConnections:(NSArray*)theUserConnections;
+(void)removeCurrentUser;

@end
