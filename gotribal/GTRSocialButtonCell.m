//
//  GTRSocialButtonCell.m
//  gotribal
//
//  Created by Muhammad Taufik on 12/24/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSocialButtonCell.h"

@interface GTRSocialButtonCell ()
@property (nonatomic, strong) UIView *baseView;
@end

@implementation GTRSocialButtonCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.textLabel.backgroundColor = [UIColor whiteColor];
        self.textLabel.font = [UIFont systemFontOfSize:16.0];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.baseView = [[UIView alloc] initWithFrame:self.bounds];
        self.baseView.backgroundColor = [UIColor whiteColor];
        
        [self.contentView addSubview:self.baseView];
        [self.contentView sendSubviewToBack:self.baseView];
        
        [self.baseView.layer setCornerRadius:4.0F];
        [self.baseView.layer setMasksToBounds:YES];
        [self.baseView.layer setBorderWidth:0.5f];
        [self.baseView.layer setBorderColor:[UIColor colorWithWhite:0.800 alpha:1.000].CGColor];
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.baseView.frame = CGRectInset(self.bounds, 8, 5);
    self.imageView.frame = CGRectMake(18, 18, 32, 32);
    self.textLabel.frame = CGRectMake(64, 19, 240, 32);
}

@end
