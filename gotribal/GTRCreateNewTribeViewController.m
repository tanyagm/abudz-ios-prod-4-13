//
//  GTRCreateNewTribeViewController.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/11/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRCreateNewTribeViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "Base64.h"
#import <UIKitExtension/UIAlertView.h>
#import "GTRTribeRestService.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "GTRTribeActivityViewController.h"
#import "GTRTribeError.h"
#import "GTRTribe.h"

#define DEBUG_COLOR 0

NSString *TRIBE_NAME_PLACEHOLDER            = @"Give a title to your Tribe...";
NSString *TRIBE_DESCRIPTION_PLACEHOLDER     = @"Description of your Tribe";
NSString *TRIBE_DUPLICATE_ERROR             = @"Try adding more detail to your Tribe name and description!";
NSString *TRIBE_NO_ACTIVITY_ERROR           = @"You need to choose an activity.";
NSString *TRIBE_NO_TRIBE_IMAGE              = @"You must add an image for your Tribe.";
NSString *TRIBE_NEED_RELOGIN_ERROR          = @"You need to relogin.";
NSString *TRIBE_DUPLICATE_RESPONSE          = @"Name has already been taken";

@interface GTRCreateNewTribeViewController ()

@property(nonatomic,strong) UITextView *descriptionTextView;
@property(nonatomic,strong) UITextView *descriptionPlaceholderTextView;
@property(nonatomic,strong) UILabel *activityLabel;

@end

@implementation GTRCreateNewTribeViewController
@synthesize descriptionPlaceholderTextView = _descriptionPlaceholderTextView;
@synthesize descriptionTextView = _descriptionTextView;
@synthesize activityLabel = _activityLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Create Tribe";
        [super initAttributes];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super setAppearance];
    
    self.imagePickerHelper.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initNewPostView
{
    [super initNewPostView];
    
    UIColor *backgroundColor = UIColorFromRGB(0xF2F2F2);
    self.view.backgroundColor = backgroundColor;
    
    CGRect postMessageFrame = super.postMessage.frame;
    self.postMessage.frame = CGRectMake(postMessageFrame.origin.x + 5, postMessageFrame.origin.y + 8,
                                        CGRectGetWidth(postMessageFrame), 30);
    self.postMessage.returnKeyType = UIReturnKeyNext;
    self.postMessagePlaceholder.text = TRIBE_NAME_PLACEHOLDER;
    self.postMessagePlaceholder.frame = self.postMessage.frame;
    self.postMessagePlaceholder.textColor = NOTIFICATIONS_CELL_SUBTEXT_COLOR;
    
    // activity label
    _activityLabel = [[UILabel alloc]initWithFrame:CGRectMake(postMessageFrame.origin.x + 10,
                                                              CGRectGetMinY(self.postMessage.frame) + CGRectGetHeight(self.postMessage.frame) - 5,
                                                              self.postMessage.frame.size.width, 18)];
    _activityLabel.backgroundColor = backgroundColor;
    _activityLabel.font = TRIBE_ACTIVITY_CHOSEN_FONT;
    _activityLabel.textAlignment = NSTextAlignmentLeft;
    _activityLabel.textColor = NOTIFICATIONS_CELL_SUBTEXT_COLOR;
    [self.view addSubview:_activityLabel];
    
    UIButton *activititiesButton = [[UIButton alloc]initWithFrame:CGRectMake(40, 3 , 28, 29)];
    activititiesButton.backgroundColor = [UIColor clearColor];
    [activititiesButton setImage:[UIImage imageNamed:@"ico-tribes-activity"] forState:UIControlStateNormal];
    [activititiesButton addTarget:self action:@selector(chooseActivity:)
                 forControlEvents:UIControlEventTouchUpInside];
    [self.postMessage.inputAccessoryView addSubview:activititiesButton];
    
    // TODO MAMAZ: ADD LOCATION
//    UIButton *locationButton = [[UIButton alloc]initWithFrame:CGRectMake(69, 3 , 29, 29)];
//    locationButton.backgroundColor = backgroundColor;
//    [locationButton setImage:[UIImage imageNamed:@"ico-location"] forState:UIControlStateNormal];
//    [locationButton addTarget:self action:@selector(chooseActivity:)
//                 forControlEvents:UIControlEventTouchUpInside];
//    [self.postMessage.inputAccessoryView addSubview:locationButton];
    
    // description
    CGRect avatarRectOffset = CGRectOffset(self.avatar.frame, 0, self.avatar.frame.size.height + 10);
    _descriptionTextView = [[UITextView alloc]initWithFrame:CGRectMake(self.postMessage.frame.origin.x,
                                                                       avatarRectOffset.origin.y,
                                                                       255,48 )];
    _descriptionTextView.font = [UIFont systemFontOfSize:14.5];
    _descriptionTextView.backgroundColor = backgroundColor;
    _descriptionTextView.textColor = CELL_TEXT_FONT_COLOR;
    _descriptionTextView.delegate = self;
    [_descriptionTextView setInputAccessoryView:self.postMessage.inputAccessoryView];
    [self.view addSubview:_descriptionTextView];
    
    _descriptionPlaceholderTextView = [[UITextView alloc]initWithFrame:_descriptionTextView.frame];
    _descriptionPlaceholderTextView.text = TRIBE_DESCRIPTION_PLACEHOLDER;
    _descriptionTextView.textAlignment = NSTextAlignmentLeft;
    _descriptionPlaceholderTextView.textColor = NOTIFICATIONS_CELL_SUBTEXT_COLOR;
    _descriptionPlaceholderTextView.font = [UIFont systemFontOfSize:14.5];
    _descriptionPlaceholderTextView.editable = false;
    _descriptionPlaceholderTextView.backgroundColor = [UIColor clearColor];
    [_descriptionPlaceholderTextView setUserInteractionEnabled:NO];
    [self.view addSubview:_descriptionPlaceholderTextView];
    
    // add tap gesture to dismiss error
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                action:@selector(dismissError)];
    [self.view addGestureRecognizer:tapGesture];
    
#if DEBUG_COLOR
    self.postMessage.backgroundColor = [UIColor blueColor];
    _descriptionTextView.backgroundColor = [UIColor grayColor];
#endif
}

#pragma mark - Button action handlers

-(void)doneButtonAction:(id)sender
{
    [self sendTribePayloadData];
}

-(void)cancelButtonAction:(id)sender
{
    [super cancelButtonAction:sender];
}

-(void)chooseActivity:(id)sender
{
    GTRTribeActivityViewController *activityController = [[GTRTribeActivityViewController alloc]init];
    activityController.delegate = self;
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:activityController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark - Textview delegate methods

- (void) textViewDidChange:(UITextView *)textView
{
    if (textView == self.postMessage) {
        self.postMessagePlaceholder.hidden = (textView.text.length != 0);
    }
    else if(textView == _descriptionTextView){
        _descriptionPlaceholderTextView.hidden = (textView.text.length != 0);
    }
    
    [self enableDoneButtonItem:[self validateInput]];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    // make tribe name textview exactly one line (24 chars)
    if ([text isEqualToString:@"\n"] && textView == self.postMessage) {
        [textView resignFirstResponder];
        [_descriptionTextView becomeFirstResponder];
        return NO;
    } else if (textView.text.length == 24 && ![text isEqualToString:@""]){
        return NO;
    }
    
    return YES;
}

#pragma mark - Validate Input

-(BOOL) validateInput
{
    return (!![self.postMessage.text length]);
}

#pragma mark - Enabling or Disabling Done button

-(void)enableDoneButtonItem:(BOOL)buttonEnabled
{
    [super enableDoneButtonItem:buttonEnabled];
}

#pragma mark - Payload handlers

-(NSDictionary*)gatherPayloads
{
    NSMutableDictionary *payloads = [[NSMutableDictionary alloc]init];
    
    if (self.postMessage.text.length > 0) {
        payloads[@"name"] = self.postMessage.text;
    }
    
    if (_activityLabel.text.length > 0) {
        payloads[@"activity"] = _activityLabel.text;
    }
    
    if (_descriptionTextView.text.length > 0) {
        payloads[@"description"] = _descriptionTextView.text;
    }
    
    if (self.postImage) {
        payloads[@"image_data"] = [UIImageJPEGRepresentation(self.postImage, 1.0f) base64EncodedString];
        payloads[@"filename"] = @"image.jpg";
        payloads[@"content_type"] = @"image/jpg";
    }
    
    // TODO MAMAZ: locazione, lat, lon
    
    return [payloads copy];
}

-(NSString*)validatePayload:(NSDictionary*)payload
{
//    return (payload[@"name"] && payload[@"activity"] && payload[@"image_data"]);
    NSString *activity = @"activity";
    NSString *image_data = @"image_data";
    if (!payload[activity]) {
        return activity;
    }
    else if(!payload[image_data]){
        return image_data;
    }
    else
    {
        return nil;
    }
}

-(void)sendTribePayloadData
{
    [GTRTribeError sharedInstance].delegate = self;
    NSDictionary *payloads = [self gatherPayloads];

    NSString *lackingPayload = [self validatePayload:payloads];
    if (lackingPayload) {
        if ([lackingPayload isEqualToString:@"activity"]) {
            [[GTRTribeError sharedInstance] showInView:self.view
                                             withTitle:@"Whoa!"
                                               message:TRIBE_NO_ACTIVITY_ERROR];
        }
        else if ([lackingPayload isEqualToString:@"image_data"]){
            [[GTRTribeError sharedInstance] showInView:self.view
                                             withTitle:@"Whoa!"
                                               message:TRIBE_NO_TRIBE_IMAGE];
        }
    } else {
        [SVProgressHUD showWithStatus:@"Creating Tribe" maskType:SVProgressHUDMaskTypeBlack];
        // send
        [GTRTribeRestService postNewTribeWithPayload:payloads onSuccess:^(id responseObject) {
            [SVProgressHUD dismiss];
            GTRTribe *tribe = [MTLJSONAdapter modelOfClass:GTRTribe.class fromJSONDictionary:responseObject error:nil];
            [self cancelButtonAction:nil];
            [self.delegate onNewPostCreated:tribe];
        } onFail:^(id response, NSError *error) {
            if ([[response valueForKey:@"error"] isEqualToString:TRIBE_DUPLICATE_RESPONSE]) {
                [[GTRTribeError sharedInstance] showInView:self.view
                                              withTitle:nil
                                                message:TRIBE_DUPLICATE_ERROR];
            }
        }];
    }
}

#pragma mark Image picker

- (void)openImagePicker:(id)sender
{
    [self.postMessage resignFirstResponder];
    [self.descriptionTextView resignFirstResponder];
//    [self.imagePickerHelper openCustomActionSheetWithActivebudzPhotos]; TODO MAMAZ: ADD FROM ACTIVEBUDZ PHOTOS
    [self.imagePickerHelper openCustomActionSheet];
}

- (void)didPickImage:(UIImage*)image
{
    [self.postMessage becomeFirstResponder];
    if (image) {
        [super showPostImage:image];
    }
}

#pragma mark - GTRTribeActivityViewControler delegate

-(void)tribeActivityViewController:(GTRTribeActivityViewController*)controller didReturnActivity:(NSString*)activity
{
    _activityLabel.text = [NSString stringWithString:activity];
    [self.postMessage resignFirstResponder]; // resign first, then viewDidAppear will make postMessage become first responder.
}

#pragma mark - GTRTRibeErrorViewDelegate

-(void)didShowErrorView:(GTRTribeErrorView *)errorView
{
    [self makeViews:self.view.subviews hidden:YES];
}

-(void)didDismissErrorView:(GTRTribeErrorView *)errorView
{
    [self makeViews:self.view.subviews hidden:NO];
}

#pragma mark - Hide / Unhide view

-(void)makeViews:(NSArray*)views hidden:(BOOL)hide
{
    NSMutableArray *array = [NSMutableArray array];
    for (UIView *view in views) {
        // not hiding the backgroundView
        if (![view isKindOfClass:[GTRTribeErrorView class]]) {
            view.hidden = hide;
            
            // get all UITextView objects
            if ([view isKindOfClass:[UITextView class]]) {
                [array addObject:view];
            }
        }
    }
    // make sure placeholder is hidden when there's a text
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self textViewDidChange:obj];
    }];
    
    // guard for hiding placeholder when the error is shown
    if (_descriptionTextView.text.length == 0 && hide) {
        _descriptionPlaceholderTextView.hidden = hide;
    }
    
    if (self.postMessage.text.length > 0 && hide) {
        self.postMessagePlaceholder.hidden = hide;
    }
}

#pragma mark - Dismiss error when user taps the view

-(void)dismissError
{
    GTRTribeError *tribeError = [GTRTribeError sharedInstance];
    
    if ([tribeError isShown]) {
        [tribeError dismiss];
    }
}

@end
