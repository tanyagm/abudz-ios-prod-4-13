//
//  GTRSettingsHelper.h
//  gotribal
//
//  Created by Hisma Mulya S on 11/29/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRSettingsCell.h"

@interface GTRSettingsHelper : NSObject

+(UIView*)createHeaderViewWithText:(NSString*)text;

+(UIView*)createGroupedViewInCell:(GTRSettingsCell*)cell
                      atIndexPath:(NSIndexPath*)indexPath;

+(void)setCellStatusValue:(NSInteger)value
             forIndexPath:(NSIndexPath*)indexPath;

+(NSNumber*)cellStatusValueForIndexPath:(NSIndexPath*)indexPath;
+(NSMutableDictionary *)getSettingDetail;
+(void)updateSettingDetail:(NSDictionary*)params;

@end
