//
//  GTREditFitnessProfileViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/25/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTREditFitnessProfileViewController.h"
#import "GTREditFitnessPageGenerator.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "AFOAuth2Client.h"
#import "GTRAppDelegate.h"
#import "GTRUser.h"
#import "GTRUserRestService.h"
#import "GTRConstants.h"
#import "GTRSideMenuViewController.h"
#import "GTRAnalyticsHelper.h"
#import <AFNetworking/UIAlertView+AFNetworking.h>

#define FONT [UIFont fontWithName:@"HelveticaNeue-Light" size:16];

@interface GTREditFitnessProfileViewController () <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong,nonatomic) GTREditFitnessPageGenerator *page;
@property BOOL isHasAnyChangeOnMyRole;
@property (strong,nonatomic) NSArray *checkRoleValue;
@property int selectedRow;

@end

@implementation GTREditFitnessProfileViewController
@class GTRSurveyPageGenerator;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Fitness Profile";
        //prepare fitness profile titles
        
            self.fitnessProfileName = @[EDIT_FITNESS_FAVORITE_WORKOUT_TIME_TITLE,
                                        EDIT_FITNESS_MY_ROLE_TITLE,
                                        EDIT_FITNESS_LOOKING_FOR_TITLE,
                                        EDIT_FITNESS_DESIRED_ACTIVEBUDZ_GENDER,
                                        EDIT_FITNESS_FITNESS_PURSUIT_TITLE,
                                        EDIT_FITNESS_FITNESS_ABILITY_TITLE,
                                        EDIT_FITNESS_DAILY_ROUTINE_TITLE,
                                        EDIT_FITNESS_LIFESTYLE_TITLE,
                                        EDIT_FITNESS_LOCATION_TITLE
                                        ];
    
        self.isHasAnyChangeOnMyRole = NO;
        self.page = [[GTREditFitnessPageGenerator alloc]init];
        [self.page initiateFitnessProfilePages];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [super.view setBackgroundColor:[UIColor colorWithWhite:0.941 alpha:1.000]];
   
    //set navigation bar back button
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn-back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonAction:)];
    
    [self setAppearance];
    
    //load previous answers
    [self.page loadPreviousAnswers];
    
    self.checkRoleValue = [self getAnswerDetailValue];
}

#pragma mark - Notification for SideMenu event

-(void)sideMenuStateEventChangedTo:(MFSideMenuStateEvent)event
{
    if (event == MFSideMenuStateEventMenuWillOpen) {
        [self updateFitnessProfile];
    }
}

- (NSArray *)getAnswerDetailValue
{
    NSArray *detailValue;
    if([(self.page.referenceDictionary[@"role"][@"value"]) isKindOfClass:[NSArray class]]) {
        detailValue = self.page.referenceDictionary[@"role"][@"value"];
    } else {
        NSNumber *value = self.page.referenceDictionary[@"role"][@"value"];
        if (value) {
            detailValue =  @[value];
        }
    }
    
    return detailValue;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(self.selectedRow == 1) {
        [self checkIsHasAnyChangeOnMyRole];
    }
}

-(void)setAppearance
{
    //prepare appearance
    
    [GTRNavigationBarHelper setLeftSideBarButtonItem:self.navigationItem withTarget:self selector:@selector(toggleSideMenu:)];
    //add scroll view
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    [self.view addSubview:self.scrollView];
    
    //add table view
    CGFloat viewBoundsPadding;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        viewBoundsPadding = 80;
    } else {
        viewBoundsPadding = 60;
    }
    
    CGFloat singleTableCellHeight = 50.0f;
    CGFloat tableViewHorizontalPadding = 5.0f;
    CGFloat tableViewTopPadding = 10.0f;
    CGFloat tableViewBottomPadding = 10.0f;
    CGFloat tableViewWidth = self.view.bounds.size.width - (tableViewHorizontalPadding*2);
    CGFloat tableViewHeight = singleTableCellHeight * (self.fitnessProfileName.count - 1) + tableViewBottomPadding > self.view.bounds.size.height - viewBoundsPadding ?
                                self.view.bounds.size.height - viewBoundsPadding : singleTableCellHeight * (self.fitnessProfileName.count - 1) + tableViewBottomPadding;
    
    self.tableView = [[UITableView alloc] initWithFrame:
                              CGRectMake(tableViewHorizontalPadding,
                                         tableViewTopPadding,
                                         tableViewWidth,
                                         tableViewHeight)];
    
    self.tableView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    self.tableView.backgroundView = nil;
    self.tableView.layer.borderWidth = 0.5f;
    self.tableView.layer.borderColor = [UIColor colorWithRed:0.8 green:0.796 blue:0.8 alpha:1.0].CGColor;
    self.tableView.layer.cornerRadius = 5.0f;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.scrollView addSubview:self.tableView];
    
    //set scrollview size
    CGFloat scrollViewContentHeight = tableViewHeight > self.view.bounds.size.height ? self.view.bounds.size.height : tableViewHeight;
    self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, scrollViewContentHeight);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)toggleSideMenu:(id)sender
{
    if (self.isHasAnyChangeOnMyRole == YES){
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please do change about who you are Looking for",nil)];
    }else {
        [self.menuContainerViewController toggleLeftSideMenuCompletion:^{}];
    }
}

-(void)updateFitnessProfile
{
    NSDictionary *parameters = self.page.referenceDictionary;
    
    dispatch_queue_t queue = dispatch_queue_create("queue", NULL);
    dispatch_async(queue, ^{
        [GTRUserRestService updateFitnessProfile:parameters onSuccess:^(id responseObject) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self performSelector:@selector(dismissProfile) withObject:nil afterDelay:1.0];
                //send analytics data
#ifdef GTR_FLURRY_ANALYTICS
                [GTRAnalyticsHelper sendFitnessProfileChangedEvent];
#endif
            });
        } onFail:^(id operation, NSError * error) {
            DDLogInfo(@"Error : %@ ", error);
            dispatch_async(dispatch_get_main_queue(), ^{
               [self performSelector:@selector(dismissProfile) withObject:nil afterDelay:1.0];
            });
        }];
    });
}

-(void)dismissProfile
{
    [self.navigationController popViewControllerAnimated:YES];
    [SVProgressHUD dismiss];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.fitnessProfileName.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"AnswerCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    cell.backgroundView = [[UIView alloc]initWithFrame:CGRectZero];
    NSString *answer = self.fitnessProfileName[indexPath.row];
    [cell.textLabel setText:answer];
    
    cell.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    cell.textLabel.font = FONT;
    cell.backgroundColor = [UIColor colorWithWhite:1.000 alpha:1.000];;
    //add custom separator
    CGFloat customSeparatorLeftPadding = 20.0f;
    CGFloat customSeperatorRightPadding = 15.0f;
    UIView *customSeparator = [[UIView alloc] initWithFrame:CGRectMake(customSeparatorLeftPadding, cell.bounds.size.height-1, self.view.bounds.size.width-customSeparatorLeftPadding-customSeperatorRightPadding, 1)];
    customSeparator.backgroundColor = [UIColor colorWithWhite:0.918 alpha:1.000];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell.contentView addSubview:customSeparator];
    return cell;
}

- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    DDLogInfo(@"Cell number %ld is clicked!", (long)indexPath.row);
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self.navigationController pushViewController:self.page.surveyQuestionPages[indexPath.row] animated:YES];
    self.checkRoleValue = [self getAnswerDetailValue];
    self.selectedRow = indexPath.row;
    if (self.isHasAnyChangeOnMyRole==YES && indexPath.row == 2){
        self.isHasAnyChangeOnMyRole = NO;
    }
}

-(void) checkIsHasAnyChangeOnMyRole
{
    if ([self checkRoleAnswerTracker]){
        NSLog(@"has any change on my role");
        self.isHasAnyChangeOnMyRole = YES;
        [self.page.surveyQuestionPages[2] deselectAllAnswers];
    }
}

-(BOOL)checkRoleAnswerTracker
{
    NSArray *currentRole = self.page.referenceDictionary[@"role"][@"value"];
    int max = currentRole.count;
    int i=0;
    int bufferCurrentRole;
    int bufferCheckRole;
    
    while (i < max) {
        //TODO  : refactor this quick fix to the phase when retrieving item from current user in page generator
        
        if ([currentRole[i] isKindOfClass:[NSString class]]) {
            bufferCurrentRole = [(NSString*)currentRole[i] intValue];
        } else {
            bufferCurrentRole = [(NSNumber*)currentRole[i] intValue];
        }
        
        if ([self.checkRoleValue[i] isKindOfClass:[NSString class]]) {
            bufferCheckRole = [(NSString*)self.checkRoleValue[i] intValue];
        } else {
            bufferCheckRole = [(NSNumber*)self.checkRoleValue[i] intValue];
        }
        
        if(currentRole.count != self.checkRoleValue.count) {
            return YES;
        }
        else if(bufferCheckRole == bufferCurrentRole) {
            i++;
        } else {
            return YES;
        }
    }
    
    return NO;
}

@end
