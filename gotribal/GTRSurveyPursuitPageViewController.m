//
//  GTRSurveyPursuitPageViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/22/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSurveyPursuitPageViewController.h"
#import "GTRConstants.h"
#import "GTRCustomSurveyCell.h"
#import "SVProgressHUD.h"

@interface GTRSurveyPursuitPageViewController ()

@property (strong, nonatomic) UITextField *otherPursuitTextField;

@end

@implementation GTRSurveyPursuitPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithReferenceDictionary:(NSMutableDictionary *)theReferenceDictionary;
{
    GTRSurveyQuestionData *questionData = [[GTRSurveyQuestionData alloc] initWithQuestionType:UpToTwoSelectionAnswer
                                                                               questionImages:nil
                                                                              questionAnswers:@[
                                                                                                PURSUIT_FITNESS_ANSWER_STRING,
                                                                                                PURSUIT_RUNNING_ANSWER_STRING,
                                                                                                PURSUIT_CYCLING_ANSWER_STRING,
                                                                                                PURSUIT_YOGA_ANSWER_STRING,
                                                                                                PURSUIT_SURFING_ANSWER_STRING,
                                                                                                PURSUIT_SWIMMING_ANSWER_STRING,
                                                                                                PURSUIT_TRIATHLON_ANSWER_STRING
                                                                                                ]
                                                                                 answerImages:@[
                                                                                                [UIImage imageNamed:PURSUIT_FITNESS_ICON_NAME],
                                                                                                [UIImage imageNamed:PURSUIT_RUNNING_ICON_NAME],
                                                                                                [UIImage imageNamed:PURSUIT_CYCLING_ICON_NAME],
                                                                                                [UIImage imageNamed:PURSUIT_YOGA_ICON_NAME],
                                                                                                [UIImage imageNamed:PURSUIT_SURFING_ICON_NAME],
                                                                                                [UIImage imageNamed:PURSUIT_SWIMMING_ICON_NAME],
                                                                                                [UIImage imageNamed:PURSUIT_TRIATHLON_ICON_NAME]
                                                                                                ]
                                                                          referenceDictionary:theReferenceDictionary referenceName:USER_DETAIL_FITNESS_PURSUIT_KEY];
    //initiate with superclass
    self = [super initWithQuestionNumber:6
                          totalQuestions:11
                            questionText:@"Your favorite fitness pursuit right now?"
                            questionData:questionData
                            nextQuestion:nil];
    
    //initiate the text field.
    self.otherPursuitTextField = [[UITextField alloc] init];

    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //add text field.
    CGFloat otherPursuitTextFieldWidth = self.answerTableView.bounds.size.width;
    CGFloat otherPursuitTextFieldHeight = 25.0;
    CGFloat otherPursuitTextFieldLeftPadding = 20.0f;
    self.otherPursuitTextField.frame = CGRectMake(otherPursuitTextFieldLeftPadding,
                                                   0,
                                                   otherPursuitTextFieldWidth,
                                                   otherPursuitTextFieldHeight);
    self.otherPursuitTextField.backgroundColor = [UIColor clearColor];
    
    //set the placeholder text design.
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.otherPursuitTextField.placeholder = @"Other";
    } else {
        self.otherPursuitTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                            initWithString:NSLocalizedString(@"Other", @"other")
                                                            attributes:@{
                                                                         NSForegroundColorAttributeName: [UIColor blackColor],
                                                                         NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:16.0f]}];
    }

    [self.footerView addSubview:self.otherPursuitTextField];
    
    //add custom separator.
    CGFloat customSeparatorLeftPadding = 20.0f;
    UIView *customSeparator = [[UIView alloc] initWithFrame:CGRectMake(customSeparatorLeftPadding,
                                                                       otherPursuitTextFieldHeight-1,
                                                                       self.answerTableView.bounds.size.width-customSeparatorLeftPadding,
                                                                       1)];
    customSeparator.backgroundColor = [UIColor colorWithWhite:0.918 alpha:1.000];
    [self.footerView addSubview:customSeparator];
    
    //change the lock button frame.
    CGFloat lockButtonTopPadding = 5.0f;
    CGFloat extraHeight = otherPursuitTextFieldHeight+lockButtonTopPadding;
    
    CGFloat lockButtonX = self.lockButton.frame.origin.x;
    CGFloat lockButtonY = self.lockButton.frame.origin.y+extraHeight;
    CGFloat lockButtonWidth = self.lockButton.bounds.size.width;
    CGFloat lockButtonHeight = self.lockButton.bounds.size.height;
    
    self.lockButton.frame = CGRectMake(lockButtonX,
                                       lockButtonY,
                                       lockButtonWidth,
                                       lockButtonHeight);
    self.lockButton.contentEdgeInsets = UIEdgeInsetsMake(5, 35, 0, 0);
    
    //resize the footer frame
    self.footerView.frame = CGRectMake(0, 0, self.answerTableView.bounds.size.width,
                                       self.answerTableView.bounds.size.height-
                                       (lockButtonY+lockButtonHeight+50+extraHeight));
    
    //resize the tableview to fit the contents.
    self.answerTableView.frame = CGRectMake(self.answerTableView.frame.origin.x,
                                            self.answerTableView.frame.origin.y,
                                            self.answerTableView.frame.size.width,
                                            self.answerTableView.frame.size.height+otherPursuitTextFieldHeight);
    [self autoSetScrollViewSize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*)getOtherPursuitText
{
    NSString *pursuitText;
    if(self.otherPursuitTextField.text)
    {
        pursuitText = self.otherPursuitTextField.text;
    }else
    {
        pursuitText = @"";
    }
    return pursuitText;
}

- (BOOL) isMaximumSelectionReached
{
    DDLogInfo(@"Pursuit maximum selection checker fired!");
    BOOL result = [super isMaximumSelectionReached];
    
    //if maximum selection reached, disable the other text, and remove the value.
    
    if(result)
    {
        [self.otherPursuitTextField setUserInteractionEnabled:NO];
        self.otherPursuitTextField.text = @"";
    }else{
        [self.otherPursuitTextField setUserInteractionEnabled:YES];
    }
    
    return result;
}

- (NSArray*)getSelectedAnswers
{
    NSArray *selectedAnswersArray = [super getSelectedAnswers];
    NSMutableArray *customAnswerArray = [[NSMutableArray alloc] init];
    
    for (NSNumber *num in selectedAnswersArray) {
        [customAnswerArray addObject:[self.questionData.questionAnswers objectAtIndex:[num intValue]]]; //add the text
    }
    
    NSUInteger count = customAnswerArray.count;
    
     //if user select less than two, and the "other" text is not a blank
    if(count < 2 && ![[self.otherPursuitTextField.text stringByReplacingOccurrencesOfString:@" "
                                                                                 withString:@""]
                      isEqualToString:@""]) {
        [customAnswerArray addObject:[self getOtherPursuitText]];
    }
    
    return [customAnswerArray copy];
}

- (void)saveDataToDictionary
{
    
    if (self.questionData.referenceDictionary && self.questionData.referenceName) {
        NSNumber *private = [NSNumber numberWithInteger:([self isAnswerLocked] ? 1 : 0)];
        NSArray *value = [self getSelectedAnswers];
        
        
        NSDictionary *inputData = @{
                                    @"private":private,
                                    @"value":value
                                    };
        
        [self.questionData.referenceDictionary setValue:inputData forKey:self.questionData.referenceName];
        
        DDLogInfo(@"%@",self.questionData.referenceDictionary);
    }
    
}

//ignited if the user clicks a selection (not other text)
- (BOOL) isSelectionValid:(NSUInteger)theSelectedPursuit
{
    BOOL valid = YES;
    NSArray *selectedAnswers = [self getSelectedAnswers];
    DDLogInfo(@"isSelectionValid invoked! current selected answers : %@",selectedAnswers);
    NSString *pursuitString = (NSString*)[self.questionData.questionAnswers objectAtIndex:theSelectedPursuit];
    
    //invalid if user already selected a different answer and written something on the other text.
    if(selectedAnswers.count == 2 //yes, the other text field counts as answer too.
       && ![((NSString*)[selectedAnswers objectAtIndex:0]) isEqualToString:pursuitString]
       && ![((NSString*)[selectedAnswers objectAtIndex:1]) isEqualToString:pursuitString]
       && [self.otherPursuitTextField.text length] > 0) {
        valid = NO;
        //send alert
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please choose 2 fitness pursuits.",nil)];
  
    }

    return valid;
}

- (void)tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    DDLogInfo(@"Custom didSelectRowAtIndexPath triggered!");
    if ([self isSelectionValid:indexPath.row]) {
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    } else {
        //de-select the invalid cell
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"AnswerCell";
    
    GTRCustomSurveyCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[GTRCustomSurveyCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    cell.backgroundView = [[UIView alloc]initWithFrame:CGRectZero];
    NSString *answer = [self.questionData.questionAnswers objectAtIndex:indexPath.row];
    [cell setText:answer];
    
    cell.accessoryView =  [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico-check"]];
    cell.accessoryView.hidden = ![((NSNumber*)[self.answerSelectedStatus objectAtIndex:indexPath.row]) boolValue];
    
    if (self.questionData.answerImages) {
        if (self.questionData.answerImages.count > indexPath.row) {
            [cell  setImage:[self.questionData.answerImages objectAtIndex:indexPath.row]];
        }
    }
    if([[UIDevice currentDevice].systemVersion floatValue]<7.0)
        cell.backgroundColor = [[UIColor alloc] initWithRed:255 green:255 blue:255 alpha:1];
    //add custom separator
    CGFloat customSeparatorLeftPadding = 20.0f;
    CGFloat customSeparatorRightPadding = 30.0f;
    UIView *customSeparator = [[UIView alloc] initWithFrame:CGRectMake(customSeparatorLeftPadding, cell.bounds.size.height-1, self.view.bounds.size.width-customSeparatorLeftPadding-customSeparatorRightPadding, 1)];
    customSeparator.backgroundColor = [UIColor colorWithWhite:0.918 alpha:1.000];
    [cell.contentView addSubview:customSeparator];
    return cell;
}

- (void)backButtonAction:(id)sender
{
    DDLogInfo(@"Back button clicked!");
    if (self.isSurveyPageMode) {
        [self.navigationController popViewControllerAnimated:YES];
        if (!self.isSurveyPageMode) {
            [self saveDataToDictionary];
        }
    } else {
        if(self.selectedAnswerTracker.count == 0 && self.otherPursuitTextField.text.length ==0) {
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please select your changes.",@"Please select your changes.")];

        } else {
            [self.navigationController popViewControllerAnimated:YES];
            if (!self.isSurveyPageMode) {
                [self saveDataToDictionary];
            }
        }
        
    }
}

-(void)setOtherPursuitText:(NSString*)thePursuitString
{
    self.otherPursuitTextField.text = thePursuitString;
}

@end
