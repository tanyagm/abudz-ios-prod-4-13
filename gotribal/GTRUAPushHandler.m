//
//  GTRUAPushHandler.m
//  gotribal
//
//  Created by Muhammad Taufik on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRUAPushHandler.h"

@implementation GTRUAPushHandler

-(id)initWithDelegate:(id<GTRUAPushHandlerDelegate>)delegate
{
    if (self = [super init]) {
        self.delegate = delegate;
    }
    return self;
}

#pragma mark - UAPushNotificationDelegate

-(void)displayNotificationAlert:(NSString *)alertMessage
{
    DDLogInfo(@"%@", alertMessage);
}

-(void)playNotificationSound:(NSString *)soundFilename
{
    DDLogInfo(@"%@", soundFilename);
}

-(void)handleBadgeUpdate:(NSInteger)badgeNumber
{
    DDLogInfo(@"badgeNumber: %d", badgeNumber);
}

-(void)receivedForegroundNotification:(NSDictionary *)notification
{
    DDLogInfo(@"%@", notification);
    
    NSDictionary *payload = [notification objectForKey:@"aps"];
    
    if ([payload objectForKey:@"conversation_id"]) {
        // TODO - Handle foreground notification
    }
    
    if ([payload objectForKey:@"accepted_friend_id"]) {
        // TODO - Handle accepted friend foreground notification
    }
    
    if ([payload objectForKey:@"pending_friend_id"]) {
        // TODO - Handle pending friend foreground notification
    }
}

-(void)launchedFromNotification:(NSDictionary *)notification
{
    DDLogInfo(@"%@", notification);
    
    NSDictionary *payload = [notification objectForKey:@"aps"];
    
    if ([payload objectForKey:@"conversation_id"]) {
        [self handleMessageNotificationPayloadWithConversationID:[payload objectForKey:@"conversation_id"]];
    }
    
    if ([payload objectForKey:@"accepted_friend_id"]) {
        [self handleAcceptedFriendRequestNotificationWithFriendID:[payload objectForKey:@"accepted_friend_id"]];
    }
    
    if ([payload objectForKey:@"pending_friend_id"]) {
        [self handlePendingFriendNotificationWithFriendID:[payload objectForKey:@"pending_friend_id"]];
    }
}

-(void)handleMessageNotificationPayloadWithConversationID:(NSNumber *)conversationID
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushHandler:openConversationWithID:)]) {
        [self.delegate pushHandler:self openConversationWithID:conversationID];
    }
}

-(void)handleAcceptedFriendRequestNotificationWithFriendID:(NSNumber *)acceptedFriendID
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushHandler:openFriendProfileWithFriendID:)]) {
        [self.delegate pushHandler:self openFriendProfileWithFriendID:acceptedFriendID];
    }
}

-(void)handlePendingFriendNotificationWithFriendID:(NSNumber *)pendingFriendID
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushHandler:openNotificationScreenWithFriendID:)]) {
        [self.delegate pushHandler:self openNotificationScreenWithFriendID:pendingFriendID];
    }
}

@end
