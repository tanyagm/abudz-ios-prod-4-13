//
//  GTRCustomSurveyCell.h
//  gotribal
//
//  Created by loaner on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTRCustomSurveyCell : UITableViewCell
@property (strong, nonatomic) UILabel *customTextLabel;
@property (strong,nonatomic ) UIImageView *customImageView;

-(void)setImage:(UIImage *)theImageName;
-(void)setText:(NSString *)aText;
@end
