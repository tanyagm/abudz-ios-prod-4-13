//
//  GTRSuggestedActiveBudzCell.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRCustomButton.h"

@interface GTRActiveBudzCell : UITableViewCell

@property (strong,nonatomic) UIImageView *customImageView;
@property (strong,nonatomic) UILabel *customLabel;

@property (strong, nonatomic) GTRCustomButton *acceptButton;
@property (strong, nonatomic) GTRCustomButton *rejectButton;
@property (strong,nonatomic) NSArray *pursuitArray;

- (id)initWithHeight:(CGFloat)height
     isSuggestedMode:(BOOL)theSuggestedMode
               style:(UITableViewCellStyle)style
     reuseIdentifier:(NSString *)reuseIdentifier;

- (void)setPursuitIcons;

@end
