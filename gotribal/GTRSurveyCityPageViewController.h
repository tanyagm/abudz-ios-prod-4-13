//
//  GTRSurveyLastPageViewController.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/22/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSurveyPageTemplateViewController.h"

@interface GTRSurveyCityPageViewController : GTRSurveyPageTemplateViewController

-(id)initWithReferenceDictionary:(NSMutableDictionary *)theReferenceDictionary;

-(NSString*)getCityText;
-(void)setCityText:(NSString*)theCityName;

@end
