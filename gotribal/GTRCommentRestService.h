//
//  GTRCommentRestService.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/12/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRPost.h"

@interface GTRCommentRestService : NSObject

+(void)retrieveCommentsFromPost:(GTRPost*) post
                      withMaxID:(NSNumber*) maxID
                      onSuccess:(void (^)(id))success
                         onFail:(void (^)(id, NSError *))errorHandler;

+(void)createCommentForPost:(GTRPost*) post
                WithMessage:(NSString*) message
                  onSuccess:(void (^)(id))success
                     onFail:(void (^)(id, NSError *))errorHandler;

@end
