//
//  GTRSurveyPageTemplateViewController.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/20/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRSurveyQuestionData.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface GTRSurveyPageTemplateViewController : GTRViewController <UITableViewDelegate>

@property int questionNumber;
@property int questionTotalNumber;
@property (strong,nonatomic) NSString *questionText;
@property (strong, nonatomic) GTRSurveyQuestionData *questionData;
@property (strong, nonatomic) UIViewController *nextQuestion; //next question view controller

//these properties set to public so can be modified by sub-class.
@property (strong, nonatomic) UIButton *lockButton;
@property (strong, nonatomic) UITableView *answerTableView;
@property (strong, nonatomic) UIView *footerView;


@property (strong, nonatomic) UIImage *lockedButtonImage;
@property (strong, nonatomic) UIImage *unlockedButtonImage;

@property (strong, nonatomic) NSMutableArray *answerSelectedStatus; //contains status whether the answer in certain index is selected or not.

@property (nonatomic, strong) TPKeyboardAvoidingScrollView *scrollView;

@property (nonatomic, strong) NSMutableArray *selectedAnswerTracker; //a queue for tracking the selected answers
@property BOOL isSelectingAnswerEnabled; //for locking the selection action, if the user taps screen furiously
@property BOOL withHeader;
@property BOOL withQuestionType;
@property BOOL isSurveyPageMode;



// set question appearance according to question data.
- (void) setQuestionAppearance;

- (id)init;

// full initiator
- (id) initWithQuestionNumber:(int)theQuestionNumber
               totalQuestions:(int)theTotalQuestionNumber
                 questionText:(NSString*)theQuestionText
                questionData:(GTRSurveyQuestionData*)theQuestionData
                 nextQuestion:(UIViewController*)theNextQuestion;

// getter and setter of several variables.

- (NSArray*)getSelectedAnswers;

- (BOOL)isAnswerLocked;
- (void)setAnswerLocked:(BOOL)theLockedStatus;

- (BOOL)isMaximumSelectionReached;

- (void)autoSetScrollViewSize;

- (void)saveDataToDictionary;

// set to public, so can be overridden by sub classes.
- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath;

//used especially for the Q4.
- (void) deselectAllAnswers;

//set to public so could be overriden
- (void)backButtonAction:(id)sender;
- (void)nextButtonAction:(id)sender;
- (void)enableRightBarButtonItem:(BOOL)barButtonEnabled;

//update answerSelectedStatus to match the selectedAnswerTracker array.
- (void)updateAnswerSelectedStatus;

//custom methods, for compability of settings page
-(void)initiateSelectedAnswerTracker;
-(void)initiateSelectedAnswerStatus;
-(void)initiateLockButton;

@end
