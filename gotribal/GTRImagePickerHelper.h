//
//  GTRImagePickerHelper.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/9/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRPhotoActionSheet.h"

typedef enum {
    ImageSourceDefault
}GTRImageSourceSet;

@protocol GTRImagePickerHelperDelegate
-(void)didPickImage:(UIImage*)image;
@end

@interface GTRImagePickerHelper : NSObject <GTRPhotoActionSheetDelegate>

@property (weak) id <GTRImagePickerHelperDelegate> delegate;

- (id)init:(UIViewController*)myViewController;
- (id)init:(UIViewController*)myViewController imageSources:(GTRImageSourceSet) imageSourceSet;
- (void)openImagePickerActionSheet;
- (void)openCustomActionSheet;
- (void)openCustomActionSheetWithActivebudzPhotos;
+ (GTRImageOrientation)checkImageOrientation:(UIImage*)image;
+ (void)setAndAdjustOrientationImageView:(UIImageView*)imageView
                                withURL:(NSURL*)url
                         andPlaceholder:(UIImage*)placeholder;

@end
