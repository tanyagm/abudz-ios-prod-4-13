//
//  GTRSurveyPageTemplateViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/20/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSurveyPageTemplateViewController.h"
#import "GTRSurveyImageView.h"
#import <UIKit/UISwipeGestureRecognizer.h>
#import <QuartzCore/QuartzCore.h>
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "AFOAuth2Client.h"
#import "GTRAppDelegate.h"
#import "GTRUser.h"
#import "GTRUserRestService.h"
#import "GTRAnalyticsHelper.h"

@interface GTRSurveyPageTemplateViewController () <UIScrollViewDelegate, UITableViewDataSource>


@property (strong, nonatomic) UIImage *starImage;

@property (strong, nonatomic) UILabel *descriptionLabel;
@property (strong, nonatomic) UIImageView *starImageView;

@property (strong, nonatomic) UIImageView *questionImageHeader; //header image
@property (strong, nonatomic) UILabel *questionNumberLabel; //the label of question number
@property (strong, nonatomic) UILabel *questionTextLabel; //the label of question text

@property (strong, nonatomic) NSArray *questionImageViews; //image view for displaying the images
@property (strong, nonatomic) NSArray *answerImageViews; //image view for displaying images on the left of answer cells.

@end

@implementation GTRSurveyPageTemplateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

//full initiator
- (id) initWithQuestionNumber:(int)theQuestionNumber
               totalQuestions:(int)theTotalQuestionNumber
                 questionText:(NSString*)theQuestionText
                 questionData:(GTRSurveyQuestionData*)theQuestionData
                 nextQuestion:(UIViewController*)theNextQuestion;
{
    self = [super init];
    if (self) {
        self.title = @"Questions";
        self.withHeader = YES;
        self.withQuestionType = YES;
        self.isSurveyPageMode = YES;
        self.questionNumber = theQuestionNumber;
        self.questionTotalNumber = theTotalQuestionNumber;
        self.questionText = theQuestionText;
        self.questionData = theQuestionData;
        self.nextQuestion = theNextQuestion;
    }
    
    return self;
}

- (id)init
{
    GTRSurveyQuestionData *dummyQuestionData = [[GTRSurveyQuestionData alloc] init];
    
    self = [self initWithQuestionNumber:1
                         totalQuestions:99
                           questionText:@"Question title here."
                           questionData:dummyQuestionData
                           nextQuestion:nil];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [super.view setBackgroundColor:[UIColor colorWithWhite:0.941 alpha:1.000]];
        
    //set navigation bar back button
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn-back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonAction:)];
    
    //if survey page mode
    if (self.isSurveyPageMode) {
        //set navigation bar next button
              self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextButtonAction:)];
    }
 
    //initiate scrollView
    self.scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.scrollView.delegate = self;

    //prepare gesture recognizers
    UISwipeGestureRecognizer *leftSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRecognizerAction:)];
    [leftSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:leftSwipeRecognizer];
    [self.scrollView addGestureRecognizer:leftSwipeRecognizer];
    
    UISwipeGestureRecognizer *rightSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRecognizerAction:)];
    [rightSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:rightSwipeRecognizer];
    [self.scrollView addGestureRecognizer:rightSwipeRecognizer];
    
    
    //prepare the answer question boolean array
    if (!self.answerSelectedStatus) {
        [self initiateSelectedAnswerStatus];
    }
        
    //prepare selected answer tracker, for enhancing UX
    if (!self.selectedAnswerTracker) {
        [self initiateSelectedAnswerTracker];
    }
    
    //prepare the selection lock too
    self.isSelectingAnswerEnabled = YES;
    
    //set question appearance
    [self setQuestionAppearance];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(self.selectedAnswerTracker.count > 0) {
        [self enableRightBarButtonItem:YES];
    } else {
        [self enableRightBarButtonItem:NO];
    }
}

#pragma mark - Appearance methods

- (void)setQuestionAppearance
{
    //measure and display question images
    CGFloat surveyImagesHeight = 35.0; //default height if there's no image.
    GTRSurveyImageView *surveyImageView;
    if (self.withHeader){
        //set question header image
        UIImage *headerImage = [UIImage imageNamed:@"bg-survey"];
        
        self.questionImageHeader = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 92.0f)];
        [self.questionImageHeader setImage:headerImage];
        self.questionImageHeader.contentMode = UIViewContentModeScaleAspectFill;
        [self.scrollView addSubview:self.questionImageHeader];
        
        //display question number
        CGFloat questionNumberLabelWidth = 75.0f;
        CGFloat questionNumberLabelHeight = 25.0f;
        CGFloat questionNumberLeftPadding = 10.0f;
        
        self.questionNumberLabel = [[UILabel alloc] initWithFrame: CGRectMake(questionNumberLeftPadding,
                                                                              (self.questionImageHeader.bounds.size.height - questionNumberLabelHeight)/2,
                                                                              questionNumberLabelWidth,
                                                                              questionNumberLabelHeight)];
        
        self.questionNumberLabel.text = [NSString stringWithFormat:@"Q%d of %d", self.questionNumber, self.questionTotalNumber];
        self.questionNumberLabel.backgroundColor = [UIColor clearColor];
        self.questionNumberLabel.textColor = [UIColor whiteColor];
        self.questionNumberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.5];
        [self.questionImageHeader addSubview:self.questionNumberLabel];
        
        
        //display question text
        CGFloat questionTextLabelLeftPadding = 100.0f;
        CGFloat questionTextLabelRightPadding = 13.0f;
        CGFloat questionTextLabelWidth = self.questionImageHeader.bounds.size.width - questionTextLabelRightPadding - questionTextLabelLeftPadding;
        CGFloat questionTextLabelHeight = self.questionImageHeader.bounds.size.height;
        
        self.questionTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(
                                                                           questionTextLabelLeftPadding,
                                                                           0,
                                                                           questionTextLabelWidth,
                                                                           questionTextLabelHeight)];
        self.questionTextLabel.text = self.questionText;
        self.questionTextLabel.numberOfLines = 0;
        self.questionTextLabel.backgroundColor = [UIColor clearColor];
        self.questionTextLabel.textColor = [UIColor whiteColor];
        self.questionTextLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
        [self.questionImageHeader addSubview:self.questionTextLabel];
        
        if (self.questionData.questionImages) {
            surveyImageView = [[GTRSurveyImageView alloc] initWithFrame:CGRectMake(0, 0, self.scrollView.bounds.size.width, 100) imageArray:self.questionData.questionImages];
            
            //by this, the view height must be changed.
            surveyImagesHeight = surveyImageView.bounds.size.height;
        }
    }
    
    //measure and display question answers in table view
    CGFloat answerTableViewHorizontalPadding = 5.0f;
    CGFloat answerTableViewWidth = (self.scrollView.bounds.size.width - (answerTableViewHorizontalPadding*2));
    CGFloat singleAnswerHeight = 40.0f; ///height of a single answer (table cell)
    CGFloat answerTableFooterHeight = 100.0f; //for lock button on footer
    CGFloat answerTableViewHeight = surveyImagesHeight+ (singleAnswerHeight*self.questionData.questionAnswers.count)+answerTableFooterHeight;
    CGFloat answerTableViewTopPadding = self.questionImageHeader.bounds.size.height + 10.0f;
    
    self.answerTableView = [[UITableView alloc] initWithFrame:CGRectMake(answerTableViewHorizontalPadding,
                                                                         answerTableViewTopPadding,
                                                                         answerTableViewWidth,
                                                                         answerTableViewHeight)
                                                        style: UITableViewStyleGrouped];
    
    self.answerTableView.backgroundColor = [UIColor colorWithWhite:1.000 alpha:1.000];
    self.answerTableView.backgroundView = nil;
    self.answerTableView.layer.borderWidth = 0.5f;
    self.answerTableView.layer.borderColor = [UIColor colorWithRed:0.800 green:0.796 blue:0.800 alpha:1.000].CGColor;
    self.answerTableView.layer.cornerRadius = 5.0f;
    self.answerTableView.dataSource = self;
    self.answerTableView.delegate = self;
    [self.answerTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.scrollView addSubview:self.answerTableView];
    
    //if there's any image, add the image view.
    if (surveyImagesHeight > 0) {
        self.answerTableView.tableHeaderView = surveyImageView;
    }
    
    //add the footerview
    CGFloat biggerLockButton = 50;
    self.footerView = [[UIView alloc] init];
    self.answerTableView.tableFooterView = self.footerView;
    
    //display lock button
    self.lockedButtonImage = [UIImage imageNamed:@"ico-padlock"];
    self.unlockedButtonImage = [UIImage imageNamed:@"ico-padlock-open"];
    
    if (!self.lockButton) {
        [self initiateLockButton];
    }
    
    self.lockButton.frame = CGRectMake(self.answerTableView.bounds.size.width
                                       -self.unlockedButtonImage.size.width-10-biggerLockButton,
                                       0,
                                       self.unlockedButtonImage.size.width+biggerLockButton,
                                       self.unlockedButtonImage.size.height+biggerLockButton);
    
    [self.lockButton setImage:self.unlockedButtonImage forState:UIControlStateNormal];
    [self.lockButton setImage:self.lockedButtonImage forState:UIControlStateSelected];
    [self.lockButton addTarget:self action:@selector(lockButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.lockButton.contentEdgeInsets = UIEdgeInsetsMake(18, 35, 0, 0);
    //add the lock button to the footerview
    self.footerView.frame = CGRectMake(0,
                                       0,
                                       self.answerTableView.bounds.size.width,
                                       self.unlockedButtonImage.size.height+biggerLockButton);
    
    [self.footerView addSubview:self.lockButton];
    
    if(self.withQuestionType){
        //add the question type first...
        self.descriptionLabel = [[UILabel alloc] init];
        self.descriptionLabel.backgroundColor = [UIColor clearColor];
        self.descriptionLabel.textColor = [UIColor blackColor];
        self.descriptionLabel.text = self.questionData.questionType == SingleSelectionAnswer? @"Choose one." : @"Choose up to two.";
        self.descriptionLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0];
        
        self.starImage = [UIImage imageNamed:@"ico-star"];
        self.starImageView = [[UIImageView alloc] initWithImage:self.starImage];
        
        if(self.questionData.questionType != SingleStringAnswer)
        {
            [self.scrollView addSubview:self.descriptionLabel];
            [self.scrollView addSubview:self.starImageView];
        }
    }
    
    //set scrollView size and add it to the main view
    [self autoSetScrollViewSize];
    
    [self.view addSubview:self.scrollView];
}

- (void)autoSetScrollViewSize
{
    CGFloat imageHeaderHeight = self.questionImageHeader.bounds.size.height;
    CGFloat answerTableViewTopPadding = 10.0f;
    CGFloat answerTableViewHeight = self.answerTableView.bounds.size.height;
    CGFloat descriptionLabelTopPadding = 30;
    CGFloat descriptionLabelHeight = 13;
    CGFloat starImageHeight= self.starImage.size.height;
    CGFloat contentExtraHeight;
    
    //handling iOS6 and iOS7 layout difference (?)
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        contentExtraHeight = 0;
    } else {
        contentExtraHeight = 77;
    }
    
    
    CGFloat totalContentHeight = imageHeaderHeight+answerTableViewHeight+answerTableViewTopPadding+descriptionLabelTopPadding+descriptionLabelHeight+starImageHeight+contentExtraHeight;
    
    self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, totalContentHeight);
    
    //set the description label and star frame too.
    
    CGFloat descriptionLabelX = 18;
    CGFloat descriptionLabelY;
    if (totalContentHeight > self.view.bounds.size.height) {
        DDLogInfo(@"Content longer than screen!");
        descriptionLabelY = totalContentHeight-contentExtraHeight-descriptionLabelHeight-10;
    } else {
        DDLogInfo(@"Content shorter than screen!");
        descriptionLabelY = self.scrollView.contentSize.height-descriptionLabelHeight-10;
    }
    
    CGFloat descriptionLabelWidth = 190;
    self.descriptionLabel.frame = CGRectMake(descriptionLabelX,
                                             descriptionLabelY,
                                             descriptionLabelWidth,
                                             descriptionLabelHeight);
    
    
    CGFloat starImageWidth = self.starImage.size.width;
    self.starImageView.frame = CGRectMake(self.descriptionLabel.frame.origin.x-starImageWidth,
                                          self.descriptionLabel.frame.origin.y-starImageHeight,
                                          starImageWidth,
                                          starImageHeight);
    
    //last - better safe than sorry.
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
}

#pragma mark - UISwipeGestureRecognizer methods

-(void)swipeRecognizerAction:(UISwipeGestureRecognizer *)recognizer {

    switch (recognizer.direction) {
        case UISwipeGestureRecognizerDirectionDown:
            {
                DDLogInfo(@"Swipe down received.");
            }
            break;
            
        case UISwipeGestureRecognizerDirectionUp:
            {
                DDLogInfo(@"Swipe up received.");
            }
            break;
            
        case UISwipeGestureRecognizerDirectionLeft:
            {
                if(self.selectedAnswerTracker.count > 0){
                DDLogInfo(@"Swipe left received.");
                [self nextButtonAction:recognizer];
                }
            }
            break;
            
        case UISwipeGestureRecognizerDirectionRight:
            {
                DDLogInfo(@"Swipe right received.");
                [self backButtonAction:recognizer];
            }
            break;
            
        default:
            {
                 DDLogInfo(@"Unrecognized swipe received.");
            }
            break;
    }
}

#pragma mark - Button actions
- (void)backButtonAction:(id)sender
{
    DDLogInfo(@"Back button clicked!");
    
    if (self.isSurveyPageMode) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        if(self.selectedAnswerTracker.count == 0){
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please select your changes.", @"Please select your changes.")];
            
        }else{
            [self.navigationController popViewControllerAnimated:YES];
            [self saveDataToDictionary];
        }
    }
}

- (void)nextButtonAction:(id)sender
{
    DDLogInfo(@"Next button clicked!");
    //save data to dictionary
    [self saveDataToDictionary];
    
    if (self.nextQuestion) {
        [self.navigationController pushViewController:self.nextQuestion animated:YES];
    } else {
        [self submitSurveyRequest];
    }
}

- (void)lockButtonAction:(id)sender
{
    DDLogInfo(@"Lock button clicked!");
    [self.lockButton setSelected: !self.lockButton.isSelected];
}

#pragma mark - UITableView delegates

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"AnswerCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    NSString *answer = [self.questionData.questionAnswers objectAtIndex:indexPath.row];
    cell.textLabel.text = answer;
    cell.backgroundView = [[UIView alloc]initWithFrame:CGRectZero];
    //cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
    cell.backgroundColor = [UIColor colorWithWhite:1.000 alpha:1.000];;
    cell.accessoryView =  [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico-check"]];
    
    if (indexPath.row < self.answerSelectedStatus.count) {
        cell.accessoryView.hidden = ![((NSNumber*)[self.answerSelectedStatus objectAtIndex:indexPath.row]) boolValue];
    } else {
        cell.accessoryView.hidden = YES;
    }
    
//    cell.accessoryView.hidden = ![((NSNumber*)[self.answerSelectedStatus objectAtIndex:indexPath.row]) boolValue];
    
    if (self.questionData.answerImages && (self.questionData.answerImages.count > indexPath.row)) {
        cell.imageView.image = [self.questionData.answerImages objectAtIndex:indexPath.row];
    }
    
    //add custom separator
    CGFloat customSeparatorLeftPadding = 20.0f;
    CGFloat customSeperatorRightPadding = 30.0f;
    UIView *customSeparator = [[UIView alloc] initWithFrame:CGRectMake(customSeparatorLeftPadding, cell.bounds.size.height-1, self.view.bounds.size.width-customSeparatorLeftPadding-customSeperatorRightPadding, 1)];
    customSeparator.backgroundColor = [UIColor colorWithWhite:0.918 alpha:1.000];
    [cell.contentView addSubview:customSeparator];
    return cell;
}

- (void)tableView: (UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    //return if it's locked
    if(!self.isSelectingAnswerEnabled) {
        DDLogInfo(@"Process locked!");
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        return;
    }
    
    self.isSelectingAnswerEnabled = NO; //lock the selection process
    
    DDLogInfo(@"Cell number %ld is clicked!", (long)indexPath.row);
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    
     //the cell isn't selected yet
    if (selectedCell.accessoryView.hidden) {
        
        //if the maximum is reached
        if ([self isMaximumSelectionReached]) {
            //deselect the most first selected number
            NSNumber *deselectedRow = ((NSNumber*) self.selectedAnswerTracker.firstObject);
            NSIndexPath *deselectedIndexPath = [NSIndexPath indexPathForRow:[deselectedRow intValue] inSection:indexPath.section];
            
            [[[tableView cellForRowAtIndexPath:deselectedIndexPath] accessoryView] setHidden:YES];
            [self.answerSelectedStatus replaceObjectAtIndex:[deselectedRow intValue] withObject:[NSNumber numberWithBool:NO]];
            [self.selectedAnswerTracker removeObjectAtIndex:0];
            DDLogInfo(@"Deselecting cell number %ld first.", (long)[deselectedRow integerValue]);
        }
        
        
        DDLogInfo(@"Selecting current cell!");
        
        selectedCell.accessoryView.hidden = NO;
        
        [self.answerSelectedStatus replaceObjectAtIndex:indexPath.row
                                             withObject:[NSNumber numberWithBool:YES]];
        
        [self.selectedAnswerTracker addObject:[NSNumber numberWithInteger:indexPath.row]];
    
    } else { //the cell is de-selected
        DDLogInfo(@"De-selecting current cell!");
        
        selectedCell.accessoryView.hidden = YES;
        [self.answerSelectedStatus replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:NO]];
        
        //remove the selected cell from tracker.
        NSUInteger count = self.selectedAnswerTracker.count;
        int buff = -1;
        NSNumber *removedCellNumber = nil;
        
        for (int i = 0; i < count; i++) {
            if( [((NSNumber*)[self.selectedAnswerTracker objectAtIndex:i]) intValue] == indexPath.row ) {
                removedCellNumber = [self.selectedAnswerTracker objectAtIndex:i];
                buff = i;
            }
        }
        
        if (removedCellNumber) {
            [self.selectedAnswerTracker removeObject:removedCellNumber];
            DDLogInfo(@"Removed cell from tracker at index %d!",buff);
        }
        
        
    }
    
    self.isSelectingAnswerEnabled = YES; //unlock the selection process
    
    //de-select the cell image
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    //check the maximum again for one more time - this will execute any proper settings due to maximum answer reached (i.e. disabling other text on pursuit page)
    [self isMaximumSelectionReached];
    
    //check whether the user has selected something, to enable the "next" button
    if(self.selectedAnswerTracker.count > 0) {
        [self enableRightBarButtonItem:YES];
    } else {
        [self enableRightBarButtonItem:NO];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    DDLogInfo(@"Question answers : %lu", (unsigned long)self.questionData.questionAnswers.count);
    return self.questionData.questionAnswers.count;
}

#pragma mark - AFNetworking requests

-(void)submitSurveyRequest {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        
    NSDictionary *parameters = self.questionData.referenceDictionary;
        
    [GTRUserRestService updateFitnessProfile:parameters onSuccess:^(id responseOject) {
        [self.view endEditing:YES];
        [SVProgressHUD dismiss];
        [self performSelector:@selector(dismissSurvey) withObject:nil afterDelay:1.0];
        
        #ifdef GTR_FLURRY_ANALYTICS
        [GTRAnalyticsHelper sendFitnessProfileChangedEvent];
        #endif

    } onFail:^(id operation, NSError *error) {
        [self.view endEditing:YES];
        [self performSelector:@selector(dismissSurvey) withObject:nil afterDelay:1.0];
    }];
}

#pragma mark - Custom initiators

-(void)initiateSelectedAnswerTracker
{
    self.selectedAnswerTracker = [[NSMutableArray alloc]init];
}

-(void)initiateSelectedAnswerStatus
{
    self.answerSelectedStatus = [[NSMutableArray alloc] init];
    int numberOfAnswers = ((int)self.questionData.questionAnswers.count);
    
    for (int i = 0; i < numberOfAnswers; i++) {
        
        [self.answerSelectedStatus addObject:[NSNumber numberWithBool:NO]];
    }
}

-(void)initiateLockButton
{
    self.lockButton = [[UIButton alloc] init];
}

- (void)saveDataToDictionary
{
    DDLogInfo(@"Save data to dictionary executed!");
    NSMutableDictionary *inputData = [[NSMutableDictionary alloc] init];
    NSNumber *private = [NSNumber numberWithInteger:([self isAnswerLocked] ? 1 : 0)];
    
    if (self.questionData.questionType == SingleSelectionAnswer) {
        DDLogInfo(@"Single Selection Answer!");
        NSNumber *value = ((NSNumber*)[[self getSelectedAnswers] firstObject]);
        
        if (value) {
            [inputData setObject:value forKey:@"value"];
            [inputData setObject:private forKey:@"private"];
        }
        
    } else if(self.questionData.questionType == UpToTwoSelectionAnswer) {
        DDLogInfo(@"UpToTwo Selection Answer!");
        NSArray *value = [self getSelectedAnswers];
        DDLogInfo(@"Values : %@", value);
        [inputData setObject:value forKey:@"value"];
        [inputData setObject:private forKey:@"private"];
    }
    
    if (self.questionData.referenceDictionary && self.questionData.referenceName) {
        DDLogInfo(@"Adding to Dictionary!");
        [self.questionData.referenceDictionary setValue:inputData forKey:self.questionData.referenceName];
    }
    
    DDLogInfo(@"%@",self.questionData.referenceDictionary);
}

#pragma mark - Survey private info setter/getter

-(BOOL)isAnswerLocked
{
    return self.lockButton.isSelected;
}

-(void)setAnswerLocked:(BOOL)theLockedStatus
{
    [self.lockButton setSelected:theLockedStatus];
}

#pragma mark - Custom getters

-(NSArray*)getAnswerStatusArray
{
    return [[NSArray alloc] initWithArray:self.answerSelectedStatus];
}

- (BOOL) isMaximumSelectionReached
{
    BOOL status = NO;
    
    if (self.questionData.questionType != SingleStringAnswer) {
        int maximumNumber = self.questionData.questionType == SingleSelectionAnswer ? 1 : 2; //if not single, then up to two
        int selectedAnswers = 0;
        
        for(NSNumber *cellStatus in self.answerSelectedStatus) {
             //if the answer is selected
            if([cellStatus boolValue]) {
                selectedAnswers++;
            }
        }
        
        //update the status
        status = !(selectedAnswers < maximumNumber);
        
        DDLogInfo(@"Maximum Number : %d, selected Answers : %d, isMaximumReached : %s", maximumNumber, selectedAnswers, status ? "YES":"NO");
    }
    
    return status;
}

- (NSArray*)getSelectedAnswers
{
    return self.selectedAnswerTracker;
}


#pragma mark - Custom actions

-(void)dismissSurvey
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

-(void)enableRightBarButtonItem:(BOOL)barButtonEnabled
{
    DDLogInfo(@"enableRightBarButtonItem invoked! status : %s", barButtonEnabled? "YES":"NO");
    UIBarButtonItem *barButtonItem = self.navigationItem.rightBarButtonItem;
    barButtonItem.enabled = barButtonEnabled;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        if (barButtonEnabled) {
            [barButtonItem setTintColor:[UIColor whiteColor]];
        } else {
            [barButtonItem setTintColor:[UIColor colorWithWhite:1.0f alpha:0.50f]];
        }
    }
}

- (void)deselectAllAnswers
{
    //clear the answer tracker
    [self.selectedAnswerTracker removeAllObjects];
    
    //reset the selectedAnswerStatus
    [self initiateSelectedAnswerStatus];

    [self.answerTableView reloadData];
}

- (void)updateAnswerSelectedStatus
{
    DDLogInfo(@"Updating answer selected status!");
    
    if (self.selectedAnswerTracker) {
        
        for (NSNumber *answerIndex in self.selectedAnswerTracker) {
            DDLogInfo(@"Updating index number %@",answerIndex);
            
            //handling if NSNumber value is 0
            if ([answerIndex integerValue]) {
                DDLogInfo(@"Value is valid!");
                [self.answerSelectedStatus replaceObjectAtIndex:[answerIndex integerValue] withObject:[NSNumber numberWithBool:YES]];
            } else {
                DDLogInfo(@"Value is invalid!");
                [self.answerSelectedStatus replaceObjectAtIndex:0 withObject:[NSNumber numberWithBool:YES]];
            }
            
        }
        
    } else {
        DDLogInfo(@"selectedAnswerTracker not found!");
    }
    
    
    if (self.answerTableView) {
        [self.answerTableView reloadData];
        DDLogInfo(@"Table reloaded!");
    } else {
        DDLogInfo(@"Table not found!");
    }
    
}

@end
