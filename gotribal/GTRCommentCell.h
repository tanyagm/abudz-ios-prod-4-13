//
//  GTRCommentCell.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/12/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRComment.h"

@interface GTRCommentCell : UITableViewCell
- (void) setContent:(GTRComment*)inpComment;
- (CGFloat) getHeight;
@end
