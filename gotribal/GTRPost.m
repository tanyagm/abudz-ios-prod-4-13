//
//  GTRPost.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/1/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRPost.h"
#import "GTRTimeFormatterHelper.h"

@implementation GTRPost

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{ @"postID": @"id",
              @"message" : @"message",
              @"imageURL" : @"image",
              @"locationName" : @"location_name",
              @"lat": @"lat",
              @"lon": @"lon",
              @"liked": @"liked",
              @"likesCount": @"likes_count",
              @"createdAt": @"created_at",
              @"owner": @"user",
              @"commentsCount": @"comments_count"
              };
}

+(NSValueTransformer *)ownerJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:GTRUser.class];
}

-(NSString*) humanDate
{
    return [GTRTimeFormatterHelper relativeTimeStringFromEpoch:self.createdAt];
}

-(void) like:(NSNumber *)likesCount
{
    if (!self.liked){
        self.liked = TRUE;
        self.likesCount = likesCount;
    }
}

-(void) unlike:(NSNumber *)likesCount
{
    if (self.liked){
        self.liked = FALSE;
        self.likesCount = likesCount;
    }
}


@end
