//
//  GTREditFitnessProfileViewController.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/25/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRSurveyQuestionData.h"

@interface GTREditFitnessProfileViewController : GTRViewController
@property (strong, nonatomic) NSArray *fitnessProfileName;
@property (strong,nonatomic) UIScrollView* scrollView;
@property (strong,nonatomic) UITableView *tableView;

-(void)setAppearance;

@end
