//
//  GTRUserDetail.h
//  gotribal
//
//  Created by Sambya Aryasa on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mantle.h"

@interface GTRUserDetail : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString *detailKey;
@property (nonatomic, assign) BOOL detailPrivate;
@property (nonatomic, strong) id detailValue;

@end
