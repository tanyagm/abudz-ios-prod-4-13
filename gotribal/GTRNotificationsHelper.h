//
//  GTRNotificationsHelper.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRSettingsHelper.h"
#import "GTRPendingRequestCell.h"

@interface GTRNotificationsHelper : NSObject

+(UIView*)createHeaderViewWithText:(NSString*)text withFont:(UIFont*)font;
+(UIView*)createGroupedViewInCell:(GTRPendingRequestCell*)cell
                      atIndexPath:(NSIndexPath*)indexPath;

@end
