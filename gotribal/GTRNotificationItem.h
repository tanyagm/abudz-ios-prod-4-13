//
//  GTRNotificationItem.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/16/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "MTLModel.h"

typedef enum{
    GTRNotificationItemTargetTypePost = 0,
    GTRNotificationItemTargetTypeActiveBudz
}GTRNotificationItemTargetType;

@interface GTRNotificationItem : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSNumber *notificationItemID;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSNumber *timestamp;
@property (strong, nonatomic) GTRUser *user;
@property (strong, nonatomic) NSNumber *targetID;
@property (strong, nonatomic) NSNumber *targetType;

-(NSString*) relativeTimestamp;

@end
