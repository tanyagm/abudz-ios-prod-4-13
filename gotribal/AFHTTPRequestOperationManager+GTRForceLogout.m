//
//  AFHTTPRequestOperationManager+GTRForceLogout.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/6/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "AFHTTPRequestOperationManager+GTRForceLogout.h"
#import "GTRAppDelegate.h"
#import "GTRIntroViewController.h"
#import "GTRMainActivitiesViewController.h"

@implementation AFHTTPRequestOperationManager(GTRForceLogout)
- (AFHTTPRequestOperation *)GET:(NSString *)URLString
                     parameters:(NSDictionary *)parameters
                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
      forceLogoutOnUnauthorized:(BOOL)forceLogout
{
    return [self GET:URLString
          parameters:parameters
             success:success
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 BOOL doCallback = TRUE;
                 if (forceLogout){
                     NSInteger statusCode = [operation.response statusCode];
                     if (statusCode == 401){
                         [self forceLogout];
                         doCallback = FALSE;
                     }
                 }
                 if (doCallback){
                     failure(operation, error);
                 }
             }];
}

- (AFHTTPRequestOperation *)DELETE:(NSString *)URLString
                        parameters:(NSDictionary *)parameters
                           success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
         forceLogoutOnUnauthorized:(BOOL)forceLogout
{
    return [self DELETE:URLString
             parameters:parameters
                success:success
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    BOOL doCallback = TRUE;
                    if (forceLogout){
                        NSInteger statusCode = [operation.response statusCode];
                        if (statusCode == 401){
                            [self forceLogout];
                            doCallback = FALSE;
                        }
                    }
                    if (doCallback){
                        failure(operation, error);
                    }
                }];
}

- (void) forceLogout
{
    GTRAppDelegate *appDelegate = (GTRAppDelegate *)[[UIApplication sharedApplication] delegate];
    [GTRUserHelper removeUserCredentials];
    GTRIntroViewController *introViewController = [[GTRIntroViewController alloc] init];
    UINavigationController *introNavigationController = [[UINavigationController alloc] initWithRootViewController:introViewController];
    
    [appDelegate.menuViewController presentViewController:introNavigationController animated:YES completion:nil];
    
    GTRMainActivitiesViewController *mainActivities = [[GTRMainActivitiesViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mainActivities];
    appDelegate.menuViewController.centerViewController = navigationController;

}

@end
