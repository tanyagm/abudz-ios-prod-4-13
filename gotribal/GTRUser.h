//
//  GTRUser.h
//  gotribal
//
//  Created by Sambya Aryasa on 11/28/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRAvatar.h"
#import "GTRUserDetail.h"
#import "Mantle.h"

@interface GTRUser : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSNumber *userID;
@property (strong, nonatomic) NSString *avatarURL;
@property (strong, nonatomic) NSArray *details;
@property (strong, nonatomic) NSNumber *tribalRank;
@property (strong, nonatomic, readonly) NSDate *birthday;
@property BOOL isFriend;
@property BOOL endorsed;
@property NSInteger friendsCount;

-(GTRUserDetail *)birthdayDetail;
-(NSString *)fullName;
-(int) userRank;

@end
