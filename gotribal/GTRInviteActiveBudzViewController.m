//
//  GTRInviteActiveBudzViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/20/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRInviteActiveBudzViewController.h"
#import "GTRActivebudzInviteTableViewCell.h"
#import "GTRActiveBudzRestService.h"
#import "GTRUser.h"
#import "GTRConstants.h"
#import "NSMutableArray+AddMantleObject.h"
#import "UIImageView+AFNetworking.h"
#import "SVProgressHUD.h"
#import "SVPullToRefresh.h"
#import "GTRTribeRestService.h"

NSString* const INVITE_ACTIVEBUDZ_TITLE = @"Activebudz";
NSString* const INVITED_ACTIVEBUDZ_KEY = @"invitedActiveBudzWithTribe-%@";

/*
 *  GTRUser+PersonInvited category
 */
#import <objc/runtime.h>

@interface GTRUser (PersonInvited)
@property (nonatomic) BOOL invited;
@property (nonatomic) BOOL memberJoined;
@end

static char const * const kPersonInvitedKey = "PersonInvited";
static char const * const kMemberJoinedKey = "MemberJoined";


@implementation GTRUser (PersonInvited)

-(BOOL)invited
{
    NSNumber *number = objc_getAssociatedObject(self, &kPersonInvitedKey);
    return [number boolValue];
}

-(void)setInvited:(BOOL)invited
{
    objc_setAssociatedObject(self, &kPersonInvitedKey, @(invited), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)memberJoined
{
    NSNumber *number = objc_getAssociatedObject(self, &kMemberJoinedKey);
    return [number boolValue];
}

-(void)setMemberJoined:(BOOL)memberJoined
{
    objc_setAssociatedObject(self, &kMemberJoinedKey, @(memberJoined), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end

@interface GTRInviteActiveBudzViewController ()

@property (nonatomic,strong) NSMutableArray *activeBudzArray;
@property (nonatomic,strong) NSNumber *maxID;
@property (nonatomic,strong) NSMutableArray *invitedActiveBudz;
@property (strong, nonatomic) UILabel *placeholderLabel;
@property (strong,nonatomic) GTRTribe* tribe;

@end

@implementation GTRInviteActiveBudzViewController
@synthesize tribe;


-(id)initWithTribe:(GTRTribe *)theTribe
{
    self = [super init];
    if (self) {
        tribe = theTribe;
        self.title = INVITE_ACTIVEBUDZ_TITLE;
        self.tableView.separatorColor = [UIColor clearColor];
        self.activeBudzArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = GTR_MAIN_ACTIVITIES_BACKGROUND_COLOR;
    NSArray *invitedActiveBudzBuff = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:INVITED_ACTIVEBUDZ_KEY, self.tribe.tribeID]];
    
    self.invitedActiveBudz = [NSMutableArray arrayWithArray:invitedActiveBudzBuff];
    
    [GTRNavigationBarHelper setBackButtonItem:self.navigationItem withTarget:self.navigationController selector:@selector(popViewControllerAnimated:)];
    __weak GTRInviteActiveBudzViewController *weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf reloadActiveBudzList:GTRReloadModeRefresh];
    }];
    
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf reloadActiveBudzList:GTRReloadModeLoadMore];
    }];
    
    [self reloadActiveBudzList:GTRReloadModeRefresh];
}

#pragma mark - UITableView delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!self.placeholderLabel) {
        [self setupPlaceholderLabel];
    }
    
    (self.activeBudzArray.count) ? (self.placeholderLabel.hidden = YES) : (self.placeholderLabel.hidden = NO);
    
    return self.activeBudzArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 34 + 10 + 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *const CellIdentifier = @"GTRUserCell";
    GTRActivebudzInviteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[GTRActivebudzInviteTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0f];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    GTRUser *user = (GTRUser*)[self.activeBudzArray objectAtIndex:indexPath.row];
    
    NSString *profileImageURL = user.avatarURL;
    NSURL *URL = [NSURL URLWithString:profileImageURL];
    cell.textLabel.text = [user fullName];
    [cell.imageView setImageWithURL:URL placeholderImage:DEFAULT_AVATAR];
    [cell.inviteButton addTarget:self action:@selector(inviteButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if (user.memberJoined) {
        cell.inviteState = GTRActivebudzInviteStateJoined;
    } else if (user.invited) {
        cell.inviteState = GTRActivebudzInviteStateInvited;
    } else {
        cell.inviteState = GTRActivebudzInviteStateUninvited;
    }

    return cell;
}

#pragma mark - Button actions

-(void)inviteButtonAction:(id)sender
{
    //retrieve the indexpath
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    GTRUser *anUser = [self.activeBudzArray objectAtIndex:indexPath.row];
    
    if (anUser.invited) return;
    if (anUser.memberJoined) return;
    
    //send the invitation
    if (indexPath && tribe) {
        
        NSNumber *friendsID = ((GTRUser*)[self.activeBudzArray objectAtIndex:indexPath.row]).userID;
        NSDictionary *parameters = @{
                                     @"platform_type": @"gotribal",
                                     @"user_id" : friendsID,
                                     };
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        
        [GTRTribeRestService inviteToTribe:tribe parameters:parameters onSuccess:^(id responseObject) {
            
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Invitation sent!", @"invite sent")];
            
            GTRUser *user = [self.activeBudzArray objectAtIndex:indexPath.row];
            user.invited = YES;
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            [self.invitedActiveBudz addObject:user.userID];
            [[NSUserDefaults standardUserDefaults] setObject:self.invitedActiveBudz forKey:[NSString stringWithFormat:INVITED_ACTIVEBUDZ_KEY, self.tribe.tribeID]];
            
            // TODO : RPS - add action / delegate here to refresh the tribe page if needed
        
        } onFail:^(id responseObject, NSError *error) {
        }];
    }
}

#pragma mark - Custom actions

-(void)removePersonFromList:(NSIndexPath*)theIndexPath
{
    [self.tableView beginUpdates];
    [self.activeBudzArray removeObjectAtIndex:theIndexPath.row];
    [self.tableView deleteRowsAtIndexPaths:@[theIndexPath] withRowAnimation:YES];
    [self.tableView endUpdates];
}

-(void)reloadActiveBudzList:(GTRReloadMode)theReloadMode
{
    
    NSNumber *buffMaxID = self.maxID;
    
    if (theReloadMode == GTRReloadModeRefresh) {
        [self.activeBudzArray removeAllObjects];
        self.maxID = nil;
    }
    
    [GTRTribeRestService retrieveInviteableTribeMembers:self.tribe MaxID:self.maxID OnSuccess:^(id responseObject) {
        self.maxID = [[responseObject objectForKey:@"pagination"] objectForKey:@"max_id"];
        
        NSArray *buffActiveBudz = [responseObject objectForKey:@"data"];
        
        for (NSDictionary *anUser in buffActiveBudz) {
            GTRUser *user = [[GTRUser alloc] init];
            user.firstName = anUser[@"first_name"];
            user.lastName = anUser[@"last_name"];
            user.userID = anUser[@"id"];
            user.avatarURL = anUser[@"avatar_url"];
            BOOL isMemberJoined = [anUser[@"tribe_member"] boolValue];
            user.memberJoined = isMemberJoined;
            [self.activeBudzArray addObject:user];
        }
        
        [self checkInvitedActiveBudz];
        
        [self.tableView reloadData];
        
        [self.tableView.infiniteScrollingView stopAnimating];
        [self.tableView.pullToRefreshView stopAnimating];
    } onFail:^(id responseObject, NSError *error) {
        [self.tableView.infiniteScrollingView stopAnimating];
        [self.tableView.pullToRefreshView stopAnimating];
        self.maxID = buffMaxID;
    }];
}

-(void)setupPlaceholderLabel
{
    NSString *placeholderString = NSLocalizedString(@"No Budz yet? Your Activebudz community is new and growing. Invite your friends and connect with them here!",
                                              @"NoActiveBudzPlaceholder");
    
    NSAttributedString *placeholderText = [[NSAttributedString alloc] initWithString:placeholderString
                                                                          attributes:@{
                                                                                       NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16.0f],
                                                                                       NSForegroundColorAttributeName : [UIColor colorWithWhite:0.333 alpha:1.000]
                                                                                     }];

    CGFloat placeholderHorizontalPadding = 8;
    CGFloat placeholderVerticalPadding = 20;
    CGFloat placeholderY = 0;
    CGFloat placeholderHeight = self.view.bounds.size.height - (placeholderVerticalPadding*2);
    CGFloat placeholderWidth = self.view.bounds.size.width - (placeholderHorizontalPadding*2);
    
    self.placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(placeholderHorizontalPadding,
                                                                      placeholderY,
                                                                      placeholderWidth,
                                                                      placeholderHeight)];
    
    self.placeholderLabel.hidden = YES;
    
    [self.tableView addSubview:self.placeholderLabel];
    
    self.placeholderLabel.backgroundColor = [UIColor clearColor];
    self.placeholderLabel.attributedText = placeholderText;
    self.placeholderLabel.textAlignment = NSTextAlignmentCenter;
    self.placeholderLabel.numberOfLines = 0;
    self.placeholderLabel.adjustsFontSizeToFitWidth = NO;
    self.placeholderLabel.contentMode = UIViewContentModeCenter;
    self.placeholderLabel.backgroundColor = [UIColor clearColor];
    [self.placeholderLabel sizeToFit];
    self.placeholderLabel.center = self.tableView.center;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.placeholderLabel.frame = CGRectOffset(self.placeholderLabel.frame, 0, -32);
    }
}

-(void)checkInvitedActiveBudz
{
    BOOL found;
    int i;
    int count;
    for (NSNumber *userID in self.invitedActiveBudz) {
        found = false;
        i = 0;
        count = self.activeBudzArray.count;
        
        while (i < count && !found) {
            GTRUser *user = (GTRUser*)[self.activeBudzArray objectAtIndex:i];
            if ([user.userID isEqual:userID]) {
                found = true;
                user.invited = YES;
            } else {
                i++;
            }
        }
    }
}

@end
