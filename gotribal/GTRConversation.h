//
//  GTRConversation.h
//  gotribal
//
//  Created by Muhammad Taufik on 11/29/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "MTLModel.h"

typedef NS_ENUM(NSUInteger, GTRConversationState) {
    GTRConversationStateUnread,
    GTRConversationStateRead
};

@class GTRUser;

@interface GTRConversation : MTLModel <MTLJSONSerializing>
@property (nonatomic, copy) NSNumber *conversationID;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, assign) GTRConversationState unreadState;
@property (nonatomic, copy) NSNumber *updatedAt;
@property (nonatomic, strong) GTRUser *user;
@end
