//
//  GTRSurveyLastPageViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/22/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSurveyCityPageViewController.h"
#import "SVProgressHUD.h"

@interface GTRSurveyCityPageViewController () <UITextFieldDelegate>

@property (strong, nonatomic) UITextField *cityTextField;

@end

@implementation GTRSurveyCityPageViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(id)initWithReferenceDictionary:(NSMutableDictionary *)theReferenceDictionary;
{
    GTRSurveyQuestionData *questionData = [[GTRSurveyQuestionData alloc] initWithQuestionType:SingleStringAnswer
                                                                               questionImages:nil
                                                                              questionAnswers:nil
                                                                                 answerImages:nil
                                                                          referenceDictionary:theReferenceDictionary referenceName:USER_DETAIL_LIVING_PLACE_KEY];
    //initiate with superclass
    self = [super initWithQuestionNumber:11
                          totalQuestions:11
                            questionText:@"Where do you spend most of your time?"
                            questionData:questionData
                            nextQuestion:nil];
    //initiate the text field.
    self.cityTextField = [[UITextField alloc] init];
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(self.cityTextField.text.length != 0) {
        [self enableRightBarButtonItem:YES];
    } else {
        [self enableRightBarButtonItem:NO];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	//add text field.
    CGFloat cityTextFieldWidth = self.answerTableView.bounds.size.width;
    CGFloat cityTextFieldHeight = 25.0;
    CGFloat cityTextFieldLeftPadding = 20.0f;
    self.cityTextField.frame = CGRectMake(cityTextFieldLeftPadding,
                                           0,
                                           cityTextFieldWidth,
                                           cityTextFieldHeight);
    
    self.cityTextField.placeholder = @"City or Postal Code";
    self.cityTextField.backgroundColor = [UIColor clearColor];
    [self.cityTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.footerView addSubview:self.cityTextField];
    
    //add right bar button
    
    if (self.isSurveyPageMode) {
        //set navigation bar next button
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(nextButtonAction:)];
    }
    
    //add custom separator.
    CGFloat customSeparatorLeftPadding = 20.0f;
    CGFloat customSeparatorRightPadding = 30.0f;
    UIView *customSeparator = [[UIView alloc] initWithFrame:CGRectMake(customSeparatorLeftPadding,
                                                                       cityTextFieldHeight-1,
                                                                       self.answerTableView.bounds.size.width-customSeparatorLeftPadding-customSeparatorRightPadding,
                                                                       1)];
    customSeparator.backgroundColor = [UIColor colorWithWhite:0.918 alpha:1.000];
    [self.footerView addSubview:customSeparator];
    
    //change the lock button frame.    
    CGFloat lockButtonTopPadding = 200.0f;
    CGFloat extraHeight = cityTextFieldHeight+1+lockButtonTopPadding;
    
    CGFloat lockButtonX = self.lockButton.frame.origin.x;
    CGFloat lockButtonY = self.lockButton.frame.origin.y+extraHeight;
    CGFloat lockButtonWidth = self.lockButton.bounds.size.width;
    CGFloat lockButtonHeight = self.lockButton.bounds.size.height;

    self.lockButton.frame = CGRectMake(lockButtonX,
                                       lockButtonY,
                                       lockButtonWidth,
                                       lockButtonHeight);
    self.lockButton.contentEdgeInsets = UIEdgeInsetsMake(5, 35, 0, 0);
    
    //resize the footer frame
    self.footerView.frame = CGRectMake(0, 0, self.answerTableView.bounds.size.width, self.answerTableView.bounds.size.height-
                                       (lockButtonY+lockButtonHeight+50+extraHeight));
    
    //resize the tableview to fit the contents.
    self.answerTableView.frame = CGRectMake(self.answerTableView.frame.origin.x,
                                            self.answerTableView.frame.origin.y,
                                            self.answerTableView.frame.size.width,
                                            self.answerTableView.frame.size.height+lockButtonTopPadding);
    [self autoSetScrollViewSize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSString*)getCityText
{
    NSString *cityText;
    if(self.cityTextField.text)
    {
        cityText = self.cityTextField.text;
    }else{
        cityText = @"";
    }
    return cityText;
}

-(void)setCityText:(NSString*)theCityName
{
    self.cityTextField.text = theCityName;
}


- (void)textFieldDidChange:(id)sender
{
    if(self.cityTextField.text.length > 0)
    {
        [self enableRightBarButtonItem:YES];
    }else{
        [self enableRightBarButtonItem:NO];
    }
}

- (void)saveDataToDictionary
{

    if(self.questionData.referenceDictionary && self.questionData.referenceName)
    {
        NSNumber *private = [NSNumber numberWithInteger:([self isAnswerLocked] ? 1 : 0)];
        
        NSDictionary *inputData = @{
                                   @"private":private,
                                   @"value":@[ [self getCityText] ]
                                   };
        
        [self.questionData.referenceDictionary setValue:inputData forKey:self.questionData.referenceName];
        DDLogInfo(@"CITY REFERENCE DATA : %@",self.questionData.referenceDictionary);
    }

}

- (void)backButtonAction:(id)sender
{
    DDLogInfo(@"Back button clicked!");
    if(self.isSurveyPageMode){
        [self.navigationController popViewControllerAnimated:YES];
        if(!self.isSurveyPageMode){
            [self saveDataToDictionary];
        }
    }else{
        if(self.cityTextField.text.length ==0){
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please set your location.", @"Please set your location.")];

        }else{
            [self.navigationController popViewControllerAnimated:YES];
            if(!self.isSurveyPageMode){
                [self saveDataToDictionary];
            }
        }
        
    }
}

@end
