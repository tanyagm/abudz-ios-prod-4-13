//
//  GTRTribeError.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/18/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRTribeErrorView.h"

@protocol GTRTribeErrorDelegate <NSObject>

@optional
-(void)didShowErrorView:(GTRTribeErrorView*)errorView;
-(void)didDismissErrorView:(GTRTribeErrorView*)errorView;

@end

@interface GTRTribeError : NSObject

@property(nonatomic,weak) id <GTRTribeErrorDelegate>delegate;
@property(nonatomic,assign,getter = isShown) BOOL shown;

+(GTRTribeError*)sharedInstance;
-(void)showInView:(UIView*)superView withTitle:(NSString*)title message:(NSString*)message;
-(void)dismiss;

@end
