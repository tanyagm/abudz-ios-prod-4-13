//
//  GTRPost.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/1/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "MTLModel.h"

@interface GTRPost : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSNumber *postID;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *imageURL;
@property (strong, nonatomic) NSString *locationName;
@property (strong, nonatomic) NSNumber *lat;
@property (strong, nonatomic) NSNumber *lon;
@property (nonatomic) BOOL liked;
@property (strong, nonatomic) NSNumber *likesCount;
@property (strong, nonatomic) NSNumber *commentsCount;
@property (strong, nonatomic) NSNumber *createdAt;
@property (strong, nonatomic) GTRUser *owner;

-(void) like:(NSNumber *)likesCount;
-(void) unlike:(NSNumber *)likesCount;
-(NSString*) humanDate;
@end
