//
//  GTRProfileViewController.h
//  gotribal
//
//  Created by Sambya Aryasa on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GTRProfileViewControllerDelegate

-(void)reloadMyProfile;

@end

@interface GTRProfileViewController : GTRViewController

@property (weak) id <GTRProfileViewControllerDelegate> delegate;
- (id)initWithFromSetting:(BOOL)isFromSetting;

@end
