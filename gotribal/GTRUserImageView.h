//
//  GTRUserImageView.h
//  gotribal
//
//  Created by loaner on 12/18/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    Left,
    Center,
    Right
}position;

@interface GTRUserImageView : UIImageView
@property (strong,nonatomic)UIImageView *userPicture;
@property (strong,nonatomic)UIImageView *badgePicture;

- (id) initWithFrame:(CGRect)frame;
-(void)setUserPictureWithURL:(NSURL *)imageURL badgeLevel:(int)userRank badgePosition:(position)badgePosition;

@end
