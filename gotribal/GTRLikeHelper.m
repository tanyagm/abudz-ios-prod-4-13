//
//  GTRLikeHelper.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/18/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRLikeHelper.h"
#import "GTRPostRestService.h"

@implementation GTRLikeHelper
+ (void) toggleLike:(GTRPost*) post
          onSuccess:(void (^)(GTRPost*))successAction
             onFail:(void (^)(id, NSError *))errorHandler
{
    if (post.liked){
        [GTRPostRestService unlikePostWithPostID:post.postID onSuccess:^(id response) {
            NSNumber *likesCount = [response objectForKey:@"likes_count"];
            [post unlike:likesCount];
            successAction(post);
        } onFail:nil];
    } else {
        [GTRPostRestService likePostWithPostID:post.postID onSuccess:^(id response) {
            NSNumber *likesCount = [response objectForKey:@"likes_count"];
            [post like:likesCount];
            successAction(post);
        } onFail:nil];
    }
}
@end

