//
//  GTRTimeFormatterHelper.h
//  gotribal
//
//  Created by Sambya Aryasa on 1/1/14.
//  Copyright (c) 2014 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTRTimeFormatterHelper : NSObject
+(NSString*)relativeTimeStringFromEpoch:(NSNumber*)epochTime;
@end
