//
//  GTRSideMenuCell.h
//  gotribal
//
//  Created by Hisma Mulya S on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

#define HEADER_FONT [UIFont fontWithName:@"HelveticaNeue-Light" size:17]

@interface GTRSideMenuCell : UITableViewCell

@property(nonatomic,strong) UIImageView *avatarImageView;
@property(nonatomic,strong) UILabel *text;
@property(nonatomic,strong) UIImageView *rightImageView;
@property(nonatomic,strong) UIImageView *lineImageView;

@end
