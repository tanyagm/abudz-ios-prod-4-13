//
//  GTRFitnessLocationPageViewController.h
//  gotribal
//
//  Created by loaner on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRSurveyCityPageViewController.h"

@interface GTRFitnessLocationPageViewController : GTRSurveyCityPageViewController

-(id)initWithTitle:(NSString *)title
        withHeader:(BOOL)header
  withQuestionType:(BOOL)questionType
  isSurveyPageMode:(BOOL)value
referenceDictionary:(NSMutableDictionary *)theReferenceDictionary;

@end
