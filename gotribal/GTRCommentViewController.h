//
//  GTRPostCommentViewController.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/12/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRUser.h"
#import "GTRPost.h"
#import "GTRComment.h"

@protocol GTRCommentViewControllerDelegate <NSObject>
- (void) onPostUpdated:(NSInteger)indexRow;
- (void) onPostDeleted:(NSInteger)indexRow;
@end

@interface GTRCommentViewController : GTRViewController
@property id<GTRCommentViewControllerDelegate> delegate;
- (id)initWithPost:(GTRPost *)inPost rowID:(NSInteger)aRowID;
@end
