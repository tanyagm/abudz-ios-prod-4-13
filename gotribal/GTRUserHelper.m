//
//  GTRUserHelper.m
//  gotribal
//
//  Created by Le Vady on 11/30/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRUserHelper.h"
#import "GTRAppDelegate.h"

@implementation GTRUserHelper

+(GTRUser *)getCurrentUser
{
    GTRAppDelegate *appDelegate = (GTRAppDelegate *)[UIApplication sharedApplication].delegate;
    if (!appDelegate.currentUser){
        appDelegate.currentUser = [self unarchiveCurrentUser];
    }
    return appDelegate.currentUser;
}

+(GTRUser *)unarchiveCurrentUser
{
    NSData *archivedCurrentUser = [[NSUserDefaults standardUserDefaults] valueForKey:CURRENT_USER_KEY];
    
    if (archivedCurrentUser) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:archivedCurrentUser];
    } else {
        return nil;
    }
}

+(void)updateCurrentUser:(NSDictionary *)params
{
    GTRAppDelegate *appDelegate = (GTRAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.currentUser = [MTLJSONAdapter modelOfClass:GTRUser.class fromJSONDictionary:params error:nil];
    
    NSData *archivedCurrentUser = [NSKeyedArchiver archivedDataWithRootObject:appDelegate.currentUser];
    [[NSUserDefaults standardUserDefaults] setObject:archivedCurrentUser forKey:CURRENT_USER_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)removeUserCredentials
{
    [AFOAuthCredential deleteCredentialWithIdentifier:[[NSURL URLWithString:BASE_URL] host]];
    
    NSString *domainName = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:domainName];
}

+(NSString *)getUDID
{
    NSString *udid = [UICKeyChainStore stringForKey:UDID_KEY];
    
    if (udid==nil)
    {
        udid = [self createUuid];
        [UICKeyChainStore setString:udid forKey:UDID_KEY];
    }
    
    return udid;
}

+(NSArray*)getArchivedUserConnections
{
    NSData *archive = [[NSUserDefaults standardUserDefaults] valueForKey:USER_CONNECTION_KEY];
    
    if (archive) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:archive];
    } else {
        return [[NSArray alloc] init];
    }
}

+(void)archiveUserConnections:(NSArray *)theUserConnections
{
    NSData *archive = [NSKeyedArchiver archivedDataWithRootObject:theUserConnections];
    [[NSUserDefaults standardUserDefaults] setObject:archive forKey:USER_CONNECTION_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)removeCurrentUser
{
    GTRAppDelegate *appDelegate = (GTRAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.currentUser = nil;
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_USER_KEY];
}

#pragma mark Private methods

+ (NSString*)createUuid
{
    CFUUIDRef newUniqueId = CFUUIDCreate(kCFAllocatorDefault);
    NSString * uuidString = (__bridge_transfer NSString*)CFUUIDCreateString(kCFAllocatorDefault, newUniqueId);
    CFRelease(newUniqueId);
    
    return uuidString;
}

@end
