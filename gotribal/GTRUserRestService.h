//
//  GTRUserRestService.h
//  gotribal
//
//  Created by Le Vady on 11/30/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    GTRUserRestServiceFriendRequestConfirm = 0,
    GTRUserRestServiceFriendRequestIgnore
} GTRUserRestServiceFriendRequest;

@interface GTRUserRestService : NSObject

+(void)retrieveCurrentUserOnSuccess:(void(^)(id responseObject))success onFail:(void (^) (id responseObject, NSError *error))errorHandler;

+(void)registerDevice:(NSString *)deviceToken;

+(void)updateFitnessProfile:(NSDictionary *)params onSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler;

+(void)retrieveCurrentUserPendingRequestWithLastMaxID:(NSNumber*)lastMaxID
                                            onSuccess:(void(^)(id responseObject))success
                                               onFail:(void (^) (id response, NSError *error))fail;

+(void)retrieveCurrentUserPendingRequestonSuccess:(void(^)(id responseObject))success
                                           onFail:(void (^) (id response, NSError *error))fail;

+(void)sendConfirmationWithType:(GTRUserRestServiceFriendRequest)type withUserID:(NSNumber*)userID onSuccess:(void(^)(id responseObject))success onFail:(void (^) (id response, NSError *error))fail;

+(void)recoverPassword:(NSString *)email onSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler;
@end
