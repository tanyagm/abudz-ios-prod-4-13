//
//  GTRSurveyPageGenerator.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/22/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSurveyPageGenerator.h"
#import "GTRSurveyPageTemplateViewController.h"
#import "GTRSurveyCityPageViewController.h"
#import "GTRSurveyPursuitPageViewController.h"
#import "GTRSurveyRolePreferencePageViewController.h"
#import "GTRSurveyRolePageViewController.h"
#import "GTRConstants.h"

@interface GTRSurveyPageGenerator()

@end

@implementation GTRSurveyPageGenerator
NSArray *temp;
- (id) init
{
    return [self initWithReferenceDictionary:nil];
}

- (id)initWithReferenceDictionary:(NSMutableDictionary*)theDictionary
{
    self.referenceDictionary = theDictionary;
    [self initiateSurveyQuestionPages];
    return self;
}

-(NSArray*) getSurveyPagesWithDestination:(UIViewController*)theDestinationViewController
{
    NSUInteger count = self.surveyQuestionPages.count;
    ((GTRSurveyPageTemplateViewController*)[self.surveyQuestionPages objectAtIndex:count-1]).nextQuestion = theDestinationViewController;
    return [self.surveyQuestionPages copy];
}

- (void) initiateReferenceDictionary
{
    #pragma mark User Detail Key
    
    self.referenceDictionary = [[NSMutableDictionary alloc] initWithDictionary:@{USER_DETAIL_WORKOUT_TIME_KEY:@{},
                                                                                 USER_DETAIL_GENDER_KEY:@{},
                                                                                 USER_DETAIL_ROLE_KEY:@{},
                                                                                 USER_DETAIL_ROLE_PREFERENCE_KEY:@{},
                                                                                 USER_DETAIL_GENDER_PREFERENCE_KEY:@{},
                                                                                 USER_DETAIL_FITNESS_PURSUIT_KEY:@{},
                                                                                 USER_DETAIL_FITNESS_LEVEL_KEY:@{},
                                                                                 USER_DETAIL_WEEKLY_ACTIVITIES_KEY:@{},
                                                                                 USER_DETAIL_LIFESTYLE_KEY:@{},
                                                                                 USER_DETAIL_AGE_GROUP_KEY:@{},
                                                                                 USER_DETAIL_LIVING_PLACE_KEY:@{}}];

}

- (void)initiateSurveyQuestionData
{
    self.surveyQuestionData = @[
                                [[GTRSurveyQuestionData alloc] initWithQuestionType:SingleSelectionAnswer
                                                                     questionImages:@[
                                                                                      [UIImage imageNamed:@"img-survey-1-1"],
                                                                                      [UIImage imageNamed:@"img-survey-1-2"]
                                                                                      ]
                                                                    questionAnswers:@[
                                                                                      @"a) The Morning",
                                                                                      @"b) At Night",
                                                                                      @"c) Both"
                                                                                      ]
                                                                       answerImages:nil
                                                                referenceDictionary:self.referenceDictionary
                                                                      referenceName:USER_DETAIL_WORKOUT_TIME_KEY],
                                
                                [[GTRSurveyQuestionData alloc] initWithQuestionType:SingleSelectionAnswer
                                                                     questionImages:@[
                                                                                      [UIImage imageNamed:@"img-survey-2-1"],
                                                                                      [UIImage imageNamed:@"img-survey-2-2"]
                                                                                      ]
                                                                    questionAnswers:@[
                                                                                      @"a) Male",
                                                                                      @"b) Female"
                                                                                      ]
                                                                       answerImages:nil
                                                                referenceDictionary:self.referenceDictionary
                                                                      referenceName:USER_DETAIL_GENDER_KEY],
                                
                                
                                [[GTRSurveyQuestionData alloc] initWithQuestionType:SingleSelectionAnswer
                                                                     questionImages:@[
                                                                                      [UIImage imageNamed:@"img-survey-5-1"],
                                                                                      [UIImage imageNamed:@"img-survey-5-2"]
                                                                                      ]
                                                                    questionAnswers:@[
                                                                                      @"a) Men",
                                                                                      @"b) Women",
                                                                                      @"c) Both"
                                                                                      ]
                                                                       answerImages:nil referenceDictionary:self.referenceDictionary
                                                                      referenceName:USER_DETAIL_GENDER_PREFERENCE_KEY],
                                
                                [[GTRSurveyQuestionData alloc] initWithQuestionType:SingleSelectionAnswer
                                                                     questionImages:@[
                                                                                      [UIImage imageNamed:@"img-survey-7-1"],
                                                                                      [UIImage imageNamed:@"img-survey-7-2"],
                                                                                      [UIImage imageNamed:@"img-survey-7-3"],
                                                                                      [UIImage imageNamed:@"img-survey-7-4"],
                                                                                      ]
                                                                    questionAnswers:@[
                                                                                      @"a) Professional",
                                                                                      @"b) Advanced",
                                                                                      @"c) Intermediate",
                                                                                      @"d) Beginner"
                                                                                      ]
                                                                       answerImages:nil
                                                                referenceDictionary:self.referenceDictionary
                                                                      referenceName:USER_DETAIL_FITNESS_LEVEL_KEY],
                                
                                [[GTRSurveyQuestionData alloc] initWithQuestionType:SingleSelectionAnswer
                                                                     questionImages:@[
                                                                                      [UIImage imageNamed:@"img-survey-9-1"],
                                                                                      [UIImage imageNamed:@"img-survey-9-2"],
                                                                                      [UIImage imageNamed:@"img-survey-9-3"]
                                                                                      ]
                                                                    questionAnswers:@[
                                                                                      @"a) In the office",
                                                                                      @"b) Stay at home Mom or Dad",
                                                                                      @"c) Training or racing"
                                                                                      ]
                                                                       answerImages:nil
                                                                referenceDictionary:self.referenceDictionary
                                                                      referenceName:USER_DETAIL_WEEKLY_ACTIVITIES_KEY],
                                
                                [[GTRSurveyQuestionData alloc] initWithQuestionType:SingleSelectionAnswer
                                                                     questionImages:@[
                                                                                      [UIImage imageNamed:@"img-survey-11-1"],
                                                                                      [UIImage imageNamed:@"img-survey-11-2"],
                                                                                      [UIImage imageNamed:@"img-survey-11-3"],
                                                                                      [UIImage imageNamed:@"img-survey-11-4"],
                                                                                      [UIImage imageNamed:@"img-survey-11-5"]
                                                                                      ]
                                                                    questionAnswers:@[
                                                                                      @"a) Single",
                                                                                      @"b) Married",
                                                                                      @"c) Full House",
                                                                                      @"d) Solo With Kids",
                                                                                      @"e) Rockin Retirement"
                                                                                      ]
                                                                       answerImages:nil
                                                                referenceDictionary:self.referenceDictionary
                                                                      referenceName:USER_DETAIL_LIFESTYLE_KEY],
                                
                                [[GTRSurveyQuestionData alloc] initWithQuestionType:SingleSelectionAnswer
                                                                     questionImages:nil
                                                                    questionAnswers:@[
                                                                                      @"a) 20-29",
                                                                                      @"b) 30-39",
                                                                                      @"c) 40-49",
                                                                                      @"d) 50-59",
                                                                                      @"e) 60-69",
                                                                                      @"f) 70-79",
                                                                                      @"g) 80-89"
                                                                                      ]
                                                                       answerImages:nil
                                                                referenceDictionary:self.referenceDictionary
                                                                      referenceName:USER_DETAIL_AGE_GROUP_KEY]
                                ];
    temp = self.surveyQuestionData;
}


- (void) initiateSurveyQuestionPages
{
    [self initiateReferenceDictionary];
    [self initiateSurveyQuestionData];
    
    self.surveyQuestionPages  = [ [NSMutableArray alloc] initWithArray:
                                @[
                                 [[GTRSurveyPageTemplateViewController alloc] initWithQuestionNumber:1
                                                                                      totalQuestions: 11
                                                                                        questionText:@"Workout for you happens most often in?"
                                                                                        questionData:[self.surveyQuestionData objectAtIndex:0]
                                                                                        nextQuestion:nil],
                                 
                                 [[GTRSurveyPageTemplateViewController alloc] initWithQuestionNumber:2
                                                                                      totalQuestions: 11
                                                                                        questionText:@"You are a ____________?"
                                                                                        questionData:[self.surveyQuestionData objectAtIndex:1]
                                                                                        nextQuestion:nil],
                                 
                                 [[GTRSurveyPageTemplateViewController alloc] initWithQuestionNumber:5
                                                                                      totalQuestions: 11
                                                                                        questionText:@"Would you like Activebudz who are _________?"
                                                                                        questionData:[self.surveyQuestionData objectAtIndex:2]
                                                                                        nextQuestion:nil],

                                 [[GTRSurveyPageTemplateViewController alloc] initWithQuestionNumber:7
                                                                                      totalQuestions: 11
                                                                                        questionText:@"What is your level of ability in your chosen fitness pursuit?"
                                                                                        questionData:[self.surveyQuestionData objectAtIndex:3]
                                                                                        nextQuestion:nil],
                                 
                                 [[GTRSurveyPageTemplateViewController alloc] initWithQuestionNumber:8
                                                                                      totalQuestions: 11
                                                                                        questionText:@"During the week, you spend most of your days ______?"
                                                                                        questionData:[self.surveyQuestionData objectAtIndex:4]
                                                                                        nextQuestion:nil],
                                 
                                 [[GTRSurveyPageTemplateViewController alloc] initWithQuestionNumber:9
                                                                                      totalQuestions: 11
                                                                                        questionText:@"Which best reflects your current lifestyle?"
                                                                                        questionData:[self.surveyQuestionData objectAtIndex:5]
                                                                                        nextQuestion:nil],
                                 
                                 [[GTRSurveyPageTemplateViewController alloc] initWithQuestionNumber:10
                                                                                      totalQuestions: 11
                                                                                        questionText:@"Age group?"
                                                                                        questionData:[self.surveyQuestionData objectAtIndex:6]
                                                                                        nextQuestion:nil]
                                 
                                 ]
     ];
    
    GTRSurveyRolePageViewController *rolePage = [[GTRSurveyRolePageViewController alloc]initWithReferenceDictionary:self.referenceDictionary];
    [self.surveyQuestionPages insertObject:rolePage atIndex:2];
    
    //insert the role preference page as the fourth
    GTRSurveyRolePreferencePageViewController *rolePreferencePage = [[GTRSurveyRolePreferencePageViewController alloc]initWithReferenceDictionary:self.referenceDictionary];
    [self.surveyQuestionPages insertObject:rolePreferencePage atIndex:3];
    
    //insert the pursuit page as the sixth
    GTRSurveyPursuitPageViewController *pursuitPage = [[GTRSurveyPursuitPageViewController alloc] initWithReferenceDictionary:self.referenceDictionary];
    [self.surveyQuestionPages insertObject:pursuitPage atIndex:5];
    
    //insert the city page as the last one
    GTRSurveyCityPageViewController *cityPage = [[GTRSurveyCityPageViewController alloc] initWithReferenceDictionary:self.referenceDictionary];
    [self.surveyQuestionPages addObject:cityPage];
    
    //link the "next questions"
    NSUInteger count = self.surveyQuestionPages.count;
    for(int i = 0; i < count-1; i++)
    {
        ((GTRSurveyPageTemplateViewController*)[self.surveyQuestionPages objectAtIndex:i]).nextQuestion = [self.surveyQuestionPages objectAtIndex:i+1];
    }
    
}


@end
