//
//  NSMutableArray+AddMantleObject.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/3/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "NSMutableArray+AddMantleObject.h"
#import <MTLJSONAdapter.h>

@implementation NSMutableArray (AddMantleObject)

-(void)addObjectsFromArray:(NSArray *)otherArray modelOfClass:(Class)modelClass
{
    for(NSDictionary *objDict in otherArray)
    {
        if (![objDict isKindOfClass:[NSDictionary class]]) {
            return;
        }
        
        id obj = [MTLJSONAdapter modelOfClass:modelClass fromJSONDictionary:objDict error:nil];
        [self addObject:obj];
    }
}

@end
