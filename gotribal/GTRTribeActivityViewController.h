//
//  GTRTribeActivityViewController.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/16/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRViewController.h"

extern NSString *const GTR_TRIBE_ACTIVITY_RUNNING;
extern NSString *const GTR_TRIBE_ACTIVITY_CYCLING;
extern NSString *const GTR_TRIBE_ACTIVITY_YOGA;
extern NSString *const GTR_TRIBE_ACTIVITY_SWIMMING;
extern NSString *const GTR_TRIBE_ACTIVITY_SURFING;
extern NSString *const GTR_TRIBE_ACTIVITY_TRIATHLON;

@class GTRTribeActivityViewController;
@protocol GTRTribeActivityViewControllerDelegate <NSObject>

@optional
-(void)tribeActivityViewController:(GTRTribeActivityViewController*)controller didReturnActivity:(NSString*)activity;

@end

@interface GTRTribeActivityViewController : GTRViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property(nonatomic,weak) id <GTRTribeActivityViewControllerDelegate>delegate;

@end
