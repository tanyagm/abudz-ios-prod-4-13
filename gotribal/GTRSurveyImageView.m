//
//  GTRSurveyImageView.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/21/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSurveyImageView.h"

@implementation GTRSurveyImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [self initWithFrame:frame imageArray:nil];
    return self;
}


- (id)initWithFrame:(CGRect)frame imageArray:(NSArray*)theImageArray
{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageArray = theImageArray;
        [self setAppearance];
    }
    return self;
}


- (void)setAppearance
{
    //create image view array for each image
    NSMutableArray *imageViewArray = [[NSMutableArray alloc] init];
    
    for(UIImage *anImage in self.imageArray)
    {
        UIImageView *anImageView = [[UIImageView alloc] initWithImage:anImage];
        anImageView.frame = CGRectMake(0, 0, anImage.size.width, anImage.size.height);
        [imageViewArray addObject:anImageView];
        [self addSubview:anImageView];
    }
    
    //set position for each image, according to the number of image
    NSUInteger count = imageViewArray.count;
    DDLogInfo(@"Image inserted : %lu", (unsigned long)count);
    
    CGFloat horizontalSpace = 25.0f;
    CGFloat verticalSpace = 5.0f; //why minus? because it works.
    CGFloat topPadding= 14.0f;
    CGFloat extraBottomPadding = 38.0f;
    
    //auto-size like a boss!
    NSUInteger numberOfRows = count / 2;
    int numberOfRemainder = count % 2;
    
    //assuming all image have the same size
    CGFloat imageHeight = ((UIImageView*)[imageViewArray objectAtIndex:0]).bounds.size.height;
    CGFloat imageWidth = ((UIImageView*)[imageViewArray objectAtIndex:0]).bounds.size.width;
    
    CGFloat heightOffset = topPadding;
    
    CGFloat rowWidth = (imageWidth*2)+horizontalSpace;
    CGFloat firstImageX = (self.bounds.size.width - rowWidth)/2;
    CGFloat secondImageX = firstImageX+imageWidth+horizontalSpace;
    
    for(int i = 0; i < numberOfRows; i++)
    {
        DDLogInfo(@"Setting image %d and %d",(i*2), (i*2)+1);
        
        
        
        ((UIImageView*)[imageViewArray objectAtIndex:(i*2)]).frame = CGRectMake(firstImageX,
                                                                                heightOffset,
                                                                                imageWidth,
                                                                                imageHeight);
        
        ((UIImageView*)[imageViewArray objectAtIndex:(i*2)+1]).frame = CGRectMake(secondImageX,
                                                                                heightOffset,
                                                                                imageWidth,
                                                                                imageHeight);
        heightOffset += verticalSpace+imageHeight;
    }
    
    DDLogInfo(@"Number of remainder : %d", numberOfRemainder);
    
    if(numberOfRemainder > 0)
    {
        CGFloat lastImageX = (self.bounds.size.width - imageWidth)/2;
        DDLogInfo(@"Setting image %lu", (unsigned long)(numberOfRows * 2) );
        ((UIImageView*)[imageViewArray objectAtIndex:(numberOfRows*2)]).frame = CGRectMake(lastImageX,
                                                                                                 heightOffset,
                                                                                                 imageWidth,
                                                                                                 imageHeight);
        heightOffset += verticalSpace+imageHeight;
    }
    
    //set this view's frame height
    [self setFrame:CGRectMake(0, 0, self.bounds.size.width, heightOffset+extraBottomPadding)];
    
}
@end
