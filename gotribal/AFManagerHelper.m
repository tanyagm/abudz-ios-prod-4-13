//
//  AFManagerHelper.m
//  gotribal
//
//  Created by Le Vady on 11/30/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "AFManagerHelper.h"
#import "GTRConstants.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>
#import <SVProgressHUD/SVProgressHUD.h>

NSString *const NO_INTERNET_CONNECTION_MESSAGE = @"No Internet connection, please check your network";

@implementation AFManagerHelper

+(AFHTTPRequestOperationManager *)authenticatedManager
{
    AFOAuthCredential *credential = [AFOAuthCredential retrieveCredentialWithIdentifier:[[NSURL URLWithString:BASE_URL] host]];
    NSString *authHeader = @"Bearer";
    
    if (credential) {
        authHeader = [NSString stringWithFormat:@"%@ %@", authHeader, credential.accessToken];
    } else {
        authHeader = [NSString stringWithFormat:@"%@ %@", authHeader, CLIENT_ACCESS_TOKEN];
    }    
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    [manager.requestSerializer setValue:authHeader forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    return manager;
}

+(void)applyReachabilityStatus
{
    AFNetworkReachabilityManager *reachabilityManager = [AFNetworkReachabilityManager sharedManager];
    
    [reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
            case AFNetworkReachabilityStatusUnknown:
                [SVProgressHUD dismiss];
                [SVProgressHUD showErrorWithStatus:NO_INTERNET_CONNECTION_MESSAGE];
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
            case AFNetworkReachabilityStatusReachableViaWWAN:
                break;
        }
    }];
    
    [reachabilityManager startMonitoring];
}

@end
