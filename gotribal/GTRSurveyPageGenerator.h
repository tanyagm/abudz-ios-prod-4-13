//
//  GTRSurveyPageGenerator.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/22/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRSurveyQuestionData.h"

@interface GTRSurveyPageGenerator : NSObject

@property (strong, nonatomic) NSArray *surveyQuestionData;
@property (strong, nonatomic) NSMutableDictionary *referenceDictionary;
@property (strong, nonatomic) NSMutableArray *surveyQuestionPages;

-(id)initWithReferenceDictionary:(NSMutableDictionary*)theDictionary;
-(NSArray*) getSurveyPagesWithDestination:(UIViewController*)theDestinationViewController;
-(void) initiateReferenceDictionary;
-(void)initiateSurveyQuestionData;

@end
