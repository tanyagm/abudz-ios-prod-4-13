//
//  GTRActiveBudzProfileViewController.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRSuggestedActiveBudzViewController.h"
#import "GTRUser.h"

typedef enum{
    ActiveBudzProfileTypeCurrentUser = 0,
    ActiveBudzProfileTypeConnected,
    ActiveBudzProfileTypeNotConnected
} ActiveBudzProfileType;

@protocol GTRActiveBudzProfileViewControllerDelegate

-(void)onRequestSent:(NSIndexPath*)indexPath;

@end

@interface GTRActiveBudzProfileViewController : GTRViewController

@property (weak) id <GTRActiveBudzProfileViewControllerDelegate> delegate;

/* You may set the indexPath to nil, if this object not created inside
 * a GTRSuggestedActiveBudzViewController. 
 */
-(id) initWithUser:(GTRUser*)theUser
         indexPath:(NSIndexPath*)theIndexPath
       connections:(NSArray*)theConnections
       profileType:(ActiveBudzProfileType)theProfileType;

@property (strong,nonatomic) NSArray *userConnections; //array of users
@property (strong,nonatomic) GTRUser *user;

-(void) refresh;

@end
