//
//  GTRActiveBudzProfileEndorsement.h
//  gotribal
//
//  Created by loaner on 12/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GTRActiveBudzProfileEndorsementDelegate <NSObject>

- (void)endorseButtonAction:(id)sender;

@end

@interface GTRActiveBudzProfileEndorsement : UIView
@property (weak) id <GTRActiveBudzProfileEndorsementDelegate> delegate;
- (id)initWithFrame:(CGRect)frame userName:(NSString*)theUserName;
-(void)hideEndorse;
-(void)showEndorse;
@end
