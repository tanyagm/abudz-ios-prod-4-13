//
//  GTRRestServiceErrorHandler.h
//  gotribal
//
//  Created by Hisma Mulya S on 1/2/14.
//  Copyright (c) 2014 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTRRestServiceErrorHandler : NSObject

+(void)handleConnectionRelatedError:(NSError *)error
                           response:(id)response
                   requestOperation:(AFHTTPRequestOperation*)operation;

@end
