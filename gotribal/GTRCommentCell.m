//
//  GTRCommentCell.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/12/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRCommentCell.h"
#import <UIImageView+AFNetworking.h>

@interface GTRCommentCell()
@property (nonatomic,strong) GTRComment *comment;
@property (nonatomic,strong) UIView *container;
@property (nonatomic,strong) UILabel *ownerNameLabel;
@property (nonatomic,strong) UILabel *messageLabel;
@property (nonatomic,strong) UILabel *dateLabel;
@property (nonatomic,strong) UIImageView *ownerAvatar;
@property (nonatomic,strong) UIView *commentFooter;
@end

@implementation GTRCommentCell
@synthesize ownerAvatar, ownerNameLabel, messageLabel, dateLabel, comment, container, commentFooter;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setAppearance];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (CGFloat) getHeight
{
    return self.frame.size.height;
}

-(void)setAppearance
{
    self.backgroundColor = [UIColor clearColor];
    
    container = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 300)];
    container.backgroundColor = [UIColor clearColor];
    
    ownerAvatar = [[UIImageView alloc]initWithFrame:CGRectMake(0, 8, 35, 35)];
    ownerAvatar.backgroundColor = [UIColor colorWithWhite:0.337 alpha:1.000];
    ownerAvatar.layer.cornerRadius = ownerAvatar.bounds.size.width/2;
    ownerAvatar.layer.masksToBounds = YES;
    
    ownerNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 5, 260, 18)];
    ownerNameLabel.font = [UIFont systemFontOfSize:13.0];
    ownerNameLabel.text = @"fullname";
    ownerNameLabel.textColor = UIColorFromRGB(0x414141);
    ownerNameLabel.backgroundColor = [UIColor clearColor];
    
    messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, 260, 150)];
    messageLabel.font = [UIFont systemFontOfSize:11.5];
    messageLabel.text = @"message";
    messageLabel.textColor = UIColorFromRGB(0x7A7A7A);
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.numberOfLines = 0;
    
    dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260, 18)];
    dateLabel.font = [UIFont systemFontOfSize:10];
    dateLabel.textColor =UIColorFromRGB(0x9A9A9A);
    dateLabel.text = @"About an hour ago";
    dateLabel.backgroundColor = [UIColor clearColor];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 20, 260, 0.5)];
    line.backgroundColor = UIColorFromRGB(0xDADADA);
    
    commentFooter = [[UIView alloc] initWithFrame:CGRectMake(40, 0, 93, 30)];
    [dateLabel addSubview:line];
    
    [commentFooter addSubview:dateLabel];
    [commentFooter addSubview:line];
    
    [self addSubview:container];
    [container addSubview:ownerNameLabel];
    [container addSubview:ownerAvatar];
    [container addSubview:messageLabel];
    [container addSubview:commentFooter];
    
    #ifdef DEBUG_COLOR
    container.backgroundColor = [UIColor cyanColor];
    ownerAvatar.backgroundColor = [UIColor blackColor];
    dateLabel.backgroundColor = [UIColor yellowColor];
    messageLabel.backgroundColor = [UIColor greenColor];
    ownerNameLabel.backgroundColor = [UIColor redColor];
    #endif
}


#pragma mark Update cell content
-(void)setContent:(GTRComment*)inpComment
{
    comment = inpComment;
    
    ownerNameLabel.text = [comment.owner fullName];
    
    [self setMessageLabelContent:comment.message];
    [self setDateLabelContent:[comment createdAtRelativeTime]];
    [self setAvatarImageViewContent:[comment.owner avatarURL]];

    [self setContainerHeight];
}

-(void)setMessageLabelContent:(NSString*)text
{
    CGRect frame = messageLabel.frame;
    frame.size = CGSizeMake(250, 500);
    messageLabel.frame = frame;
    messageLabel.text = text;
    [messageLabel sizeToFit];
}

-(void) setContainerHeight{
    const double totalTopPadding = 7;
    double containerHeight = 0.0;
    
    containerHeight += totalTopPadding;
    containerHeight += [self getCommentDateHeight];
    containerHeight += [self getCommentBodyHeight];
    
    CGRect frame = container.frame;
    frame.size.height = containerHeight;
    container.frame = frame;
    self.frame = frame;
}

-(void)setDateLabelContent:(NSString*)text
{
    const double totalTopPadding = 5;
    dateLabel.text = text;
    CGRect frame = commentFooter.frame;
    frame.origin.y = [self getCommentBodyHeight] + totalTopPadding;
    commentFooter.frame = frame;
}


-(void) setAvatarImageViewContent:(NSString*)avatarURL
{
    if (avatarURL){
        [ownerAvatar setImageWithURL:[NSURL URLWithString:avatarURL] placeholderImage:[UIImage imageNamed:@"ico-camera-signup"]];
    } else {
        [ownerAvatar setImage:[UIImage imageNamed:@"default_avatar"]];
    }
}

-(double) getCommentBodyHeight{
    return messageLabel.frame.size.height +  ownerNameLabel.frame.size.height;
}

-(double) getCommentDateHeight{
    return dateLabel.frame.size.height;
}

@end
