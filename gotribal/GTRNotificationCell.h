//
//  GTRNotificationCell.h
//  
//
//  Created by Sambya Aryasa on 12/16/13.
//
//

#import "GTRSettingsCell.h"

@interface GTRNotificationCell : GTRSettingsCell
@property(nonatomic,strong) UIImageView *avatarImageView;
@property(nonatomic,strong) UILabel *subtext;
@end
