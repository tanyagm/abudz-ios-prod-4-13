//
//  GTRSideMenuViewController.h
//  gotribal
//
//  Created by Muhammad Taufik on 11/15/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTRSideMenuViewController : GTRTableViewController
-(void)notificationsViewAction;
@end
