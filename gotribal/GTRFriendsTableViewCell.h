//
//  GTRFriendsTableViewCell.h
//  gotribal
//
//  Created by Muhammad Taufik on 12/19/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTRFriendsTableViewCell : UITableViewCell

@property (nonatomic, assign) BOOL invited;

@property (nonatomic, strong) UIButton *inviteButton;

@end
