//
//  GTREditFitnessPageGenerator.h
//  gotribal
//
//  Created by loaner on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRSurveyPageGenerator.h"
#import "GTRFitnessPageTemplateViewController.h"
#import "GTRFitnessLocationPageViewController.h"
#import "GTRFitnessPursuitViewController.h"
#import "GTRFitnessRolePreferencePageViewController.h"
#import "GTRFitnessRolePageViewController.h"

@interface GTREditFitnessPageGenerator : GTRSurveyPageGenerator

-(void)initiateFitnessProfilePages;
-(void)loadPreviousAnswers;

@end
