//
//  GTRActiveBudzPageViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActiveBudzPageViewController.h"
#import "MFSideMenu.h"
#import "GTRSuggestedActiveBudzViewController.h"
#import "GTRMyActiveBudzViewController.h"
#import "GTRInviteFriendsViewController.h"

@interface GTRActiveBudzPageViewController ()

@property (strong,nonatomic) UISegmentedControl *segmentedControl;
@property (strong,nonatomic) UIScrollView *scrollView;

@property (strong,nonatomic) GTRMyActiveBudzViewController *myActiveBudzViewController;
@property (strong,nonatomic) GTRSuggestedActiveBudzViewController *suggestedActiveBudzViewController;

@property (strong,nonatomic) UIBarButtonItem *inviteActiveBudzButton;
@property (strong,nonatomic) UIBarButtonItem *editActiveBudzButton;
@property (strong,nonatomic) UIBarButtonItem *editDonebutton;

@end

@implementation GTRActiveBudzPageViewController
@synthesize  inviteActiveBudzButton, editActiveBudzButton, editDonebutton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Activebudz";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.941 alpha:1.000];
    
    //set UIControllSegment font to fit the frame for IOS 6.0
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
        [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica" size:12.0], UITextAttributeFont, nil] forState:UIControlStateNormal];
    }
    
    [self setNavigationBar];
    [self setAppearance];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Navigation Bar

- (void) setNavigationBar
{
    [GTRNavigationBarHelper setLeftSideBarButtonItem:self.navigationItem withTarget:self selector:@selector(toggleSideMenu:)];
    inviteActiveBudzButton = [GTRNavigationBarHelper createBarButtonItemWithTarget:self
                                                                          selector:@selector(inviteButtonAction:)
                                                                        imageNamed:@"ico-menu-invite"];
    editActiveBudzButton = [GTRNavigationBarHelper createBarButtonItemWithTarget:self
                                                                        selector:@selector(editButtonAction:)
                                                                     buttonTitle:NSLocalizedString(@"Edit", @"Edit")];
    editDonebutton = [GTRNavigationBarHelper createBarButtonItemWithTarget:self
                                                                  selector:@selector(doneButtonAction:)
                                                               buttonTitle:NSLocalizedString(@"Done", @"Done")];
    [self showInviteButton];
}

- (void) showInviteButton
{
    [GTRNavigationBarHelper setRightBarButtonItem:self.navigationItem
                                       withButton:inviteActiveBudzButton
                                         animated:NO
                                   withSpacerType:GTRSpacerTypeButton];
}

- (void) showEditMyActiveBudzButton
{
    [GTRNavigationBarHelper setRightBarButtonItem:self.navigationItem
                                       withButton:editActiveBudzButton
                                         animated:NO
                                   withSpacerType:GTRSpacerTypePlainText];
}

- (void) showDoneEditingMyActiveBudzButton
{
    [GTRNavigationBarHelper setRightBarButtonItem:self.navigationItem
                                       withButton:editDonebutton
                                         animated:NO
                                   withSpacerType:GTRSpacerTypePlainText];
}


#pragma mark - Set appearance methods

-(void)setAppearance
{
    //set the segmented control
    CGFloat segmentedControlTopPadding = 10.0f;
    CGFloat segmentedControlHorizontalPadding = 15.0f;
    CGFloat segmentedControlHeight = 30.0f;
    CGFloat segmentedControlWidth = self.view.bounds.size.width-(segmentedControlHorizontalPadding)*2;
    NSArray *segmentArray = @[@"My Activebudz",@"Suggested Activebudz"];
    
    self.segmentedControl = [[UISegmentedControl alloc] initWithItems:segmentArray];
    self.segmentedControl.frame = CGRectMake(segmentedControlHorizontalPadding,
                                             segmentedControlTopPadding,
                                             segmentedControlWidth,
                                             segmentedControlHeight);
    self.segmentedControl.backgroundColor = [UIColor clearColor];
    self.segmentedControl.tintColor = [UIColor colorWithRed:0.114 green:0.525 blue:0.541 alpha:1.000];
    self.segmentedControl.selectedSegmentIndex = 1;
    self.segmentedControl.segmentedControlStyle = UISegmentedControlStylePlain;
    [self.segmentedControl addTarget:self action:@selector(toggleSegmentedControl:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:self.segmentedControl];
    
    //set the child view controller frame
    CGFloat childFrameY = (segmentedControlTopPadding*2) + segmentedControlHeight;
    CGFloat childFrameWidth = self.view.bounds.size.width;
    CGFloat childFrameHeight = self.view.bounds.size.height - childFrameY;
    CGRect childFrame = CGRectMake(0, childFrameY, childFrameWidth, childFrameHeight);
    
    //initiate the suggested active budz VC
    self.suggestedActiveBudzViewController = [[GTRSuggestedActiveBudzViewController alloc] init];
    self.suggestedActiveBudzViewController.view.frame = childFrame; //CGRectMake(0, 90, 100, 100);
    [self addChildViewController:self.suggestedActiveBudzViewController];
    [self.view addSubview:self.suggestedActiveBudzViewController.view];
    
    //initiate the current active budz VC
    self.myActiveBudzViewController = [[GTRMyActiveBudzViewController alloc] init];
    self.myActiveBudzViewController.view.frame = childFrame;
    self.myActiveBudzViewController.view.hidden = YES;
    [self addChildViewController:self.myActiveBudzViewController];
    [self.view addSubview:self.myActiveBudzViewController.view];
}

#pragma mark - Custom button actions

-(void)toggleSegmentedControl:(id)sender
{
    DDLogInfo(@"Toggle segmented control! current segment : %d", ((int)self.segmentedControl.selectedSegmentIndex));
    if(self.segmentedControl.selectedSegmentIndex == 0)
    {
        self.suggestedActiveBudzViewController.view.hidden = YES;
        self.myActiveBudzViewController.view.hidden = NO;
        [self showEditMyActiveBudzButton];
    }else{
        self.suggestedActiveBudzViewController.view.hidden = NO;
        self.myActiveBudzViewController.view.hidden = YES;
        [self.myActiveBudzViewController.tableView setEditing:NO animated:NO];
        [self showInviteButton];
    }
}

-(void)inviteButtonAction:(id)sender
{
    DDLogInfo(@"Invite button clicked!");
    GTRInviteFriendsViewController *inviteFriendsViewController = [[GTRInviteFriendsViewController alloc] initWithPushedMode:YES tribeMode:NO];
    
    [self.navigationController pushViewController:inviteFriendsViewController animated:YES];
}

-(void) editButtonAction:(id)sender
{
    DDLogInfo(@"Edit button clicked!");
    if(self.myActiveBudzViewController.activeBudzArray.count > 0)
    {
        [self.myActiveBudzViewController.tableView setEditing:YES animated:YES];
        [self showDoneEditingMyActiveBudzButton];
    }
}

-(void) doneButtonAction:(id)sender
{
    DDLogInfo(@"Done button clicked!");
    [self.myActiveBudzViewController.tableView setEditing:NO animated:YES];
    [self showEditMyActiveBudzButton];
}

@end
