//
//  GTRSettingsHelper.m
//  gotribal
//
//  Created by Hisma Mulya S on 11/29/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSettingsHelper.h"
#import "GTRSideMenuCell.h"
#import "GTRAppDelegate.h"

@implementation GTRSettingsHelper

+(UIView*)createHeaderViewWithText:(NSString*)text
{
    UIView *headerView = [[UIView alloc] init];
    headerView.frame = CGRectMake(0, 0, 320, 45);
    headerView.backgroundColor = TABLEVIEW_BACKGROUND_COLOR;
    
    UILabel *textLabel = [[UILabel alloc]initWithFrame:CGRectOffset(headerView.frame, 10, 0)];
    textLabel.backgroundColor = TABLEVIEW_BACKGROUND_COLOR;
    textLabel.text = text;
    textLabel.font = HEADER_FONT;
    textLabel.textColor = CELL_TEXT_FONT_COLOR;
    textLabel.contentMode = UIViewContentModeCenter;
    [headerView addSubview:textLabel];
    
    return headerView;
}

+(UIView*)createGroupedViewInCell:(GTRSettingsCell*)cell atIndexPath:(NSIndexPath*)indexPath
{
    UITableView *tableView = (UITableView*)cell.superview.superview;
    CGFloat cornerRadius = 5.f;
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
    CGMutablePathRef pathRef = CGPathCreateMutable();
    CGRect bounds = CGRectInset(cell.bounds, 5, 0);
    BOOL addLine = NO;
    
    if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
        CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
    } else if (indexPath.row == 0) {
        CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
        CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
        addLine = YES;
    } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
        CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
        CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
    } else {
        CGPathAddRect(pathRef, nil, bounds);
        addLine = YES;
    }
    
    UIColor *lineColor = [UIColor colorWithRed:219/255.0
                                         green:219/255.0
                                          blue:219/255.0 alpha:1.0];
    
    layer.path = pathRef;
    CFRelease(pathRef);
    layer.fillColor = [UIColor colorWithWhite:1.f alpha:1.0f].CGColor;
    
    if (addLine == YES) {
        CALayer *lineLayer = [[CALayer alloc] init];
        CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
        lineLayer.frame = CGRectMake(CGRectGetMinX(bounds)+10, bounds.size.height-lineHeight, bounds.size.width-10, lineHeight);
        lineLayer.backgroundColor = lineColor.CGColor;
        [layer addSublayer:lineLayer];
    }
    
    // TODO MAMAZ: ADD OUTSIDE BORDER
//    // border color
//    layer.lineWidth = 0.8;
//    layer.lineWidth = (1.f / [UIScreen mainScreen].scale);
//    layer.strokeColor = lineColor.CGColor;
    
    UIView *testView = [[UIView alloc] initWithFrame:bounds];
    [testView.layer insertSublayer:layer atIndex:0];
    testView.backgroundColor = TABLEVIEW_BACKGROUND_COLOR;
    
    return testView;
}

+(void)setCellStatusValue:(NSInteger)value forIndexPath:(NSIndexPath*)indexPath
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:value]
                                              forKey:[NSString stringWithFormat:@"notification-%d-%d",
                                                      indexPath.row,indexPath.section]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSNumber*)cellStatusValueForIndexPath:(NSIndexPath*)indexPath
{
    NSNumber *value = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"notification-%d-%d",
                                                                          indexPath.row,indexPath.section]];
    if (value) {
        return value;
    }
    else {
        return nil;
    }
}

+(NSMutableDictionary *)getSettingDetail
{
    GTRAppDelegate *appDelegate = (GTRAppDelegate *)[UIApplication sharedApplication].delegate;
    return [appDelegate.settingConf mutableCopy];
}

+(void)updateSettingDetail:(NSMutableDictionary*)params
{
    GTRAppDelegate *appDelegate = (GTRAppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.settingConf = params;
}


@end
