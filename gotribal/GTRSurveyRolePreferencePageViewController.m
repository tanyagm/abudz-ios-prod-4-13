//
//  GTRSurveyLookingForPageViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/26/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSurveyRolePreferencePageViewController.h"
#import "SVProgressHUD.h"


@interface GTRSurveyRolePreferencePageViewController () <UITableViewDelegate>

@end

@implementation GTRSurveyRolePreferencePageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithReferenceDictionary:(NSMutableDictionary *)theReferenceDictionary;
{

    GTRSurveyQuestionData *questionData = [[GTRSurveyQuestionData alloc] initWithQuestionType:UpToTwoSelectionAnswer
                                                                               questionImages:@[
                                                                                                [UIImage imageNamed:@"img-survey-4-1"],
                                                                                                [UIImage imageNamed:@"img-survey-4-2"],
                                                                                                [UIImage imageNamed:@"img-survey-4-3"]
                                                                                                ]
                                                                              questionAnswers:@[
                                                                                                @"a) Mentor",
                                                                                                @"b) Mentee",
                                                                                                @"c) Training Buddy"
                                                                                                ]
                                                                                 answerImages:nil referenceDictionary:theReferenceDictionary
                                                                                referenceName:USER_DETAIL_ROLE_PREFERENCE_KEY];
    //initiate with superclass
    self = [super initWithQuestionNumber:4
                          totalQuestions:11
                            questionText:@"Who are you looking for?"
                            questionData:questionData
                            nextQuestion:nil];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    DDLogInfo(@"Custom didSelectRowAtIndexPath triggered!");
    
    if([self isSelectionValid:indexPath.row])
    {
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    }else{
        //de-select the cell image
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}

//takes user selected role on Q3.
- (int) getSelectedRole
{
    int role = -1;
    NSDictionary *roledata = [super.questionData.referenceDictionary objectForKey:@"role"];
    NSArray* roleAnswerTracker = [roledata objectForKey:@"value"];
    int i = 0;
    int max = roleAnswerTracker.count;
    if ([roledata objectForKey:@"value"]) {
        while(i<max) {
            role = [((NSNumber*)roleAnswerTracker[i]) intValue];
            if(role == 0 || role == 1) {
                return role;
            }
            else {
                i++;
            }
        }
    }
    DDLogInfo(@"role number : %d",role);
    return role;
}

//checks whether user;s current selection valid, according to selected role and the rules.
- (BOOL) isSelectionValid:(NSUInteger)theSelectedRole
{
    BOOL valid = NO;
    int role = [self getSelectedRole];
  
    //TODO : RPS - Update this later, if the logic is already clear!
    switch (role) {
        case 0: //mentor
        {
            if(role == theSelectedRole) //if looking for another mentor, send an alert.
            {
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please change your selection in Q3 if you would like a mentor.",nil)];
        
            }else{ //if not, proceed.
                valid = YES;
            }
        }
            break;
        case 1: //mentee
        {
            if(role == theSelectedRole) //if looking for another mentee, send an alert.
            {
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please change your selection in Q3 if you would like a mentee.",nil)];

            }else{ //if not, proceed.
                valid = YES;
            }
        }
            break;
        case 2: //training buddy
        {
            if(theSelectedRole == 1) //if looking for another mentee, send an alert.
            {
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please change your selection in Q3 if you would like a mentee.",nil)];
                
            }else{ //if not, proceed.
                valid = YES;
            }
        }
            break;
        default: //no role selected
        {
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please select a role in Q3 first.",nil)];
         
        }
            break;
    }
    
    
    return valid;
}

//override the super class, so it can be used to reset the page answers.
- (void)backButtonAction:(id)sender
{
    if(self.isSurveyPageMode){ //only if it's on the survey page.
        [super deselectAllAnswers];
    }

    DDLogInfo(@"Back button clicked!");
    if(self.isSurveyPageMode){
        [self.navigationController popViewControllerAnimated:YES];
        if(!self.isSurveyPageMode){
            [self saveDataToDictionary];
        }
    }else{
        if(self.selectedAnswerTracker.count == 0 && [self getSelectedRole] != -1 ){
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please select your changes.",nil)];
          
        }else{
            [self.navigationController popViewControllerAnimated:YES];
            if(!self.isSurveyPageMode){
                [self saveDataToDictionary];
            }
        }
    }
}

@end
