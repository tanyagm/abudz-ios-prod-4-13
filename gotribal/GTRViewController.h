//
//  GTRViewController.h
//  gotribal
//
//  Created by Muhammad Taufik on 12/8/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRNavigationBarHelper.h"

@interface GTRViewController : UIViewController

-(void)toggleSideMenu:(id)sender;

@end
