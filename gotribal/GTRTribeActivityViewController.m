//
//  GTRTribeActivityViewController.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/16/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRTribeActivityViewController.h"
#import <TPKeyboardAvoiding/TPKeyboardAvoidingTableView.h>
#import "GTRSideMenuHelper.h"
#import "GTRSideMenuCell.h"
#import "GTRNavigationBarHelper.h"
#import "GTRTribeHelper.h"

NSString *const PLACEHOLDER_LABEL               = @"Add A New Activity...";
NSString *const GTR_TRIBE_ACTIVITY_RUNNING      = @"Running";
NSString *const GTR_TRIBE_ACTIVITY_CYCLING      = @"Cycling";
NSString *const GTR_TRIBE_ACTIVITY_YOGA         = @"Yoga";
NSString *const GTR_TRIBE_ACTIVITY_SWIMMING     = @"Swimming";
NSString *const GTR_TRIBE_ACTIVITY_SURFING      = @"Surfing";
NSString *const GTR_TRIBE_ACTIVITY_TRIATHLON    = @"Triathlon";

@interface GTRTribeActivityViewController ()

@property(nonatomic,strong) TPKeyboardAvoidingTableView *table;
@property(nonatomic,strong) NSMutableDictionary *imageCache;
@property(nonatomic,strong) UITextField *textField;

@end

@implementation GTRTribeActivityViewController
@synthesize table = _table;
@synthesize imageCache = _imageCache;
@synthesize textField = _textField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Choose Your Activity";
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self cacheImages];
    [self addTableView];
    
    [GTRNavigationBarHelper setLeftBarButtonItemWithText:NSLocalizedString(@"Cancel", @"cancel")
                                        onNavigationItem:self.navigationItem
                                              withTarget:self
                                                selector:@selector(cancelButtonAction:)];
    [self addNewActivityTextView];
}

-(void)addTableView
{
    CGRect tableFrame = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? CGRectMake(0, 45,320, self.view.bounds.size.height - 109): CGRectMake(0, 45, 320, self.view.bounds.size.height - 65);
    
    _table = [[TPKeyboardAvoidingTableView alloc]initWithFrame:tableFrame];
    _table.backgroundView = nil;
    _table.backgroundColor = [UIColor whiteColor];
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    _table.delegate = self;
    _table.dataSource = self;
    [self.view addSubview:_table];
}

-(void)addNewActivityTextView
{
    _textField = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
    _textField.backgroundColor = UIColorFromRGB(0xFFFFFF);
    _textField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _textField.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
    _textField.textColor = NOTIFICATIONS_CELL_SUBTEXT_COLOR;
    _textField.placeholder = PLACEHOLDER_LABEL;
    _textField.font = TRIBE_ACTIVITY_FONT;
    _textField.textAlignment = NSTextAlignmentLeft;
    _textField.returnKeyType = UIReturnKeyDone;
    _textField.delegate = self;
    [self.view addSubview:_textField];
    
    UIImageView *plusImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico-plus"]];
    plusImageView.frame = CGRectMake(0,0,39,19);
    plusImageView.contentMode = UIViewContentModeCenter;
    _textField.leftViewMode = UITextFieldViewModeAlways;
    _textField.leftView = plusImageView;
}

-(void)cancelButtonAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source and delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 62;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NormalCell";
    GTRSideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[GTRSideMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = GTR_MAIN_ACTIVITIES_BACKGROUND_COLOR;
        cell.rightImageView.hidden = YES;
        cell.avatarImageView.backgroundColor = GTR_MAIN_ACTIVITIES_BACKGROUND_COLOR;
        cell.text.backgroundColor = GTR_MAIN_ACTIVITIES_BACKGROUND_COLOR;
        cell.text.textColor = [UIColor grayColor];
    }
    
    NSString *textString;
    UIImage *image;
    switch (indexPath.row) {
        case 0:
            textString = @"Running";
            image = _imageCache[PURSUIT_RUNNING_ICON_NAME];
            break;
        case 1:
            textString = @"Cycling";
            image = _imageCache[PURSUIT_CYCLING_ICON_NAME];
            break;
        case 2:
            textString = @"Yoga";
            image = _imageCache[PURSUIT_YOGA_ICON_NAME];
            break;
        case 3:
            textString = @"Swimming";
            image = _imageCache[PURSUIT_SWIMMING_ICON_NAME];
            break;
        case 4:
            textString = @"Surfing";
            image = _imageCache[PURSUIT_SURFING_ICON_NAME];
            break;
        case 5:
            textString = @"Triathlon";
            image = _imageCache[PURSUIT_TRIATHLON_ICON_NAME];
            break;
    }
    
    [GTRTribeHelper setAvatarImage:image text:textString inCell:cell];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    __weak UITextField *weakTextField = _textField;
    __weak GTRTribeActivityViewController *tribeController = self;
    
    if (weakTextField.text.length > 0) {
        if ([self.delegate respondsToSelector:@selector(tribeActivityViewController:didReturnActivity:)]) {
            [self.delegate tribeActivityViewController:self didReturnActivity:weakTextField.text];
        }
    } else {
        NSString *activity = [tribeController retrieveActivityNameFromIndexPath:indexPath];
        if ([self.delegate respondsToSelector:@selector(tribeActivityViewController:didReturnActivity:)]) {
            [self.delegate tribeActivityViewController:self didReturnActivity:activity];
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Cache Images

-(void)cacheImages
{
    _imageCache = [[NSMutableDictionary alloc]initWithCapacity:6];
    
    if (!_imageCache[PURSUIT_RUNNING_ICON_NAME]) {
        _imageCache[PURSUIT_RUNNING_ICON_NAME] = [UIImage imageNamed:PURSUIT_RUNNING_ICON_NAME];
    }
    
    if (!_imageCache[PURSUIT_CYCLING_ICON_NAME]) {
        _imageCache[PURSUIT_CYCLING_ICON_NAME] = [UIImage imageNamed:PURSUIT_CYCLING_ICON_NAME];
    }

    if (!_imageCache[PURSUIT_YOGA_ICON_NAME]) {
        _imageCache[PURSUIT_YOGA_ICON_NAME] = [UIImage imageNamed:PURSUIT_YOGA_ICON_NAME];
    }

    if (!_imageCache[PURSUIT_SWIMMING_ICON_NAME]) {
        _imageCache[PURSUIT_SWIMMING_ICON_NAME] = [UIImage imageNamed:PURSUIT_SWIMMING_ICON_NAME];
    }

    if (!_imageCache[PURSUIT_SURFING_ICON_NAME]) {
        _imageCache[PURSUIT_SURFING_ICON_NAME] = [UIImage imageNamed:PURSUIT_SURFING_ICON_NAME];
    }

    if (!_imageCache[PURSUIT_TRIATHLON_ICON_NAME]) {
        _imageCache[PURSUIT_TRIATHLON_ICON_NAME] = [UIImage imageNamed:PURSUIT_TRIATHLON_ICON_NAME];
    }
}

#pragma mark - Retrieve Activity name form IndexPath
-(NSString*)retrieveActivityNameFromIndexPath:(NSIndexPath*)indexPath
{
    switch (indexPath.row) {
        case 0:
            return GTR_TRIBE_ACTIVITY_RUNNING;
            break;
        case 1:
            return GTR_TRIBE_ACTIVITY_CYCLING;
            break;
        case 2:
            return GTR_TRIBE_ACTIVITY_YOGA;
            break;
        case 3:
            return GTR_TRIBE_ACTIVITY_SWIMMING;
            break;
        case 4:
            return GTR_TRIBE_ACTIVITY_SURFING;
            break;
        case 5:
            return GTR_TRIBE_ACTIVITY_TRIATHLON;
            break;
    }
    
    return nil;
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([self.delegate respondsToSelector:@selector(tribeActivityViewController:didReturnActivity:)]) {
        [self.delegate tribeActivityViewController:self didReturnActivity:textField.text];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    return YES;
}

@end
