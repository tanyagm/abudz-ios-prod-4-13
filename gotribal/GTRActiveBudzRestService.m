//
//  GTRActiveBudzRestService.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/9/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActiveBudzRestService.h"
#import "GTRPaginationHelper.h"
#import "GTRRestServiceErrorHandler.h"

@implementation GTRActiveBudzRestService

+(void)retrieveSuggestedActiveBudz:(void(^)(id responseObject))success
                    onFail:(void (^) (id responseObject, NSError *error))errorHandler{
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager GET:FRIENDS_SUGGESTION_ENDPOINT parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        
        if (success) success(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
        
    }];
}

+(void)retrieveSuggestedActiveBudzWithStartPoint:(int)theStartPoint
                                  onSuccess:(void(^)(id responseObject))success
                                     onFail:(void (^) (id responseObject, NSError *error))errorHandler
{

    NSString *endpoint = [NSString stringWithFormat:@"%@/%d/%d",
                               FRIENDS_SUGGESTION_ENDPOINT,theStartPoint,NUMBER_OF_ACTIVEBUDZ_DATA_RETRIEVED];
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager GET:endpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (success) success(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"    Error : %@", error);
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    
    }];
}

+(void)retrieveMyActiveBudz:(void(^)(id responseObject))success
                     onFail:(void (^) (id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager GET:FRIENDS_ENDPOINT parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@" Response : %@", responseObject);
 
        if (success) success(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"    Error : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}

+(void)retrieveMyActiveBudzWithMaxID:(NSNumber*)theMaxID
                           onSuccess:(void(^)(id responseObject))success
                              onFail:(void (^) (id responseObject, NSError *error))errorHandler
{

    NSString *endpoint = [GTRPaginationHelper paginate:FRIENDS_ENDPOINT
                                                  withLimit:[NSNumber numberWithInt: NUMBER_OF_ACTIVEBUDZ_DATA_RETRIEVED]
                                                   andMaxID: theMaxID];
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager GET:endpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"    Response : %@", responseObject);

        if (success) success(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"    Error : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error); 
    }];
}

+(void)retrievePublicProfile:(GTRUser*)theUser
                   onSuccess:(void(^)(id responseObject))success
                      onFail:(void (^) (id responseObject, NSError *error))errorHandler{
    
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager GET:GET_USER_ENDPOINT(theUser.userID) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@" Response : %@", responseObject);
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"    Error : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}

+(void)addActiveBudzRequestWithUser:(GTRUser*)theUser onSuccess:(void(^)(id responseObject))success onFail:(void (^) (id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager POST:ADD_OR_REMOVE_FRIENDS_ENDPOINT(theUser.userID) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@" Response : %@", responseObject);
        
        if (success) success(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"    Error : %@", error);
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        
        if (errorHandler) errorHandler(operation, error);
    }];
}

+(void)removeActiveBudzWithUser:(GTRUser*)theUser onSuccess:(void(^)(id responseObject))success onFail:(void (^) (id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    
    [manager DELETE:ADD_OR_REMOVE_FRIENDS_ENDPOINT(theUser.userID) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@" Response : %@", responseObject);
        
        if (success) success(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"    Error : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}

+(void)endorseWithId:(NSNumber *)theUserId
           onSuccess:(void (^)(id))success onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    DDLogInfo(@"Endorse User with ID %@", theUserId);
    
    [manager POST:ENDORSE_ENDPOINT(theUserId) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@", error);
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}

+(void)unEndorseWithId:(NSNumber *)theUserId
            onSuccess:(void (^)(id))success onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    DDLogInfo(@"Endorse User with ID %@", theUserId);
    
    [manager DELETE:ENDORSE_ENDPOINT(theUserId) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@", error);
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}

@end
