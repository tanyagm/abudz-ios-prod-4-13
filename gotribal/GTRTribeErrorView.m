//
//  GTRTribeErrorView.m
//  gotribal
//
//  Created by Hisma Mulya S on 12/18/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRTribeErrorView.h"

#define COLOR_DEBUG 0
NSInteger TEXT_WIDTH = 239;
NSInteger TITLE_LABEL_HEIGHT    = 18;
NSInteger MESSAGE_LABEL_HEIGHT  = 37;

@implementation GTRTribeErrorView

- (id)initWithFrame:(CGRect)frame withTarget:(id)target dismissSelctor:(SEL)dismissSelector
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:1.0 green:0.30 blue:0.28 alpha:1.0];
        UIColor *labelColor = [UIColor whiteColor];
        
        // imageView
        self.imageView = [[UIImageView alloc]initWithFrame:CGRectMake(15,20,31,31)];
        self.imageView.backgroundColor = self.backgroundColor;
        self.imageView.image = [UIImage imageNamed:@"btn-x"];
        self.imageView.userInteractionEnabled = YES;
        [self addSubview:self.imageView];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:target
                                                                                    action:dismissSelector];
        [self addGestureRecognizer:tapGesture];
        
        // self.titleLabel
        self.titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(61, 17, TEXT_WIDTH, TITLE_LABEL_HEIGHT)];
        self.titleLabel.backgroundColor = self.backgroundColor;
        self.titleLabel.textColor = labelColor;
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.numberOfLines = 1;
        self.titleLabel.font = TRIBE_ERROR_FONT;
        [self addSubview:self.titleLabel];
        
        CGRect titleLabelFrame = self.titleLabel.frame;
        self.messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(61,titleLabelFrame.origin.y + titleLabelFrame.size.height - 3, TEXT_WIDTH, MESSAGE_LABEL_HEIGHT)];
        self.messageLabel.backgroundColor = self.backgroundColor;
        self.messageLabel.textColor = labelColor;
        self.messageLabel.textAlignment = NSTextAlignmentLeft;
        self.messageLabel.numberOfLines = 0;
        self.messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.messageLabel.font = TRIBE_ERROR_FONT;
        [self addSubview:self.messageLabel];
        
#if COLOR_DEBUG
//        self.titleLabel.backgroundColor = [UIColor grayColor];
        self.messageLabel.backgroundColor = [UIColor grayColor];;
#endif
    }
    
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.titleLabel.text.length == 0) {
        CGFloat originY = (self.messageLabel.frame.size.height + self.titleLabel.frame.size.height)/2.0 - self.titleLabel.frame.size.height/2.0;
        self.messageLabel.frame = CGRectMake(self.messageLabel.frame.origin.x, originY, TEXT_WIDTH, MESSAGE_LABEL_HEIGHT);
    }
}

@end
