//
//  GTRTribeDetailHeaderCell.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/13/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRTribeDetailHeaderCell.h"
#import <UIImageView+AFNetworking.h>
#import "GTRPursuitHelper.h"
#import "GTRConstants.h"
#import "GTRImagePickerHelper.h"

//button icon names
NSString* const LEAVE_TRIBE_BUTTON_ICON_NAME = @"ico-leave-tribe";
NSString* const NEW_POST_BUTTON_ICON_NAME = @"ico-new-post";

//button strings
NSString* const LEAVE_TRIBE_BUTTON_STRING = @"Leave The Tribe";
NSString* const NEW_POST_BUTTON_STRING = @"New Post";

#define DESCRIPTION_TEXT_PLACEHOLDER_STRING @"No description available."

@interface GTRTribeDetailHeaderCell()

@property (strong,nonatomic) GTRTribe *tribe;

@property (strong,nonatomic) UIImageView *ownerAvatar;
@property (strong,nonatomic) UILabel *tribeNameLabel;
@property (strong,nonatomic) UILabel *tribeActivityLabel;
@property (strong,nonatomic) UILabel *tribeDescriptionLabel;
@property (strong,nonatomic) UIImageView * activityImageView;
@property (strong,nonatomic) UIImageView *tribeImageView;
@property (strong,nonatomic) UIButton *leaveTribeButton;
@property (strong,nonatomic) UIButton *postButton;
@property (strong,nonatomic) UIView *customContentView;

@end

@implementation GTRTribeDetailHeaderCell
@synthesize tribe, ownerAvatar, tribeNameLabel, tribeActivityLabel, tribeDescriptionLabel, activityImageView, tribeImageView, leaveTribeButton, postButton, customContentView, delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setAppearance];
    }
    return self;
}

#pragma mark - Initial appearance setter
-(void)setAppearance
{
    self.backgroundColor = [UIColor clearColor];
    
    CGFloat customContentViewHeight = self.bounds.size.height;
    CGFloat customContentViewWidth = self.bounds.size.width;
    
    customContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, customContentViewWidth, customContentViewHeight)];
    
    customContentView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:customContentView];
}

#pragma mark - Content setter
-(void)setContent:(GTRTribe *)theTribe
{
    [self refreshCustomContentView];
    
    tribe = theTribe;
    
    //set owner's avatar
    CGFloat ownerAvatarX = 7;
    CGFloat ownerAvatarY = 9;
    CGFloat ownerAvatarHeight = 45;
    CGFloat ownerAvatarWidth = 45;
    
    ownerAvatar = [[UIImageView alloc] initWithFrame:CGRectMake(ownerAvatarX, ownerAvatarY, ownerAvatarWidth, ownerAvatarHeight)];
    
    [ownerAvatar setImageWithURL:[NSURL URLWithString:tribe.tribeOwner.avatarURL]
                placeholderImage:DEFAULT_AVATAR];
    ownerAvatar.layer.cornerRadius = ownerAvatarWidth/2;
    ownerAvatar.layer.masksToBounds = YES;
    
    [customContentView addSubview:ownerAvatar];
    
    //set activity image view
    
    UIImage *activityImage;
    
    if ([GTRPursuitHelper isCustomPursuit:tribe.tribeActivity]) {
         activityImage = [UIImage imageNamed:PURSUIT_OTHER_ICON_NAME];
    } else {
        activityImage = [UIImage imageNamed:[GTRPursuitHelper getPursuitIconName:tribe.tribeActivity]];
    }
    
    CGFloat activityImageViewWidth = activityImage.size.width;
    CGFloat activityImageViewHeight = activityImage.size.height;
    CGFloat activityImageViewX = customContentView.bounds.size.width - activityImageViewWidth - 10; //add some small padding
    CGFloat activityImageViewY = ownerAvatarY + (ownerAvatarHeight - activityImageViewHeight)/2;
    
    activityImageView = [[UIImageView alloc] initWithFrame:CGRectMake(activityImageViewX, activityImageViewY, activityImageViewWidth, activityImageViewHeight)];
    
    activityImageView.backgroundColor = [UIColor clearColor];
    activityImageView.contentMode = UIViewContentModeCenter;
    [activityImageView setImage:activityImage];
    
    [customContentView addSubview:activityImageView];
    
    //set tribe name label
    CGFloat tribeNameLabelX = ownerAvatarX + ownerAvatarWidth + 10;
    CGFloat tribeNameLabelY = ownerAvatarY + 5;
    CGFloat tribeNameLabelWidth = activityImageViewX - tribeNameLabelX;
    CGFloat tribeNameLabelHeight = 20;
    
    NSAttributedString *tribeNameLabelString = [[NSAttributedString alloc] initWithString:tribe.tribeName
                                                                               attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold"
                                                                                                                                  size:16]}];
    
    tribeNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(tribeNameLabelX, tribeNameLabelY, tribeNameLabelWidth, tribeNameLabelHeight)];
    tribeNameLabel.attributedText = tribeNameLabelString;
    tribeNameLabel.textColor = CELL_TEXT_FONT_COLOR;
    tribeNameLabel.backgroundColor = [UIColor clearColor];
    
    [customContentView addSubview:tribeNameLabel];
    
    //set tribe activity label
    CGFloat tribeActivityLabelX = tribeNameLabelX;
    CGFloat tribeActivityLabelY = tribeNameLabelY + tribeNameLabelHeight;
    CGFloat tribeActivityLabelWidth = activityImageViewX - tribeActivityLabelX;
    CGFloat tribeActivityLabelHeight = 17;
    
    tribeActivityLabel = [[UILabel alloc] initWithFrame:CGRectMake(tribeActivityLabelX, tribeActivityLabelY, tribeActivityLabelWidth, tribeActivityLabelHeight)];
    tribeActivityLabel.text = tribe.tribeActivity;
    tribeActivityLabel.font = TRIBE_ACTIVITY_CHOSEN_FONT;
    tribeActivityLabel.textColor = CELL_SUBTEXT_FONT_COLOR;
    tribeActivityLabel.backgroundColor = [UIColor clearColor];
    tribeActivityLabel.numberOfLines = 0;
    [customContentView addSubview:tribeActivityLabel];
    
    //set tribe description label
    CGFloat tribeDescriptionLabelX = tribeActivityLabelX;
    CGFloat tribeDescriptionLabelY = tribeActivityLabelY + tribeActivityLabelHeight + 7;
    CGFloat tribeDescriptionLabelWidth = activityImageViewX - tribeDescriptionLabelX;
    CGFloat tribeDescriptionLabelHeight = 20;
    
    NSString *descriptionString = DESCRIPTION_TEXT_PLACEHOLDER_STRING;
    
    if (tribe.tribeDescription) {
        descriptionString = tribe.tribeDescription;
    }
    
    NSAttributedString *tribeDescriptionLabelString = [[NSAttributedString alloc] initWithString:descriptionString
                                                                                   attributes:@{NSFontAttributeName : CELL_FONT}];
    
    tribeDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(tribeDescriptionLabelX, tribeDescriptionLabelY, tribeDescriptionLabelWidth, tribeDescriptionLabelHeight)];
    tribeDescriptionLabel.attributedText = tribeDescriptionLabelString;
    tribeDescriptionLabel.textColor = CELL_SUBTEXT_FONT_COLOR;
    tribeDescriptionLabel.backgroundColor = [UIColor clearColor];
    tribeDescriptionLabel.numberOfLines = 0;
    [tribeDescriptionLabel sizeToFit];
    [customContentView addSubview:tribeDescriptionLabel];
    
    //set tribe image
    CGFloat tribeImageX = 0;
    CGFloat tribeImageY = tribeDescriptionLabelY + tribeDescriptionLabel.frame.size.height + 5;
    CGFloat tribeImageWidth = customContentView.bounds.size.width;
    CGFloat tribeImageHeight = 215;
    
    tribeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(tribeImageX, tribeImageY, tribeImageWidth, tribeImageHeight)];
    
    if  (tribe.tribeImageURL) {
        [GTRImagePickerHelper setAndAdjustOrientationImageView:tribeImageView
                                                       withURL:[NSURL URLWithString:tribe.tribeImageURL]
                                                andPlaceholder:DEFAULT_ACTIVITY_PLACEHOLDER];
    } else {
        [tribeImageView setImage:DEFAULT_ACTIVITY_PLACEHOLDER];
    }
    
    tribeImageView.backgroundColor = [UIColor whiteColor];
    tribeImageView.clipsToBounds = YES;
    [customContentView addSubview:tribeImageView];
    
    //set tribe header buttons
    CGFloat leaveTribeButtonWidth = customContentView.bounds.size.width/2;
    CGFloat leaveTribeButtonHeight = 30;
    CGFloat leaveTribeButtonX = 0;
    CGFloat leaveTribeButtonY = tribeImageY + tribeImageHeight;
    
    CGFloat newPostTribeButtonWidth = leaveTribeButtonWidth;
    CGFloat newPostTribeButtonHeight = leaveTribeButtonHeight;
    CGFloat newPostTribeButtonX = leaveTribeButtonWidth;
    CGFloat newPostTribeButtonY = leaveTribeButtonY;
    
    if (tribe.isJoined) {
        
        NSAttributedString *leaveTribeButtonString = [[NSAttributedString alloc] initWithString:LEAVE_TRIBE_BUTTON_STRING attributes:@{
                                                                                                                                       NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:12],
                                                                                                                                       NSForegroundColorAttributeName : CELL_SUBTEXT_FONT_COLOR
                                                                                                                                       }];
        
        leaveTribeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        leaveTribeButton.frame = CGRectMake(leaveTribeButtonX, leaveTribeButtonY, leaveTribeButtonWidth, leaveTribeButtonHeight);
        [leaveTribeButton setAttributedTitle:leaveTribeButtonString forState:UIControlStateNormal];
        [leaveTribeButton setImage:[UIImage imageNamed:LEAVE_TRIBE_BUTTON_ICON_NAME] forState:UIControlStateNormal];
        [leaveTribeButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        leaveTribeButton.backgroundColor = [UIColor whiteColor];
        
        [customContentView addSubview:leaveTribeButton];
        
        if  ([(id) delegate respondsToSelector:@selector(leaveTribeButtonAction:)]) {
            [leaveTribeButton addTarget:delegate action:@selector(leaveTribeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        NSAttributedString *postButtonString = [[NSAttributedString alloc] initWithString:NEW_POST_BUTTON_STRING attributes:@{
                                                                                                                              NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:12],
                                                                                                                              NSForegroundColorAttributeName : CELL_SUBTEXT_FONT_COLOR
                                                                                                                              }];
        
        postButton = [UIButton buttonWithType:UIButtonTypeCustom];
        postButton.frame = CGRectMake(newPostTribeButtonX, newPostTribeButtonY, newPostTribeButtonWidth, newPostTribeButtonHeight);
        [postButton setAttributedTitle:postButtonString forState:UIControlStateNormal];
        [postButton setImage:[UIImage imageNamed:NEW_POST_BUTTON_ICON_NAME] forState:UIControlStateNormal];
        [postButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        postButton.backgroundColor = [UIColor whiteColor];
        
        if  ([(id) delegate respondsToSelector:@selector(postButtonAction:)]) {
            [postButton addTarget:delegate action:@selector(postButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        
        [customContentView addSubview:postButton];
        
        //set button divider
        CGFloat buttonDividerViewHeight = leaveTribeButtonHeight*0.65;
        CGFloat buttonDividerViewWidth = 1;
        CGFloat buttonDividerViewX = leaveTribeButtonX + leaveTribeButtonWidth - (buttonDividerViewWidth/2);
        CGFloat buttonDividerViewY = leaveTribeButtonY + ((leaveTribeButtonHeight - buttonDividerViewHeight)/2);
        
        UIView *buttonDividerView = [[UIView alloc] initWithFrame:CGRectMake(buttonDividerViewX, buttonDividerViewY, buttonDividerViewWidth, buttonDividerViewHeight)];
        buttonDividerView.backgroundColor = [UIColor colorWithWhite:0.941 alpha:1.000];
        [customContentView addSubview:buttonDividerView];
        
    }
    
    //resize cell's frame
    CGRect bufferFrame = self.frame;
    bufferFrame.size.height = newPostTribeButtonY + newPostTribeButtonHeight;
    self.frame = bufferFrame;
    
    //resize customContentView's bounds
    bufferFrame = customContentView.frame;
    bufferFrame.size.height = newPostTribeButtonY + newPostTribeButtonHeight;
    customContentView.frame = bufferFrame;
}

#pragma mark customContentView refresher
-(void)refreshCustomContentView
{
    for (UIView *subView in customContentView.subviews) {
        [subView removeFromSuperview];
    }
}

@end
