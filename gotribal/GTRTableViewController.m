//
//  GTRTableViewController.m
//  gotribal
//
//  Created by Muhammad Taufik on 12/8/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRTableViewController.h"
#ifdef GTR_FLURRY_ANALYTICS
#import "Flurry.h"
#endif

@interface GTRTableViewController ()

@end

@implementation GTRTableViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    }
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) self.edgesForExtendedLayout = UIRectEdgeNone;
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) self.automaticallyAdjustsScrollViewInsets = YES;
    
    //log page view numbers
    #ifdef GTR_FLURRY_ANALYTICS
    [Flurry logAllPageViews:self.navigationController];
    #endif
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //send timed page view log
    #ifdef GTR_FLURRY_ANALYTICS
    [Flurry logEvent:@"Page viewed" withParameters:@{ @"Name" : (self.title ? self.title : @"")} timed:YES];
    #endif
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //stop page view log timer
    #ifdef GTR_FLURRY_ANALYTICS
    [Flurry endTimedEvent:@"Page viewed" withParameters:@{ @"Name" : (self.title ? self.title : @"")}];
    #endif
}

-(void)toggleSideMenu:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

@end
