//
//  GTRSurveyData.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/21/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    SingleSelectionAnswer,
    UpToTwoSelectionAnswer,
    SingleStringAnswer
} SurveyQuestionType;

@interface GTRSurveyQuestionData : NSObject


@property SurveyQuestionType questionType;
@property (strong, nonatomic) NSArray *questionImages; //0, 2,3,4, or 5 images.
@property (strong, nonatomic) NSArray *questionAnswers;
@property (strong, nonatomic) NSArray *answerImages; //used for the images of Q6

@property (strong, nonatomic) NSMutableDictionary *referenceDictionary;
@property (strong, nonatomic) NSString *referenceName;

-(id) initWithQuestionType:(SurveyQuestionType)theQuestionType
            questionImages:(NSArray*)theQuestionImages
           questionAnswers:(NSArray*) theQuestionAnswers
              answerImages:(NSArray*) theAnserImages
       referenceDictionary:(NSMutableDictionary*)theDictionary
             referenceName:(NSString*)theName;


@end
