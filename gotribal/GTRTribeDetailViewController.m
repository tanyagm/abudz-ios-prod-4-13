//
//  GTRTribeDetailViewController.m
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/13/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRTribeDetailViewController.h"
#import "GTRTribeDetailHeaderCell.h"
#import "GTRTribeDetailMemberCell.h"
#import "GTRTribeRestService.h"
#import "GTRNavigationBarHelper.h"
#import "GTRPost.h"
#import "GTRStreamPostCell.h"
#import "SVPullToRefresh.h"
#import "SVProgressHUD.h"
#import "GTRNewPostViewController.h"
#import "GTRPostRestService.h"
#import "GTRLikeHelper.h"
#import "GTRInviteFriendsViewController.h"
#import "GTRTribeSearchViewController.h"
#import "GTRMyTribeViewController.h"
#import <UIKitExtension/UIAlertView.h>
#import "GTRCustomActionSheetHelper.h"
#import "GTRActiveBudzHelper.h"

#define TRIBE_HEADER_SECTION 0
#define TRIBE_MEMBER_SECTION 1
#define TRIBE_POST_SECTION 2

NSString* const TRIBE_HEADER_CELL_IDENTIFIER = @"tribeDetailHeaderCell";
NSString* const TRIBE_MEMBER_CELL_IDENTIFIER = @"tribeDetailMemberCell";
NSString* const TRIBE_POST_CELL_IDENTIFIER = @"tribeDetailPostCell";
NSString* const JOIN_TRIBE_BUTTON_TEXT = @"Join The Tribe";

@interface GTRTribeDetailViewController () <UITableViewDataSource, UITableViewDelegate, GTRTribeDetailHeaderCellDelegate, GTRTribeDetailMemberCellDelegate, GTRNewPostViewControllerDelegate,UIActionSheetDelegate, GTRCustomActionSheetHelperDelegate>

@property (strong,nonatomic) GTRTribe *tribe;
@property (strong,nonatomic) GTRTribeDetailHeaderCell *tribeHeader;
@property (strong,nonatomic) GTRTribeDetailMemberCell *tribeMember;
@property (strong,nonatomic) NSNumber *maxID;
@property (strong,nonatomic) GTRTribeDetailHeaderCell *tribeHeaderHeightCalculator;
@property (strong,nonatomic) UIButton *joinButton;
@property (strong, nonatomic) GTRCustomActionSheetHelper *customActionSheet;

@end

@implementation GTRTribeDetailViewController
@synthesize tribe, tribeHeader, tribeMember, maxID, delegate, tribeHeaderHeightCalculator, joinButton, customActionSheet;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Tribe Details", @"tribe details title");
        
        //initiate the tribe header
        tribeHeader = [[GTRTribeDetailHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TRIBE_HEADER_CELL_IDENTIFIER];
        tribeHeader.delegate = self;
        
        //initiate the tribe member
        tribeMember = [[GTRTribeDetailMemberCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TRIBE_MEMBER_CELL_IDENTIFIER];
        tribeMember.delegate = self;
        
        //initate tribe header height calculator
        tribeHeaderHeightCalculator = [[GTRTribeDetailHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"tribeHeaderHeightCalculator"];

        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //set the navigation bar buttons
    [GTRNavigationBarHelper setBackButtonItem:self.navigationItem withTarget:self selector:@selector(backButtonAction:)];
    
    [GTRNavigationBarHelper setRightBarButtonItem:self.navigationItem withTarget:self selector:@selector(inviteButtonAction:) imageNamed:@"ico-menu-invite" animated:NO];

    [self initiateJoinButton];
    [self initiateActionSheet];
    [self initiateReloadFunctionality];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom initiators

-(void)initiateActionSheet
{
    customActionSheet = [[GTRCustomActionSheetHelper alloc]initWithSuperViewController:self];
    customActionSheet.delegate = self;
}

-(void)initiateJoinButton
{
 
    //prepare the join button
    NSAttributedString *joinButtonText = [[NSAttributedString alloc] initWithString:JOIN_TRIBE_BUTTON_TEXT attributes:@{
                                                                                                                             NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16.0f],
                                                                                                                             NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                                                                             }];
    CGFloat joinButtonHorizontalPadding = 22;
    CGFloat joinButtonY = 50;
    CGFloat joinButtonHeight = 50;
    CGFloat joinButtonWidth = self.streamTableView.bounds.size.width - (joinButtonHorizontalPadding*2);
    
    joinButton = [[UIButton alloc] initWithFrame:CGRectMake(joinButtonHorizontalPadding, joinButtonY, joinButtonWidth, joinButtonHeight)];
    [joinButton setAttributedTitle:joinButtonText forState:UIControlStateNormal];
    joinButton.backgroundColor = [UIColor colorWithRed:0.114 green:0.525 blue:0.541 alpha:1.000];
    joinButton.layer.cornerRadius = 5.0f;
    [joinButton addTarget:self action:@selector(joinButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //prepare the table's footer view
    UIView *customFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.streamTableView.bounds.size.width, joinButtonY + joinButtonHeight)];
    customFooterView.backgroundColor = [UIColor clearColor];
    
    //add the button and view
    [customFooterView addSubview:joinButton];
    self.streamTableView.tableFooterView = customFooterView;
    
    //in case the join button is created later than setContent
    if (tribe && tribe.isJoined) {
        joinButton.hidden = YES;
    }
    
}

#pragma mark - Overriden superclass methods

-(void)reloadStreamPage:(GTRStreamPageReloadMode)theReloadMode
{
    __weak NSMutableArray *weakArray = self.streamPostArray;
    __weak UITableView *weakTable = self.streamTableView;
    
    if (!tribe.isJoined) {
        [weakTable.infiniteScrollingView stopAnimating];
        [weakTable.pullToRefreshView stopAnimating];
        return;
    }
    
    //add code to reload and refresh the table here
    DDLogInfo(@"reload invoked! mode : %d",theReloadMode);
    
    NSNumber *buffMaxID = maxID;
    
    if(theReloadMode == GTRStreamPageReloadModeRefresh) {
        maxID = nil;
    }
    
    [GTRTribeRestService retrieveTribePosts:tribe MaxID:maxID OnSuccess:^(id responseObject) {
        
        if(theReloadMode == GTRStreamPageReloadModeRefresh) {
            [weakArray removeAllObjects];
            [weakTable reloadData];
        }
        
        NSArray *posts = [((NSDictionary*)responseObject) objectForKey:@"data"];
        
        for (NSDictionary *dictionary in posts) {
            GTRPost *post = [MTLJSONAdapter modelOfClass:[GTRPost class] fromJSONDictionary:dictionary error:nil];
            [weakArray addObject:post];
        }
        
        maxID = ((GTRPost*)[weakArray lastObject]).postID;
        
        [weakTable.infiniteScrollingView stopAnimating];
        [weakTable.pullToRefreshView stopAnimating];
        
        [weakTable reloadData];
        [self togglePlaceholderLabelVisibility];
        
    } onFail:^(id responseObject, NSError *error) {
        
        //revert the max ID
        maxID = buffMaxID;
        
        [weakTable.infiniteScrollingView stopAnimating];
        [weakTable.pullToRefreshView stopAnimating];
        [self togglePlaceholderLabelVisibility];
        
    }];
    
}

#pragma mark New Post button action

-(void)openNewPostwithImage:(UIImage*) postImage
{
    GTRNewPostViewController *newPostVC = [[GTRNewPostViewController alloc] init];
    newPostVC.delegate = self;
    newPostVC.postImage = postImage;
    newPostVC.tribe = tribe;
    newPostVC.isOnTribe = YES;
    
    UINavigationController *NewPostNavVC = [[UINavigationController alloc]
                                            initWithRootViewController:newPostVC];
    
    [self presentViewController:NewPostNavVC animated:YES completion:nil];
}

#pragma mark Like button action
-(void)likeButtonAction:(id)sender
{
    
    NSInteger rowID = ((UIControl *) sender).tag;
    __weak GTRPost *post = [self.streamPostArray objectAtIndex:rowID];
    __weak UITableView *tableView = self.streamTableView;
    
    [GTRLikeHelper toggleLike:post
                    onSuccess:^(GTRPost *updatedPost) {
                        [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:rowID inSection:TRIBE_POST_SECTION]]
                                         withRowAnimation: UITableViewRowAnimationNone];
                    } onFail:nil];
}

#pragma mark More button action
-(void)moreButtonAction:(id)sender
{
    GTRUser *currentUser = [GTRUserHelper getCurrentUser];
    self.currentSelectedPost = sender;
    NSInteger rowID = ((UIControl *) sender).tag;
    __weak GTRPost *post = [self.streamPostArray objectAtIndex:rowID];
    if([currentUser.userID isEqual:post.owner.userID]){
        [customActionSheet openCustomActionSheetWithMode:GTRCustomActionSheetModeOwnedPost];
    } else {
        [customActionSheet openCustomActionSheetWithMode:GTRCustomActionSheetModeOtherPost];
    }
}

#pragma mark - Appearance methods
-(void)setContent:(GTRTribe*)theTribe
{
    tribe = theTribe;
    
    //prepare the post array
    [self.streamPostArray removeAllObjects];
    
    //set the table header
    [tribeHeader setContent:tribe];
    
    //set the tribe members section
    [tribeMember setContent:tribe];
    
    //set the tribe posts section or join button
    if (tribe.isJoined) {
        [self reloadStreamPage:GTRStreamPageReloadModeRefresh];
        joinButton.hidden = YES;
    } else {
        joinButton.hidden = NO;
    }
}

#pragma mark - UITableView delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case TRIBE_HEADER_SECTION:
            return 1;
            break;
        case TRIBE_MEMBER_SECTION:
            return 1;
            break;
        case TRIBE_POST_SECTION:
            return self.streamPostArray.count;
            break;
        default:
            return 0;
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //TODO : RPS - fix this height later!
    NSInteger section = indexPath.section;
    switch (section) {
        case TRIBE_HEADER_SECTION:
            {
                [tribeHeaderHeightCalculator setContent:tribe];
                return tribeHeaderHeightCalculator.frame.size.height;
            }
            break;
        case TRIBE_MEMBER_SECTION:
            return TRIBE_DETAIL_MEMBER_CELL_HEIGHT;
            break;
        case TRIBE_POST_SECTION:
            return [self calculatePostCellHeightAtRow:indexPath.row];
            break;
        default:
            return 100;
            break;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case TRIBE_HEADER_SECTION:
            {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TRIBE_HEADER_CELL_IDENTIFIER];
                if (cell == nil) {
                    cell = tribeHeader;
                }
                return cell;
            }
            break;
        case TRIBE_MEMBER_SECTION:
            {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TRIBE_MEMBER_CELL_IDENTIFIER];
                
                if (cell == nil){
                    cell = tribeMember;
                }
                
                return cell;
            }
            break;
        case TRIBE_POST_SECTION:
            {
                return [self tableView: tableView postCellAtIndexPath:indexPath];
            }
            break;
        default:
            return [[UITableViewCell alloc] init];
            break;
    }
}

#pragma mark - GTRTribeDetailHeaderCell delegates
-(void)leaveTribeButtonAction:(id)sender
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    [GTRTribeRestService leaveTribe:tribe OnSuccess:^(id responseObject) {
        [SVProgressHUD showSuccessWithStatus:@"Left tribe!"];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        [joinButton setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
        joinButton.alpha = 0.0;
        
        if ([(id) delegate respondsToSelector:@selector(onLeftFromTribe)]) {
            [delegate onLeftFromTribe];
        }
        
    } onFail:^(id responseObject, NSError *error) {
    }];
}

-(void)postButtonAction:(id)sender
{
    [self openNewPost:sender];
}

#pragma mark - GTRTribeDetailMemberCell delegates
-(void)onMemberSelected:(GTRUser *)theMember
{
    __weak GTRTribeDetailViewController *weakSelf = self;

    [GTRActiveBudzHelper pushActiveBudzProfile:theMember indexPath:nil rootViewController:weakSelf onSuccess:^(id responseObject) {
        
    } onFail:^(id responseObject, NSError *error) {
        
    }];
}

#pragma mark - GTRNewPost delegates

- (void)onNewPostCreated:(GTRPost *)post
{
    [self reloadStreamPage:GTRStreamPageReloadModeRefresh];
}

#pragma mark - GTRCustomActionSheet delegates

-(void)deleteAction
{
    NSInteger rowID = ((UIControl *) self.currentSelectedPost).tag;
    __weak GTRPost *post = self.streamPostArray[rowID];
    
    [GTRPostRestService deletePostWithPostID:post.postID onSuccess:^(id response) {
        [self removePostOnIndex:rowID];
    } onFail:nil];
}

#pragma mark - Button actions

-(void)backButtonAction:(id)sender
{
    NSArray *arrayOfViewControllers = self.navigationController.viewControllers;
    if ([arrayOfViewControllers[arrayOfViewControllers.count - 2] isKindOfClass:[GTRTribeSearchViewController class]]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        GTRMyTribeViewController *mytribeViewController = arrayOfViewControllers[0];
        [mytribeViewController reload:0]; // refresh tribe
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)inviteButtonAction:(id)sender
{
    GTRInviteFriendsViewController *inviteFriendsViewController = [[GTRInviteFriendsViewController alloc] initWithPushedMode:YES tribeMode:YES];
    inviteFriendsViewController.tribe = tribe;
    [self.navigationController pushViewController:inviteFriendsViewController animated:YES];
}

#pragma mark - Join Button

-(void)joinButtonAction:(id)sender
{
    __weak GTRTribeDetailViewController *weakSelf = self;
    [UIView animateWithDuration:0.4 animations:^{
        [joinButton setTransform:CGAffineTransformMakeScale(0.0, 0.0)];
        joinButton.alpha = 0.0;
    } completion:^(BOOL finished) {
        [weakSelf joinTribe];
    }];
}

-(void)joinTribe
{
    __weak GTRTribeDetailViewController *weakSelf = self;
    [GTRTribeRestService joinTribe:tribe OnSuccess:^(id responseObject) {
        GTRTribe *updatedTribe = [MTLJSONAdapter modelOfClass:GTRTribe.class
                                           fromJSONDictionary:responseObject error:nil];
        [weakSelf setContent:updatedTribe];
    } onFail:^(id responseObject, NSError *error) {
        [UIAlertView showNoticeWithTitle:@"Oops!" message:@"Something went wrong" cancelButtonTitle:@"OK"];
    }];
}

@end
