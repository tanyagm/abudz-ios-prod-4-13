//
//  GTRFitnessPageTemplateViewController.h
//  gotribal
//
//  Created by loaner on 11/26/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRSurveyPageTemplateViewController.h"
#import "GTRSurveyQuestionData.h"

@interface GTRFitnessPageTemplateViewController : GTRSurveyPageTemplateViewController;

// full initiator
-(id)initWithTitle:(NSString *)aTitle
              data:(GTRSurveyQuestionData *)theData
        withHeader:(BOOL)theHeaderValue
  withQuestionType:(BOOL)theTypeValue
  isSurveyPageMode:(BOOL)value;

@end
