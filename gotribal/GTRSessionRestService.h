//
//  GTRSessionRestService.h
//  gotribal
//
//  Created by Le Vady on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTRSessionRestService : NSObject

+(void)login:(NSDictionary *)params onSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler;
+(void)logoutOnSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler;
+(void)saveOAuthCredential:(NSDictionary *)accessToken;

+(void)facebookLogin:(NSDictionary *)params onSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success onFail:(void (^)(id responseObject, NSError *error))errorHandler;
@end
