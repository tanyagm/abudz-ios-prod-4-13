//
//  GTRInviteFriendsViewController.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 12/19/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRViewController.h"
#import "GTRTribe.h"

@interface GTRInviteFriendsViewController : GTRTableViewController

@property (strong,nonatomic) GTRTribe *tribe; //for passing the tribe data

-(id)initWithPushedMode:(BOOL)thePushedMode tribeMode:(BOOL)theTribeMode;

@end
