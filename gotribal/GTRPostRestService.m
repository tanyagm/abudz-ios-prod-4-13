//
//  GTRPostRestService.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/3/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRPostRestService.h"
#import "GTRRestServiceErrorHandler.h"

@implementation GTRPostRestService

+(void)likePostWithPostID:(NSNumber*) postID
                onSuccess:(void (^)(id))success onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    DDLogInfo(@"Request like post with ID %@", postID);

    [manager POST:POST_VOTE_ENDPOINT(postID) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}

+(void)unlikePostWithPostID:(NSNumber*) postID
                  onSuccess:(void (^)(id))success onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    DDLogInfo(@"Request unlike post with ID %@", postID);
    
    [manager DELETE:POST_VOTE_ENDPOINT(postID) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}

+(void)createPostWithParam:(NSDictionary*) params
                 onSuccess:(void (^)(id))success onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    DDLogInfo(@"Request create post with param %@", params);
    
    [manager POST:CREATE_POST_ENDPOINT parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}

+(void)deletePostWithPostID:(NSNumber *)postID
                  onSuccess:(void (^)(id))success onFail:(void (^)(AFHTTPRequestOperation *operation, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    DDLogInfo(@"Request unlike post with ID %@", postID);
    
    [manager DELETE:DELETE_POST_ENDPOINT(postID) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];

}

+(void)createTribePost:(GTRTribe*)theTribe WithParam:(NSDictionary*) params
             onSuccess:(void (^)(id))success onFail:(void (^)(id responseObject, NSError *error))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    DDLogInfo(@"Request create tribe post with param %@", params);
    NSString *endpoint = [NSString stringWithFormat:@"%@/%@/%@",TRIBE_ENDPOINT,theTribe.tribeID,@"posts"];
    
    [manager POST:endpoint parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DDLogInfo(@"Response : %@", responseObject);
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error : %@", error);
        
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}

@end
