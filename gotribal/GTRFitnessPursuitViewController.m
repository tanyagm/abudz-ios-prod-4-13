//
//  GTRFitnessPersuitViewController.m
//  gotribal
//
//  Created by loaner on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRFitnessPursuitViewController.h"

@interface GTRFitnessPursuitViewController ()

@end

@implementation GTRFitnessPursuitViewController

- (id) initWithTitle:(NSString *)title
          withHeader:(BOOL)header
    withQuestionType:(BOOL)questionType
    isSurveyPageMode:(BOOL)value
referenceDictionary:(NSMutableDictionary *)theReferenceDictionary
{
    self = [super initWithReferenceDictionary:theReferenceDictionary];
        if(self){
        self.title = title;
        self.withHeader = header;
        self.isSurveyPageMode = value;
    }
    
    return self;
}



@end
