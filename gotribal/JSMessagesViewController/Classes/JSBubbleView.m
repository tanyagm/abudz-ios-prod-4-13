//
//  Created by Jesse Squires
//  http://www.hexedbits.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSMessagesViewController
//
//
//  The MIT License
//  Copyright (c) 2013 Jesse Squires
//  http://opensource.org/licenses/MIT
//

#import "JSBubbleView.h"

#import "JSMessageInputView.h"
#import "JSAvatarImageFactory.h"
#import "NSString+JSMessagesView.h"

static const CGFloat kMarginTop = 8.0f;
static const CGFloat kMarginBottom = 4.0f;
static const CGFloat kPaddingTop = 8.0f;
static const CGFloat kPaddingBottom = 8.0f;
static const CGFloat kBubblePaddingRight = 35.0f;
static const CGFloat kBubbleSideMargin = 52.0f;
static const CGFloat kTextLabelSideMargin = 6.0f;
static const CGFloat kMaxWidth = 236.0f;

@interface JSBubbleView()

- (void)setup;

- (CGSize)textSizeForText:(NSString *)txt;
- (CGSize)bubbleSizeForText:(NSString *)txt;

@end


@implementation JSBubbleView

#pragma mark - Setup

- (void)setup
{
    self.backgroundColor = [UIColor clearColor];
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame
                   bubbleType:(JSBubbleMessageType)bubleType
              bubbleImageView:(UIImageView *)bubbleImageView
{
    self = [super initWithFrame:frame];
    if(self) {
        [self setup];
        
        _type = bubleType;
        
        bubbleImageView.userInteractionEnabled = YES;
        [self addSubview:bubbleImageView];
        _bubbleImageView = bubbleImageView;
        
        UILabel *textLabel = [[UILabel alloc] init];
        textLabel.numberOfLines = 0;
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        textLabel.textColor = [UIColor blackColor];
        textLabel.userInteractionEnabled = YES;
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.clipsToBounds = NO;
        [self addSubview:textLabel];
        [self bringSubviewToFront:textLabel];
        _textLabel = textLabel;
        
        if([_textLabel respondsToSelector:@selector(textContainerInset)]) {
            //_textLabel.textContainerInset = UIEdgeInsetsMake(4.0f, 4.0f, 4.0f, 4.0f);
        }
        
//        NOTE: TODO: textView frame & text inset
//        --------------------
//        future implementation for textView frame
//        in layoutSubviews : "self.textView.frame = textFrame;" is not needed
//        when setting the property : "_textView.textContainerInset = UIEdgeInsetsZero;"
//        unfortunately, this API is available in iOS 7.0+
//        update after dropping support for iOS 6.0
//        --------------------
    }
    return self;
}

- (void)dealloc
{
    _bubbleImageView = nil;
    _textLabel = nil;
}

#pragma mark - Setters

- (void)setType:(JSBubbleMessageType)type
{
    _type = type;
    [self setNeedsLayout];
}

- (void)setText:(NSString *)text
{
    self.textLabel.text = text;
    [self setNeedsLayout];
}

- (void)setFont:(UIFont *)font
{
    self.textLabel.font = font;
    [self setNeedsLayout];
}

- (void)setTextColor:(UIColor *)textColor
{
    self.textLabel.textColor = textColor;
    [self setNeedsLayout];
}

#pragma mark - Getters

- (NSString *)text
{
    return self.textLabel.text;
}

- (UIFont *)font
{
    return self.textLabel.font;
}

- (UIColor *)textColor
{
    return self.textLabel.textColor;
}

- (CGRect)bubbleFrame
{
    CGSize bubbleSize = [self bubbleSizeForText:self.textLabel.text];
    
    return CGRectIntegral(CGRectMake((self.type == JSBubbleMessageTypeOutgoing ? self.frame.size.width - bubbleSize.width - kBubbleSideMargin : kBubbleSideMargin),
                      kMarginTop,
                      bubbleSize.width,
                      bubbleSize.height));
}

- (CGFloat)neededHeightForCell;
{
    CGSize bubbleSize = [self bubbleSizeForText:self.textLabel.text];
    return bubbleSize.height;
}

#pragma mark - Bubble view

- (CGSize)bubbleSizeForText:(NSString *)txt
{
	CGSize textSize = [self textSizeForText:txt];
    CGSize bubbleSize = CGSizeMake(kMaxWidth, textSize.height + kTextLabelSideMargin*2);
    
	return bubbleSize;
}

- (CGSize)textSizeForText:(NSString *)txt
{
    CGSize textSize = [txt sizeWithFont:self.textLabel.font constrainedToSize:CGSizeMake(kMaxWidth - kTextLabelSideMargin*3, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    
    return textSize;
}

#pragma mark - Layout

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.bubbleImageView.frame = [self bubbleFrame];
    
    CGFloat textXOffset = self.bubbleImageView.frame.origin.x + kTextLabelSideMargin;
    
    if(self.type == JSBubbleMessageTypeIncoming) {
        textXOffset += kTextLabelSideMargin;
    }
    
    CGRect textFrame = CGRectMake(textXOffset,
                                  self.bubbleImageView.frame.origin.y + kTextLabelSideMargin,
                                  self.bubbleImageView.frame.size.width - kTextLabelSideMargin*3,
                                  self.bubbleImageView.frame.size.height - kTextLabelSideMargin*2);
    
    self.textLabel.frame = CGRectIntegral(textFrame);
    
    self.timestampLabel.frame = CGRectIntegral(CGRectMake(self.bubbleImageView.frame.origin.x, self.bubbleImageView.frame.size.height + 10, self.bubbleImageView.bounds.size.width, 15));
    self.timestampLabel.textColor = [UIColor colorWithHue:1.000 saturation:0.040 brightness:0.380 alpha:1.000];
    self.timestampLabel.textAlignment = (self.type == JSBubbleMessageTypeOutgoing) ? NSTextAlignmentLeft : NSTextAlignmentRight;
}

@end