//
//  GTRSettingsRestService.h
//  gotribal
//
//  Created by loaner on 12/30/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTRSettingsRestService : NSObject
+(void)retrieveSettingOnSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler;
+(void)updateSetting:(NSDictionary *)params onSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler;
@end
