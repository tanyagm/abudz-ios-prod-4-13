//
//  GTRSurveyLookingForPageViewController.h
//  gotribal
//
//  Created by Ricardo Pramana Suranta on 11/26/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSurveyPageTemplateViewController.h"

@interface GTRSurveyRolePreferencePageViewController : GTRSurveyPageTemplateViewController

-(id)initWithReferenceDictionary:(NSMutableDictionary *)theReferenceDictionary;
- (int) getSelectedRole;
@end
