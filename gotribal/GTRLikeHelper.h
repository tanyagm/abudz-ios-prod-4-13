//
//  GTRLikeHelper.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/18/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTRPost.h"

@interface GTRLikeHelper : NSObject
+ (void) toggleLike:(GTRPost*) post
          onSuccess:(void (^)(GTRPost*))likeAction
             onFail:(void (^)(id, NSError *))errorHandler;
@end
