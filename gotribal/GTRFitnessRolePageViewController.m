//
//  GTRFitnessRolePageViewController.m
//  gotribal
//
//  Created by loaner on 12/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRFitnessRolePageViewController.h"

@interface GTRFitnessRolePageViewController ()

@end

@implementation GTRFitnessRolePageViewController

- (id) initWithTitle:(NSString *)title
          withHeader:(BOOL)header
    withQuestionType:(BOOL)questionType
    isSurveyPageMode:(BOOL)value
 referenceDictionary:(NSMutableDictionary *)theReferenceDictionary
{
    self = [super initWithReferenceDictionary:theReferenceDictionary];
    if(self){
        self.title = title;
        self.withHeader = header;
        self.isSurveyPageMode = value;
    }
    
    return self;
}

@end
