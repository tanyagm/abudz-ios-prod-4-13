//
//  GTRNotificationItemCell.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/16/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRNotificationItemCell.h"
#import <UIImageView+AFNetworking.h>

#define MAX_DESCRIPTION_LABEL_SIZE CGSizeMake(240, 40);

NSInteger const NOTIFICATION_CELL_HEIGHT = 60;

@interface GTRNotificationItemCell()
@property (nonatomic, strong) GTRNotificationItem *notificationItem;
@property (nonatomic, strong) UILabel *timestampLabel;
@end

@implementation GTRNotificationItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self view:self.text setOriginOrdinate:8];
        [self view:self.subtext setOriginOrdinate:25];
        [self view:self.avatarImageView setOriginOrdinate:7];
        
        _timestampLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 10, 93, 18)];
        _timestampLabel.font = TIMESTAMP_FONT;
        _timestampLabel.textAlignment = NSTextAlignmentRight;
        _timestampLabel.textColor =UIColorFromRGB(0x9A9A9A);
        _timestampLabel.text = @"3 days ago";
        
        [self addSubview:_timestampLabel];
    }
    return self;
}

#pragma mark Helper
- (void)view:(UIView*) view setOriginOrdinate:(CGFloat)ordinate
{
    CGRect frame = view.frame;
    frame.origin.y = ordinate;
    view.frame = frame;
}

#pragma mark Content
- (void) setContent:(GTRNotificationItem*) inpNotificationItem
{
    _notificationItem = inpNotificationItem;
    self.text.text = [_notificationItem.user fullName];
    [self setSubtextWithString:_notificationItem.description];
    [self setAvatarImageViewContent:_notificationItem.user.avatarURL];
    _timestampLabel.text = [_notificationItem relativeTimestamp];
}


- (void) setSubtextWithString:(NSString*)description
{
    CGRect frame = self.subtext.frame;
    frame.size = MAX_DESCRIPTION_LABEL_SIZE;
    self.subtext.frame = frame;
    self.subtext.text = description;
    [self.subtext sizeToFit];
}

-(void) setAvatarImageViewContent:(NSString*)avatarURL
{
    if (avatarURL){
        [self.avatarImageView setImageWithURL:[NSURL URLWithString:avatarURL] placeholderImage:DEFAULT_AVATAR];
    } else {
        [self.avatarImageView setImage:DEFAULT_AVATAR];
    }
}

@end
