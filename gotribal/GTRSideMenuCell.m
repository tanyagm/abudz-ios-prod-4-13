//
//  GTRSideMenuCell.m
//  gotribal
//
//  Created by Hisma Mulya S on 11/27/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSideMenuCell.h"
#import "GTRSideMenuHelper.h"


@implementation GTRSideMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = GTRSIDE_MENU_COLOR;
        
        // avatar
        self.avatarImageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 5, 30, 30)];
        self.avatarImageView.backgroundColor = GTRSIDE_MENU_COLOR;
        [self.contentView addSubview:self.avatarImageView];
        
        // text label
        self.text = [[UILabel alloc]initWithFrame:CGRectMake(65, 4, 120,30)];
        self.text.backgroundColor = GTRSIDE_MENU_COLOR;
        self.text.font = CELL_FONT;
        self.text.textColor = [UIColor whiteColor];
        self.text.textAlignment = NSTextAlignmentLeft;
        self.text.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:self.text];
        
        // right image
        CGFloat rightViewOrigin = 5 + self.avatarImageView.frame.size.width + 20 + self.text.frame.size.width + 10;
        self.rightImageView = [[UIImageView alloc]initWithFrame:CGRectMake(rightViewOrigin, 10, 40, 21)];
        self.rightImageView.backgroundColor = GTRSIDE_MENU_COLOR;
        [self.contentView addSubview:self.rightImageView];
        
        self.lineImageView = [[UIImageView alloc]init];
        self.lineImageView.image = [UIImage imageNamed:@"line-menu"];
        self.lineImageView.backgroundColor = GTRSIDE_MENU_COLOR;
        self.lineImageView.frame = CGRectMake(LEFT_PADDING, self.contentView.frame.size.height - 1, 220, 1);
        [self.contentView addSubview:self.lineImageView];
        
        UILabel *numberLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,0, self.rightImageView.frame.size.width, self.rightImageView.frame.size.height)];
        numberLabel.tag = NOTIFICATION_LABEL_TAG;
        numberLabel.backgroundColor = [UIColor clearColor];
        numberLabel.contentMode = UIViewContentModeCenter;
        numberLabel.textAlignment = NSTextAlignmentCenter;
        numberLabel.font = CELL_FONT;
        numberLabel.textColor = [UIColor whiteColor];
        [self.rightImageView addSubview:numberLabel];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
