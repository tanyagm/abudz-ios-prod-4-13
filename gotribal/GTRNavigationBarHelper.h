//
//  GTRNavigationBarHelper.h
//  gotribal
//
//  Created by Hisma Mulya S on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    GTRSpacerTypePlainText,
    GTRSpacerTypeButton,
    GTRSpacerTypePlaintextPushediOS6
} GTRSpacerType;

@interface GTRNavigationBarHelper : NSObject

+(void)setLeftSideBarButtonItem:(UINavigationItem*)navigationItem
                     withTarget:(id)target
                       selector:(SEL)selector;

+(void)setBackButtonItem:(UINavigationItem*)navigationItem
              withTarget:(id)target
                selector:(SEL)selector;

+(UIBarButtonItem*)createBarButtonItemWithTarget:(id)target
                                        selector:(SEL)selector
                                      imageNamed:(NSString *)imageName;

+(UIBarButtonItem*)createBarButtonItemWithTarget:(id)target
                                        selector:(SEL)selector
                                     buttonTitle:(NSString*)buttonTitle;

+(void)setRightBarButtonItem:(UINavigationItem*)navigationItem
            withTarget:(id)target
              selector:(SEL)selector
            imageNamed:(NSString *)imageName
              animated:(BOOL)animated;

+(void)setRightBarButtonItem:(UINavigationItem*)navigationItem
                  withButton:(UIBarButtonItem*)button
                    animated:(BOOL)animated
              withSpacerType:(GTRSpacerType)spacerType;

+(void)setRightBarButtonItems:(UINavigationItem*)navigationItem
                    withItems:(NSArray*)items
                     animated:(BOOL)animated;

+(void)setLeftBarButtonItems:(UINavigationItem*)navigationItem
                   withItems:(NSArray*)items
                    animated:(BOOL)animated;

+(UIBarButtonItem*)setRightBarButtonItemWithText:(NSString*)buttonTitle
                              onNavigationItem:(UINavigationItem*)navigationItem
                                    withTarget:(id)target
                                      selector:(SEL)selector;

+(UIBarButtonItem*)setLeftBarButtonItemWithText:(NSString*)buttonTitle
                                     onNavigationItem:(UINavigationItem*)navigationItem
                                           withTarget:(id)target
                                             selector:(SEL)selector;

+(UIBarButtonItem*)setLeftBarButtonItemInPushedViewControllerWithText:(NSString*)buttonTitle
                                                     onNavigationItem:(UINavigationItem*)navigationItem
                                                           withTarget:(id)target
                                                             selector:(SEL)selector;

@end
