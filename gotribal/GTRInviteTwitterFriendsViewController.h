//
// Created by Muhammad Taufik on 12/19/13.
// Copyright (c) 2013 gotribal. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GTRTribe;

@interface GTRInviteTwitterFriendsViewController : GTRTableViewController

- (id)initWithTribe:(GTRTribe *)tribe;
@end