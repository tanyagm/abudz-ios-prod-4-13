//
//  GTRBirthdayTextField.m
//  gotribal
//
//  Created by Muhammad Taufik on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRBirthdayTextField.h"

@implementation GTRBirthdayTextField
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(cut:) ||
        action == @selector(copy:) ||
        action == @selector(paste:) ||
        action == @selector(select:) ||
        action == @selector(selectAll:)) return NO;
    return [super canPerformAction:action withSender:sender];
}
@end
