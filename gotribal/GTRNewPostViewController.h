//
//  GTRNewPostViewController.h
//  gotribal
//
//  Created by Sambya Aryasa on 12/4/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTRPost.h"
#import "GTRImagePickerHelper.h"
#import "GTRTribe.h"

@protocol GTRNewPostViewControllerDelegate
-(void)onNewPostCreated:(id)post;
@end

@interface GTRNewPostViewController : GTRViewController
@property (weak) id <GTRNewPostViewControllerDelegate> delegate;
@property (nonatomic,strong) UIImage* postImage;

-(void)initAttributes;
-(void)setAppearance;
-(void)initNavBarButtons;
-(void)initNewPostView;
-(void)cancelButtonAction:(id)sender;
-(void)enableDoneButtonItem:(BOOL)buttonEnabled;
-(void)showPostImage:(UIImage*)image;
-(void)hidePostImage;


@property (nonatomic, strong) GTRUser *currentUser;
@property (nonatomic, strong) UITextView *postMessage;
@property (nonatomic, strong) UITextView *postMessagePlaceholder;
@property (nonatomic, strong) UIBarButtonItem *doneButton;
@property (nonatomic, strong) UIImagePickerController *imagePickerController;
@property (nonatomic, strong) UIButton *postImageView;
@property (nonatomic, strong) UIButton *removePostImageButton;
@property (strong, nonatomic) GTRImagePickerHelper *imagePickerHelper;
@property (strong, nonatomic) UIImageView *avatar;
@property BOOL isOnTribe;
@property (strong, nonatomic) GTRTribe *tribe;

@end
