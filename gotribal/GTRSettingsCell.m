//
//  GTRSettingsCell.m
//  gotribal
//
//  Created by Hisma Mulya S on 11/29/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRSettingsCell.h"

@interface UIView (OPCloning)

- (id) clone;

@end

@implementation UIView (OPCloning)

- (id) clone {
    NSData *archivedViewData = [NSKeyedArchiver archivedDataWithRootObject: self];
    id clone = [NSKeyedUnarchiver unarchiveObjectWithData:archivedViewData];
    return clone;
}

@end

NSInteger const LEFT_PADDING = 15;
NSInteger const TOP_ROW_SEPARATOR = 9090;

@implementation GTRSettingsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIColor *whiteColor = [UIColor whiteColor];
        self.backgroundColor = whiteColor;
        self.text = [[UILabel alloc]initWithFrame:CGRectMake(LEFT_PADDING, 10, 170,30)];
        self.text.backgroundColor = whiteColor;
        self.text.font = CELL_FONT;
        self.text.textColor = CELL_TEXT_FONT_COLOR;
        self.text.textAlignment = NSTextAlignmentLeft;
        self.text.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:self.text];
        
        self.rightView = [[UIView alloc]initWithFrame:CGRectZero];
        self.rightView.backgroundColor = whiteColor;
        [self.contentView addSubview:self.rightView];
        
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setRightViewWithText:(NSString *)text
{
    
    UILabel *rightText = [[UILabel alloc]init];
    rightText.frame = CGRectMake(0,0,290, self.frame.size.height);
    rightText.text = text;
    rightText.textAlignment = NSTextAlignmentRight;
    rightText.backgroundColor = [UIColor clearColor];
    rightText.font = CELL_FONT;
    rightText.textColor = CELL_TEXT_FONT_COLOR;
    [self.contentView addSubview:rightText];
    
}

@end
