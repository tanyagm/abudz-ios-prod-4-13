//
//  GTRActivityStreamRestService.m
//  gotribal
//
//  Created by Sambya Aryasa on 12/2/13.
//  Copyright (c) 2013 gotribal. All rights reserved.
//

#import "GTRActivityStreamRestService.h"
#import "GTRPaginationHelper.h"
#import "GTRRestServiceErrorHandler.h"

@implementation GTRActivityStreamRestService

+(void)retrieveActivityStreamWithMaxID:(NSNumber*) maxID
                    onSuccess:(void (^)(id))success onFail:(void (^)(id, NSError *))errorHandler
{
    AFHTTPRequestOperationManager *manager = [AFManagerHelper authenticatedManager];
    NSString *endpoint = [GTRPaginationHelper paginate:GET_ACTIVITY_STREAM_ENDPOINT withLimit: [NSNumber numberWithInt:MAX_PER_PAGE] andMaxID:maxID];
    
    [manager GET:endpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [GTRRestServiceErrorHandler handleConnectionRelatedError:error response:operation.responseObject requestOperation:operation];
        if (errorHandler) errorHandler(operation, error);
    }];
}

@end
